public class LC_AsignacionGuardiasController {
  /***
  * Obtiene las guardias existentes
  *
  * @param desarrolloComercialId el id desarrollo comercial
  *
  * @return lista de guardias
  */
  @AuraEnabled
  public static List<Guardia__c> obtenerGuardiasExistentes(Id desarrolloComercialId){
    List<Guardia__c> guardias = [
    SELECT Id, Nombre_Asesor__c, Nombre_Asesor__r.Name, Fecha_Guardia__c, Roll__c, Contador_temp__c
    FROM Guardia__c
    WHERE Desarrollo_Comercial__c = :desarrolloComercialId AND Fecha_Guardia__c = THIS_WEEK
    order by Fecha_Guardia__c,Roll__c
    ];
    System.debug('guardias org =>' + guardias);
    for (Guardia__c guardia:guardias){
      System.debug(guardia.Fecha_Guardia__c);
      System.debug(guardia.Nombre_Asesor__c);
      Integer listLeads= [
      SELECT count()
      FROM Lead
      WHERE OwnerId = :guardia.Nombre_Asesor__c
      AND Dia_asignacion__c = :guardia.Fecha_Guardia__c
      ];
      System.debug(listLeads);
      guardia.Contador_temp__c = listLeads;
    }
    System.debug('guardias =>' + guardias);
    return guardias;
  }
  /***
  * Borra una sola guardia, elegida por el usuario
  *
  * @param guardiaId el id de la guardia a borrar
  *
  * @return boolean de exito
  */
  @AuraEnabled
  public static boolean eliminarGuardia(Id guardiaId){
    List<Guardia__c> guardias = [
    SELECT Id
    FROM Guardia__c
    WHERE Id = :guardiaId
    LIMIT 1
    ];
    if(!guardias.isEmpty()){
      Database.DeleteResult result = Database.delete(guardias.get(0));
      System.debug('Se borró exitosamente =>' + result.success);
      return result.success;
      }else{
        return false;
      }
    }
  /***
  * Borrar todas las guardias
  *
  * @param desarrolloComercialId el id desarrollo comercial
  *
  */
  @AuraEnabled
  public static void borrarTodasGuardias(Id desarrolloComercialId){
    List<Guardia__c> guardias = [
    SELECT Id, Nombre_Asesor__c, Nombre_Asesor__r.Name
    FROM Guardia__c
    WHERE Desarrollo_Comercial__c = :desarrolloComercialId
    ];
    List<Database.DeleteResult> results = Database.delete(guardias);
  }
  /**
  * Agregar el usuario elegido a la guardia
  *
  * @param idUsuario id del usuario
  * @param desarrolloComercialId id del desarrollo comercial
  * @param fechaaplicar date seleccionado
  *
  * @return objeto de la guardia
  */
  @AuraEnabled
  public static Guardia__c agregarUsuarioAGuardia(Id idUsuario, Id desarrolloComercialId, String fechaaplicar, String rollid){
    System.debug('idUsuario: '+idUsuario);
    System.debug('desarrolloComercialId: '+desarrolloComercialId);
    System.debug('fechaaplicar: '+fechaaplicar);
    System.debug('rollid: '+rollid);
    String[] fechaarray = fechaaplicar.split('-');
    Integer year = integer.valueof(fechaarray.get(0));
    Integer month = integer.valueof(fechaarray.get(1));
    Integer day = integer.valueof(fechaarray.get(2));
    Decimal strToDec = decimal.valueOf(rollid);
    Date myDate = date.newinstance(year, month, day);
    Integer guardias = [
    SELECT Count()
    FROM Guardia__c
    WHERE Nombre_Asesor__c = :idUsuario
    AND Fecha_Guardia__c =:myDate
    AND Desarrollo_Comercial__c =: desarrolloComercialId
    ];
    System.debug('guardias: '+guardias);
    if(guardias == 0){
      Date fechaGuardia = Date.today();
      System.debug(fechaGuardia);
      Guardia__c nuevaGuardia = new Guardia__c();
      nuevaGuardia.Desarrollo_Comercial__c = desarrolloComercialId;
      nuevaGuardia.Nombre_Asesor__c = idUsuario;
      nuevaGuardia.Fecha_Guardia__c = myDate;
      nuevaGuardia.Roll__c = strToDec;
      System.debug(nuevaGuardia);
      try{
        insert nuevaGuardia;
        System.debug('Id gen: '+nuevaGuardia.Id);
      }
      catch(Exception ex){
        System.debug(ex.getMessage());
      }
      return nuevaGuardia;
      }else{
        System.debug('Se cuenta con guardia para el dia: ');
        return null;
      }
    }
  /***
  * Buscar usuarios pertenecientes al mismo grupo del gerente del desarrollo comercial
  *
  * @param String termino de busqueda
  * @param desarrolloComercialId el id desarrollo comercial
  *
  * @return lista de usuarios
  */
  @AuraEnabled
  public static List<User> busquedaUsuarios(String termino, Id desarrolloComercialId){
    System.debug('desarrollo comercial id =>' + desarrolloComercialId);
    System.debug('termino =>' + termino);
    List<Desarrollo_Comercial__c> comerciales = [
    SELECT Id, Gerente_Ventas__c, Name, Gerente_Ventas__r.UserRoleId
    FROM Desarrollo_Comercial__c
    WHERE Id = :desarrolloComercialId
    LIMIT 1
    ];
    System.debug('desarrollo comerciales =>' + comerciales);
    String userType = Schema.SObjectType.User.getKeyPrefix();
    Set<Id> userIdsInGroup = new Set<Id>();
    List<User> usersInGroup = new List<User>();
    termino = '%' + termino + '%';
    System.debug('termino =>' + termino);
    if(!comerciales.isEmpty()){
      Desarrollo_Comercial__c desarrolloComercial = comerciales.get(0);
      if(desarrolloComercial.Gerente_Ventas__c != null){
        System.debug('rol del gerente =>' + desarrolloComercial.Gerente_Ventas__r.UserRoleId);
        List<Group> groupDetails = [
        SELECT g.Type, g.Name, g.Id, g.Email, RelatedId, (SELECT Id, UserOrGroupId FROM GroupMembers)
        FROM Group g
        WHERE (g.Name = :desarrolloComercial.Name OR RelatedId = :desarrolloComercial.Gerente_Ventas__r.UserRoleId)
        AND (g.Type = 'Regular' OR g.Type='Role' OR g.Type='RoleAndSubordinates')
        ];
        System.debug('group details =>' + groupDetails);
        Set<String> groupIds = new Set<String>();
        for(Group gr : groupDetails){
          groupIds.add(gr.Id);
        }
        if(groupDetails != null && !groupDetails.isEmpty()){
          List<GroupMember> records = [
          SELECT Id, GroupId, UserOrGroupId
          FROM GroupMember
          WHERE GroupId IN :groupIds
          AND (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')
          ];
          System.debug('records =>' + records);
          System.debug('user type =>' + userType);
          List<String> roleIds = new List<String>();
          Set<Id> roleAndSubordinateIds = new Set<Id>();
          for (GroupMember record : records) {
            if ((record.UserOrGroupId + '').startsWith(userType)) {
              userIdsInGroup.add(record.UserOrGroupId);
              } else if((record.UserOrGroupId + '').startsWith(Schema.SObjectType.Group.getKeyPrefix())) {
                Group theGroup = [SELECT Id, Name, Type, RelatedId FROM Group WHERE Id = :record.UserOrGroupId];
                System.debug(theGroup);
                if(theGroup != null){
                  roleIds.add(theGroup.relatedId);
                }
              }
            }
            System.debug('user ids in group =>' + userIdsInGroup);
            System.debug('role ids in group =>' + roleIds);
            for(String r : roleIds){
              roleAndSubordinateIds.addAll(getSubordinateRoles(r));
            }
            usersInGroup = [
            SELECT Id, Name, Estatus__c
            FROM User
            WHERE (Id IN :userIdsInGroup
            OR UserRoleId IN :roleAndSubordinateIds)
            AND Name LIKE :termino
            ];
            System.debug('users in group =>' + usersInGroup);
          }
        }
      }
      return usersInGroup;
    }
  /**
  * Obtener grupos de usuario
  *
  * @param userId
  *
  * @return
  */
  public static List<Group> obtenerGruposParaUsuario(Id userId){
    System.debug('obteniendo grupos para  =>' + userId);
    Set<Id> groupIds = obtenerGruposPorIds(new Set<Id>{userId});
    return [
    SELECT Id, Name
    FROM Group
    WHERE Id IN: groupIds
    ];
  }
  public static Set<Id> obtenerGruposPorIds(Set<Id> userOrGroupIds){
    String userType = Schema.SObjectType.User.getKeyPrefix();
    Set<Id> output = new Set<Id>();
    Set<Id> nestedGroupIds = new Set<Id>();
    List<GroupMember> records = [
    SELECT Id, GroupId, UserOrGroupId
    FROM GroupMember
    WHERE UserOrGroupId IN :userOrGroupIds
    AND UserOrGroupId != NULL
    AND (Group.Type = 'Regular' OR Group.Type='Role' OR Group.Type='RoleAndSubordinates')
    ];
    System.debug('records =>' + records);
    for (GroupMember record : records) {
      if (!(record.UserOrGroupId + '').startsWith(userType)) {
        nestedGroupIds.add(record.UserOrGroupId);
      }
      else {
        output.add(record.GroupId);
      }
    }
    if (nestedGroupIds.size() > 0) {
      output.addAll(obtenerGruposPorIds(nestedGroupIds));
    }
    System.debug('todos los grupos =>' + output);
    return output;
  }
  public static Set<Id> getSubordinateRoles(Id roleId) {
    Map<Id, Set<Id>> parentAndChildren = new Map<Id, Set<Id>>();
    set<Id> children;
    for(UserRole ur : [select Id, ParentRoleId from UserRole]) {
      children = parentAndChildren.containsKey(ur.ParentRoleId) ? parentAndChildren.get(ur.ParentRoleId) : new set<Id>();
      children.add(ur.Id);
      parentAndChildren.put(ur.ParentRoleId, children);
    }
    return getSubordinateRoles(roleId, parentAndChildren);
  }
  public static Set<Id> getSubordinateRoles(Id roleId, Map<Id, Set<Id>> parentAndChildren) {
    Set<Id> subordinateRoles = new Set<Id>();
    Set<Id> remainingSubordinateRoles = new Set<Id>();
    if(parentAndChildren.containsKey(roleId)) {
      subordinateRoles.addAll(parentAndChildren.get(roleId));
      for(Id subRoleId : subordinateRoles) {
        remainingSubordinateRoles.addAll(getSubordinateRoles(subRoleId, parentAndChildren));
      }
    }
    subordinateRoles.addAll(remainingSubordinateRoles);
    return subordinateRoles;
  }
}