global with sharing class CONTROL_CrearBitacora {

	public Bitacora__c bitacora {get; set;}
	public Account cliente{get;set;}
	public Lead prospecto {get; set;}
	public Boolean bitacoraTieneProspecto {get; set;}
	public String crearCita {get; set;}
	public Set<Id> idsAsesores;
	public Map<String,Task> mapaAsesorHorasTareas;
	public Map<String,Event> mapaAsesorHorasEventos;
	public List<SelectOption> horariosDisponibles {get; set;}
	public String horarioSeleccionado {get; set;}	
	public Integer numeroIntentosGeneral {get; set;}
	public Id desarrolloComercialOriginal;
	public Integer cantidadNoShows {get; set;}
	
	public CONTROL_CrearBitacora() {
		
		bitacora = new Bitacora__c();
		
		crearCita = '';
		idsAsesores = new Set<Id>();
		mapaAsesorHorasTareas = new Map<String,Task>();
		mapaAsesorHorasEventos = new Map<String,Event>();
		horariosDisponibles = new List<SelectOption>();
		horariosDisponibles.add(new SelectOption('','--Ninguna--'));
		
		horarioSeleccionado = null;
		
		obtenerDatosIniciales();
	}
	
	//Metodo para inicializar las variables necesarias
	public void obtenerDatosIniciales(){

		prospecto = [SELECT Id, Name, F_CM__c, Desarrollo_Comercial__c, Desarrollo_Comercial__r.Hora_Inicio__c, Desarrollo_Comercial__r.Hora_Fin__c, OwnerId, Agente__c, Grado_de_interes__c, Cantidad_No_Shows__c, Status, Asesor_Primario__c, Estatus_Cita__c, Desarrollo_Web__c, Desarrollo__c, Estatus_Prospecto_BDC__c FROM Lead WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
		prospecto.F_CM__c = null;
		desarrolloComercialOriginal = prospecto.Desarrollo_Comercial__c;
		cantidadNoShows = (String.isBlank(String.valueOf(prospecto.Cantidad_No_Shows__c)) == true ? 0 : Integer.valueOf(prospecto.Cantidad_No_Shows__c));
		numeroIntentosGeneral = [SELECT COUNT() FROM Bitacora__c WHERE Lead__c =: prospecto.Id];
	}
	
	//Metodo para obtener las formas de contacto de un prospecto al crear una bitacora
	public List<SelectOption> getFormasContacto(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Forma_de_Contacto__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para obtener las contestaciones del contacto al crear una bitacora
	public List<SelectOption> getContestaciones(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Contesta__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para obtener los resultados del contacto al crear una bitacora en base a la forma de contacto elegida cuando no contesto
	public List<SelectOption> getResultados(){
		
		System.debug('*****bitacora.Forma_de_Contacto__c: ' + bitacora.Forma_de_Contacto__c);
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Resultado__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
        String[] arregloFormaContacto;
        String formaContacto = '';
		for(Schema.PicklistEntry f : ple){
			
			System.debug('*****f.getValue(): ' + f.getValue());
			System.debug('*****f.getValue().contains(-): ' + f.getValue().contains('-'));
			if(f.getValue().contains('-')){				
				
				arregloFormaContacto = f.getValue().split('-');
				formaContacto = arregloFormaContacto.get(0);
				System.debug('*****formaContacto: ' + formaContacto);
				if(formaContacto == bitacora.Forma_de_Contacto__c){
					System.debug('*****agregado: ' + f.getValue());
					optionList.add(new SelectOption(f.getValue(), f.getLabel()));
				}
			}
		}
		
		System.debug('*****optionList: ' + optionList);
		return optionList;
	}
	
	//Metodo para obtener las opciones para crear o no una cita cuando un prospecto si a contestado
	public List<SelectOption> getOpcionesCrearCita(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		optionList.add(new SelectOption('Si','Si'));
		optionList.add(new SelectOption('No','No'));
		return optionList;
	}
	
	//Metodo para obtener los estatus cuando un prospecto ha contestado pero no desea agendar cita
	public List<SelectOption> getEstatusNoAgendaCita(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Estatus_No_Agenda_Cita__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para obtener los estatus cuando un prospecto ha contestado pero no desea agendar cita
	public List<SelectOption> getGradosInteres(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Lead.Grado_de_interes__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para buscar los horarios disponibles de los asesores en base a la fecha seleccionada y las guardias existentes
	public void buscarHorariosDisponibles(){
		
		idsAsesores = new Set<Id>();
		mapaAsesorHorasTareas = new Map<String,Task>();
		mapaAsesorHorasEventos = new Map<String,Event>();		
		horariosDisponibles = new List<SelectOption>();
		horariosDisponibles.add(new SelectOption('','--Ninguna--'));
		horarioSeleccionado = null;
		
		System.debug('*****prospecto.F_CM__c: ' + prospecto.F_CM__c);
		System.debug('*****prospecto.Desarrollo_Comercial__c: ' + prospecto.Desarrollo_Comercial__c);
		
		//Se obtienen las guardias disponibles para el dia seleccionado
		List<Guardia__c> guardias = new List<Guardia__c>();
		
		//Si el estatus de prospecto BDC es rescate se toman en cuenta solo las guardias del asesor primario (agente de ventas al que se le habia 
		//asignado anteriormente una cita)
		System.debug('*****prospecto.Status: ' + prospecto.Status);
		System.debug('*****prospecto.Estatus_Cita__c: ' + prospecto.Estatus_Cita__c);
		if(prospecto.Estatus_Prospecto_BDC__c == 'Rescate'){
			System.debug('*****guardias 1');
			guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c AND Fecha_Guardia__c =: prospecto.F_CM__c AND Nombre_Asesor__c =: prospecto.Asesor_Primario__c];
		}
		//Si el estatus de cita del prospecto es cancelada
		else if(prospecto.Estatus_Cita__c == 'Cancelada'){
			
			System.debug('*****guardias 2');
			//Si se cambio el desarrollo comercial se toman en cuenta las guardias existentes sin la restriccion de asesor primario
			if((desarrolloComercialOriginal != prospecto.Desarrollo_Comercial__c)){
				System.debug('*****guardias 3');
				guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c AND Fecha_Guardia__c =: prospecto.F_CM__c];
			}
			//Si no se cambio el desarrollo comercial se toman en cuenta solo las guardias del asesor primario (agente de ventas al que se le habia 
			//asignado anteriormente una cita)
			else{
				System.debug('*****guardias 4');
				guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c AND Fecha_Guardia__c =: prospecto.F_CM__c AND Nombre_Asesor__c =: prospecto.Asesor_Primario__c];
			}			
		}
		//Si el estatus de prospecto BDC no es rescate se toman en cuenta las guardias existentes sin la restriccion de asesor primario
		else{
			System.debug('*****guardias 5');
			guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c AND Fecha_Guardia__c =: prospecto.F_CM__c];
		}
		System.debug('*****guardias: ' + guardias);
		
		//Se obtienen los asesores asignados en las guardias disponibles para el dia seleccionado
		for(Guardia__c g : guardias){
			idsAsesores.add(g.Nombre_Asesor__c);
		}
		System.debug('*****idsAsesores: ' + idsAsesores);
		
		//Se obtienen todas las tareas (citas) de los asesores obtenidos previamente
		List<Task> tareasAsesores = [SELECT Id, OwnerId, Fecha_Cita__c, ActivityDate FROM Task WHERE OwnerId IN : idsAsesores AND DAY_ONLY(Fecha_Cita__c) =: prospecto.F_CM__c];
		System.debug('*****tareasAsesores: ' + tareasAsesores);
		
		//En base a las tareas obtenidas se crea un mapa con el asesor y la hora de la tarea
		for(Task ta : tareasAsesores){
			mapaAsesorHorasTareas.put(ta.OwnerId + '-' + ta.Fecha_Cita__c.hour(),ta);
		}
		System.debug('*****mapaAsesorHorasTareas: ' + mapaAsesorHorasTareas);		
		
		//Se obtienen todos los eventos de los asesores obtenidos previamente
		List<Event> eventosAsesores = [SELECT Id, OwnerId, StartDateTime, EndDateTime, ActivityDate, DurationInMinutes FROM Event WHERE OwnerId IN : idsAsesores AND ActivityDate =: prospecto.F_CM__c];
		System.debug('*****eventosAsesores: ' + eventosAsesores);
		
		//En base a los eventos obtenidos se crea un mapa con el asesor y la hora del evento
		Integer horasDuracionEvento;
		Integer j;
		Integer sumaHora;
		for(Event ea : eventosAsesores){
			
			//Se obtienen cuantas horas dura el evento
			horasDuracionEvento = Integer.valueOf(Math.ceil(Decimal.valueOf(ea.DurationInMinutes) / 60.0));
			
			//En base al numero de horas que dura el evento se crean elementos en el mapa comenzando con la hora de inicio del evento
			j = 1;
			sumaHora = ea.StartDateTime.hour();
			while(horasDuracionEvento >= j){
				mapaAsesorHorasEventos.put(ea.OwnerId + '-' + sumaHora,ea);
				j++;
				sumaHora++;
			}
		}
		System.debug('*****mapaAsesorHorasEventos: ' + mapaAsesorHorasEventos);
		
		System.debug('*****prospecto.Desarrollo_Comercial__r.Hora_Inicio__c: ' + prospecto.Desarrollo_Comercial__r.Hora_Inicio__c);
		System.debug('*****prospecto.Desarrollo_Comercial__r.Hora_Inicio__c: ' + prospecto.Desarrollo_Comercial__r.Hora_Inicio__c.hour());
		System.debug('*****prospecto.Desarrollo_Comercial__r.Hora_Fin__c: ' + prospecto.Desarrollo_Comercial__r.Hora_Fin__c.hour());
		Integer horaInicio = prospecto.Desarrollo_Comercial__r.Hora_Inicio__c.hour();
		Integer horaFin = prospecto.Desarrollo_Comercial__r.Hora_Fin__c.hour() - 1;
		Boolean horarioDisponible = false;
		String llaveAsesorHora = '';
		/*List<Integer> horariosDisponiblesInt = new List<Integer>();*/
		//Se hace un ciclo desde la hora de inicio hasta la hora de fin de actividades del desarrollo comercial
		for(Integer i = horaInicio; i <= horaFin; i++){
			
			System.debug('*****i:' + i);
			//Se hace un ciclo por cada uno de los asesores
			horarioDisponible = false;
			for(Id ida : idsAsesores){
				
				//Si ni el mapa de tareas ni el mapa de eventos contienen el asesor y la hora es una hora disponible a elegir 
				llaveAsesorHora = ida + '-' + i;
				System.debug('*****llaveAsesorHora: ' + llaveAsesorHora);
				if(mapaAsesorHorasTareas.containsKey(llaveAsesorHora) == false && mapaAsesorHorasEventos.containsKey(llaveAsesorHora) == false){
					horarioDisponible = true;
				}
				
				System.debug('*****horarioDisponible: ' + horarioDisponible);
			}
			
			if(horarioDisponible == true){
				/*horariosDisponiblesInt.add(i);*/				
				horariosDisponibles.add(new SelectOption(String.valueOf(i),(String.valueOf(i).length() == 1 ? '0'+i+':00' : i+':00')));
			}
		}		
		/*System.debug('*****horariosDisponiblesInt:' + horariosDisponiblesInt);*/
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la forma de contacto seleccionada
	public void limpiaVariablesFormaContacto(){
		
		bitacora.Contesta__c = null;
		bitacora.Resultado__c = null;
		crearCita = null;
		bitacora.Estatus_No_Agenda_Cita__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		//prospecto.F_CM__c = null;
		horarioSeleccionado = null;
		//prospecto.Grado_de_interes__c = null;
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la contestacion seleccionada
	public void limpiaVariablesContesta(){
		
		bitacora.Resultado__c = null;
		crearCita = null;
		bitacora.Estatus_No_Agenda_Cita__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		//prospecto.F_CM__c = null;
		horarioSeleccionado = null;
		//prospecto.Grado_de_interes__c = null;
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la opcion de crear cita seleccionada
	public void limpiaVariablesCrearCita(){
		
		bitacora.Resultado__c = null;
		bitacora.Estatus_No_Agenda_Cita__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		//prospecto.F_CM__c = null;
		horarioSeleccionado = null;
		//prospecto.Grado_de_interes__c = null;
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la opcion de crear cita seleccionada
	public void limpiaVariablesEstatusNoAgendaCita(){
		
		bitacora.Resultado__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		prospecto.F_CM__c = null;
		horarioSeleccionado = null;
		prospecto.Grado_de_interes__c = null;
	}
	
	//Metodo para guardar una bitacora y en su caso una cita
	public PageReference guardar(){
		
		//Validaciones
		Boolean errorFormaContacto = false;
		Boolean errorContesta = false;
		Boolean errorResultado = false;
		Boolean errorCrearCita = false;
		Boolean errorEstatusNoCrearCita = false;
		Boolean errorFechaCita = false;
		Boolean errorHorarioCita = false;
		Boolean errorGradoInteres = false;
		Boolean existeAlgunError = false;
		
		if(String.isBlank(bitacora.Forma_de_Contacto__c) == true){
			errorFormaContacto = true;
		}
		if(String.isBlank(bitacora.Contesta__c) == true){
			errorContesta = true;
		}		
		if(String.isBlank(bitacora.Resultado__c) == true){
			errorResultado = true;
		}
		if(String.isBlank(crearCita) == true){
			errorCrearCita = true;
		}
		if(String.isBlank(bitacora.Estatus_No_Agenda_Cita__c) == true){
			errorEstatusNoCrearCita = true;
		}
		if(String.isBlank(String.valueOf(prospecto.F_CM__c)) == true){
			errorFechaCita = true;
		}
		if(String.isBlank(horarioSeleccionado) == true){
			errorHorarioCita = true;
		}
		if(String.isBlank(prospecto.Grado_de_interes__c) == true){
			errorGradoInteres = true;
		}		
		
		//Si no se selecciono forma de contacto y una contestacion se manda error
		if(errorFormaContacto == true || errorContesta == true){
			
			if(errorFormaContacto == true){
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar una forma de contacto.');
	    		ApexPages.addMessage(mensaje);
			}
			if(errorContesta == true){
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar si ha habido contestación o no por parte del prospecto.');
	    		ApexPages.addMessage(mensaje);
			}
			
			existeAlgunError = true;
		}
		//Si si se selecciono una forma de contacto y una contestacion
		else{
			
			//Si la contestacion fue No
			if(bitacora.Contesta__c == 'No'){
				
				//Si no se selecciono un resultado se manda error
				if(errorResultado == true){
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un resultado.');
	    			ApexPages.addMessage(mensaje);	    			
	    			existeAlgunError = true;
				}
			}
			//Si la contestacion fue Si
			else if(bitacora.Contesta__c == 'Si'){
				
				//Si no se selecciono una opcion de crear una cita se manda error
				if(errorCrearCita == true){
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar si se creará o no una cita.');
	    			ApexPages.addMessage(mensaje);	    			
	    			existeAlgunError = true;
				}
				//Si si se selecciono una opcion de crear una cita
				else{
					
					//Si no se desea crear una cita
					if(crearCita == 'No'){
						
						//Si no se selecciono un estatus se manda error
						if(errorEstatusNoCrearCita == true){
							ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar el estatus de la no creación de la cita.');
		    				ApexPages.addMessage(mensaje);		    			
		    				existeAlgunError = true;
						}
					}
					//Si si se desea crear una cita
					else if(crearCita == 'Si'){
						
						//Si no se selecciono una fecha y un horario para la cita se manda error
						if(errorFechaCita == true || errorHorarioCita == true || errorGradoInteres == true){
							
							if(errorFechaCita == true){
								ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar una fecha para la cita.');
					    		ApexPages.addMessage(mensaje);					    		
					    		existeAlgunError = true;
							}
							if(errorHorarioCita == true){
								ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un horario para la cita.');
					    		ApexPages.addMessage(mensaje);					    		
					    		existeAlgunError = true;
							}
							if(errorGradoInteres == true){
								ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un grado de interés del prospecto.');
					    		ApexPages.addMessage(mensaje);					    		
					    		existeAlgunError = true;
							}
						}
					}
				}
			}
		}
		
		//Si hubo error(es) en las validaciones se muestra mensaje de error y se regresa a la pagina
		if(existeAlgunError == true){
			return null;
		}
		//Si no hubo error(es) en las validaciones se procede con el guardado de la bitacora y en su caso con el guardado de la cita
		else{
			
			if(desarrolloComercialOriginal != prospecto.Desarrollo_Comercial__c){
				//asignarAgenteBDC();
				
				System.debug('*****prospecto.Desarrollo_Comercial__c:' + prospecto.Desarrollo_Comercial__c);
				Desarrollo_Comercial__c desarrolloComercialAux = [SELECT Id, Name FROM Desarrollo_Comercial__c WHERE Id =: prospecto.Desarrollo_Comercial__c];
				Desarrollo__c desarrolloAux = [SELECT Id, Name FROM Desarrollo__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c LIMIT 1];
				prospecto.Desarrollo_Web__c = desarrolloComercialAux.Name.replaceAll(' ','');
				prospecto.Desarrollo__c = desarrolloAux.Id;
				
				System.debug('*****prospecto.Desarrollo_Web__c:' + prospecto.Desarrollo_Web__c);
				System.debug('*****prospecto.Desarrollo__c:' + prospecto.Desarrollo__c);
			}
			
			Integer cantidadIntentos = 0;
			cantidadIntentos = [SELECT COUNT() FROM Bitacora__c WHERE Lead__c =: prospecto.Id];

			this.numeroIntentosGeneral = cantidadIntentos;
			bitacora.Lead__c = prospecto.Id;

			bitacora.Intento__c = String.valueOf(cantidadIntentos + 1);
			bitacora.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
			
			//Si el prospecto no contesto se guarda la bitacora y se actualiza la etapa del prospecto a En Proceso
			if(bitacora.Contesta__c == 'No'){
				
				insert bitacora;
				
				prospecto.Status = 'En Proceso';
				update prospecto;
			}			
			//Si el prospecto si contesto se guarda la bitacora y se actualiza el estatus del prospecto BDC a Contactado o Citado
			if(bitacora.Contesta__c == 'Si'){
				
				insert bitacora;
				
				if(crearCita == 'No'){
					prospecto.Status = 'Contactado';
				}
				else if(crearCita == 'Si'){
					
					prospecto.Estatus_Prospecto_BDC__c = 'Citado';
					
					String[] arregloHorarioElegido = horarioSeleccionado.split('\\:');
					Integer horaElegida = Integer.valueOf(arregloHorarioElegido[0]);
					System.debug('*****horaElegida:' + horaElegida);
					
					//Se recorren los asesores para verificar cuales estan disponibles en el horario seleccionado
					String llaveAsesorHora = '';
					Set<Id> idsAsesoresDisponibles = new Set<Id>();
					System.debug('*****idsAsesores Guardar:' + idsAsesores);
					for(Id ida : idsAsesores){
						
						//Si ni el mapa de tareas ni el mapa de eventos contienen el asesor y la hora es un asesor al que se le puede asignar la cita
						llaveAsesorHora = ida + '-' + horaElegida;
						System.debug('*****llaveAsesorHora: ' + llaveAsesorHora);
						if(mapaAsesorHorasTareas.containsKey(llaveAsesorHora) == false && mapaAsesorHorasEventos.containsKey(llaveAsesorHora) == false){
							idsAsesoresDisponibles.add(ida);
						}
					}
					System.debug('*****idsAsesoresDisponibles: ' + idsAsesoresDisponibles);
					
					for(User testAsesor : [SELECT Id, Name FROM User WHERE Id IN : idsAsesoresDisponibles]){
						System.debug('*****Asesor Disponible: ' + testAsesor.Name);
					}
		
					//Se buscan las guardias de los asesores disponibles para la comparacion de roll y contador de citas para determinar a cual asesor se
					//le asigna la cita
					List<Guardia__c> guardiasAsesoresDisponibles = [SELECT Nombre_Asesor__c, Roll__c, Contador_Citas__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c AND Fecha_Guardia__c =: prospecto.F_CM__c AND Nombre_Asesor__c IN : idsAsesoresDisponibles ORDER BY Roll__c];
					System.debug('*****guardiasAsesoresDisponibles: ' + guardiasAsesoresDisponibles);
					
					Guardia__c guardiaActual = new Guardia__c();
					Guardia__c guardiaSiguiente = new Guardia__c();
					Integer contadorCitasGuardiaActual = 0;
					Integer contadorCitasGuardiaSiguiente = 0;
					for(Integer i = 0; i < guardiasAsesoresDisponibles.size(); i++){
						
						if(i==0){
							guardiaActual = guardiasAsesoresDisponibles.get(i);	
						}							
						
						if(i < (guardiasAsesoresDisponibles.size() - 1)){
							
							guardiaSiguiente = guardiasAsesoresDisponibles.get(i + 1);
							
							System.debug('*****guardiaActual:' + guardiaActual);
							System.debug('*****guardiaSiguiente:' + guardiaSiguiente);
							
							contadorCitasGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c));
							contadorCitasGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Citas__c));
							System.debug('*****contadorCitasGuardiaActual:' + contadorCitasGuardiaActual);
							System.debug('*****contadorCitasGuardiaSiguiente:' + contadorCitasGuardiaSiguiente);
							
							if(contadorCitasGuardiaActual <= contadorCitasGuardiaSiguiente){
								guardiaActual = guardiaActual;
								System.debug('****entro a if');
							}
							else{
								guardiaActual = guardiaSiguiente;
								System.debug('****entro a else');
							}
						}
						else{
							break;
						}
					}
					
					System.debug('*****guardiaActual: ' + guardiaActual);
					
					Task cita = new Task();
					cita.Subject = 'Cita';
					cita.OwnerId = guardiaActual.Nombre_Asesor__c;
					cita.WhoId = prospecto.Id;
					cita.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
					cita.Fecha_Cita__c = DateTime.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day(),horaElegida,0,0);
					cita.ActivityDate = Date.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day());
					cita.Estatus_de_Visita__c = 'Pendiente';
					cita.Nombre_Prospecto__c = prospecto.Name;
					
					try{
						insert cita;
					}
					catch(Exception e){
						System.debug('*****Error al crear la cita:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    		ApexPages.addMessage(mensaje);
					}
					System.debug('*****Cita (tarea) creada:' + cita);

					//Emoran craete  new Event
					Event evento = new Event();
					evento.OwnerId = prospecto.Agente__c;
					evento.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
				    evento.WhatId = bitacora.Id;
				    evento.StartDateTime =  DateTime.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day(),horaElegida,0,0);
				    evento.EndDateTime =  DateTime.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day(),horaElegida +  1,0,0);
				    evento.ActivityDateTime = DateTime.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day(),horaElegida,0,0);
				    evento.Subject = 'Cita';

				    try{
						insert evento;
					}
					catch(Exception e){
						System.debug('*****Error al crear el evento:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    		ApexPages.addMessage(mensaje);
					}
					System.debug('*****Cita (evento) creada:' + evento);	
					
					guardiaActual.Contador_Citas__c = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c)) + 1;
					try{
						update guardiaActual;
					}
					catch(Exception e){
						System.debug('*****Error al actualizar la guardia:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    		ApexPages.addMessage(mensaje);
					}

					prospecto.OwnerId = guardiaActual.Nombre_Asesor__c;
					prospecto.Asesor_Primario__c = guardiaActual.Nombre_Asesor__c;
					prospecto.Estatus_Cita__c = 'Pendiente';
				}
				
				System.debug('*****guardar prospecto.OwnerId: ' + prospecto.OwnerId);
				System.debug('*****guardar prospecto.Agente__c: ' + prospecto.Agente__c);
				System.debug('*****guardar prospecto.Desarrollo_Comercial__c: ' + prospecto.Desarrollo_Comercial__c);
				try{
					update prospecto;
				}
				catch(Exception e){
					System.debug('*****Error al actualizar el prospecto:' + e.getMessage());
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    	ApexPages.addMessage(mensaje);
				}
			}
			
			PageReference pageRef = new PageReference('/' + bitacora.Id);
			return pageRef;
		}
	}
	
	//Metodo para asignar un agente BDC cuando el desarrollo comercial cambio
	public void asignarAgenteBDC(){
		
		Id idDesarrolloComercialBDC = [SELECT Id FROM Desarrollo_Comercial__c WHERE Name = 'BDC'].Id;
		List<Guardia__c> guardiasAsesoresDisponibles = new List<Guardia__c>();
		Integer horaActual = DateTime.now().hour();
		
		//Si el horario es entre las 4 pm y las 7 pm se obtienen solo las guardias del dia actual y del rol 2
		if(horaActual >= 16 && horaActual < 19){
			
			System.debug('*****Entro a query 1 horaActual >= 16 && horaActual < 19');
			guardiasAsesoresDisponibles = [
			SELECT 
				Nombre_Asesor__c, 
				Roll__c, 
				Desarrollo_Comercial_Primario__c, 
				Desarrollo_Comercial_Secundario__c, 
				Contador_Prospectos__c 
			FROM 
				Guardia__c 
			WHERE 
				Desarrollo_Comercial__c =: idDesarrolloComercialBDC 
			AND(
				Desarrollo_Comercial_Primario__c =: prospecto.Desarrollo_Comercial__c 
			OR 
				Desarrollo_Comercial_Secundario__c =: prospecto.Desarrollo_Comercial__c) 
			AND 
				Fecha_Guardia__c =: Date.today() 
			AND 
				Roll__c = 2];
		}
		//Si el horario es entre las 0 am y las 4 pm se obtienen solo las guardias del dia actual y de ambos roles
		else if(horaActual >= 0 && horaActual < 16){
			
			System.debug('*****Entro a query 2 horaActual >= 0 && horaActual < 16');
			guardiasAsesoresDisponibles = [
			SELECT 
				Nombre_Asesor__c, 
				Roll__c, 
				Desarrollo_Comercial_Primario__c, 
				Desarrollo_Comercial_Secundario__c, 
				Contador_Prospectos__c 
			FROM 
				Guardia__c 
			WHERE 
				Desarrollo_Comercial__c =: idDesarrolloComercialBDC 
			AND(
				Desarrollo_Comercial_Primario__c =: prospecto.Desarrollo_Comercial__c  
			OR 
				Desarrollo_Comercial_Secundario__c =: prospecto.Desarrollo_Comercial__c) 
			AND 
				Fecha_Guardia__c =: Date.today() 
			ORDER BY Roll__c DESC];
		}
		//Si el horario es entre las 19 pm y las 23 pm se obtienen solo las guardias del dia siguiente y de ambos roles
		else if(DateTime.now().hour() >= 19 && DateTime.now().hour() <= 23 && DateTime.now().minute() <= 59 && DateTime.now().second() <= 59){
			
			System.debug('*****Entro a query 3 DateTime.now().hour() >= 19 && DateTime.now().hour() <= 23 && DateTime.now().minute() <= 59 && DateTime.now().second() <= 59');
			guardiasAsesoresDisponibles = [
			SELECT 
				Nombre_Asesor__c, 
				Roll__c, 
				Desarrollo_Comercial_Primario__c, 
				Desarrollo_Comercial_Secundario__c, 
				Contador_Prospectos__c 
			FROM 
				Guardia__c 
			WHERE 
				Desarrollo_Comercial__c =: idDesarrolloComercialBDC
			AND(
				Desarrollo_Comercial_Primario__c =: prospecto.Desarrollo_Comercial__c  
			OR 
				Desarrollo_Comercial_Secundario__c =: prospecto.Desarrollo_Comercial__c) 
			AND 
				Fecha_Guardia__c =: Date.today().addDays(1) 
			ORDER BY Roll__c DESC];
		}
		
		//List<Guardia__c> guardiasElegibles = new List<Guardia__c>();
		List<GuardiaWrapper> guardiasElegibles = new List<GuardiaWrapper>();
		Guardia__c guardiaActual = new Guardia__c();
		Guardia__c guardiaSiguiente = new Guardia__c();
		Integer contadorProspectosGuardiaActual = 0;
		Integer contadorProspectosGuardiaSiguiente = 0;
			
		//guardiasElegibles = new List<Guardia__c>();
		guardiasElegibles = new List<GuardiaWrapper>();
		for(Guardia__c g : guardiasAsesoresDisponibles){
				
			if(prospecto.Desarrollo_Comercial__c == g.Desarrollo_Comercial_Primario__c || prospecto.Desarrollo_Comercial__c == g.Desarrollo_Comercial_Secundario__c){
				//guardiasElegibles.add(g);
				guardiasElegibles.add(new GuardiaWrapper(g));
			}
		}
			
		System.debug('*****guardiasElegibles: ' + guardiasElegibles);
		guardiasElegibles.sort();
		System.debug('*****guardiasElegibles sort:' + guardiasElegibles);
			
		guardiaActual = new Guardia__c();
		guardiaSiguiente = new Guardia__c();
		contadorProspectosGuardiaActual = 0;
		contadorProspectosGuardiaSiguiente = 0;
		for(Integer i = 0; i < guardiasElegibles.size(); i++){
				
			if(i==0){
				guardiaActual = guardiasElegibles.get(i).oppy;	
			}							
				
			if(i < (guardiasElegibles.size() - 1)){
					
				guardiaSiguiente = guardiasElegibles.get(i + 1).oppy;
					
				System.debug('*****guardiaActual:' + guardiaActual);
				System.debug('*****guardiaSiguiente:' + guardiaSiguiente);
					
				contadorProspectosGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Prospectos__c));
				contadorProspectosGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Prospectos__c));
				System.debug('*****contadorProspectosGuardiaActual:' + contadorProspectosGuardiaActual);
				System.debug('*****contadorProspectosGuardiaSiguiente:' + contadorProspectosGuardiaSiguiente);
					
				if(contadorProspectosGuardiaActual <= contadorProspectosGuardiaSiguiente){
					guardiaActual = guardiaActual;
					System.debug('****entro a if');
				}
				else{
					guardiaActual = guardiaSiguiente;
					System.debug('****entro a else');
				}
			}
			else{
				break;
			}
		}

		prospecto.OwnerId = guardiaActual.Nombre_Asesor__c;
		prospecto.Agente__c = guardiaActual.Nombre_Asesor__c;
		prospecto.Status = 'Asignado';
			
		guardiaActual.Contador_Prospectos__c = (String.isBlank(String.valueOf(guardiaActual.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Prospectos__c)) + 1;
		update guardiaActual;	
	}
	
	global class GuardiaWrapper implements Comparable {

    public Guardia__c oppy;
    
    // Constructor
    public GuardiaWrapper(Guardia__c op) {
        oppy = op;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        GuardiaWrapper compareToOppy = (GuardiaWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (oppy.Roll__c > compareToOppy.oppy.Roll__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (oppy.Roll__c < compareToOppy.oppy.Roll__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}
	
	//Metodo para cancelar la creacion de una bitacora
	public PageReference cancelar(){
		
		PageReference pageRef = new PageReference('/' + prospecto.Id);
		return pageRef;
	}
}