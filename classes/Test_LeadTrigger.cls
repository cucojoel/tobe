@isTest
public with sharing class Test_LeadTrigger {
  @testSetup
  static void setup() {
    Configuracion_Enkontrol__c configuracionEnkontrol = new Configuracion_Enkontrol__c();
    configuracionEnkontrol.Service_Endpoint__c = 'https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc';
    configuracionEnkontrol.Username__c = 'Username';
    configuracionEnkontrol.Password__c = 'Password';
    insert configuracionEnkontrol;
    Compania__c compania = Bg_Utils_Cls.genCompania('companiaName', '1');
    insert compania;
    User gerenteUser = Bg_Utils_Cls.genUser('gereUser', 'Gerente de ventas');
    insert gerenteUser;
    Desarrollo_Comercial__c desarrolloComercial = Bg_Utils_Cls.genDesComercial('test', compania.Id, gerenteUser.Id );
    insert desarrolloComercial;
    Desarrollo_Comercial__c desarrolloComercial1 = Bg_Utils_Cls.genDesComercial('test2', compania.Id, gerenteUser.Id );
    insert desarrolloComercial1;
    Desarrollo_Comercial__c desarrolloComercialBDC = Bg_Utils_Cls.genDesComercial('BDC', compania.Id, gerenteUser.Id );
    insert desarrolloComercialBDC;
    Desarrollo__c desarrollo = Bg_Utils_Cls.genDesarrollo('test', 'BO4', desarrolloComercial.Id);
    insert desarrollo;
    Desarrollo__c desarrolloOne = Bg_Utils_Cls.genDesarrollo('test2', 'CO5', desarrolloComercial1.Id);
    insert desarrolloOne;
    Guardia__c guardiaUno = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial.Id, gerenteUser.Id, 2);
    guardiaUno.Fecha_Guardia__c = Date.today();
    insert guardiaUno;
    Guardia__c guardiaDos = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial.Id, gerenteUser.Id, 2);
    guardiaDos.Fecha_Guardia__c = Date.today().addDays(1);
    insert guardiaDos;
    Guardia__c guardiaTres = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial1.Id, gerenteUser.Id, 2);
    guardiaTres.Fecha_Guardia__c = Date.today();
    insert guardiaTres;
    Guardia__c guardiaCuatro = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial1.Id, gerenteUser.Id, 2);
    insert guardiaCuatro;
    Guardia__c guardiaCinco = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial1.Id, gerenteUser.Id, 1);
    guardiaCinco.Fecha_Guardia__c = Date.today().addDays(1);
    insert guardiaCinco;
    Guardia__c guardiaSeis = Bg_Utils_Cls.genGuardia(desarrolloComercial.Id, desarrolloComercial1.Id, gerenteUser.Id, 1);
    guardiaSeis.Fecha_Guardia__c = Date.today().addDays(1);
    insert guardiaSeis;
    Lead leadDup = new Lead();
    leadDup.FirstName = 'leadDup';
    leadDup.LastName = 'leadDup';
    leadDup.Email = 'leadDup@test.com';
    leadDup.Phone = '1234567894';
    leadDup.LeadSource = '1';
    leadDup.Agente__c = gerenteUser.Id;
    leadDup.Desarrollo_web__c = 'test';
    leadDup.Desarrollo_Comercial__c = desarrolloComercial.Id;
    leadDup.Medio__c = 'Conoce la marca Be Grand';
    insert leadDup;
    Lead leadDupBis = [SELECT FirstName, LastName, Email, Email2__c, Phone, LeadSource, Desarrollo_web__c, Medio__c, Desarrollo_Comercial__c FROM Lead WHERE Id = :leadDup.Id];
  }
  public static testMethod void testTriggerCapturaCompletaSuccess() {
    Map<String, String> mockResponses = new Map<String, String>();
    User gerenteUser = [SELECT Id FROM User where Id_Enkontrol__c = '1' AND Id_Enkontrol__c != null Limit 1];
    Desarrollo__c desarrollo = [SELECT Id FROM Desarrollo__c WHERE Id_Desarrollo_Enkontrol__c = 'BO4'];
    Lead leadOne = new Lead();
    leadOne.FirstName = 'leadOne';
    leadOne.LastName = 'leadOne';
    leadOne.Email = 'leadOne@test.com';
    leadOne.Phone = '1234567891';
    leadOne.LeadSource = '1';
    leadOne.Agente__c = gerenteUser.Id;
    leadOne.Desarrollo__c = desarrollo.Id;
    leadOne.Web_to_lead__c = false;
    leadOne.Medio__c = 'Conoce la marca Be Grand';
    insert leadOne;
    update leadOne;
    /*     *     */
    Helper_RecursiveTrigger.setIsExecuting(true);
    Boolean isExe = Helper_RecursiveTrigger.getIsExecuting();
    Helper_RecursiveTrigger.setPrendeBandera();
    /*     *     */
    Lead leadTwo = new Lead();
    leadTwo.FirstName = 'leadTwo';
    leadTwo.LastName = 'leadTwo';
    leadTwo.Email = 'leadTwo@test.com';
    leadTwo.Phone = '1234567892';
    leadTwo.LeadSource = '1';
    leadTwo.Agente__c = gerenteUser.Id;
    leadTwo.Desarrollo__c = null;
    leadTwo.Web_to_lead__c = true;
    leadTwo.Medio__c = 'Conoce la marca Be Grand';
    insert leadTwo;
    /*     *     */
    Lead leadThree = new Lead();
    leadThree.FirstName = 'leadThree';
    leadThree.LastName = 'leadThree';
    leadThree.Email = 'leadThree@test.com';
    leadThree.Phone = '1234567893';
    leadThree.LeadSource = '6';
    leadThree.Agente__c = gerenteUser.Id;
    leadThree.Desarrollo_web__c = 'test';
    leadThree.Medio__c = 'Conoce la marca Be Grand';
    insert leadThree;
    /*     *     */
    System.runAs(gerenteUser) {
      Test.startTest();
      mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
      mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_FAIL);
      mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
      Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
      Test.setMock(HttpCalloutMock.class, multipleMock);
      System.assertNotEquals(null, mockResponses);
      Test.stopTest();
    }
    Helper_RecursiveTrigger.setApagaBandera();
  }
  /*   *   */
  public static testMethod void testQueueableServiceBasicoSuccess() {
    List<Lead> leadList = new List<Lead>();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
      Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
      Telefono2__c, Telefono_3__c, Email, Email2__c, Street, City, Country, Nacionalidad__c,
      ID_Prospecto_Enkontrol__c, PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c,
      Compania__r.Id_Compania_Enkontrol__c, CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c,
      LeadSource, LastModifiedById, Desarrollo_Comercial__c
      FROM Lead
      LIMIT 1
    ];
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, true);
    //ID jobID = System.enqueueJob(caseJob);
    System.assertNotEquals(null, caseJob);
    Test.stopTest();
  }
   /*   *   */
  public static testMethod void testQueueableServiceCompletoCallout() {
    List<Lead> leadList = new List<Lead>();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
      Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
      Telefono2__c, Telefono_3__c, Email, Email2__c, Street, City, Country, Nacionalidad__c, ID_Prospecto_Enkontrol__c,
      PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
      CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c
      FROM Lead
      LIMIT 1
    ];
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    System.assertNotEquals(null, caseJob);
    Test.stopTest();
  }
  /*   *   */
  public static testMethod void testTriggerCapturaCompletaUpdateSuccess() {
    List<Lead> leadList = new List<Lead>();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
      Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
      Telefono2__c, Telefono_3__c, Email, Email2__c, Street, City, Country, Nacionalidad__c, ID_Prospecto_Enkontrol__c,
      PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
      CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c
      FROM Lead
      LIMIT 1
    ];
    /*     *     */
    Desarrollo_Comercial__c desarrolloComercial = [SELECT Id FROM Desarrollo_Comercial__c WHERE Name = 'test2'];
    /*     *     */
    for(Lead l : leadList) {
      l.Sincronizado_en_Enkontrol__c = true;
      l.ID_Prospecto_Enkontrol__c = '90092';
      l.Desarrollo_Comercial__c = desarrolloComercial.Id;
    }
    update leadList;
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    System.assertNotEquals(null, caseJob);
    Test.stopTest();
  }
  /*   *   */
  public static testMethod void testTriggerMediosDigitalesCompletaSuccess() {
    List<Lead> leadList = new List<Lead>();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
      Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
      Telefono2__c, Telefono_3__c, Email, Email2__c, Street, City, Country, Nacionalidad__c, ID_Prospecto_Enkontrol__c,
      PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
      CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c
      FROM Lead
      LIMIT 1
    ];
    for(Lead l : leadList) {
      l.LeadSource = '10';
    }
    update leadList;
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    System.assertNotEquals(null, caseJob);
    Test.stopTest();
  }
  /*   * Administrador del sistema   */
  public static testMethod void testTriggerCapturaBasicaFail() {
    Desarrollo__c desarrollo = [SELECT Id FROM Desarrollo__c WHERE Id_Desarrollo_Enkontrol__c = 'BO4'];
    List<Lead> leadList = new List<Lead>();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrolTest/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_FAIL);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    Desarrollo_Comercial__c desarrolloComercial = [SELECT Id FROM Desarrollo_Comercial__c WHERE Name = 'test2'];
    User gerenteUser = [SELECT Id FROM User where Id_Enkontrol__c = '1' AND Id_Enkontrol__c != null Limit 1];
    Lead leadFour = new Lead();
    leadFour.FirstName = 'leadFour';
    leadFour.LastName = 'leadFour';
    leadFour.Email = 'leadFour@test.com';
    leadFour.Phone = '1234567894';
    leadFour.Desarrollo_Comercial__c = desarrolloComercial.Id;
    leadFour.LeadSource = '6';
    leadFour.Agente__c = gerenteUser.Id;
    leadFour.OwnerId = gerenteUser.Id;
    leadFour.Desarrollo__c = desarrollo.Id;
    leadFour.Medio__c = 'Conoce la marca Be Grand';
    insert leadFour;
    leadList.add(leadFour);
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    System.assertNotEquals(null, caseJob);
    Test.stopTest();
  }
  /* BG init */
  public static testMethod void testDesarooloWeb() {
    Map<String, String> mockResponses = new Map<String, String>();
    Lead leadFive = new Lead();
    leadFive.FirstName = 'leadFive';
    leadFive.LastName = 'leadFive';
    leadFive.Email = 'leadFive@test.com';
    leadFive.Phone = '1234567895';
    leadFive.Desarrollo_web__c = 'test';
    leadFive.Desarrollo__c = null;
    leadFive.LeadSource = '12';
    leadFive.Medio__c = 'Conoce la marca Be Grand';
    insert leadFive;
    Lead leadSix = new Lead();
    leadSix.FirstName = 'leadSix';
    leadSix.LastName = 'leadOne';
    leadSix.Email = 'lead_test03@test.com';
    leadSix.Phone = '2345678901';
    leadSix.Desarrollo_web__c = 'test';
    leadSix.Desarrollo__c = null;
    leadSix.LeadSource = '13';
    leadSix.Medio__c = 'Conoce la marca Be Grand';
    insert leadSix;
    System.assertNotEquals(null, leadSix);
  }
  /* BG end */
  public static testMethod void testAletrno() {
    Desarrollo_Comercial__c desarrolloComercial = [SELECT Id FROM Desarrollo_Comercial__c WHERE Name = 'test'];
    List<Lead> leadList = new List<Lead>();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, true);
    caseJob.createObjectClass();
    Service_LeadTrigger controllerOne = new Service_LeadTrigger();
    controllerOne.createObjectClass();
    Handler_LeadTrigger controllerTwo = new Handler_LeadTrigger();
    controllerTwo.createObjectClass();
    Utils_WS_EnkontrolRequestBuilder controller = new Utils_WS_EnkontrolRequestBuilder();
    controller.createObjectClass();
    Lead leadDup = new Lead();
    leadDup.FirstName = 'leadDup';
    leadDup.LastName = 'leadDup';
    leadDup.Email = 'leadDup@test.com';
    leadDup.Phone = '1234567894';
    leadDup.LeadSource = '1';
    leadDup.Desarrollo_web__c = 'test';
    leadDup.Desarrollo_Comercial__c = desarrolloComercial.Id;
    leadDup.Medio__c = 'Conoce la marca Be Grand';
    //insert leadDup;
    System.assertNotEquals(null, leadDup);
  }
}