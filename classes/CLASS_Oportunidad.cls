public with sharing class CLASS_Oportunidad {

	//Metodo para eliminar los attachments de la carta oferta y de las carpetas asociados a una oportunidad cuando la oportunidad se elimina
	public void eliminaAttachments(Map<Id,Opportunity> oportunidadesEliminadas){
		
		//SI LOS ATTACHMENTS ESTAN ASOCIADOS A LA OPORTUNIDAD
		/*Set<Id> idsOportunidadesEliminadas = oportunidadesEliminadas.keySet();
		List<Attachment> attachmentsEliminar = [SELECT Id FROM Attachment WHERE ParentId IN : idsOportunidadesEliminadas];
		delete attachmentsEliminar;*/		
		
		Set<Id> idsOportunidadesEliminadas = oportunidadesEliminadas.keySet();
		
		//SI LOS ATTACHMENTS ESTAN ASOCIADOS A LA CARTA OFERTA		
		Set<Id> idsCartasOferta = new Set<Id>();
		for(C_Oferta__c cartaOferta : [SELECT Id, Oportunidad__c FROM C_Oferta__c WHERE Oportunidad__c IN : idsOportunidadesEliminadas]){
			idsCartasOferta.add(cartaOferta.Id);
		}
		List<Attachment> attachmentsCartaOfertaEliminar = [SELECT Id FROM Attachment WHERE ParentId IN : idsCartasOferta];
		delete attachmentsCartaOfertaEliminar;
		
		//SI LOS ATTACHMENTS ESTAN ASOCIADOS A UNA CARPETA
		Set<Id> idsCarpetas = new Set<Id>();
		for(Carpeta__c carpeta : [SELECT Id, Oportunidad__c FROM Carpeta__c WHERE Oportunidad__c IN : idsOportunidadesEliminadas]){
			idsCarpetas.add(carpeta.Id);
		}
		List<Attachment> attachmentsCarpertaEliminar = [SELECT Id FROM Attachment WHERE ParentId IN : idsCarpetas];
		delete attachmentsCarpertaEliminar;
	}
}