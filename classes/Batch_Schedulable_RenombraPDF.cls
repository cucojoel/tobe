// Renombrar los PDF's de Account con la estructura "ECtaBG_XXXXXXXXXXXXXXXXXX" a "ECtaBG_Oportunidad_Mes_Año.pdf"
global class Batch_Schedulable_RenombraPDF implements Schedulable {	
	global void execute(SchedulableContext SC) {
		list<String> oppsEdoCta = new list<String>();
		map<String, String> mes = new map<String, String>();
		map<String, String> mapOppId_OppName = new map<String, String>();
		map<String, String> mapOriginalNNuevo = new map<String, String>();
		// Query: [select Id, Name, CreatedDate from Attachment where Name like 'ECtaBG_%'];
		
		mes = new map<String, String>();
		mes.put('01','Enero');		mes.put('05','Mayo');		mes.put('09','Septiembre');
		mes.put('02','Febrero');	mes.put('06','Junio');		mes.put('10','Octubre');
		mes.put('03','Marzo');		mes.put('07','Julio');		mes.put('11','Noviembre');
		mes.put('04','Abril');		mes.put('08','Agosto');		mes.put('12','Diciembre');
		
		list<Attachment> adjuntos = Database.query(system.label.Batch_RenombraPDF);
		for(Attachment forData : adjuntos) {
			String alMapa = forData.Name;
			String tmp = forData.Name;
			tmp = tmp.replace('ECtaBG_','');
			tmp = tmp.subString(0,18);
			oppsEdoCta.add(tmp);
			mapOriginalNNuevo.put(alMapa,tmp);		
		}
		
		for(Opportunity forData : [select Id, Name from Opportunity where Id IN : oppsEdoCta]) { mapOppId_OppName.put(forData.Id, forData.Name); }
		
		for(Attachment forData : adjuntos) {
			String conversion = String.valueOf(forData.CreatedDate);
			String[] fechaPartida = conversion.split('-');
			forData.Name = 'Estado_de_Cuenta_' + mapOppId_OppName.get(mapOriginalNNuevo.get(forData.Name)) + '_' + mes.get(fechaPartida[1]) + '_' + fechaPartida[0];
		}
		update adjuntos;
	}
}