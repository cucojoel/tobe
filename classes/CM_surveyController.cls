public with sharing class CM_surveyController {
  public List<SurveyQuestion> getSurveyQuestion {get;set;}
  public List<PagPregRes> getPagPregRes {get;set;}
  public String invitationId {get;set;}
  public String surveyVersionId {get;set;}
  public String surveyId {get;set;}
  public String userId {get;set;}
  public String opportunityId {get;set;}
  public String tipoEncuesta {get;set;}
  public String leadId {get;set;}
  public Boolean isCompleted {get;set;}
  public CM_surveyController()
  {
    userId = UserInfo.getUserId();
    isCompleted = false;
    String sfId = ApexPages.currentPage().getParameters().get('id');
    String sfObj = ApexPages.currentPage().getParameters().get('obj');
    tipoEncuesta=sfObj;
    Integer countlid = [SELECT count() From Lead WHERE Id =:sfId];
    Integer countopp = [SELECT count() From Opportunity WHERE Id =:sfId];
    if(countlid > 0) {
      leadId = sfId;
      Integer getCompleted = [SELECT Count() From Realizadas_cm_survey__c WHERE LeadId__c =:sfId];
      if(getCompleted == 1) {
        isCompleted = true;
      }
    }
    Integer getCompleted= 0;
    if(countopp > 0) {
      opportunityId = sfId;
      getCompleted = [SELECT Count() From Realizadas_cm_survey__c WHERE OpportunityId__c =:sfId AND Tipo_Encuesta__c = :sfObj];
      Opportunity thisOpp = [SELECT Id, Name, Id_Prospecto_Enkontrol__c, StageName From Opportunity WHERE Id =:sfId];
      if (thisOpp.StageName != 'Carta Oferta') {
        sfObj = 'P';
        tipoEncuesta=sfObj;
        getCompleted = [SELECT Count() From Realizadas_cm_survey__c WHERE OpportunityId__c =:sfId AND Tipo_Encuesta__c = :sfObj];
      }
      if(getCompleted >= 1) {
        isCompleted = true;
      }
    }
    Set<String> getSurveyQuestionIds = new Set<String>();
    Survey getSurvey = [SELECT ActiveVersionId,Description,DeveloperName,Id,Name FROM Survey limit 1];
    surveyVersionId = getSurvey.ActiveVersionId;
    surveyId = getSurvey.Id;
    SurveyInvitation surveyInvitation = [SELECT Id,InviteExpiryDateTime,SurveyId FROM SurveyInvitation WHERE SurveyId =: getSurvey.Id AND InviteExpiryDateTime = null limit 1];
    invitationId = surveyInvitation.Id;
    List<SurveyPage> getSurveyPage = [SELECT Id,Name,SurveyVersionId FROM SurveyPage where SurveyVersionId =:getSurvey.ActiveVersionId ORDER BY Name];
    getSurveyQuestion = [SELECT Id,Name,QuestionType,SurveyPageId, SurveyPage.Name,SurveyVersionId FROM SurveyQuestion WHERE SurveyVersionId =: getSurvey.ActiveVersionId AND SurveyPage.Name LIKE : sfObj+'%' order by Id ];
    for(SurveyQuestion sQuestion:getSurveyQuestion) {
      getSurveyQuestionIds.add(sQuestion.Id);
    }
    Map<String, List<SurveyQuestionChoice>> mapSurveyQuestionChoice = new Map<String, List<SurveyQuestionChoice>>();
    List<SurveyQuestionChoice> getSurveyQuestionChoice = [SELECT Id,Name,QuestionId,SurveyVersionId FROM SurveyQuestionChoice Where QuestionId in:getSurveyQuestionIds order by Name];
    getPagPregRes = new List<PagPregRes>();
    for(SurveyPage sp:getSurveyPage) {
      List<PregRes> getSpregRes = new List<PregRes>();
      PagPregRes pagPregRes = new PagPregRes();
      pagPregRes.surveyPage = sp;
      for(SurveyQuestion sq:getSurveyQuestion) {
        if(sq.SurveyPage.Name == sp.Name) {
          PregRes pregRes = new PregRes();
          pregRes.surveyQuestion = sq;
          List<SurveyQuestionChoice> lsttmp = new List<SurveyQuestionChoice>();
          for(SurveyQuestionChoice sqc:getSurveyQuestionChoice) {
            if(sqc.QuestionId == sq.Id) {
              lsttmp.add(sqc);
            }
          }
          pregRes.surveyQuestionChoice = lsttmp;
          getSpregRes.add(pregRes);
        }
      }
      pagPregRes.regRes = getSpregRes;
      getPagPregRes.add(pagPregRes);
    }
  }
  @RemoteAction(callout=true)
  public static String saveRes(Realizadas_cm_survey__c surveyObj, List<RespuestasCmSurvey__c> respuestasCmSurvey)
  {
    if (!Test.isRunningTest()) {
      try{
        insert surveyObj;
      }
      catch (Exception ex) {
        System.debug('try one');
        System.debug(ex.getMessage());
      }
      for(RespuestasCmSurvey__c respuestaSurv : respuestasCmSurvey) {
        respuestaSurv.ResponseId__c= surveyObj.Id;
      }
      try{
        insert respuestasCmSurvey;
      }
      catch (Exception ex) {
        System.debug('try two');
        System.debug(ex.getMessage());
      }
      if(String.isNotBlank(surveyObj.LeadId__c)) {
        return surveyObj.LeadId__c;
      }
      if(String.isNotBlank(surveyObj.OpportunityId__c)) {
        return surveyObj.OpportunityId__c;
      }
    }
    return 'true';
  }
  public class PregRes {
    public SurveyQuestion surveyQuestion {get;set;}
    public List<SurveyQuestionChoice> surveyQuestionChoice {get;set;}
  }
  public class PagPregRes {
    public SurveyPage surveyPage {get;set;}
    public List<PregRes> regRes {get;set;}
  }
}