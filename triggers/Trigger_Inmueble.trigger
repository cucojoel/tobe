/* Test: Test_InmuebleTrigger */
trigger Trigger_Inmueble on Inmueble__c (before update, after insert, after update) {
    if(trigger.isAfter) {
    	if(trigger.isUpdate) {
    		Handler_InmuebleTrigger.isAfterUpdate(Trigger.New, Trigger.oldMap);    		
    	}
    }
    
    // Cuando pasa el Departamento a - Vendido - se buscara la Oportunidad mediante el valor - Id_Edo_Cta__c -
    if(trigger.isBefore) {
    	if(trigger.isUpdate) {
    		if(system.label.DetonaEmpateCtaOppDpto == 'Si') {
    			map <String, Inmueble__c> completarDepartamento = new map<String, Inmueble__c>();
    			for(Inmueble__c forData : trigger.new) {
	    			//if(trigger.oldMap.get(forData.Id).Estatus__c != 'VENDIDO' && forData.Estatus__c == 'VENDIDO')
	    			if((trigger.oldMap.get(forData.Id).Estatus__c != system.label.SFDC_TriggerInmuebleEstatus1 && forData.Estatus__c == system.label.SFDC_TriggerInmuebleEstatus1) || (trigger.oldMap.get(forData.Id).Estatus__c != system.label.SFDC_TriggerInmuebleEstatus2 && forData.Estatus__c == system.label.SFDC_TriggerInmuebleEstatus2) || (trigger.oldMap.get(forData.Id).Estatus__c != system.label.SFDC_TriggerInmuebleEstatus3 && forData.Estatus__c == system.label.SFDC_TriggerInmuebleEstatus3) || (trigger.oldMap.get(forData.Id).Estatus__c != system.label.SFDC_TriggerInmuebleEstatus4 && forData.Estatus__c == system.label.SFDC_TriggerInmuebleEstatus4) || (trigger.oldMap.get(forData.Id).Estatus__c != system.label.SFDC_TriggerInmuebleEstatus5 && forData.Estatus__c == system.label.SFDC_TriggerInmuebleEstatus5) ){   
	    				completarDepartamento.put(forData.Id_Edo_Cta__c, forData);
	    			}
    			}
    			if(!completarDepartamento.isEmpty()) {
    				for(Opportunity forData : [select Id, Id_Prospecto_Enkontrol__c, ID_Cliente_Enkontrol__c, Id_Edo_Cta_2__c from Opportunity where Id_Edo_Cta_2__c IN : completarDepartamento.keySet() and AccountId <> null]) {
    					if(completarDepartamento.get(forData.Id_Edo_Cta_2__c) != null) {
    						Inmueble__c tmp = completarDepartamento.get(forData.Id_Edo_Cta_2__c);
    						tmp.Id_Prospecto_Enkontrol__c = forData.Id_Prospecto_Enkontrol__c;
    						tmp.Id_Cliente_Enkontrol__c = forData.ID_Cliente_Enkontrol__c;
    						tmp.OportunidadTexto__c = forData.Id;
    						completarDepartamento.put(forData.Id_Edo_Cta_2__c, tmp);
    					}    					
    				}
    			}
    		}    		    		
    	}
    }
    
    // Estados de Cuenta - 17 Septiembre 2019 - Generacion asincrona de los Estados de Cuenta */
    if(trigger.isAfter) {
        if(trigger.isUpdate) {
            if(system.label.AutorizaWSEdoCtaMasivo == 'Si') {
                for(Inmueble__c forData : trigger.new) {
                	/* Se quedo obsoleto ya que se hara por el dato - Id Edo Cta - lleva la clase - EnviarPorCorreoMasivo_idCliente - asociada */
                	/*
                	if(forData.Estatus__c == 'VENDIDO' && (forData.Id_Cliente_Enkontrol__c == '' || forData.OportunidadTexto__c == '')) {
                		if(!system.isFuture() && !system.isBatch()) {
                			EnviarPorCorreoMasivo_idCliente.relacionaInformacion(forData.Id);
                		}               			
                	}
                	*/
                    if(trigger.oldMap.get(forData.Id).Edo_Cta_Ejecutado__c == false && forData.Edo_Cta_Ejecutado__c == true && forData.OportunidadTexto__c != '') {
                    	//if(!system.isFuture() && !system.isBatch()) {
                    	//if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()) {
                        	EnviarPorCorreoMasivo.SavePdf(forData.OportunidadTexto__c);
                    	//}
                    }
                }
            }           
        }       
    }
}