/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Enkontrol_Hdr
* @Autor       Joel Soto
* @Date        09/12/2019
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
public class Enkontrol_Hdr {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResult> creacionProveedoresHandler(List<Enkontrol_Ws.Proveedor> listProveedorWs) {
    return Enkontrol_Cls.creacionProveedoresService(listProveedorWs);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResult> actualizacionProveedoresHandler(List<Enkontrol_Ws.Proveedor> listProveedorWs) {
    return Enkontrol_Cls.actualizacionProveedoresService(listProveedorWs);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResult> creacionDeficienciasHandler(List<Enkontrol_Ws.Caso> listCaseWs) {
    return Enkontrol_Cls.creacionCasosService(listCaseWs);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResult> actualizacionProspectosHandler(List<Enkontrol_Ws.Prospecto> listLeadWs) {
    return Enkontrol_Cls.actualizacionProspectosService(listLeadWs);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResult> creacionOportunidadesHandler(List<Enkontrol_Ws.Oportunidad> listOppWs) {
    return Enkontrol_Cls.creacionOportunidadesService(listOppWs);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResult> actualizacionOportunidadesHandler(List<Enkontrol_Ws.Oportunidad> listOppWs) {
    return Enkontrol_Cls.actualizacionOportunidadesService(listOppWs);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Enkontrol_Ws.SaveResultEstatus> actualizarEstatusDeptoHandler(List<Enkontrol_Ws.ActualizarEstatusDepto> listDeptoWs) {
    return Enkontrol_Cls.actualizarEstatusDeptoService(listDeptoWs);
  }
}