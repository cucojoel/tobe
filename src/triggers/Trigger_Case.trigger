/**
 * Created by scarlettcastillo on 8/31/18.
 */

trigger Trigger_Case on Case (before update) {

    List<Case> casosAMandar = new List<Case>();

    if(Trigger.isUpdate){
        for(Case aCase : Trigger.new){
            if(aCase.Status == 'Resuelto' && aCase.Cierre_Sincronizado_En_Enkontrol__c == false){
                casosAMandar.add(aCase);
            }
        }

        if(!casosAMandar.isEmpty()){
            Handler_CaseTrigger.mandarCasosCerradosAEnkontrol(casosAMandar);
        }
    }
}