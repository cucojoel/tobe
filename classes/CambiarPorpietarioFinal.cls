public with sharing class CambiarPorpietarioFinal {
	
	@AuraEnabled
	public static void asignarPropietario( ){
		
		
		List<Lead> lstLeadsNuevos =[SELECT Id, OwnerId,Agente__c,Propietario_asignado__c FROM Lead  WHERE Propietario_asignado__c != null ];
		
		System.debug('lstLeadsNuevos; '+lstLeadsNuevos);
		
		List<Lead> lstLeadsNuevosUpdate = new List<Lead>();
		
		for( Lead leadNuevo : lstLeadsNuevos ){
			if(leadNuevo.Propietario_asignado__c != leadNuevo.OwnerId ){
				leadNuevo.Agente__c		= leadNuevo.Propietario_asignado__c;
				leadNuevo.OwnerId		= leadNuevo.Propietario_asignado__c;
				lstLeadsNuevosUpdate.add(leadNuevo);
			}
		}
		
		System.debug('lstLeadsNuevos; '+lstLeadsNuevosUpdate);
		
		if(lstLeadsNuevosUpdate != null && lstLeadsNuevosUpdate.size() > 0){
			update lstLeadsNuevosUpdate;	
		}
	}

}