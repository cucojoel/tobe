/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Bg_DropCaso_Ctr
* @Autor       Joel Soto
* @Date        28/11/2019
*
* @Group       BeGrand
* @Description aura controller para mostrar componente
* @Changes
*/
public with sharing class Bg_DropCaso_Ctr {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description sincroniza el checklisturl del registro actual
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  @AuraEnabled
  public static void syncDrop(Id recordId) {
    List<Case> listCase = [SELECT Id, ChecklistUrl__c FROM Case WHERE Id =: recordId limit 1];
    Set<String> recordIds = new Set<String> {listCase.get(0).Id};
    Service_WS_EnkontrolIntegration_Bis.processRecords(recordIds);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description obtiene perfil para saber si tiene permiso de usar el componente
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  @AuraEnabled
  public static Boolean getProfile() {
    Boolean isVisible = false;
    String profileId = String.valueof(userinfo.getProfileId());
    Profile pro = [SELECT Id, Name FROM Profile WHERE Id = :profileId];
    Integer botonCaso = [SELECT Count() FROM Boton_caso__c where SetupOwnerId = :profileId limit 1];
    if (botonCaso > 0 ) {
      isVisible = true;
    }
    return isVisible;
  }
}