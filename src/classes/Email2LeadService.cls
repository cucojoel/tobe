/*
* Created by scarlettcastillo on 9/19/18.
*/
global class Email2LeadService implements Messaging.InboundEmailHandler{
  public Map<String,String> mapDesarrollos {get;set;}
  /*
  *
  */
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
    String emailTextBody;
    if (String.isNotBlank(email.plainTextBody)){
      emailTextBody = email.plainTextBody.stripHtmlTags();
    }
    else {
      emailTextBody = email.htmlBody.stripHtmlTags();
    }
    emailTextBody = emailTextBody.replaceAll(':',' ');
    emailTextBody = emailTextBody.replaceAll('\\+',' ');
    emailTextBody = emailTextBody.replaceAll(',',' ');
    emailTextBody = emailTextBody.replaceAll('\\*',' ');
    emailTextBody = emailTextBody.replaceAll('®',' ');
    emailTextBody = emailTextBody.replaceAll(' ',' ');
    emailTextBody = emailTextBody.replaceAll('\\n',' ');
    emailTextBody = emailTextBody.replaceAll('�',' ');
    emailTextBody = emailTextBody.replaceAll('\\s+',' ');
    emailTextBody = emailTextBody.toLowerCase();
    emailTextBody = emailTextBody.trim();
    mapDesarrollos = new Map<String,String>();
    mapDesarrollos.put('alto pedregal','AltoPedregal');
    mapDesarrollos.put('reforma','Reforma');
    mapDesarrollos.put('alto polanco','AltoPolanco');
    mapDesarrollos.put('coapa','Coapa');
    mapDesarrollos.put('contadero','Contadero');
    mapDesarrollos.put('park bosques','ParkBosques');
    mapDesarrollos.put('san angel','SanAngel');
    mapDesarrollos.put('coyoacan','Coyoacan');
    Lead insLead = new Lead();
    if(emailTextBody.contains('inmuebles24')){
      insLead = inmuebles24(emailTextBody);
    }
    if(emailTextBody.contains('icasas')){
      insLead = iCasas(emailTextBody);
    }
    if(emailTextBody.contains('lamudi')){
      insLead = laMudi(emailTextBody);
    }
    if(emailTextBody.contains('mercadolibre') || emailTextBody.contains('mercado libre') ||emailTextBody.contains('ir a la cotización') || emailTextBody.contains('metroscúbicos')){
      insLead = mercadoLibre(emailTextBody);
    }
    if(emailTextBody.contains('nuevo registro de contacto')){
      insLead = programatics(emailTextBody);
    }

    insert insLead;
    return null;
  }
  /*
  *
  */
  public Lead programatics(String emailTextBody){
    String algo = 'nuevo registro de contacto nombre rosalba correo tapiarosy@hotmail.com teléfono 5519542688 ¿cómo te enteraste de nosotros? descubre be grand propiedad alto polanco fecha / hora 2019/03/28 - 11 00 be grand derechos reservados 2018';

        String firstName  = 'portal';
    String lastName = '[No proporcionado]';
    String nombreApellido = emailTextBody.substringBetween('nombre ', 'correo');
    if(String.isNotBlank(nombreApellido) ){
      String[] nombreApellidoSplit = nombreApellido.split(' ',2);
      firstName = nombreApellidoSplit[0];
      if(String.isNotBlank(nombreApellidoSplit[1]) ){
        lastName = nombreApellidoSplit[1];
      }
    }
    String emailLead = emailTextBody.substringBetween('correo ', ' teléfono');
    String phoneLead = emailTextBody.substringBetween('teléfono ', ' ¿');
    String cleanPhoneLead = '';
    if(String.isNotEmpty(phoneLead)){
      phoneLead = phoneLead.trim();
      phoneLead = phoneLead.replaceAll('\\ ','');
      phoneLead = '00000'+phoneLead;
      cleanPhoneLead = phoneLead.right(10);
    }
    String desarrolloWeb;
    for (String desKey:mapDesarrollos.keySet()){
      if(emailTextBody.contains(desKey)){
        desarrolloWeb = mapDesarrollos.get(desKey);
      }
    }

    Lead unLead = new Lead();
    unLead.FirstName = firstName.capitalize();
    unLead.LastName = lastName.capitalize();
    unLead.Email = emailLead;
    unLead.Phone = cleanPhoneLead;
    unLead.LeadSource = '6';
    unLead.Medio__c = 'Programmatics';
    unLead.Origen_Prospeccion__c = 'Internet';
    unLead.Origen_Medio__c = 'Programmatics';
    unLead.Desarrollo_Web__c = desarrolloWeb;
    unLead.Email2lead__c = emailTextBody;
    unLead.Web_to_lead__c = true;
    return unLead;
  }
  /*
  *
  */
  public Lead inmuebles24(String emailTextBody){
    String firstName  = 'portal';
    String lastName = '[No proporcionado]';
    String nombreApellido = emailTextBody.substringBetween('datos del interesado ', ' tel').trim();
    if(String.isNotBlank(nombreApellido) ){
      String[] nombreApellidoSplit = nombreApellido.split(' ',2);
      if(nombreApellidoSplit.size() > 1){
      	    firstName = nombreApellidoSplit[0];
          if(String.isNotBlank(nombreApellidoSplit[1]) ){
            lastName = nombreApellidoSplit[1];
          }  
      }else{
          firstName = nombreApellidoSplit[0];
      }
      
    }
    String emailLead = emailTextBody.substringBetween('mail ', ' el mensaje');
    String phoneLead = emailTextBody.substringBetween('tel. ', ' e-mail');
    String cleanPhoneLead = '';
    if(String.isNotEmpty(phoneLead)){
      phoneLead = phoneLead.trim();
      phoneLead = phoneLead.replaceAll('\\ ','');
      phoneLead = '00000'+phoneLead;
      cleanPhoneLead = phoneLead.right(10);
    }
    String desarrolloWeb;
    for (String desKey:mapDesarrollos.keySet()){
      if(emailTextBody.contains(desKey)){
        desarrolloWeb = mapDesarrollos.get(desKey);
      }
    }
    Lead unLead = new Lead();
    unLead.FirstName = firstName.capitalize();
    unLead.LastName = lastName.capitalize();
    unLead.Email = emailLead;
    unLead.Phone = cleanPhoneLead;
    unLead.LeadSource = '12';
    unLead.Medio__c = 'inmuebles 24';
    unLead.Desarrollo_Web__c = desarrolloWeb;
    unLead.Email2lead__c = emailTextBody;
    unLead.Web_to_lead__c = true;
    return unLead;
  }
  /*
  *
  */
  public Lead iCasas(String emailTextBody){
    String firstName  = 'portal';
    String lastName = '[No proporcionado]';
    String nombreApellido = emailTextBody.substringBetween('nombre y apellidos ', ' teléfono contacto');
    if(String.isNotBlank(nombreApellido) ){
      String[] nombreApellidoSplit = nombreApellido.split(' ', 2);
      if(nombreApellidoSplit.size() > 1){
      	    firstName = nombreApellidoSplit[0];
          if(String.isNotBlank(nombreApellidoSplit[1]) ){
            lastName = nombreApellidoSplit[1];
          }  
      }else{
          firstName = nombreApellidoSplit[0];
      }
    }
    String email = emailTextBody.substringBetween('email ', ' observaciones');
    String phoneLead = emailTextBody.substringBetween('teléfono contacto ', ' email');
    String cleanPhoneLead = '';
    if(String.isNotEmpty(phoneLead)){
      phoneLead = phoneLead.trim();
      phoneLead = phoneLead.replaceAll('\\ ','');
      phoneLead = '00000'+phoneLead;
      cleanPhoneLead = phoneLead.right(10);
    }
    String desarrolloWeb;
    for (String desKey:mapDesarrollos.keySet()){
      if(emailTextBody.contains(desKey)){
        desarrolloWeb = mapDesarrollos.get(desKey);
      }
    }
    Lead unLead = new Lead();
    unLead.FirstName = firstName.capitalize();
    unLead.LastName = lastName.capitalize();
    unLead.Email = email;
    unLead.Phone = cleanPhoneLead;
    unLead.LeadSource = '12';
    unLead.Medio__c = 'ICasas';
    unLead.Desarrollo_Web__c = desarrolloWeb;
    unLead.Email2lead__c = emailTextBody;
    unLead.Web_to_lead__c = true;
    return unLead;
  }
  /*
  *
  */
  public Lead laMudi(String emailTextBody){
    String firstName  = 'portal';
    String lastName = '[No proporcionado]';
    String nombreApellido = emailTextBody.substringBetween('nombre ', ' correo');
    if(String.isNotBlank(nombreApellido) ){
      String[] nombreApellidoSplit = nombreApellido.split(' ', 2);
      if(nombreApellidoSplit.size() > 1){
      	    firstName = nombreApellidoSplit[0];
          if(String.isNotBlank(nombreApellidoSplit[1]) ){
            lastName = nombreApellidoSplit[1];
          }  
      }else{
          firstName = nombreApellidoSplit[0];
      }
    }
    String emailLead = emailTextBody.substringBetween('correo electrónico ', ' teléfono');
    String phoneLead = emailTextBody.substringBetween('teléfono ', ' detalles');
    String cleanPhoneLead = '';
    if(String.isNotEmpty(phoneLead)){
      phoneLead = phoneLead.trim();
      phoneLead = phoneLead.replaceAll('\\ ','');
      phoneLead = '00000'+phoneLead;
      cleanPhoneLead = phoneLead.right(10);
    }
    String desarrolloWeb;
    for (String desKey:mapDesarrollos.keySet()){
      if(emailTextBody.contains(desKey)){
        desarrolloWeb = mapDesarrollos.get(desKey);
      }
    }
    Lead unLead = new Lead();
    unLead.FirstName = firstName.capitalize();
    unLead.LastName = lastName.capitalize();
    unLead.Email = emailLead;
    unLead.Phone = cleanPhoneLead;
    unLead.LeadSource = '12';
    unLead.Medio__c = 'Lamudi';
    unLead.Desarrollo_Web__c = desarrolloWeb;
    unLead.Email2lead__c = emailTextBody;
    unLead.Web_to_lead__c = true;
    return unLead;
  }
  /*
  *
  */
  public Lead mercadoLibre(String emailTextBody){
    String firstName  = 'portal';
    String lastName = '[No proporcionado]';
    String nombreApellido = emailTextBody.substringBetween('', ' cotizó');
    if(String.isNotBlank(nombreApellido) ){
      String[] nombreApellidoSplit = nombreApellido.split(' ',2);
      if(nombreApellidoSplit.size() > 1){
      	    firstName = nombreApellidoSplit[0];
          if(String.isNotBlank(nombreApellidoSplit[1]) ){
            lastName = nombreApellidoSplit[1];
          }  
      }else{
          firstName = nombreApellidoSplit[0];
      }
    }
    String emailLead = emailTextBody.substringBetween('contacto son ', ' y');
    String phoneLead = emailTextBody.substringBetween(' y ', '.');
    String cleanPhoneLead = '';
    if(String.isNotEmpty(phoneLead)){
      phoneLead = phoneLead.trim();
      phoneLead = phoneLead.replaceAll('\\ ','');
      phoneLead = '00000'+phoneLead;
      cleanPhoneLead = phoneLead.right(10);
    }
    String desarrolloWeb;
    for (String desKey:mapDesarrollos.keySet()){
      if(emailTextBody.contains(desKey)){
        desarrolloWeb = mapDesarrollos.get(desKey);
      }
    }
    Lead unLead = new Lead();
    unLead.FirstName = firstName.capitalize();
    unLead.LastName = lastName.capitalize();
    unLead.Email = emailLead;
    unLead.Phone = cleanPhoneLead;
    unLead.LeadSource = '12';
    unLead.Medio__c = 'Mercado Libre';
    unLead.Desarrollo_Web__c = desarrolloWeb;
    unLead.Email2lead__c = emailTextBody;
    unLead.Web_to_lead__c = true;
    return unLead;
  }
}