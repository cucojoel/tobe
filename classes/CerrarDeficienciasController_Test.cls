@IsTest
public class CerrarDeficienciasController_Test {
    
    static testMethod void wsService_1() {
        
        Account cuenta = new Account();
        cuenta.LastName = 'Melquides';
        cuenta.FirstName = 'Marco';
        cuenta.PersonEmail = 'test@gmail.com';
        cuenta.RFC__pc = 'CASJ811111RD9';
        insert cuenta;

        Account cliente = Test_Utilities.crearClientePrueba();

        
        Contact contacto = [SELECT Id FROM Contact WHERE AccountId =: cuenta.Id LIMIT 1];
        
        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        String idEnkontrolDesarrollo = 'AP12';
        Desarrollo__c desarrollo = new Desarrollo__c();
        desarrollo.Name = 'Alto Polanco';
        desarrollo.Id_Desarrollo_Enkontrol__c = idEnkontrolDesarrollo;
        desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
        desarrollo.Inicio__c = myTime;
        insert desarrollo;
        Etapa__c unaEtapa = new Etapa__c();
        unaEtapa.Name = 'Torre 2';
        unaEtapa.Id_Etapa_Enkontrol__c = '203';
        unaEtapa.No_de_Pisos__c = 23;
        unaEtapa.Desarrollo__c = desarrollo.Id;
        insert unaEtapa;
        
        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba('102', desarrolloComercial.Id, '1', 'AP12');
        unInmueble.Nivel__c = '11';
        update unInmueble;
        
        Carta_Oferta__c cEntrega = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);
        Case nCase = Test_Utilities.crearCaso(cEntrega.Id, cliente.Id);
        
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(nCase.Id);
        detalle.Id_Falla_WS__c = '12345AP12';
        update detalle;
        
                Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

                /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);
        
        test.startTest();
        PageReference pageRef = Page.CerrarDeficiencias;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',nCase.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController( nCase );
        CerrarDeficienciasController upcontroller = new CerrarDeficienciasController(sc);      
        System.assertNotEquals(null,upcontroller.updateRecords());
        test.stopTest();
    }
}