@isTest
public class TEST_ClassCrearBitacoraController {
    
    @isTest
    public static void init(){
        
        Account cliente = new Account();
        cliente.FirstName = 'cliente';
        cliente.LastName = 'cliente';
        cliente.F_CM__c = Date.today().addDays(1);
        insert cliente;
        
        Opportunity oportunidad = new Opportunity();
        oportunidad.Name = 'Oportunidad Prueba';
        oportunidad.StageName = 'Nueva';
        oportunidad.CloseDate = Date.today().addDays(1);
        oportunidad.AccountId = cliente.Id;
        oportunidad.ID_Cliente_Enkontrol__c = '1234';
        insert oportunidad;
        
        id idOportunidad = Schema.SObjectType.Bitacora__c.getRecordTypeInfosByName().get('Oportunidad').getRecordTypeId();
        Bitacora__c bitacoraDummy = new Bitacora__c();
        bitacoraDummy.Forma_de_Contacto__c = 'Correo';
        bitacoraDummy.Forma_de_Contacto__c = 'Correo';
        bitacoraDummy.Contesta__c = 'No';
        bitacoraDummy.Resultado__c = null;
        bitacoraDummy.Oportunidad__c = oportunidad.Id;
        bitacoraDummy.Fecha_futura_de_contacto__c = DateTime.now().addDays(1);
        bitacoraDummy.RecordTypeId = idOportunidad;
        insert bitacoraDummy;
    
        CLASS_CrearBitacoraController.identificaTipo(oportunidad.Id);
        CLASS_CrearBitacoraController.getResultados('Correo');
        CLASS_CrearBitacoraController.getEstatusNoAgendaCita('Correo');    
    }
}