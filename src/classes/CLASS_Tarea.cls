public with sharing class CLASS_Tarea {

	//Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'Confirmada' para crear un evento al asesor de ventas, 
	//cambiar el propietario del prospecto asociado a la cita y el estatus de este
	public void cambioEstatusCitaConfirmada(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales){
	
		Set<Id> idsProspectos = new Set<Id>();
		for(Task tareaActual : tareasActuales.values()){
			idsProspectos.add(tareaActual.WhoId);
		}
		
		Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Status, Estatus_Cita__c, OwnerId, Agente__c FROM Lead WHERE Id IN : idsProspectos]);
		
		Lead prospecto = new Lead();
		List<Lead> prospectosActualizar = new List<Lead>();
		for(Task tareaActual : tareasActuales.values()){
			
            Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
            
            if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'Confirmada'){
            	
            	Event evento = new Event();
				evento.OwnerId = tareaActual.OwnerId;
				evento.Desarrollo_Comercial__c = tareaActual.Desarrollo_Comercial__c;
				evento.WhoId = tareaActual.WhoId;
				evento.StartDateTime = tareaActual.Fecha_Cita__c;
				evento.EndDateTime = tareaActual.Fecha_Cita__c.addHours(1);
				evento.ActivityDateTime = tareaActual.Fecha_Cita__c;
				evento.Subject = 'Cita Confirmada';

				try{
					insert evento;
				}
				catch(Exception e){
					System.debug('*****Error al crear el evento:' + e.getMessage());
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    	ApexPages.addMessage(mensaje);
				}
				System.debug('*****Cita (evento) creada:' + evento);            	
            	
            	prospecto = prospectosCitas.get(tareaActual.WhoId);
            	prospecto.Estatus_Prospecto_BDC__c = 'Confirmado';
            	prospecto.Estatus_Cita__c = 'Confirmada';
            	prospecto.OwnerId = tareaActual.OwnerId; 
            	prospectosActualizar.add(prospecto);
            }
		}
		
		update prospectosActualizar;
	}
	
	//Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'No Confirmada' para cambiar el estatus del prospecto
	public void cambioEstatusCitaNoConfirmada(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales){
	
		Set<Id> idsProspectos = new Set<Id>();
		for(Task tareaActual : tareasActuales.values()){
			idsProspectos.add(tareaActual.WhoId);
		}
		
		Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Status, Estatus_Cita__c, OwnerId, Agente__c FROM Lead WHERE Id IN : idsProspectos]);
		
		Lead prospecto = new Lead();
		List<Lead> prospectosActualizar = new List<Lead>();
		for(Task tareaActual : tareasActuales.values()){
			
            Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
            
            if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'No Confirmada'){           	
            	
            	prospecto = prospectosCitas.get(tareaActual.WhoId);
            	prospecto.Estatus_Cita__c = 'Cancelada';
            	prospectosActualizar.add(prospecto);
            }
		}
		
		update prospectosActualizar;
	}
	
	//Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'No Visito' para cambiar el propietario del prospecto asociado a la cita
	public void cambioEstatusCitaNoAsistio(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales){
		
		Set<Id> idsProspectos = new Set<Id>();
		for(Task tareaActual : tareasActuales.values()){
			idsProspectos.add(tareaActual.WhoId);
		}
		
		Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, OwnerId, Agente__c, Cantidad_No_Shows__c, Status FROM Lead WHERE Id IN : idsProspectos]);
		
		Lead prospecto = new Lead();
		List<Lead> prospectosActualizar = new List<Lead>();
		for(Task tareaActual : tareasActuales.values()){
			
            Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
            
            if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'No Visitó'){
            	prospecto = prospectosCitas.get(tareaActual.WhoId);
            	prospecto.OwnerId = prospecto.Agente__c;
            	prospecto.Cantidad_No_Shows__c = (String.isBlank(String.valueOf(prospecto.Cantidad_No_Shows__c)) == true ? 0 : Integer.valueOf(prospecto.Cantidad_No_Shows__c)) + 1;
            	
            	if(prospecto.Cantidad_No_Shows__c >= 4){
            		prospecto.Estatus_Prospecto_BDC__c = 'No Contactar';
            	}
            	else {
            		prospecto.Estatus_Prospecto_BDC__c = 'Rescate';
            	}
            	
            	prospectosActualizar.add(prospecto);
            }
		}
		
		update prospectosActualizar;
	}
}