/**
* Created by scarlettcastillo on 8/31/18.  Borrar esta clase
*/

public with sharing class Handler_CaseTrigger {
    /*
    public static void mandarCasosCerradosAEnkontrol(List<Case> casos){

        QueueableService_CaseEnkontrol caseJob = new QueueableService_CaseEnkontrol(casos);

        if(Test.isRunningTest()){
            ID jobID = System.enqueueJob(caseJob);
        }
    }

    public static void crearProspectoWeb(List<Case> casos){

        List<Lead> lstLeads = new List<Lead>();

        Id rtAsignacion = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(Label.RTAsignado).getRecordTypeId();

        for( Case caso: casos ){

            Lead prospecto = new Lead();
            prospecto.FirstName     = caso.NombreLiveAgent__c ;
            prospecto.LastName      = caso.ApellidoLiveAgent__c ;
            prospecto.Email         = caso.EmailLiveAgent__c ;
            prospecto.Phone         = caso.TelefonoLiveAgent__c ;
            prospecto.LeadSource    = Label.LeadSource;
            prospecto.Medio__c      = Label.MedioChat;
            prospecto.Formulario__c = Label.FormularioOffline;
            prospecto.Status        = Label.StatusAsignado;
            prospecto.RecordTypeId  = rtAsignacion;

            if( caso.DesarrolloLiveAgent__c == Label.DesarrolloAltoPolanco ){
                prospecto.Desarrollo_Web__c			=	Label.DesarrolloWebAltoPolanco;
                prospecto.Desarrollo_P__c			=	Label.DesarrolloAltoPolanco.toUpperCase() ;
                prospecto.Compania__c				=	Label.CompaniaIdAPO;
                prospecto.Desarrollo_Comercial__c	=	Label.DesarrolloComerciaIdAPO;
                prospecto.Desarrollo__c				=	Label.DesarrolloIdAPO;
                lstLeads.add( prospecto );

            }else if(caso.DesarrolloLiveAgent__c == Label.DesarrolloAltoPedregal){
                prospecto.Desarrollo_Web__c			=	Label.DesarrolloWebAltoPedregal;
                prospecto.Desarrollo_P__c			=	Label.DesarrolloAltoPedregal.toUpperCase() ;
                prospecto.Compania__c				=	Label.CompaniaIdAPE;
                prospecto.Desarrollo_Comercial__c	=	Label.DesarrolloComerciaIdAPE;
                prospecto.Desarrollo__c				=	Label.DesarrolloIdAPE;
                lstLeads.add( prospecto );

            }else if(caso.DesarrolloLiveAgent__c == Label.DesarrolloContadero){
                prospecto.Desarrollo_Web__c			=	Label.DesarrolloContadero;
                prospecto.Desarrollo_P__c			=	Label.DesarrolloContadero.toUpperCase() ;
                prospecto.Compania__c				=	Label.CompaniaIdCON;
                prospecto.Desarrollo_Comercial__c	=	Label.DesarrolloComerciaIdCON;
                prospecto.Desarrollo__c				=	Label.DesarrolloIdCON;
                lstLeads.add( prospecto );

            }else if(caso.DesarrolloLiveAgent__c == Label.DesarrolloParkBosques){
                prospecto.Desarrollo_Web__c			=	Label.DesarrolloWebParkBosques;
                prospecto.Desarrollo_P__c			=	Label.DesarrolloParkBosques.toUpperCase() ;
                prospecto.Compania__c				=	Label.CompaniaIdPKB;
                prospecto.Desarrollo_Comercial__c	=	Label.DesarrolloComerciaIdPKB;
                prospecto.Desarrollo__c				=	Label.DesarrolloIdPKB;
                lstLeads.add( prospecto );

            }else if(caso.DesarrolloLiveAgent__c == Label.DesarrolloReforma){
                prospecto.Desarrollo_Web__c			=	Label.DesarrolloReforma;
                prospecto.Desarrollo_P__c			=	Label.DesarrolloReforma.toUpperCase() ;
                prospecto.Compania__c				=	Label.CompaniaIdREF;
                prospecto.Desarrollo_Comercial__c	=	Label.DesarrolloComerciaIdREF;
                prospecto.Desarrollo__c				=	Label.DesarrolloIdREF;
                lstLeads.add( prospecto );

            }else if(caso.DesarrolloLiveAgent__c == Label.DesarrolloCoapa){
                prospecto.Desarrollo_Web__c			=	Label.DesarrolloCoapa;
                prospecto.Desarrollo_P__c			=	Label.DesarrolloCoapa.toUpperCase() ;
                prospecto.Compania__c				=	Label.CompaniaIdCoapa;
                prospecto.Desarrollo_Comercial__c	=	Label.DesarrolloComerciaIdCoapa;
                prospecto.Desarrollo__c				=	Label.DesarrolloIdCoapa;
                lstLeads.add( prospecto );
            }
        }

        if( lstLeads != null && lstLeads.size() > 0  ){
            if(!Test.isRunningTest()){
                insert lstLeads;
            }
        }

    }
    */
}