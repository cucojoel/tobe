public without sharing class Handler_EntregaTrigger {

    public static void isAfterUpdate(List<Carta_Oferta__c> TriggerNew, Map<Id,Carta_Oferta__c > oldMap){
        Service_EntregaTrigger.crearCasoDeficiencias(TriggerNew, oldMap);
    }
    
}