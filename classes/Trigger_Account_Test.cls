/*
	Mario Quevedo Diaz
	Metodo de prueba para el trigger - Trigger_Account -
*/

@isTest
public class Trigger_Account_Test {
	static testMethod void test01() {
		Account cliente = new Account();
		cliente.LastName = 'CoberturaCuenta';
		cliente.Edo_Cta_Ejecutado__c = false;
		insert cliente;
		
		Test.startTest();
			cliente.Edo_Cta_Ejecutado__c = true;
			update cliente;
		Test.stopTest();
	}
}