/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Bg_DropCaso_Test
* @Autor       Joel Soto
* @Date        28/11/2019
*
* @Group       BeGrand
* @Description Clase test para la clase Bg_DropCaso_Ctr
* @Changes
*/

@isTest
public class Bg_DropCaso_Test {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Cobertura del 91%
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  static testMethod void oneTest() {
    Carta_Oferta__c entrega = new Carta_Oferta__c();
    insert entrega;
    Case myCase = new Case();
    myCase.Status = 'Resuelto';
    myCase.Type = 'Obra Supervisión';
    myCase.Entrega__c = entrega.Id;
    insert myCase;
    Bg_DropCaso_Ctr.syncDrop(myCase.Id);
    Bg_DropCaso_Ctr.getProfile();
    System.assertNotEquals(null, entrega);
  }
}