/*
	Para el test de esta clase cambiar las siguientes Etiquetas personalizadas:
		SFDC_Attachment_sizeMayor800	||	select Id           from Attachment where Name like 'ECtaBG%' and BodyLength > 800
		SFDC_Attachment_sizeMenor800	||	select Id           from Attachment where Name like 'ECtaBG%' and BodyLength < 800
		SFFC_ClienteConEnvios			||	select Id, ParentId from Attachment where Name like 'ECtaBG%' and BodyLength > 800
		SFFC_InformePorCampana			||	select Id, Name     from Attachment where Name like 'ECtaBG%' and BodyLength > 800 
	al valor:
		select Id, Name, ParentId from Attachment
	en produccion ya poner los valores originales en cada etiqueta
*/
global class Batch_Schedulable_ReportaPDFBlanco implements Schedulable {
	global void execute(SchedulableContext SC) {
		// Contabilizacion de PDF generados correctamente , incorrectamente
		list<Attachment> PDFs_creadosList = Database.query(system.label.SFDC_Attachment_sizeMayor800);				// select Id from Attachment where Name like 'ECtaBG%' and BodyLength > 800
		list<Attachment> PDFs_enBlancoList = Database.query(system.label.SFDC_Attachment_sizeMenor800);				// select Id from Attachment where Name like 'ECtaBG%' and BodyLength < 800
		list<Inmueble__c> Departamentos_con_cruceList = Database.query(system.label.SFDC_DepartamentosConCruce); 	// select Id from Inmueble__c where OportunidadTexto__c <> null
		
		// Conteos de Clientes y por Compañia
		String Recuento_por_companiaString = '';
		list<String> oppsEdoCta = new list<String>();
		set<String> Clientes_con_envioList = new set<String>();
		map<String, integer> conteos = new map<String, integer>();
		for(Attachment forData : Database.query(system.label.SFFC_ClienteConEnvios)) { 						// select Id, ParentId from Attachment where Name like 'ECtaBG%' and BodyLength > 800 */
			Clientes_con_envioList.add(forData.ParentId);
		}
		for(Attachment forData : Database.query(system.label.SFFC_InformePorCampana)) {						// select Id, Name from Attachment where BodyLength > 800 and Name like 'ECtaBG_%'
			String tmp = forData.Name;
			tmp = tmp.replace('ECtaBG_','');
			tmp = tmp.subString(0,18);
			oppsEdoCta.add(tmp);
		}
		if(!oppsEdoCta.isEmpty()) {
			for(Opportunity forData : Database.query(system.label.SFDC_NumeroCompania)) {					// select Id, Compania__r.Id_Compania_Enkontrol__c from Opportunity where Id IN : oppsEdoCta
				if(!conteos.containsKey(forData.Compania__r.Id_Compania_Enkontrol__c)) {
					conteos.put(forData.Compania__r.Id_Compania_Enkontrol__c, 0);
				}
				integer incremento = conteos.get(forData.Compania__r.Id_Compania_Enkontrol__c);
				conteos.put(forData.Compania__r.Id_Compania_Enkontrol__c, incremento+1);
			}
		}
		for(String forData : conteos.keySet()) {
			Recuento_por_companiaString += 'Cia: ' + forData + ' cantidad: ' + conteos.get(forData) + ' *** ';
		}
		
		// Haciendo el recuento de los generado para reportarlo mas adelante		
		insert new PreparaEstadosCuenta__c(
			Name = 'ReporteEstadosCuenta_' + String.valueOf(system.today()), 
			PDFs_creados__c = PDFs_creadosList.size(),
			PDFs_en_blanco__c = PDFs_enBlancoList.size(),
			Clientes_con_envio__c = Clientes_con_envioList.size(),
			Departamentos_con_cruce__c = Departamentos_con_cruceList.size(),
			Recuento_por_compania__c = Recuento_por_companiaString
		);
		
		// Mata el proceso Batch Batchable
		list<CronTrigger> ct = new list<CronTrigger>();
		ct = [select Id from CronTrigger where CronJobDetail.Name = 'GeneraPDFBatchable'];
		if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
		
		// Genera proceso BatchNuevo para el envio de Estados de Cuenta
		DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
		Batch_Schedulable_EnviaPDF_idCliente bsPDF = new Batch_Schedulable_EnviaPDF_idCliente();
		String job = System.schedule('EnviaPDFCte', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		
		// Se eliminan los PDF en blanco
		Database.delete(PDFs_enBlancoList, false);
	}
}