/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Service_LeadTrigger
* @Autor       Joel Soto
* @Date        09/01/2020
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global class Service_LeadTrigger {
  /** **/
  public Decimal cquer {get; set;}
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void sincronizarProspectos(List<Lead> triggerLeads, Boolean esActualizacion) {
    Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();
    if(!Test.isRunningTest() && configuracionEnkontrol.Activar__c == true) {
      QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(triggerLeads, esActualizacion);
      Id jobID = System.enqueueJob(caseJob);
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void asignarDesarrollo(List<Lead> listLeadsTrigger) {
    Map<String, Desarrollo_Comercial__c> mapDesarrollosComName = new Map<String, Desarrollo_Comercial__c>();
    Map<String, Desarrollo_Comercial__c> mapDesarrollosComId = new Map<String, Desarrollo_Comercial__c>();
    DateTime alertaHoras;
    Time antesde10 = Time.newInstance(10, 0, 0, 0);
    Time despde5 = Time.newInstance(17, 1, 0, 0);
    if(Datetime.now().time() > antesde10 && Datetime.now().time() < despde5) {
      alertaHoras = Datetime.now().addHours(2);
    } else {
      Date myDate = System.today().addDays(1);
      Time myTime = Time.newInstance(11, 0, 0, 0);
      alertaHoras = DateTime.newInstance(myDate, myTime);
    }
    Map<String, Desarrollo__c> mapDesarollosEnk = new Map<String, Desarrollo__c>([SELECT Id, Name, Desarrollo_Comercial__c, Desarrollo_Comercial__r.Name, Desarrollo_Comercial__r.Compania__c FROM Desarrollo__c]);
    List<Desarrollo_Comercial__c> listdesarrolloComercial = [SELECT Id, Name, Compania__c FROM Desarrollo_Comercial__c];
    for(Desarrollo_Comercial__c desarrolloComercial :listdesarrolloComercial) {
      String desarrolloComerciallower = desarrolloComercial.Name.toLowerCase().replaceAll(' ', '').replaceAll('á', 'a').replaceAll('é', 'exp').replaceAll('í', 'idx').replaceAll('ó', 'o').replaceAll('ú', 'u');
      mapDesarrollosComName.put(desarrolloComerciallower, desarrolloComercial);
      mapDesarrollosComId.put(desarrolloComercial.Id, desarrolloComercial);
    }
    for(Lead leadTrigger : listLeadsTrigger) {
      leadTrigger.Alerta_4_horas__c = alertaHoras;
      if(String.isBlank(String.valueOf(leadTrigger.Dia_asignacion__c)) == true ) {
        leadTrigger.Dia_asignacion__c = Date.today();
      }
      if(leadTrigger.Desarrollo_Web__c != null) {
        String desWeblower = leadTrigger.Desarrollo_Web__c.toLowerCase().replaceAll(' ', '').replaceAll('á', 'a').replaceAll('é', 'exp').replaceAll('í', 'idx').replaceAll('ó', 'o').replaceAll('ú', 'u');
        if (desWeblower == 'altopolancoii' || desWeblower == 'altopolancoiii') {
          desWeblower = 'altopolanco';
        }
        leadTrigger.Desarrollo_Comercial__c = mapDesarrollosComName.get(desWeblower).Id;
        leadTrigger.Compania__c = mapDesarrollosComId.get(leadTrigger.Desarrollo_Comercial__c).Compania__c;
      }
      if(leadTrigger.Desarrollo__c != null && mapDesarollosEnk.containsKey(leadTrigger.Desarrollo__c)) {
        Desarrollo__c desaSf = mapDesarollosEnk.get(leadTrigger.Desarrollo__c);
        leadTrigger.Desarrollo_Comercial__c = mapDesarollosEnk.get(leadTrigger.Desarrollo__c).Desarrollo_Comercial__c;
        leadTrigger.Compania__c = mapDesarollosEnk.get(leadTrigger.Desarrollo__c).Desarrollo_Comercial__r.Compania__c;
      }
      if (String.isBlank(leadTrigger.Desarrollo_Comercial__c) && mapDesarollosEnk.containsKey(leadTrigger.Desarrollo__c)) {
        leadTrigger.Desarrollo_Comercial__c = mapDesarollosEnk.get(leadTrigger.Desarrollo__c).Desarrollo_Comercial__c;
      }
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void asignarDesarrolloPrimario(List<Lead> listLeadsTrigger) {
    for(Lead leadTrigger : listLeadsTrigger) {
      leadTrigger.Desarrollo_Comercial_Primario__c = leadTrigger.Desarrollo_Comercial__c;
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void asignarGuardia(List<Lead> listLeadsTrigger) {
    List<Profile> listProfiles = [SELECT Id, Name FROM Profile];
    Map<String, Profile> mapProfilesbyName = new Map<String, Profile>();
    for (Profile pro : listProfiles) {
      mapProfilesbyName.put(pro.Name.toLowerCase().replaceAll(' ', ''), pro);
    }
    Profile profileAsesor = mapProfilesbyName.get('asesordeventas');
    Profile profileAsistente = mapProfilesbyName.get('asistentedeventas');
    Profile profileGerente = mapProfilesbyName.get('gerentedeventas');
    Profile profileRecepcion = mapProfilesbyName.get('recepción');
    Map<Id, Desarrollo_Comercial__c> desarrollosComercialesBDC = new Map<Id, Desarrollo_Comercial__c>([SELECT Id, Name FROM Desarrollo_Comercial__c WHERE Name IN('Alto Pedregal', 'Contadero', 'Park Bosques', 'Reforma', 'Coapa', 'Alto Polanco')]);
    Set<Id> setDesarrolloComIds = new Set<Id>();
    Set<Lead> prospectosAsignarBDC = new Set<Lead>();
    Set<Id> desarrollosComercialesAsignarBDC = new Set<Id>();
    for(Lead leadTrigger : listLeadsTrigger) {
      if(String.isNotBlank(leadTrigger.Desarrollo_Comercial__c) && (leadTrigger.LeadSource == '6' || leadTrigger.LeadSource == '10' || leadTrigger.LeadSource == '12') && (leadTrigger.Medio__c != Label.MedioChat) && String.isBlank(String.valueOf(leadTrigger.Cita_Web__c)) == true) {
        if(desarrollosComercialesBDC.containsKey(leadTrigger.Desarrollo_Comercial__c) == true) {
          prospectosAsignarBDC.add(leadTrigger);
          desarrollosComercialesAsignarBDC.add(leadTrigger.Desarrollo_Comercial__c);
        } else {
          setDesarrolloComIds.add(leadTrigger.Desarrollo_Comercial__c);
        }
      } else {
        if ((UserInfo.getProfileId() == profileAsesor.Id || UserInfo.getProfileId() == profileAsistente.Id || UserInfo.getProfileId() == profileGerente.Id || UserInfo.getProfileId() == profileRecepcion.Id || UserInfo.getProfileId() == profileGerente.Id) && leadTrigger.Status != 'Asignado') {
          leadTrigger.Status.addError('El estatus de un prospecto nuevo no puede ser diferente de asignado.');
        }
      }
    }
    if(prospectosAsignarBDC.size() > 0) {
      Integer horaActual = DateTime.now().hour();
      asignarAgenteBDC(prospectosAsignarBDC, desarrollosComercialesAsignarBDC, horaActual);
    }
    if(setDesarrolloComIds.size() > 0) {
      Map<String, Desarrollo_Comercial__c> mapDesarrolloComIds = new Map<String, Desarrollo_Comercial__c>();
      Integer hour = DateTime.now().hour();
      Integer minute = DateTime.now().minute();
      Time timeNow = Time.newInstance(hour, minute, 0, 0);
      Time timeToCompare = Time.newInstance(19, 0, 0, 0);
      Date diaAsignacion = Date.today();
      if(timeNow > timeToCompare) {
        diaAsignacion = Date.today().addDays(1);
      }
      List<Desarrollo_Comercial__c> listDesarrolloComercial = new List<Desarrollo_Comercial__c>([SELECT Id, Gerente_Ventas__r.Email, Asig_Automatica__c, Gerente_Ventas__c
          FROM Desarrollo_Comercial__c
          WHERE Id IN: setDesarrolloComIds]);
      for(Desarrollo_Comercial__c desacom :listDesarrolloComercial) {
        mapDesarrolloComIds.put(desacom.Id, desacom);
      }
      List<Guardia__c> listGuardias = new List<Guardia__c>([SELECT Id, Name, Nombre_Asesor__c, Roll__c, Fecha_Guardia__c, Contador_temp__c
          FROM Guardia__c
          WHERE Desarrollo_Comercial__c
          IN: listDesarrolloComercial
          AND Fecha_Guardia__c = : diaAsignacion
          ORDER BY Fecha_Guardia__c, Roll__c ASC]);
      for(Lead prospecto : listLeadsTrigger) {
        Desarrollo_Comercial__c desa = new Desarrollo_Comercial__c();
        desa = mapDesarrolloComIds.get(prospecto.Desarrollo_Comercial__c);
        prospecto.Email_gerente__c = desa.Gerente_Ventas__r.Email;
        Id usuarioId = null;
        usuarioId = obtenerUsuarioAsignado(listGuardias);
        prospecto.Dia_asignacion__c = diaAsignacion;
        prospecto.Fecha_Asignacion__c = Datetime.now();
        if(usuarioId != null) {
          prospecto.Agente__c = null;
          prospecto.OwnerId = null;
          if(desa.Asig_Automatica__c == true) {
            prospecto.Agente__c = usuarioId;
            prospecto.OwnerId = usuarioId;
          } else {
            usuarioId = desa.Gerente_Ventas__c;
            prospecto.Agente__c = usuarioId;
            prospecto.OwnerId = usuarioId;
          }
        }
      }
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void asignarAgenteBDC(Set<Lead> prospectos, Set<Id> desarrollosComerciales, Integer horaActual) {
    Id idDesarrolloComercialBDC = [SELECT Id FROM Desarrollo_Comercial__c WHERE Name = 'BDC'].Id;
    List<Guardia__c> guardiasAsesoresDisponibles = new List<Guardia__c>();
    if(horaActual >= 16 && horaActual < 19) {
      guardiasAsesoresDisponibles = [SELECT Id, Nombre_Asesor__c, Roll__c, Desarrollo_Comercial_Primario__c, Desarrollo_Comercial_Secundario__c, Contador_Prospectos__c
        FROM Guardia__c
        WHERE Desarrollo_Comercial__c =: idDesarrolloComercialBDC
        AND( Desarrollo_Comercial_Primario__c IN : desarrollosComerciales OR Desarrollo_Comercial_Secundario__c IN : desarrollosComerciales)
        AND Fecha_Guardia__c =: Date.today()
        AND Roll__c = 2];
    } else if(horaActual >= 0 && horaActual < 16)   {
      guardiasAsesoresDisponibles = [SELECT Id, Nombre_Asesor__c, Roll__c, Desarrollo_Comercial_Primario__c, Desarrollo_Comercial_Secundario__c, Contador_Prospectos__c
        FROM Guardia__c
        WHERE Desarrollo_Comercial__c =: idDesarrolloComercialBDC
        AND(Desarrollo_Comercial_Primario__c IN : desarrollosComerciales OR Desarrollo_Comercial_Secundario__c IN : desarrollosComerciales)
        AND Fecha_Guardia__c =: Date.today()
        ORDER BY Roll__c DESC];
    } else if(DateTime.now().hour() >= 19 && DateTime.now().hour() <= 23 && DateTime.now().minute() <= 59 && DateTime.now().second() <= 59)   {
      guardiasAsesoresDisponibles = [SELECT Id, Nombre_Asesor__c, Roll__c, Desarrollo_Comercial_Primario__c, Desarrollo_Comercial_Secundario__c, Contador_Prospectos__c FROM Guardia__c
        WHERE
        Desarrollo_Comercial__c =: idDesarrolloComercialBDC
        AND( Desarrollo_Comercial_Primario__c IN : desarrollosComerciales
        OR Desarrollo_Comercial_Secundario__c IN : desarrollosComerciales)
        AND Fecha_Guardia__c =: Date.today().addDays(1)
        ORDER BY Roll__c DESC];
    }
    List<GuardiaWrapper> guardiasElegibles = new List<GuardiaWrapper>();
    Guardia__c guardiaActual = new Guardia__c();
    Guardia__c guardiaSiguiente = new Guardia__c();
    Integer contadorProspectosGuardiaActual = 0;
    Integer contadorProspectosGuardiaSiguiente = 0;
    for(Lead p : prospectos) {
      guardiasElegibles = new List<GuardiaWrapper>();
      for(Guardia__c guard : guardiasAsesoresDisponibles) {
        if(p.Desarrollo_Comercial__c == guard.Desarrollo_Comercial_Primario__c || p.Desarrollo_Comercial__c == guard.Desarrollo_Comercial_Secundario__c) {
          guardiasElegibles.add(new GuardiaWrapper(guard));
        }
      }
      guardiasElegibles.sort();
      guardiaActual = new Guardia__c();
      guardiaSiguiente = new Guardia__c();
      contadorProspectosGuardiaActual = 0;
      contadorProspectosGuardiaSiguiente = 0;
      for(Integer idx = 0; idx < guardiasElegibles.size(); idx++) {
        if(idx == 0) {
          guardiaActual = guardiasElegibles.get(idx).oppy;
        }
        if(idx < (guardiasElegibles.size() - 1)) {
          guardiaSiguiente = guardiasElegibles.get(idx + 1).oppy;
          contadorProspectosGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Prospectos__c));
          contadorProspectosGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Prospectos__c));
          if(contadorProspectosGuardiaActual <= contadorProspectosGuardiaSiguiente) {
            guardiaActual = guardiaActual;
          } else {
            guardiaActual = guardiaSiguiente;
          }
        } else {
          break;
        }
      }
      p.OwnerId = guardiaActual.Nombre_Asesor__c;
      p.Agente__c = guardiaActual.Nombre_Asesor__c;
      p.Asignado_BDC__c = true;
      p.Status = 'Asignado';
      guardiaActual.Contador_Prospectos__c = (String.isBlank(String.valueOf(guardiaActual.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Prospectos__c)) + 1;
      update guardiaActual;
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String obtenerUsuarioAsignado(List<Guardia__c> listGuardias) {
    String usuarioId = null;
    Integer menor = 100;
    Set<Id> guardiasSet = new Set<Id>();
    Map<String, Guardia__c> mapGuardiasRoll = new Map<String, Guardia__c>();
    Integer hour = DateTime.now().hour();
    Integer minute = DateTime.now().minute();
    Time timeNow = Time.newInstance(hour, minute, 0, 0);
    Time timeToCompare = Time.newInstance(19, 0, 0, 0);
    Date diaAsignacion = Date.today();
    if(timeNow > timeToCompare) {
      diaAsignacion = Date.today().addDays(1);
    }
    for(Guardia__c guard : listGuardias) {
      mapGuardiasRoll.put(guard.Nombre_Asesor__c, guard);
      guardiasSet.add(guard.Nombre_Asesor__c);
    }
    List<Lead> leadList = new List<Lead>([SELECT Id, OwnerId, Dia_asignacion__c FROM Lead
        WHERE Dia_asignacion__c = : diaAsignacion
        AND OwnerId IN: mapGuardiasRoll.keySet()
        AND LeadSource in('6', '10', '12')
        ORDER by CreatedDate]);//query dentro de funcion proceso for
    if(leadList != null) {
      Map<Id, Integer> mapeo = new Map<Id, Integer>();
      for(Lead leader : leadList) {
        if(mapeo.containsKey(leader.OwnerId)) {
          mapeo.put(leader.OwnerId, mapeo.get(leader.OwnerId) + 1);
        } else {
          mapeo.put(leader.OwnerId, 1);
        }
      }
      for(String idUsuario : guardiasSet) {
        if(!mapeo.containsKey(idUsuario)) {
          mapeo.put(idUsuario, 0);
        }
      }
      List<Id> userIds = new List<Id>();
      if(mapeo != null) {
        userIds.addAll(mapeo.keySet());
        for(Integer idx = 0; idx < userIds.size(); idx++) {
          if(idx == 0) {
            usuarioId = userIds[idx];
            menor = mapeo.get(userIds[idx]);
          } else {
            if(mapeo.get(userIds[idx]) < menor) {
              usuarioId = userIds[idx];
              menor = mapeo.get(userIds[idx]);
            }
          }
        }
      }
    } else {
      for (Guardia__c guardia : listGuardias) {
        if (guardia.Roll__c == 1) {
          usuarioId = guardia.Id;
        } else {
          usuarioId = listGuardias.get(0).Id;
        }
      }
    }
    return usuarioId;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void crearCitaProspectos(Set<Lead> prospectos) {
    Guardia__c guardiaActual = new Guardia__c();
    Guardia__c guardiaSiguiente = new Guardia__c();
    Integer contadorCitasGuardiaActual = 0;
    Integer contadorCitasGuardiaSiguiente = 0;
    List<Task> tareasInsertar = new List<Task>();
    List<Event> eventosInsertar = new List<Event>();
    List<Guardia__c> guardiasActualizar = new List<Guardia__c>();
    for(Lead prospecto : prospectos) {
      if(prospecto.Desarrollo_Comercial__c != null) {
        List<Guardia__c> guardiasAsesoresDisponibles = [SELECT Nombre_Asesor__c, Roll__c, Contador_Citas__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: prospecto.Desarrollo_Comercial__c AND Fecha_Guardia__c =: prospecto.Cita_Web__c.date() ORDER BY Roll__c];//query dentro for
        guardiaActual = new Guardia__c();
        guardiaSiguiente = new Guardia__c();
        contadorCitasGuardiaActual = 0;
        contadorCitasGuardiaSiguiente = 0;
        for(Integer idx = 0; idx < guardiasAsesoresDisponibles.size(); idx++) {
          if(idx == 0) {
            guardiaActual = guardiasAsesoresDisponibles.get(idx);
          }
          if(idx < (guardiasAsesoresDisponibles.size() - 1)) {
            guardiaSiguiente = guardiasAsesoresDisponibles.get(idx + 1);
            contadorCitasGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c));
            contadorCitasGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Citas__c));
            if(contadorCitasGuardiaActual <= contadorCitasGuardiaSiguiente) {
              guardiaActual = guardiaActual;
            } else {
              guardiaActual = guardiaSiguiente;
            }
          } else {
            break;
          }
        }
        Task cita = new Task();
        cita.Subject = 'Cita programada';
        cita.OwnerId = guardiaActual.Nombre_Asesor__c;
        cita.WhoId = prospecto.Id;
        cita.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
        cita.Fecha_Cita__c = prospecto.Cita_Web__c;
        cita.ActivityDate = prospecto.Cita_Web__c.date();
        cita.Estatus_de_Visita__c = 'Pendiente';
        cita.Nombre_Prospecto__c = prospecto.Name;
        tareasInsertar.add(cita);
        Event evento = new Event();
        evento.Subject = 'Cita programada';
        evento.OwnerId = guardiaActual.Nombre_Asesor__c;
        evento.WhoId = prospecto.Id;
        evento.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
        evento.StartDateTime = prospecto.Cita_Web__c;
        evento.EndDateTime = prospecto.Cita_Web__c.addHours(1);
        evento.ActivityDateTime = prospecto.Cita_Web__c;
        evento.IsReminderSet = true;
        evento.ReminderDateTime = prospecto.Cita_Web__c.addDays(-1);
        eventosInsertar.add(evento);
        guardiasActualizar.add(guardiaActual);
      }
    }
    try {
      insert tareasInsertar;
    } catch(Exception exp) {
      System.debug('log- Service_LeadTrigger_cls mapDesarollosEnk: ' + exp);
    }
    try {
      insert eventosInsertar;
    } catch(Exception exp) {
      System.debug('log- Service_LeadTrigger_cls mapDesarollosEnk: ' + exp);
    }
    for(Guardia__c guard : guardiasActualizar) {
      guard.Contador_Citas__c = (String.isBlank(String.valueOf(guard.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guard.Contador_Citas__c)) + 1;
    }
    try {
      update guardiasActualizar;
    } catch(Exception exp) {
      System.debug('log- Service_LeadTrigger_cls mapDesarollosEnk: ' + exp);
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  global class GuardiaWrapper implements Comparable {
    public Guardia__c oppy;
    // Constructor
    public GuardiaWrapper(Guardia__c op) {
      oppy = op;
    }
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
      // Cast argument to OpportunityWrapper
      GuardiaWrapper compareToOppy = (GuardiaWrapper)compareTo;
      // The return value of 0 indicates that both elements are equal.
      Integer returnValue = 0;
      if (oppy.Roll__c > compareToOppy.oppy.Roll__c) {
        // Set return value to a positive value.
        returnValue = 1;
      } else if (oppy.Roll__c < compareToOppy.oppy.Roll__c) {
        // Set return value to a negative value.
        returnValue = -1;
      }
      return returnValue;
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public void createObjectClass() {
    String alias0;
    String alias1;
    String alias2;
    String alias3;
    String alias4;
    String alias5;
    String alias6;
    String alias7;
    String appartment0;
    String appartment1;
    String appartment2;
    String appartment3;
    String appartment4;
    String appartment5;
    String appartment6;
    String appartment7;
    String assignatedToName0;
    String assignatedToName1;
    String assignatedToName2;
    String assignatedToName3;
    String assignatedToName4;
    String assignatedToName5;
    String assignatedToName6;
    String assignatedToName7;
    String bill0;
    String bill1;
    String bill2;
    String bill3;
    String bill4;
    String bill5;
    String bill6;
    String bill7;
    String contractNumber0;
    String contractNumber1;
    String contractNumber2;
    String contractNumber3;
    String contractNumber4;
    String contractNumber5;
    String contractNumber6;
    String contractNumber7;
    String contractTerm0;
    String contractTerm1;
    String contractTerm2;
    String contractTerm3;
    String contractTerm4;
    String contractTerm5;
    String contractTerm6;
    String contractTerm7;
    String description0;
    String description1;
    String description2;
    String description3;
    String description4;
    String description5;
    String description6;
    String description7;
    String development0;
    String development1;
    String development2;
    String development3;
    String development4;
    String development5;
    String development6;
    String development7;
    String divisionName0;
    String divisionName1;
    String divisionName2;
    String divisionName3;
    String divisionName4;
    String divisionName5;
    String divisionName6;
    String divisionName7;
    String errorMessage0;
    String errorMessage1;
    String errorMessage2;
    String errorMessage3;
    String errorMessage4;
    String errorMessage5;
    String errorMessage6;
    String errorMessage7;
    String idDropbox0;
    String idDropbox1;
    String idDropbox2;
    String idDropbox3;
    String idDropbox4;
    String idDropbox5;
    String idDropbox6;
    String idDropbox7;
    String idRequest0;
    String idRequest1;
    String idRequest2;
    String idRequest3;
    String idRequest4;
    String idRequest5;
    String idRequest6;
    String idRequest7;
    String language0;
    String language1;
    String language2;
    String language3;
    String language4;
    String language5;
    String language6;
    String language7;
    String messageFailure0;
    String messageFailure1;
    String messageFailure2;
    String messageFailure3;
    String messageFailure4;
    String messageFailure5;
    String messageFailure6;
    String messageFailure7;
    String messageSuccess0;
    String messageSuccess1;
    String messageSuccess2;
    String messageSuccess3;
    String messageSuccess4;
    String messageSuccess5;
    String messageSuccess6;
    String messageSuccess7;
    String name0;
    String name1;
    String name2;
    String name3;
    String name4;
    String name5;
    String name6;
    String name7;
    String nextStep0;
    String nextStep1;
    String nextStep2;
    String nextStep3;
    String nextStep4;
    String nextStep5;
    String nextStep6;
    String nextStep7;
    String objectId0;
    String objectId1;
    String objectId2;
    String objectId3;
    String objectId4;
    String objectId5;
    String objectId6;
    String objectId7;
    String operation0;
    String operation1;
    String operation2;
    String operation3;
    String operation4;
    String operation5;
    String operation6;
    String operation7;
    String opportunity0;
    String opportunity1;
    String opportunity2;
    String opportunity3;
    String opportunity4;
    String opportunity5;
    String opportunity6;
    String opportunity7;
    String origin0;
    String origin1;
    String origin2;
    String origin3;
    String origin4;
    String origin5;
    String origin6;
    String origin7;
    String ownerName0;
    String ownerName1;
    String ownerName2;
    String ownerName3;
    String ownerName4;
    String ownerName5;
    String ownerName6;
    String ownerName7;
    String prospectAgent0;
    String prospectAgent1;
    String prospectAgent2;
    String prospectAgent3;
    String prospectAgent4;
    String prospectAgent5;
    String prospectAgent6;
    String prospectAgent7;
    String prospectBirthdate0;
    String prospectBirthdate1;
    String prospectBirthdate2;
    String prospectBirthdate3;
    String prospectBirthdate4;
    String prospectBirthdate5;
    String prospectBirthdate6;
    String prospectBirthdate7;
    String prospectCity0;
    String prospectCity1;
    String prospectCity2;
    String prospectCity3;
    String prospectCity4;
    String prospectCity5;
    String prospectCity6;
    String prospectCity7;
    String prospectColony0;
    String prospectColony1;
    String prospectColony2;
    String prospectColony3;
    String prospectColony4;
    String prospectColony5;
    String prospectColony6;
    String prospectColony7;
    String prospectCompany0;
    String prospectCompany1;
    String prospectCompany2;
    String prospectCompany3;
    String prospectCompany4;
    String prospectCompany5;
    String prospectCompany6;
    String prospectCompany7;
    String prospectCountry0;
    String prospectCountry1;
    String prospectCountry2;
    String prospectCountry3;
    String prospectCountry4;
    String prospectCountry5;
    String prospectCountry6;
    String prospectCountry7;
    String prospectDes0;
    String prospectDes1;
    String prospectDes2;
    String prospectDes3;
    String prospectDes4;
    String prospectDes5;
    String prospectDes6;
    String prospectDes7;
    String prospectFirstName0;
    String prospectFirstName1;
    String prospectFirstName2;
    String prospectFirstName3;
    String prospectFirstName4;
    String prospectFirstName5;
    String prospectFirstName6;
    String prospectFirstName7;
    String prospectGender0;
    String prospectGender1;
    String prospectGender2;
    String prospectGender3;
    String prospectGender4;
    String prospectGender5;
    String prospectGender6;
    String prospectGender7;
    String prospectLastName0;
    String prospectLastName1;
    String prospectLastName2;
    String prospectLastName3;
    String prospectLastName4;
    String prospectLastName5;
    String prospectLastName6;
    String prospectLastName7;
    String prospectMoral0;
    String prospectMoral1;
    String prospectMoral2;
    String prospectMoral3;
    String prospectMoral4;
    String prospectMoral5;
    String prospectMoral6;
    String prospectMoral7;
    String prospectNacionality0;
    String prospectNacionality1;
    String prospectNacionality2;
    String prospectNacionality3;
    String prospectNacionality4;
    String prospectNacionality5;
    String prospectNacionality6;
    String prospectNacionality7;
    String prospectName0;
    String prospectName1;
    String prospectName2;
    String prospectName3;
    String prospectName4;
    String prospectName5;
    String prospectName6;
    String prospectName7;
    String prospectPhone0;
    String prospectPhone1;
    String prospectPhone2;
    String prospectPhone3;
    String prospectPhone4;
    String prospectPhone5;
    String prospectPhone6;
    String prospectPhone7;
    String prospectPuntoProspectacion0;
    String prospectPuntoProspectacion1;
    String prospectPuntoProspectacion2;
    String prospectPuntoProspectacion3;
    String prospectPuntoProspectacion4;
    String prospectPuntoProspectacion5;
    String prospectPuntoProspectacion6;
    String prospectPuntoProspectacion7;
    String prospectRFC0;
    String prospectRFC1;
    String prospectRFC2;
    String prospectRFC3;
    String prospectRFC4;
    String prospectRFC5;
    String prospectRFC6;
    String prospectRFC7;
    String prospectState0;
    String prospectState1;
    String prospectState2;
    String prospectState3;
    String prospectState4;
    String prospectState5;
    String prospectState6;
    String prospectState7;
    String prospectStreet0;
    String prospectStreet1;
    String prospectStreet2;
    String prospectStreet3;
    String prospectStreet4;
    String prospectStreet5;
    String prospectStreet6;
    String prospectStreet7;
    String quoteToName0;
    String quoteToName1;
    String quoteToName2;
    String quoteToName3;
    String quoteToName4;
    String quoteToName5;
    String quoteToName6;
    String quoteToName7;
    String reference0;
    String reference1;
    String reference2;
    String reference3;
    String reference4;
    String reference5;
    String reference6;
    String reference7;
    String request0;
    String request1;
    String request2;
    String request3;
    String request4;
    String request5;
    String request6;
    String request7;
    String requestBody0;
    String requestBody1;
    String requestBody2;
    String requestBody3;
    String requestBody4;
    String requestBody5;
    String requestBody6;
    String requestBody7;
    String requestParameter0;
    String requestParameter1;
    String requestParameter2;
    String requestParameter3;
    String requestParameter4;
    String requestParameter5;
    String requestParameter6;
    String requestParameter7;
    String requestResponse0;
    String requestResponse1;
    String requestResponse2;
    String requestResponse3;
    String requestResponse4;
    String requestResponse5;
    String requestResponse6;
    String requestResponse7;
    String signature0;
    String signature1;
    String signature2;
    String signature3;
    String signature4;
    String signature5;
    String signature6;
    String signature7;
    String source0;
    String source1;
    String source2;
    String source3;
    String source4;
    String source5;
    String source6;
    String source7;
    String specialTerms0;
    String specialTerms1;
    String specialTerms2;
    String specialTerms3;
    String specialTerms4;
    String specialTerms5;
    String specialTerms6;
    String specialTerms7;
    String stage0;
    String stage1;
    String stage2;
    String stage3;
    String stage4;
    String stage5;
    String stage6;
    String stage7;
    String subStatus0;
    String subStatus1;
    String subStatus2;
    String subStatus3;
    String subStatus4;
    String subStatus5;
    String subStatus6;
    String subStatus7;
    String successMessage0;
    String successMessage1;
    String successMessage2;
    String successMessage3;
    String successMessage4;
    String successMessage5;
    String successMessage6;
    String successMessage7;
    String title0;
    String title1;
    String title2;
    String title3;
    String title4;
    String title5;
    String title6;
    String title7;
    String typeRequest0;
    String typeRequest1;
    String typeRequest2;
    String typeRequest3;
    String typeRequest4;
    String typeRequest5;
    String typeRequest6;
    String typeRequest7;
    String zone0;
    String zone1;
    String zone2;
    String zone3;
    String zone4;
    String zone5;
    String zone6;
    String zone7;
  }
}