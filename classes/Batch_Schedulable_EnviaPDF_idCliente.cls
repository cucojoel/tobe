global class Batch_Schedulable_EnviaPDF_idCliente implements Schedulable {
	global void execute(SchedulableContext SC) {
		list<SFDC_BG_EdoCta_temp__c> pdfIndividual = Database.query(system.label.Batch_Schedulable_EnviaPDF_idCliente);		// select Id, OportunidadId__c, Cuenta__c from SFDC_BG_EdoCta_temp__c where IsGroupedAccountStatementF__c = false limit 10
		if(!pdfIndividual.isEmpty()) {
			set<Id> accountMarcar = new set<Id>();
			list<Account> accountAMarcar= new list<Account>();
			
			for(SFDC_BG_EdoCta_temp__c forData : Database.query(system.label.Batch_Schedulable_EnviaPDF_idCliente)) {
				EnviarPorCorreoMasivo_idCliente.enviarMail(forData.OportunidadId__c);
				accountMarcar.add(forData.Cuenta__c);
			}
			
			if(!accountMarcar.isEmpty()) {
				for(Account forData : [select Id, Edo_Cta_EjecutadoU__c from Account where Id IN : accountMarcar]) {
					forData.Edo_Cta_EjecutadoU__c = true;
					accountAMarcar.add(forData);								
				}
			}
			Database.update(accountAMarcar, false);				// "false" indica que si falla un registro los restantes continuan actualizandose
			
			// Mata el proceso de PDF en blanco
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'BorrandoPDFBlanco'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Mata el proceso de envio
			list<CronTrigger> ct1 = new list<CronTrigger>();
			ct1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCte1'];
			if(!ct1.isEmpty()) { System.abortJob(ct1[0].Id); }
			
			// Genera proceso BatchNuevo
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_EnviaPDF1_idCliente bsPDF = new Batch_Schedulable_EnviaPDF1_idCliente();
			String job = System.schedule('EnviaPDFCte1', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
		else {
			// Mata el proceso de PDF en blanco
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'BorrandoPDFBlanco'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Mata el proceso de envio
			list<CronTrigger> ct1 = new list<CronTrigger>();
			ct1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCte1'];
			if(!ct1.isEmpty()) { System.abortJob(ct1[0].Id); }
			
			// Genera proceso de EnvioMix
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_EnviaPDF_idClienteMix bsPDF = new Batch_Schedulable_EnviaPDF_idClienteMix();
			String job = System.schedule('EnviaPDFCteX', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
	}
}