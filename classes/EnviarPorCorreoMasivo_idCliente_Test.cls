@isTest
public class EnviarPorCorreoMasivo_idCliente_Test {
/*
	Mario Quevedo Diaz
	Metodo de prueba para la clase - EnviarPorCorreoMasivo_idCliente -
*/
	static testMethod void test03() {
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			PersonEmail = 'a@a.a',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
		
		Opportunity oportunidad = new Opportunity(
			Name = 'CoberturaOportunidad',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		Opportunity oportunidad2 = new Opportunity(
			Name = 'CoberturaOportunidad',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad2;
		
		Attachment adjunto = new Attachment(
			Name = 'ECtaBG_' + oportunidad.Id,
			Body = Blob.valueOf('CoberturaCuerpoPDF'),
			ParentId = cliente.Id
		);
        insert adjunto;
        
        Attachment adjunto2 = new Attachment(
			Name = 'ECtaBG_' + oportunidad.Id + '_x',
			Body = Blob.valueOf('CoberturaCuerpoPDF'),
			ParentId = cliente.Id
		);
        insert adjunto2;
		
		list<SFDC_BG_EdoCta_temp__c> temporalLista = new list<SFDC_BG_EdoCta_temp__c>();
		SFDC_BG_EdoCta_temp__c temporal1 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'BGR',
			InteresMoratorio__c = '0.00',
			Email__c = 'a@a.a'
		);
		temporalLista.add(temporal1);
		
		SFDC_BG_EdoCta_temp__c temporal2 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'PFU',
			InteresMoratorio__c = '10.00',
			Email__c = 'a@a.a'
		);
		temporalLista.add(temporal2);
		insert temporalLista;
        
		Test.startTest();
			EnviarPorCorreoMasivo_idCliente.enviarMail(temporalLista[0].Oportunidad__c);
		Test.stopTest();
	}
}