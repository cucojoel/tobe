/*---------------------------------------------------------------------------------------------------------
Autor: Scarlett Castillo - 08-2018 - CloudCo
Descripción:
Test Class:
---------------------------------------------------------------------------------------------------------*/
public  class Service_DropboxIntegration {
    public static final String DROPBOX_API_ENDPOINT = 'https://api.dropboxapi.com';
    public static final String DROPBOX_CONTENT_ENDPOINT = 'https://content.dropboxapi.com';
    public static final String LIST_FOLDER_ENDPOINT = '/2/files/list_folder';
    public static final String TEMPORARY_URL_ENDPOINT = '/2/files/get_temporary_link';
    public static final String FILE_DOWNLOAD_ENDPOINT = '/2/files/download';
    public static final String FILE_THUMBNAIL_ENDPOINT = '/2/files/get_thumbnail';
    public static final String FILE_UPLOAD_ENDPOINT = '/2/files/upload';
    public static Wrapper_DropboxIntegration.Metadata crearArchivosDeDropbox(Historial_dropbox__c sffile) {

      DocumentoxEsquema__c cf = new DocumentoxEsquema__c();
      try {
         cf = [SELECT Documento__c,Etapa__c,Id,Name,Nivel__c,Obligatorio__c,Tipo_de_persona__c FROM DocumentoxEsquema__c WHERE Documento__c = :sffile.Tipo_de_documento__c];

         } catch (Exception ex){
            System.debug('fallo');
        }
        Configuracion_Dropbox__c dropbox = Configuracion_Dropbox__c.getInstance();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(DROPBOX_CONTENT_ENDPOINT + FILE_UPLOAD_ENDPOINT);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + dropbox.Access_Token__c);
        request.setHeader('Content-Type','application/octet-stream');
        String basePathtest = '/Be_Grand_Mobile/test';
        String pathTest = basePathtest+'/'+sffile.Id_compania__c+'/'+sffile.Desarrollo__c+'/'+sffile.Id_prospecto__c+'/'+sffile.Id_lote__c+'/'+cf.Nivel__c+'/'+sffile.Name;
        Wrapper_DropboxIntegration.DropboxRequest req = new Wrapper_DropboxIntegration.DropboxRequest();
        req.path = pathTest;
        req.mode = 'add';
        req.autorename = true;
        req.mute = false;
        req.strict_conflict = false;
        String jsonReqString = JSON.serialize(req,true);
        request.setHeader('Dropbox-API-Arg', jsonReqString);
        request.setBody('{"data-binary":'+sffile.Url_Salesforce__c+'}');
        //Blob body = Blob.valueOf('Error de carga de datos');
        //request.setBodyAsBlob(body);
        System.debug('request => ' + request);
        HttpResponse response = http.send(request);
        System.debug('Response => ' + response);
        System.debug('Response body => ' + response.getBody());
        String jsonResString = response.getBody();
        if(jsonResString.contains('.tag')) jsonResString = jsonResString.replace('.tag', 'x_tag');
        Wrapper_DropboxIntegration.Metadata elements = new Wrapper_DropboxIntegration.Metadata();
        elements = (Wrapper_DropboxIntegration.Metadata) System.JSON.deserialize(jsonResString, Wrapper_DropboxIntegration.Metadata.class);
        return elements;
    }
    public static Wrapper_DropboxIntegration.ElementList obtenerArchivosDeDropbox(String folderUrl) {
        Configuracion_Dropbox__c dropbox = Configuracion_Dropbox__c.getInstance();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(DROPBOX_API_ENDPOINT + LIST_FOLDER_ENDPOINT);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + dropbox.Access_Token__c);
        request.setHeader('Content-Type','application/json');
        Wrapper_DropboxIntegration.DropboxRequest req = new Wrapper_DropboxIntegration.DropboxRequest();
        req.path = folderUrl;
        String jsonReqString = JSON.serialize(req,true);
        request.setBody(jsonReqString);
        System.debug('request => ' + request);
        HttpResponse response = http.send(request);
        System.debug('Response => ' + response);
        System.debug('Response body => ' + response.getBody());
        String jsonResString = response.getBody();
        if(jsonResString.contains('.tag')) jsonResString = jsonResString.replace('.tag', 'x_tag');
        Wrapper_DropboxIntegration.ElementList elements = (Wrapper_DropboxIntegration.ElementList) System.JSON.deserialize(jsonResString, Wrapper_DropboxIntegration.ElementList.class);
        return elements;
    }
    public static String obtenerLinkTemporalDropbox(String fileURL) {
        Configuracion_Dropbox__c dropbox = Configuracion_Dropbox__c.getInstance();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(DROPBOX_API_ENDPOINT + TEMPORARY_URL_ENDPOINT);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + dropbox.Access_Token__c);
        request.setHeader('Content-Type','application/json');
        Wrapper_DropboxIntegration.DropboxRequest req = new Wrapper_DropboxIntegration.DropboxRequest();
        req.path = fileURL;
        String jsonReqString = JSON.serialize(req,true);
        request.setBody(jsonReqString);
        System.debug('request => ' + request);
        HttpResponse response = http.send(request);
        System.debug('Response => ' + response);
        System.debug('Response body => ' + response.getBody());
        String jsonResString = response.getBody();
        if(jsonResString.contains('.tag')) jsonResString = jsonResString.replace('.tag', 'x_tag');
        Wrapper_DropboxIntegration.DropboxFile file = (Wrapper_DropboxIntegration.DropboxFile) System.JSON.deserialize (jsonResString, Wrapper_DropboxIntegration.DropboxFile.class);
        System.debug('File => ' + file);
        return file.link;
    }
    public static Blob descargarArchivosDropbox(String fileName, String fileURL) {
        Configuracion_Dropbox__c dropbox = Configuracion_Dropbox__c.getInstance();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(DROPBOX_CONTENT_ENDPOINT + FILE_DOWNLOAD_ENDPOINT);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + dropbox.Access_Token__c);
        Wrapper_DropboxIntegration.DropboxRequest req = new Wrapper_DropboxIntegration.DropboxRequest();
        req.path = fileURL;
        String jsonReqString = JSON.serialize(req,true);
        System.debug('jsonReqString: ' + jsonReqString);
        request.setHeader('Dropbox-API-Arg', jsonReqString);
        HttpResponse response = http.send(request);
        Blob fileContent;
        System.debug('La respuesta a descargarArchivosDropbox es: ' + response);
        if (response.getStatus() == 'OK'){
            fileContent = response.getBodyAsBlob();
            System.debug('El blob no es nulo: ' + fileContent);
        }
        else {
            fileContent = null;
            System.debug('El blob es nulo: ' + fileContent);
        }
        return fileContent;
    }
    
     public static Blob descargarArchivosDropboxGarantias(String fileName, String fileURL) {
        DropboxIntegration__c dropbox = DropboxIntegration__c.getInstance();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(DROPBOX_CONTENT_ENDPOINT + FILE_DOWNLOAD_ENDPOINT);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + dropbox.AccessCode__c);
        Wrapper_DropboxIntegration.DropboxRequest req = new Wrapper_DropboxIntegration.DropboxRequest();
        req.path = fileURL;
        String jsonReqString = JSON.serialize(req,true);
        System.debug('jsonReqString: ' + jsonReqString);
        request.setHeader('Dropbox-API-Arg', jsonReqString);
        HttpResponse response = http.send(request);
        Blob fileContent;
        System.debug('La respuesta a descargarArchivosDropbox es: ' + response);
        if (response.getStatus() == 'OK'){
            fileContent = response.getBodyAsBlob();
            System.debug('El blob no es nulo: ' + fileContent);
        }
        else {
            fileContent = null;
            System.debug('El blob es nulo: ' + fileContent);
        }
        return fileContent;
    }
}