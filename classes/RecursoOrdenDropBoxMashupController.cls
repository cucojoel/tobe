global with sharing class RecursoOrdenDropBoxMashupController {

	public RecursoOrdenDropBoxMashupController(ApexPages.StandardController stdController) {

    }

	@RemoteAction
	global static String customSettingInformation(){
		Map<String,DropboxIntegration__c> customSettingRecordMap = new Map<String,DropboxIntegration__c>([SELECT AccessCode__c
		 FROM DropboxIntegration__c where Name='DropBoxIntegration (Perfil)' limit 1]);
		return JSON.serialize(customSettingRecordMap.values());
	}

	@RemoteAction
	global static String getRecursoOrdenResources(String RecursoOrdenId){
		Map<String,Recurso_Orden__c> customSettingRecordMap = new Map<String,Recurso_Orden__c>([SELECT Id,Descripcion__c,Fecha__c,Id_Enkontrol__c,Name,SystemModstamp,URL_Pdf__c 
			FROM Recurso_Orden__c where Name=:RecursoOrdenId ]);
		return JSON.serialize(customSettingRecordMap.values());
	}
}