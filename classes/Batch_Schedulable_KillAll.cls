global class Batch_Schedulable_KillAll implements Schedulable {
	global void execute(SchedulableContext SC) {
		/*
		// Mata el proceso EnviaPDF y EnviaPDF1
		list<CronTrigger> ct1 = new list<CronTrigger>();
		ct1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDF'];
		if(!ct1.isEmpty()) { System.abortJob(ct1[0].Id); }
		
		list<CronTrigger> ct2 = new list<CronTrigger>();
		ct2 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDF1'];
		if(!ct2.isEmpty()) { System.abortJob(ct2[0].Id); }
		
		list<CronTrigger> ct3 = new list<CronTrigger>();
		ct3 = [select Id from CronTrigger where CronJobDetail.Name = 'CalculaHacerPDF_Kill'];
		if(!ct3.isEmpty()) { System.abortJob(ct3[0].Id); }
		*/
		
		// Mata todos los proceso que se usan
		list<CronTrigger> ct1 = new list<CronTrigger>();
		ct1 = [select Id from CronTrigger where CronJobDetail.Name IN ('CalculaHacerPDF_Kill','GeneraPDFSchedulable','GeneraPDFBatchable','BorrandoPDFBlanco','EnviaPDFCte','EnviaPDFCte1','EnviaPDFCteX','EnviaPDFCteX1','EnviaPDF','EnviaPDF1')];
		if(!ct1.isEmpty()) { for(CronTrigger forData : ct1) { System.abortJob(forData.Id); }
		}
	}    
}