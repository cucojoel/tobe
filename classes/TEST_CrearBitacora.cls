@isTest
private class TEST_CrearBitacora {

	private static Desarrollo_Comercial__c desarrolloComercial1;
	private static Desarrollo_Comercial__c desarrolloComercial2;
	private static Desarrollo_Comercial__c desarrolloComercialBDC;
	private static Desarrollo__c desarrollo;
	private static User usuario;
	private static Lead prospecto;
	private static Bitacora__c bitacoraDummy;
	private static Guardia__c guardia1DesarrolloComercial1;
	private static Guardia__c guardia2DesarrolloComercial1;
	private static Guardia__c guardia1DesarrolloComercial2;
	private static Guardia__c guardia2DesarrolloComercial2;
	private static Guardia__c guardia1DesarrolloComercialBDC;
	private static Guardia__c guardia2DesarrolloComercialBDC;
	private static Guardia__c guardia3DesarrolloComercialBDC;
	private static Guardia__c guardia4DesarrolloComercialBDC;
	private static Task tarea;
	private static Event evento;

	private static void init(){

		desarrolloComercial1 = new Desarrollo_Comercial__c();
		desarrolloComercial1.Name = 'Reforma';
		desarrolloComercial1.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercial1.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrolloComercial2 = new Desarrollo_Comercial__c();
		desarrolloComercial2.Name = 'test';
		desarrolloComercial2.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercial2.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrolloComercialBDC = new Desarrollo_Comercial__c();
		desarrolloComercialBDC.Name = 'BDC';
		desarrolloComercialBDC.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercialBDC.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrollo = new Desarrollo__c();
		desarrollo.Name = 'Reforma';
		desarrollo.Inicio__c = Time.newInstance(7,0,0,0);
		desarrollo.Fin__c = Time.newInstance(22,0,0,0);

		usuario = new User();
		usuario.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id;
		usuario.Alias = 'usreje';
		usuario.Country = 'México';
		usuario.Email = 'ejecutivo@email.com';
		usuario.EmailEncodingKey = 'UTF-8';
		usuario.LastName = 'ejecutivo';
		usuario.LanguageLocaleKey = 'es_MX';
		usuario.LocaleSidKey = 'es_MX';
		usuario.TimeZoneSidKey = 'America/Mexico_City';
		usuario.UserName='ejecutivo@email.com';
		usuario.Country = 'Mexico';

		prospecto = new Lead();
		prospecto.FirstName = 'prospecto';
		prospecto.LastName = 'prospecto';
		prospecto.Email = 'prospecto@test.com';
		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Cantidad_No_Shows__c = 0;
		prospecto.LeadSource = '6';
		prospecto.RFC__c = 'CASJ821112UT6';
		prospecto.Phone = '2222334455';

		bitacoraDummy = new Bitacora__c();

		guardia1DesarrolloComercial1 = new Guardia__c();
		guardia1DesarrolloComercial1.Fecha_Guardia__c = Date.today().addDays(1);
		guardia1DesarrolloComercial1.Roll__c = 1;

		guardia2DesarrolloComercial1 = new Guardia__c();
		guardia2DesarrolloComercial1.Fecha_Guardia__c = Date.today().addDays(1);
		guardia2DesarrolloComercial1.Roll__c = 2;

		guardia1DesarrolloComercial2 = new Guardia__c();
		guardia1DesarrolloComercial2.Fecha_Guardia__c = Date.today().addDays(1);
		guardia1DesarrolloComercial2.Roll__c = 1;

		guardia2DesarrolloComercial2 = new Guardia__c();
		guardia2DesarrolloComercial2.Fecha_Guardia__c = Date.today().addDays(1);
		guardia2DesarrolloComercial2.Roll__c = 2;

		guardia1DesarrolloComercialBDC = new Guardia__c();
		guardia1DesarrolloComercialBDC.Fecha_Guardia__c = Date.today();
		guardia1DesarrolloComercialBDC.Roll__c = 1;

		guardia2DesarrolloComercialBDC = new Guardia__c();
		guardia2DesarrolloComercialBDC.Fecha_Guardia__c = Date.today();
		guardia2DesarrolloComercialBDC.Roll__c = 2;

		guardia3DesarrolloComercialBDC = new Guardia__c();
		guardia3DesarrolloComercialBDC.Fecha_Guardia__c = Date.today().addDays(1);
		guardia3DesarrolloComercialBDC.Roll__c = 1;

		guardia4DesarrolloComercialBDC = new Guardia__c();
		guardia4DesarrolloComercialBDC.Fecha_Guardia__c = Date.today().addDays(1);
		guardia4DesarrolloComercialBDC.Roll__c = 2;

		tarea = new Task();
		tarea.Fecha_Cita__c = Date.today().addDays(1);
		tarea.ActivityDate = Date.today().addDays(1);

		evento = new Event();
		evento.StartDateTime = DateTime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day(), 10, 0, 0);
		evento.EndDateTime = DateTime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day(), 11, 0, 0);
		evento.ActivityDate = Date.today().addDays(1);
		evento.DurationInMinutes = 60;
	}

	static testMethod void testGetters(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo_Web__c = null;
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		insert prospecto;

		bitacoraDummy.Forma_de_Contacto__c = 'Correo';

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();
		controller.cliente = new Account();
		controller.bitacoraTieneProspecto = true;
		controller.bitacora = bitacoraDummy;

		controller.getFormasContacto();
		controller.getContestaciones();
		controller.getResultados();
		controller.getOpcionesCrearCita();
		controller.getEstatusNoAgendaCita();
		controller.getGradosInteres();
	}

	//Abarca: prospecto.Estatus_Prospecto_BDC__c == 'Rescate'
	static testMethod void testBuscarHorariosDisponibles1(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		prospecto.Asesor_Primario__c = usuario.Id;
		prospecto.Estatus_Prospecto_BDC__c = 'Rescate';
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();

		controller.buscarHorariosDisponibles();
	}

	//Abarca: else if(prospecto.Estatus_Cita__c == 'Cancelada') y if((desarrolloComercialOriginal != prospecto.Desarrollo_Comercial__c))
	static testMethod void testBuscarHorariosDisponibles2(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		prospecto.Asesor_Primario__c = usuario.Id;
		prospecto.Estatus_Prospecto_BDC__c = '';
		prospecto.Estatus_Cita__c = 'Cancelada';
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();
		controller.desarrolloComercialOriginal = desarrolloComercial2.Id;

		controller.buscarHorariosDisponibles();

		/*prospecto.Estatus_Prospecto_BDC__c = 'Pendiente';
		prospecto.Estatus_Cita__c = 'Cancelada';
		prospecto.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		controller.prospecto = prospecto;
		controller.desarrolloComercialOriginal = desarrolloComercial1.Id;
		controller.buscarHorariosDisponibles();*/
	}

	//Abarca: else if(prospecto.Estatus_Cita__c == 'Cancelada') y if((desarrolloComercialOriginal == prospecto.Desarrollo_Comercial__c))
	static testMethod void testBuscarHorariosDisponibles3(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		prospecto.Asesor_Primario__c = usuario.Id;
		prospecto.Estatus_Prospecto_BDC__c = '';
		prospecto.Estatus_Cita__c = 'Cancelada';
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();
		controller.desarrolloComercialOriginal = desarrolloComercial1.Id;

		controller.buscarHorariosDisponibles();
	}

	//Abarca: else
	static testMethod void testBuscarHorariosDisponibles4(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Secundario__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.Agente__c = usuario.Id;
		prospecto.Asesor_Primario__c = usuario.Id;
		prospecto.Estatus_Prospecto_BDC__c = '';
		prospecto.Estatus_Cita__c = '';
		prospecto.F_CM__c = Date.today().addDays(1);
		insert prospecto;

		tarea.OwnerId = usuario.Id;
		insert tarea;

		evento.OwnerId = usuario.Id;
		insert evento;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();
		controller.desarrolloComercialOriginal = desarrolloComercial1.Id;

		controller.buscarHorariosDisponibles();
	}

	static testMethod void testLimpiarVariables(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo_Web__c = null;
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		insert prospecto;

		bitacoraDummy.Forma_de_Contacto__c = 'Correo';

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();
		controller.bitacora = bitacoraDummy;

		controller.limpiaVariablesFormaContacto();
		controller.limpiaVariablesContesta();
		controller.limpiaVariablesCrearCita();
		controller.limpiaVariablesEstatusNoAgendaCita();
	}

	static testMethod void testValidacionesGuardar(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

    prospecto.Desarrollo_Web__c = null;
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = null;
		prospecto.Agente__c = usuario.Id;
		prospecto.Grado_de_interes__c = null;
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();

		//Abarca: if(errorFormaContacto == true || errorContesta == true)
		bitacoraDummy.Forma_de_Contacto__c = null;
		bitacoraDummy.Contesta__c = null;
		controller.bitacora = bitacoraDummy;
		controller.guardar();

		//Abarca: if(bitacora.Contesta__c == 'No') y if(errorResultado == true)
		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Contesta__c = 'No';
		bitacoraDummy.Resultado__c = null;
		controller.bitacora = bitacoraDummy;
		controller.guardar();

		//Abarca: else if(bitacora.Contesta__c == 'Si') y if(errorCrearCita == true)
		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Contesta__c = 'Si';
		controller.bitacora = bitacoraDummy;
		controller.crearCita = null;
		controller.guardar();

		//Abarca: else if(bitacora.Contesta__c == 'Si') y if(crearCita == 'No') y if(errorEstatusNoCrearCita == true)
		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Contesta__c = 'Si';
		bitacoraDummy.Estatus_No_Agenda_Cita__c = null;
		controller.bitacora = bitacoraDummy;
		controller.crearCita = 'No';
		controller.guardar();

		//Abarca: else if(bitacora.Contesta__c == 'Si') y if(crearCita == 'Si') y else if(errorFechaCita == true || errorHorarioCita == true || errorGradoInteres == true)
		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Contesta__c = 'Si';
		controller.bitacora = bitacoraDummy;
		controller.crearCita = 'Si';
		controller.horarioSeleccionado = null;
		controller.guardar();

		//Abarca: else if(bitacora.Contesta__c == 'Si') y if(crearCita == 'Si') y if(errorCantidadCitasPendientes == true)
		tarea.Subject = 'Cita';
		tarea.WhoId = prospecto.Id;
		tarea.Estatus_de_Visita__c = 'Pendiente';
		tarea.OwnerId = usuario.Id;
		insert tarea;

		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Contesta__c = 'Si';
		controller.bitacora = bitacoraDummy;
		controller.crearCita = 'Si';
		controller.guardar();
	}

	static testMethod void testGuardar1(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

    prospecto.Desarrollo_Web__c = null;
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		prospecto.Grado_de_interes__c = 'Interesado';
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();

		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Resultado__c = 'Contesto';
		bitacoraDummy.Estatus_No_Agenda_Cita__c = 'Derecho ARCO';
		controller.crearCita = 'Si';
		controller.horarioSeleccionado = '9:00';
		controller.desarrolloComercialOriginal = desarrolloComercial2.Id;

		//Abarca: if(bitacora.Contesta__c == 'No')
		bitacoraDummy.Contesta__c = 'No';
		controller.bitacora = bitacoraDummy;
		controller.guardar();
	}

	static testMethod void testGuardar2(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

    prospecto.Desarrollo_Web__c = null;
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		prospecto.Grado_de_interes__c = 'Interesado';
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();

		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Resultado__c = 'Contesto';
		bitacoraDummy.Estatus_No_Agenda_Cita__c = 'Derecho ARCO';
		bitacoraDummy.Fecha_futura_de_contacto__c = Date.today().addDays(1);
		controller.crearCita = 'Si';
		controller.horarioSeleccionado = '9:00';
		controller.desarrolloComercialOriginal = desarrolloComercial2.Id;

		//Abarca: if(bitacora.Contesta__c == 'Si') y if(crearCita == 'No') y if(bitacora.Estatus_No_Agenda_Cita__c == 'No interesado por el momento' || bitacora.Estatus_No_Agenda_Cita__c == 'Interesado a futuro')
		bitacoraDummy.Contesta__c = 'Si';
		bitacoraDummy.Estatus_No_Agenda_Cita__c= 'No interesado por el momento';
		controller.crearCita = 'No';
		controller.bitacora = bitacoraDummy;
		controller.guardar();
	}

	static testMethod void testGuardar3(){

		init();

		insert desarrolloComercial1;
		insert desarrolloComercial2;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial1;

		guardia2DesarrolloComercial1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercial1.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial1;

		guardia1DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia1DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercial2;

		guardia2DesarrolloComercial2.Desarrollo_Comercial__c = desarrolloComercial2.Id;
		guardia2DesarrolloComercial2.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercial2;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

    prospecto.Desarrollo_Web__c = null;
		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		prospecto.Grado_de_interes__c = 'Interesado';
		insert prospecto;

		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearBitacora controller = new CONTROL_CrearBitacora();

		bitacoraDummy.Forma_de_Contacto__c = 'Correo';
		bitacoraDummy.Resultado__c = 'Contesto';
		bitacoraDummy.Estatus_No_Agenda_Cita__c = 'Derecho ARCO';
		bitacoraDummy.Fecha_futura_de_contacto__c = Date.today().addDays(1);
		controller.crearCita = 'Si';
		controller.horarioSeleccionado = '9:00';
		controller.desarrolloComercialOriginal = desarrolloComercial2.Id;

		Set<Id> idsAsesores = new Set<Id>();
		idsAsesores.add(usuario.Id);
		//Abarca: if(bitacora.Contesta__c == 'Si') y if(crearCita == 'Si') y if(bitacora.Estatus_No_Agenda_Cita__c == 'No interesado por el momento' || bitacora.Estatus_No_Agenda_Cita__c == 'Interesado a futuro')
		bitacoraDummy.Contesta__c = 'Si';
		bitacoraDummy.Estatus_No_Agenda_Cita__c= 'No interesado por el momento';
		controller.crearCita = 'Si';
		controller.bitacora = bitacoraDummy;
		controller.idsAsesores = idsAsesores;
		controller.guardar();
	}
}