@isTest
public class LC_SyncFailedControllerTest {
  @isTest static void getLeadTest(){
    /** Creando configuracion personalizada **/
    Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
    /** Creando mocks **/
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    Lead candidato = Test_Utilities.crearLeadPruebaSinInsertar();
    insert candidato;
    Test.startTest();
    Lead candidatoN = LC_SyncFailedController.getLead(candidato.id);
    //System.assertEquals(candidatoN.Id, candidato.Id);
    Test.stopTest();
  }
  @isTest static void sincronizarProspectosTest(){
    /** Creando configuracion personalizada **/
    Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
    /** Creando mocks **/
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    Lead candidato = Test_Utilities.crearLeadPruebaSinInsertar();
    insert candidato;
    Test.startTest();
    LC_SyncFailedController.sincronizarProspectos(candidato.Id);
    Test.stopTest();
  }
  @isTest static void getProfileTest(){
    Test.startTest();
    Profile pro = LC_SyncFailedController.getProfile();
    Test.stopTest();
  }
}