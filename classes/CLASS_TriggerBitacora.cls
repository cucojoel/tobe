public class CLASS_TriggerBitacora {

    public void creaTareasEventos(List<Bitacora__c> lstNew){
        	
        id idProspecto = Schema.SObjectType.Bitacora__c.getRecordTypeInfosByName().get('Prospecto').getRecordTypeId();
        id idOportunidad = Schema.SObjectType.Bitacora__c.getRecordTypeInfosByName().get('Oportunidad').getRecordTypeId();
            
		for(Bitacora__c forData : lstNew) {
			
            if(forData.RecordTypeId == idOportunidad && forData.Fecha_futura_de_contacto__c != null){
                
                Event evento = new Event();
                Task tarea = new Task();
                           
                evento.Subject = 'Contactar el ' + forData.Fecha_futura_de_contacto__c;
                evento.WhatId = forData.Oportunidad__c;
                           
                tarea.Subject = 'Contactar el ' + forData.Fecha_futura_de_contacto__c;
                tarea.WhatId = forData.Oportunidad__c;
                            
                /*}else{
                           
                   	evento.Subject = 'Contactar a Prospecto el ' + forData.Fecha_futura_de_contacto__c;
                    evento.WhoId = forData.Lead__c;
                           
                     tarea.Subject = 'Contactar a Prospecto el ' + forData.Fecha_futura_de_contacto__c;
                     tarea.WhoId = forData.Lead__c;
                     tarea.Nombre_Prospecto__c = forData.Lead_Name__c;
                 }*/
                        
						
				evento.OwnerId = forData.OwnerId;
				evento.Desarrollo_Comercial__c = forData.Desarrollo_Comercial__c;
				evento.StartDateTime = forData.Fecha_futura_de_contacto__c;
				evento.ActivityDateTime = forData.Fecha_futura_de_contacto__c;
				evento.EndDateTime = forData.Fecha_futura_de_contacto__c.addHours(1);
				evento.IsReminderSet = true;
				evento.ReminderDateTime = DateTime.newInstance(forData.Fecha_futura_de_contacto__c.year(),forData.Fecha_futura_de_contacto__c.month(),forData.Fecha_futura_de_contacto__c.day(),forData.Fecha_futura_de_contacto__c.Hour() - 1,forData.Fecha_futura_de_contacto__c.Minute(),0);
				insert evento;
					    
				tarea.OwnerId = forData.OwnerId;
				tarea.Fecha_Cita__c = forData.Fecha_futura_de_contacto__c;
				tarea.ActivityDate = Date.newInstance(forData.Fecha_futura_de_contacto__c.year(), forData.Fecha_futura_de_contacto__c.month(), forData.Fecha_futura_de_contacto__c.day());
				tarea.Desarrollo_Comercial__c = forData.Desarrollo_Comercial__c;
				insert tarea;						
			}														
		}
    }
    
}