@isTest
public class Test_InmuebleTrigger {
	static TestMethod void myUnitTest(){
		List<Account> cuentas = Test_Service.CrearCuenta(1);
        insert cuentas;
        system.assertNotEquals(0, cuentas.size());

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        List<Desarrollo__c> desarrollos = Test_Service.CrearDesarrollo(1);
        Time myTime = Time.newInstance(1, 2, 3, 4);

        for(Desarrollo__c des : desarrollos){
            des.Desarrollo_Comercial__c = desarrolloComercial.Id;
            des.Inicio__c = myTime;
        }
        insert desarrollos;
        system.assertNotEquals(0, desarrollos.size());

        List<Etapa__c> etapas = Test_Service.CrearEtapa(1, desarrollos[0].Id);
        insert etapas;
        system.assertNotEquals(0, etapas.size());

        List<Inmueble__c> inmuebles = Test_Service.CrearInmueble(1, etapas[0].Id);
        insert inmuebles;
        system.assertNotEquals(0, inmuebles.size());

        Test.startTest();
        	inmuebles[0].Programar_entrega__c = true;
        	update inmuebles;
        	
        	Carta_Oferta__c EntregaCreada = [SELECT Id FROM Carta_Oferta__c WHERE Inmueble__c =: inmuebles[0].Id ORDER BY CreatedDate DESC LIMIT 1];
        	system.assertNotEquals(null, EntregaCreada);
        Test.stopTest();
    }
	
	static testMethod void test01() {
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
        
        Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'BGR',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
        
        Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'DISPONIBLE',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = false
		);
		insert departamento;
		
        Opportunity oportunidad = new Opportunity(
			Name = '15810-8',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			Desarrollo__c = desarrollo.Id,
			Etapa__c = etapa.Id,
			Departamento__c = departamento.Id,
			Compania__c = compania.Id,
			Lote_Id__c = '322',
			Id_Prospecto_Enkontrol__c = '15810',
			ID_Cliente_Enkontrol__c = '5108',
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		Test.startTest();
			departamento.Estatus__c = 'VENDIDO';
			departamento.Edo_Cta_Ejecutado__c = true;
			update departamento;
		Test.stopTest();
	}
}

/*
	static TestMethod void myUnitTest() {
		List<Account> cuentas = Test_Service.CrearCuenta(1);
        insert cuentas;
        system.assertNotEquals(0, cuentas.size());

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        List<Desarrollo__c> desarrollos = Test_Service.CrearDesarrollo(1);
        Time myTime = Time.newInstance(1, 2, 3, 4);
        
        for(Desarrollo__c des : desarrollos) {
        	des.Desarrollo_Comercial__c = desarrolloComercial.Id;
            des.Inicio__c = myTime;
        }
        insert desarrollos;
        system.assertNotEquals(0, desarrollos.size());

        List<Etapa__c> etapas = Test_Service.CrearEtapa(1, desarrollos[0].Id);
        insert etapas;
        system.assertNotEquals(0, etapas.size());

        List<Inmueble__c> inmuebles = Test_Service.CrearInmueble(1, etapas[0].Id);
        insert inmuebles;
        system.assertNotEquals(0, inmuebles.size());

        Test.startTest();
        	inmuebles[0].Programar_entrega__c = true;
        	update inmuebles;
        	Carta_Oferta__c EntregaCreada = [SELECT Id FROM Carta_Oferta__c WHERE Inmueble__c =: inmuebles[0].Id ORDER BY CreatedDate DESC LIMIT 1];
        	system.assertNotEquals(null, EntregaCreada);
        Test.stopTest();
    }
*/