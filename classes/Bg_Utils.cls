public class Bg_Utils {
  public static Compania__c genCompania(String name, String idEnk) {
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = idEnk;
    compania.Name = name;
    return compania;
  }
  public static User genUser(String alias, String profile) {
    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = :profile];
    User gerenteUser = new User();
    gerenteUser.Alias = ('alias'+alias).right(8);
    gerenteUser.Desarrollo__c = 'Coapa';
    gerenteUser.Email = alias+'@begrand.com';
    gerenteUser.EmailEncodingKey = 'UTF-8';
    gerenteUser.Id_Enkontrol__c ='00001';
    gerenteUser.LanguageLocaleKey = 'en_US';
    gerenteUser.LastName = 'LastName';
    gerenteUser.LocaleSidKey = 'en_US';
    gerenteUser.ProfileId = profileGerente.Id;
    gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
    gerenteUser.Username = alias+'@begrand.com';
    return gerenteUser;
  }
  public static Desarrollo_Comercial__c genDesComercial(String name, String companiaId, String gerenteId) {
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = name;
    desarrolloComercial.Compania__c = CompaniaId;
    desarrolloComercial.Gerente_Ventas__c = gerenteId;
    desarrolloComercial.Gerente_Administrativo__c = gerenteId;
    return desarrolloComercial;
  }
  public static Guardia__c genGuardia(String desComUnoId, String desComDosId, String asesorId, Decimal roll) {
    Guardia__c guardia = new Guardia__c();
    guardia.Nombre_Asesor__c = asesorId;
    guardia.Fecha_Guardia__c = Date.today();
    guardia.Roll__c = roll;
    guardia.Desarrollo_Comercial__c = desComUnoId;
    guardia.Desarrollo_Comercial_Primario__c = desComDosId;
    guardia.Desarrollo_Comercial_Secundario__c = desComDosId;
    return guardia;
  }
  public static Desarrollo__c genDesarrollo(String name, String enkId, String desComId) {
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = name;
    desarrollo.Id_Desarrollo_Enkontrol__c = enkId;
    desarrollo.Desarrollo_Comercial__c = desComId;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    return desarrollo;
  }
  public static Account genAccount(String name, String enkId, String recordTypeId) {
    Account acc = new Account();
    acc.LastName = name;
    acc.ID_Prospecto__pc = enkId;
    acc.RecordTypeId = recordTypeId;
    acc.PersonEmail = name.trim().replaceAll(' ', '')+'@test.com';
    return acc;
  }
  public static Opportunity genOpportunity(String name, String stageName, String desarrolloId, String accId) {
    Opportunity opp = new Opportunity();
    opp.Name = name;
    opp.AccountId = accId;
    opp.CloseDate = Date.today().addDays(10);
    opp.Desarrollo__c = desarrolloId;
    opp.StageName = stageName;
    return opp;
  }
  public static Attachment genAttachment(String name, String parentId) {
    Attachment attach = new Attachment();
    attach.Body = Blob.valueOf('Some Text');
    attach.ParentId = parentId;
    attach.Name = name;
    attach.Description = 'description;comentario;observacion';
    return attach;
  }
  public static RecordType getRtype(String rtypeName, String rtypeObject) {
    return [SELECT DeveloperName,Id,IsActive,SobjectType FROM RecordType WHERE SobjectType = : rtypeObject AND DeveloperName = : rtypeName limit 1];
  }
}