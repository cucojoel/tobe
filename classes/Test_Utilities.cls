/*---------------------------------------------------------------------------------------------------------

Autor: Scarlett Castillo - 10-2018 - CloudCo
Descripción: Clase utility para las demás clases testing.

---------------------------------------------------------------------------------------------------------*/
@IsTest
public class Test_Utilities {
  public static void testCrearConfiguracionPersonalizada() {
    Configuracion_Dropbox__c configuracionDropbox = new Configuracion_Dropbox__c();
    configuracionDropbox.Access_Token__c = 'Test_Access_Token_001';
    insert configuracionDropbox;
  }
  public static void testCrearConfiguracionPersonalizadaEnkontrol() {
    Configuracion_Enkontrol__c configuracionEnkontrol = new Configuracion_Enkontrol__c();
    configuracionEnkontrol.Service_Endpoint__c = 'https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc';
    configuracionEnkontrol.Username__c = 'Test_Access_Token_001';
    configuracionEnkontrol.Password__c = 'Test:Password';
    insert configuracionEnkontrol;
  }
  public static Matriz_Deficiencias__c crearMatrizPrueba(Integer idZona,
    String zona,
    Integer idEspecialidad,
    String especialidad,
    Integer idSubespecialidad,
    String subEspecialidad,
    Id idEtapa,
    Id idProveedor,
    Boolean esCuadrilla) {
    Matriz_Deficiencias__c md = new Matriz_Deficiencias__c();
    md.Zona__c = zona;
    md.id_Zona_Enkontrol__c = idZona;
    md.Especialidad__c = especialidad;
    md.Id_Especialidad_Enkontrol__c = idEspecialidad;
    md.Subespecialidad__c = subEspecialidad;
    md.Id_Subespecialidad_Enkontrol__c = idSubespecialidad;
    md.Etapa__c = idEtapa;
    md.Cuadrilla__c = esCuadrilla;
    md.Proveedor__c = idProveedor;
    insert md;
    return md;
  }
  public static Desarrollo_Comercial__c crearDesarrolloComercial(String gerenteAdministrativo, String gerenteVentas) {
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Compania__c = crearCompania().Id;
    desarrolloComercial.Name = 'Polanco';
    desarrolloComercial.Gerente_Administrativo__c = gerenteAdministrativo;
    desarrolloComercial.Gerente_Ventas__c = gerenteVentas;
    insert desarrolloComercial;
    return desarrolloComercial;
  }
  public static Inmueble__c crearInmueblePrueba(String nombreInmueble, String desarrolloComercial, String idEnkontrolEtapa, String idEnkontrolDesarrollo) {
    Time myTime = Time.newInstance(1, 2, 3, 4);
    Desarrollo__c unDesarrollo = new Desarrollo__c();
    unDesarrollo.Name = 'Alto Polanco';
    unDesarrollo.Id_Desarrollo_Enkontrol__c = idEnkontrolDesarrollo;
    unDesarrollo.Desarrollo_Comercial__c = desarrolloComercial;
    unDesarrollo.Inicio__c = myTime;
    insert unDesarrollo;
    Etapa__c unaEtapa = new Etapa__c();
    unaEtapa.Name = 'Torre 2';
    unaEtapa.Id_Etapa_Enkontrol__c = idEnkontrolEtapa;
    unaEtapa.No_de_Pisos__c = 23;
    unaEtapa.Desarrollo__c = unDesarrollo.Id;
    insert unaEtapa;
    Inmueble__c unInmueble = new Inmueble__c();
    unInmueble.Name = nombreInmueble;
    unInmueble.Name = nombreInmueble;
    //unInmueble.Inicio__c = System.today();
    unInmueble.Lote_Id__c = nombreInmueble;
    unInmueble.Area_depto__c = 72;
    unInmueble.Area_Total__c = 99;
    unInmueble.Estacionamientos__c = 2;
    unInmueble.Etapa__c = unaEtapa.Id;
    unInmueble.Nivel__c = '1';
    insert unInmueble;
    return unInmueble;
  }
  public static Carta_Oferta__c crearEntregaPrueba(Id idInmueble, Id idCliente) {
    Carta_Oferta__c cartaOferta = new Carta_Oferta__c();
    cartaOferta.Inmueble__c = idInmueble;
    cartaOferta.Cliente__c = idCliente;
    cartaOferta.Estatus__c = 'Agendado';
    insert cartaOferta;
    return cartaOferta;
  }
  public static Case crearCaso(Id cartaOferta, Id clienteId) {
    Case aCase = new Case(
      Entrega__c = cartaOferta, Origin = 'Deficiencias', Status = 'Nuevo', AccountId = clienteId, Folio_Enkontrol__c = 1001
      );
    insert aCase;
    return aCase;
  }
  public static Desarrollo_Comercial__c crearDesarrolloComercial(Id companiaId, Id gerenteVentasId, Id gerenteAdminId) {
    Desarrollo_Comercial__c unDesarrolloComercial = new Desarrollo_Comercial__c();
    unDesarrolloComercial.Name = 'Alto Polanco';
    unDesarrolloComercial.Compania__c = companiaId;
    unDesarrolloComercial.Gerente_Ventas__c = gerenteVentasId;
    unDesarrolloComercial.Gerente_Administrativo__c = gerenteAdminId;
    insert unDesarrolloComercial;
    return unDesarrolloComercial;
  }
  public static Desarrollo__c crearDesarrollo(Id desarrolloComercialId) {
    Time myTime = Time.newInstance(1, 2, 3, 4);
    Desarrollo__c unDesarrollo = new Desarrollo__c();
    unDesarrollo.Name = 'Alto Polanco';
    unDesarrollo.Id_Desarrollo_Enkontrol__c = 'AP2';
    unDesarrollo.Desarrollo_Comercial__c = desarrolloComercialId;
    unDesarrollo.Desarrollo_Activo__c = true;
    unDesarrollo.Inicio__c = myTime;
    insert unDesarrollo;
    return unDesarrollo;
  }
  public static Compania__c crearCompania() {
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '12';
    compania.Name = 'GRAND COAPATEST S.A. DE C.V.';
    insert compania;
    return compania;
  }
  public static Detalle_del_Caso__c crearDeficiencia(Id casoId) {
    Detalle_del_Caso__c dc = new Detalle_del_Caso__c();
    dc.Casos__c = casoId;
    dc.Descripcion__c = 'Test';
    dc.Tipo_Falla__c = 'D';
    dc.Id_Falla_WS__c = '001';
    dc.Dropbox_Folder_URL__c = '/TEST/test.jpeg';
    insert dc;
    return dc;
  }
  public static Account crearClientePrueba() {
    Account acc = new Account();
    acc.Name = 'Cliente Muestra';
    insert acc;
    return acc;
  }
  public static Account crearClientePruebaIdProspecto(String idEnkontrol, String nombre) {
    Id proveedorRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Persona Física'].Id;
    Account acc = new Account();
    acc.ID_Prospecto__pc = idEnkontrol;
    acc.Name = nombre;
    acc.RecordTypeId = proveedorRTId;
    insert acc;
    return acc;
  }
  public static Account crearProveedorPrueba(Integer idEnkontrol, String nombre) {
    Id proveedorRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Proveedor'].Id;
    Account acc = new Account();
    acc.Id_Proveedor_Enkontrol__c = idEnkontrol;
    acc.Name = nombre;
    acc.RecordTypeId = proveedorRTId;
    insert acc;
    return acc;
  }
  public static User crearUsuarioPrueba(String idEnkontrol) {
    User u;
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    // Insert account as current user
    System.runAs(thisUser) {
      u = new User(
        ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id,
        LastName = 'last',
        Email = 'puser000@test.com',
        Username = 'puser000@test.com' + System.currentTimeMillis(),
        CompanyName = 'TEST',
        Title = 'title',
        Alias = 'alias',
        TimeZoneSidKey = 'America/Los_Angeles',
        EmailEncodingKey = 'UTF-8',
        LanguageLocaleKey = 'en_US',
        LocaleSidKey = 'en_US',
        Id_Enkontrol__c = idEnkontrol
        );
      insert u;
    }
    return u;
  }
  public static WS_EnkontrolIntegration.Oportunidades testOportunidadesWS() {
    WS_EnkontrolIntegration.Oportunidades opp = new WS_EnkontrolIntegration.Oportunidades();
    opp.loteId = null;
    opp.desarrollo = null;
    opp.idProspecto = null;
    opp.numeroCliente = null;
    opp.agente = null;
    opp.idEnkontrol = null;
    opp.puntoProspectacion = null;
    opp.compania = null;
    opp.etapa = null;
    opp.observaciones = null;
    opp.gradoDeInteres = null;
    opp.referencia = null;
    //opp.dbxUrls = null;
    return opp;
  }
  public static WS_EnkontrolIntegration.Prospectos testProspectosWS() {
    WS_EnkontrolIntegration.Prospectos pros = new WS_EnkontrolIntegration.Prospectos();
    pros.idSalesforce = null;
    pros.compania = null;
    pros.puntoProspectacion = null;
    pros.idEnkontrol = null;
    pros.compania = null;
    pros.agente = null;
    pros.desarrollo = null;
    pros.personaMoral = null;
    pros.empresa = null;
    pros.fechaDeNacimiento = null;
    pros.ciudad = null;
    pros.estado = null;
    pros.colonia = null;
    pros.calleYNumero = null;
    pros.nacionalidad = null;
    pros.sexo = null;
    pros.email1 = null;
    pros.email2 = null;
    pros.telefono2 = null;
    pros.telefono3 = null;
    pros.rfc = null;
    pros.nombre = null;
    pros.apellidoMaterno = null;
    pros.apellidoPaterno = null;
    pros.telefono1 = null;
    pros.cp = null;
    pros.pais = null;
    return pros;
  }
  public static Contact crearContactoDeProveedorPrueba(Id idProveedor) {
    Contact con = new Contact();
    con.FirstName = 'First Name Contact';
    con.LastName = 'Test';
    con.Email = 'test@test.test';
    con.AccountId = idProveedor;
    return con;
  }
  public static Campaign crearCampaniaPrueba(String name, String type) {
    Campaign camp = new Campaign();
    camp.Name = name;
    camp.Type = type;
    camp.IsActive = true;
    insert camp;
    return camp;
  }
  public static Lead crearLeadPruebaSinInsertar() {
    Lead unLead = new Lead();
    unLead.FirstName = 'Test name';
    unLead.LastName = 'Test LastName';
    unLead.Phone = '1222443434';
    unLead.Email = 'test@test.com';
    return unLead;
  }
  public static Lead crearLeadPruebaCompletoSinInsertar() {
    Lead unLead = new Lead();
    unLead.FirstName = 'Test name';
    unLead.LastName = 'Test LastName';
    unLead.Phone = '1222443434';
    unLead.Email = 'test@test.com';
    return unLead;
  }
  public static Case crearCasoPruebaSinInsertar() {
    Case unCaso = new Case();
    unCaso.Status = 'Nuevo';
    return unCaso;
  }
  public static String imagenMuestraBase64 = '' +
    'EBMQGhcbFhUWGxcpIBwcICkvJyUnLzkzMzlHREddXX0BBQUFBQUFBQYGBQgIBwgICwoJCQoLEQwNs' +
    'DA0MERoQExAQExAaFxsWFRYbFykgHBwgKS8nJScvOTMzOUdER11dff/CABEIASwBLAMBIgACEQED' +
    'EQH/xAAdAAEAAwADAQEBAAAAAAAAAAAABwgJAwQGBQEC/9oACAEBAAAAALlgAAAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB834wAB6Dt+' +
    'b8L9733H5oDs+iAq9QgAA0yhin/U/ZhuvmMBKeooFXqG6A+MpZbWWqDTf0YP0E5XkaO3MppZizPl' +
    'aMeujC3kqZ/S3aaqHR1FAq9Q3YCMsx9DrE1FlOvlW9gu6j7NGbq0at+9Z/1q/jRie8gbEX7r18O0' +
    'IHz/AIvqYgzH0OsUUUq1rX2nY+nG2XEiWym/sfM/fR93IGxF+wAIfzH0OsUUUqlyE66Pq5Ui8jyz' +
    'JeOTHFkDYi/YAEP5j6HWKKKVZvpyPVzd43xMuxVBFXWq/ouLIGxF+wAIfzH0OsUUUq3sF3RUilGw' +
    'PdQNnBonYXiyBsRfsACH8x9DrFFFKt7Bd0VIo9eW2X7Uqjmks58WQNiL9gArVTyP9DrFFFKqyX/Q' +
    '875H9k3vRTLOmXjM8Y1svfsAEJV5WflQrpC4HLKUUPZWu+n5KnH7Lll35+gAAAAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/8QA' +
    'FAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAB//8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAxAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//8QATRAAAAQDAQcOCgcHBQAA' +
    'AAAAAQIDBQAEBgcIEBESVpSyExQWGDE2N1JVcnR1kdEXICEiMDM0NUFRMkJhYnOxsxUjJHGBgsJA' +
    'U4CQo//aAAgBAQABPwD/AJ4uTw1NCZFnJxlpRI5sUp11ATKI/Lzo2dUblU152n3xs6o3KprztPvj' +
    'Z1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xs6o3KprztPvjZ1RuV' +
    'TXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xs6o3KprztPvjZ1RuVTXnaf' +
    'fElOyTlKpzcjOJTEup9BVI4HIPNEt57rGlqb853fpOTHiKKgBx/pBrebK0zmIaqCfzBFQwQ0WqWe' +
    'PqiaMlVkkKp/IBTn1PSxYKYpygYpgEB3BAd2JmYl5RBVddUiSCZcY5zjigUOMYY2dUXlS152n3xs' +
    '6o3KprztPvjZ1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xKVbS0/' +
    'MJS0rUbcsuoOKmklMEOc4/YAD6C6x3q0/wBY/wCH+gsBEAsopsekfqRbBdAzSU3N0/SEwCYJCZOZ' +
    'cS6KUTc5Nzy55iamVF1VBwmUUMJzCP8AMb3lAYoG2ir6FWQSLOHnmwB8+SXNoDukGHyrmWt7Iqnd' +
    '2lfHRUalynJ9dI+J5SHD0Fi3CfSXS/QXWO9Wn+sf8IIAGOQB3BEAhvubLO5mQkF1AcMdaXTUHAvx' +
    'wi1Gwih6Qod6em7XmvJUiYp463GOBb1i1jdH15SJ3Z3Cb1yE4sj+5VxAwFgbmWzgCj7xziHqVSkH' +
    'h1k0sOpITSyRMO7gIYShFhFmtN2hrVCR7BfBKEQFLUT4n04t1s8YbPHlmk2UF9TmJQVVNWPjjCBQ' +
    'OskQdwxygPbErc02cqyssofX+FRJM/tHHCNrLZx8nHOI2slm/wAnHOIr+5/oOmqNqB4kdfa5lJUV' +
    'Esde8hVi9KXNTYeUUEk3PnWk0jfEmqKGwiWBHCOEYsgsGUrSSSfH2YUlms/s6KfrV4SufrK0kgT2' +
    'OmU++ZdSKjuZqFcJVb9jHmW2axfMNjiqnFS0840s9uLM4pYkzKKYhvkPyMH2GCLPavmmJGqGgVR1' +
    'i7tUyioQR8gKlIJiHvWOWLUbXFGJu7sE3roZlVMdSWxAwFhS5ms3KRT3juGH18OSCcs4zyCWHESX' +
    'UTLh+RTYIsQoVjr6pp9tedX1BKSMsXUj4g4+MBY2stnHycc4i2qi2agquSaGfVtbDIorDqpsccJ4' +
    'sW4T6S6X6C6x3q0/1j/hCPrU+eWGT3O19ER0Ai3ngqqrmo/qXrl7g6V61XgS4QMHzhzuWQcXJwnd' +
    'mGJrhdRXE1rxhiyeyLwYKvR/23r7XxEg9TqeJqUXV2+WmugH04lvaZf8Qv5w3+wSX4BNEL9qsrMz' +
    'lnlVoSyB1llJIwEIQuE5h+6EbCawyWc81U7orhtcZGwWgE5uUVQOi5rgomoQSCGNjYMIGvWYT0lP' +
    'WfUirInIKRW9JPAX4HIGAwX7qOUQQr6SXIQAOu2JmV5xTiEFMYo4SjgG9c1cGaHT14V9UrzDaMPf' +
    'vh26WtpjFyvv6duqVdMt60+wototRketkWs8EsmjqWoY/wBAYoy5xCkamaH3ZTrnWS2PqWtsTG9A' +
    '5szS8IpouTdKzaZRxiEXTBUCjxvOjYNRmSrXmqXdBEyJkAhAApSlwAAbgBFvPBVVXNR/UvXL3B0r' +
    '1qv4l1dvlproB9OJYcEzLiP+4X84kajp4shJAL5JAJUSYf4gny/nGySn+XJLO';
  public static final String RESPUESTA_ENKONTROL = '' +
    '<?xml version="1.0" encoding="UTF-8"?>' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" s:mustUnderstand="1">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-04T04:13:26.532Z</u:Created>' +
    '        <u:Expires>2018-09-04T04:18:26.532Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <FailReportExecuteResponse xmlns="http://tempuri.org/">' +
    '      <FailReportExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" ' +
    '        xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" />' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" ' +
    '          xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO" i:nil="true" />' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </FailReportExecuteResult>' +
    '    </FailReportExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T18:23:13.461Z</u:Created>' +
    '        <u:Expires>2018-09-20T18:28:13.461Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <ProspectExecuteResponse xmlns="http://tempuri.org/">' +
    '      <ProspectExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">' +
    '          <b:ProspectDTO>' +
    '            <b:AgentKey>1</b:AgentKey>' +
    '            <b:AgentName>0</b:AgentName>' +
    '            <b:CompanyId>11</b:CompanyId>' +
    '            <b:CompanyName i:nil="true"/>' +
    '            <b:DevelopmentKey>AP2</b:DevelopmentKey>' +
    '            <b:DevelopmentName>0</b:DevelopmentName>' +
    '            <b:Email>kim.jaime@test.tst</b:Email>' +
    '            <b:FirstName>Kim</b:FirstName>' +
    '            <b:Gender>M</b:Gender>' +
    '            <b:HomePhoneNumber>7788990099</b:HomePhoneNumber>' +
    '            <b:Id>49568</b:Id>' +
    '            <b:IdGroup>80045</b:IdGroup>' +
    '            <b:IsMoralPerson>false</b:IsMoralPerson>' +
    '            <b:LastName>Jaime</b:LastName>' +
    '            <b:ProspectingTypeId>1</b:ProspectingTypeId>' +
    '            <b:ProspectingTypeName i:nil="true"/>' +
    '            <b:SecondLastName>Lopez</b:SecondLastName>' +
    '            <b:Address>Calzada Ignacio Zaragoza</b:Address>' +
    '            <b:BirthDate>0001-01-01T00:00:00</b:BirthDate>' +
    '            <b:City>Ciudad de México</b:City>' +
    '            <b:Country>Mexico</b:Country>' +
    '            <b:CreatedDate>0001-01-01T00:00:00</b:CreatedDate>' +
    '            <b:District>Agricola Oriental</b:District>' +
    '            <b:Email2>kim.jaime1@test.tst</b:Email2>' +
    '            <b:Nationality>MEXICANA</b:Nationality>' +
    '            <b:OfficePhoneNumber>7788990098</b:OfficePhoneNumber>' +
    '            <b:OtherPhoneNumber>7788990097</b:OtherPhoneNumber>' +
    '            <b:PostalCode>15000</b:PostalCode>' +
    '            <b:PropertyTypeName i:nil="true"/>' +
    '            <b:RFC i:nil="true"/>' +
    '            <b:SaleOpportunityList i:nil="true"/>' +
    '            <b:SalesForceId i:nil="true"/>' +
    '            <b:State>Ciudad de México</b:State>' +
    '          </b:ProspectDTO>' +
    '        </List>' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </ProspectExecuteResult>' +
    '    </ProspectExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_FAIL = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T17:55:58.239Z</u:Created>' +
    '        <u:Expires>2018-09-20T18:00:58.239Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <s:Fault>' +
    '      <faultcode>s:Client</faultcode>' +
    '      <faultstring xml:lang="es-MX">Service operation ProspectExecute failed due to validation errors: &#xD; &#xD;Identificador del punto de prospeccion no valido &#xD;' +
    '      </faultstring>' +
    '    </s:Fault>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_BASICO_SUCCESS = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T17:46:47.269Z</u:Created>' +
    '        <u:Expires>2018-09-20T17:51:47.269Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <BasicProspectExecuteResponse xmlns="http://tempuri.org/">' +
    '      <BasicProspectExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">' +
    '          <b:ProspectDTO>' +
    '            <b:AgentKey>1</b:AgentKey>' +
    '            <b:AgentName>0</b:AgentName>' +
    '            <b:CompanyId>11</b:CompanyId>' +
    '            <b:CompanyName i:nil="true"/>' +
    '            <b:DevelopmentKey>AP2</b:DevelopmentKey>' +
    '            <b:DevelopmentName>0</b:DevelopmentName>' +
    '            <b:Email i:nil="true"/>' +
    '            <b:FirstName>Jimena</b:FirstName>' +
    '            <b:Gender>M</b:Gender>' +
    '            <b:HomePhoneNumber>1223334455</b:HomePhoneNumber>' +
    '            <b:Id>80045</b:Id>' +
    '            <b:IdGroup>80045</b:IdGroup> +' +
    '            <b:IsMoralPerson>false</b:IsMoralPerson>' +
    '            <b:LastName>Sanchez</b:LastName>' +
    '            <b:ProspectingTypeId>1</b:ProspectingTypeId>' +
    '            <b:ProspectingTypeName i:nil="true"/>' +
    '            <b:SecondLastName>Martinez</b:SecondLastName>' +
    '            <b:Address i:nil="true"/>' +
    '            <b:BirthDate>0001-01-01T00:00:00</b:BirthDate>' +
    '            <b:City i:nil="true"/>' +
    '            <b:Country i:nil="true"/>' +
    '            <b:CreatedDate>0001-01-01T00:00:00</b:CreatedDate>' +
    '            <b:District i:nil="true"/>' +
    '            <b:Email2 i:nil="true"/>' +
    '            <b:Nationality i:nil="true"/>' +
    '            <b:OfficePhoneNumber i:nil="true"/>' +
    '            <b:OtherPhoneNumber i:nil="true"/>' +
    '            <b:PostalCode i:nil="true"/>' +
    '            <b:PropertyTypeName i:nil="true"/>' +
    '            <b:RFC i:nil="true"/>' +
    '            <b:SaleOpportunityList i:nil="true"/>' +
    '            <b:SalesForceId i:nil="true"/>' +
    '            <b:State i:nil="true"/>' +
    '          </b:ProspectDTO>' +
    '        </List>' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </BasicProspectExecuteResult>' +
    '    </BasicProspectExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_BASICO_FAIL = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T17:39:45.174Z</u:Created>' +
    '        <u:Expires>2018-09-20T17:44:45.174Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <s:Fault>' +
    '      <faultcode>s:Client</faultcode>' +
    '      <faultstring xml:lang="es-MX"> ' +
    '        Service operation BasicProspectExecute failed due to validation errors: &#xD; &#xD; Numero de teléfono de casa no valido (10 digitos) &#xD; ' +
    '      </faultstring>' +
    '    </s:Fault>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_DEFICIENCIAS_SUCCESS = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-10-23T17:58:38.519Z</u:Created>' +
    '        <u:Expires>2018-10-23T18:03:38.519Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <FailDetailExecuteResponse xmlns="http://tempuri.org/">' +
    '      <FailDetailExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">' +
    '          <b:FailReportDetailDTO>' +
    '            <b:Comments i:nil="true"/>' +
    '            <b:DetailOrderNumber>1</b:DetailOrderNumber>' +
    '            <b:EmployeeId>0</b:EmployeeId>' +
    '            <b:FailId>1</b:FailId>' +
    '            <b:FailLocationId>4</b:FailLocationId>' +
    '            <b:FailProviderId>0</b:FailProviderId>' +
    '            <b:FailStatus>Reasign</b:FailStatus>' +
    '            <b:FailTypeId>12</b:FailTypeId>' +
    '            <b:Folio>1863</b:Folio>' +
    '            <b:IsAlternativeProvider>true</b:IsAlternativeProvider>' +
    '            <b:IsLocalProvider>true</b:IsLocalProvider>' +
    '            <b:PreviousOrderNumber>0</b:PreviousOrderNumber>' +
    '            <b:PropertyDevelopmentKey>AP2</b:PropertyDevelopmentKey>' +
    '            <b:SalesForceId>FDC-00000671</b:SalesForceId>' +
    '            <b:Type i:nil="true"/>' +
    '          </b:FailReportDetailDTO>' +
    '        </List>' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </FailDetailExecuteResult>' +
    '    </FailDetailExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
}