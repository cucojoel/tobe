public class CLASS_TriggerConvertedLead {

    public void relacionaBitacoras(List<Opportunity> lstNew){

        map<String,String> convertedAccountIds = new map<String,String>();
		Set<String> setsId = new Set<String>();
    	Set<String> setsLeadsId = new Set<String>();


        Set<String> setsIdAcounts = new Set<String>();
        Set<String> setsIdOpportinity = new Set<String>();
        map<String,String> mapOppo = new map<String,String>();


        for (Opportunity opp : lstNew) {
        	setsIdAcounts.add(opp.AccountId);
            mapOppo.put(opp.AccountId, opp.Id);
		}

        List<Opportunity> lstOppo = new List<Opportunity>();
        //joel soto - se agrega limite de consulta
        lstOppo = [SELECT Id, AccountId FROM Opportunity WHERE AccountId IN: setsIdAcounts limit 5000];

        map<String,List<Opportunity>> mapOpo = new map<String,List<Opportunity>>();

        if(!lstOppo.isEmpty()){

            for (Opportunity opo2 : lstOppo)  {
            	if(!mapOpo.containsKey(opo2.AccountId)){
               		List<Opportunity> lstOppo2 = new List<Opportunity>();
                	lstOppo2.add(opo2);
                	mapOpo.put(opo2.AccountId, lstOppo2);
            	}else{
                	List<Opportunity> lstOppoTemp = mapOpo.get(opo2.AccountId);
                	lstOppoTemp.add(opo2);
                	mapOpo.put(opo2.AccountId, lstOppoTemp);
            	}
			}

            List<Bitacora__c> lstBitacorasUp  = new List<Bitacora__c>();
            List<Bitacora__c> lstBita = new List<Bitacora__c>();
            //joel soto - se agrega limite de consulta
            lstBita = [SELECT ID, Oportunidad__c, Lead__r.ConvertedAccountId FROM Bitacora__c WHERE Lead__r.ConvertedAccountId IN: setsIdAcounts limit 5000];

            string RecordTipeIdOpo =  Schema.SObjectType.Bitacora__c.getRecordTypeInfosByName().get('Oportunidad').getRecordTypeId();

            map<String,List<Bitacora__c>> mapBita = new map<String,List<Bitacora__c>>();

            if(!lstBita.isEmpty()){
                for(Bitacora__c bt : lstBita){
                    if(!mapBita.containsKey(bt.Lead__r.ConvertedAccountId)){
                        List<Bitacora__c> lstBitaPa = new List<Bitacora__c>();
                        lstBitaPa.add(bt);
                        mapBita.put(bt.Lead__r.ConvertedAccountId, lstBitaPa);
                    }else{
                        List<Bitacora__c> lstBitaPaTemp = mapBita.get(bt.Lead__r.ConvertedAccountId);
                		lstBitaPaTemp.add(bt);
                		mapBita.put(bt.Lead__r.ConvertedAccountId, lstBitaPaTemp);
                    }
                }



            	if(!mapOpo.isEmpty()){
                	for(List<Opportunity> opoFin : mapOpo.values()){
                        if(!opoFin.IsEmpty() && opoFin.size() == 1){
                            for(Opportunity oppoFin2 : opoFin){
                                if(mapBita.containsKey(oppoFin2.AccountId)){
                        			for(Bitacora__c bitaFin : mapBita.get(oppoFin2.AccountId) ){
                            			Bitacora__c btTemp = new Bitacora__c();
                      					btTemp.Oportunidad__c = oppoFin2.Id;
                      					btTemp.RecordTypeId = RecordTipeIdOpo;
                      					btTemp.Id = bitaFin.Id;
                	  					lstBitacorasUp.add(btTemp);
                        			}
                    			}
                            }
                        }
                	}
            	}

            	update lstBitacorasUp;

        	}
        }
    }

}