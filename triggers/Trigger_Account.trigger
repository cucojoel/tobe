trigger Trigger_Account on Account (after update) {
	if(trigger.isAfter) {
		if(trigger.isUpdate) {
			if(system.label.AutorizaEnvioEdoCtaMasivo == 'Si') {
				for(Account forData : trigger.new) {
					if(trigger.oldMap.get(forData.Id).Edo_Cta_Ejecutado__c == false && forData.Edo_Cta_Ejecutado__c == true) {
						//if(!system.isFuture() && !system.isBatch()) {
							EnviarPorCorreoMasivo.enviarMail(forData.Id);
						//}					
					}				
				}
			}
		}
	}    
}