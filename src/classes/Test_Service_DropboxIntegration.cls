/**
 * Created by scarlettcastillo on 8/28/18.
 */

 @isTest
 public with sharing class Test_Service_DropboxIntegration {

  public static testMethod void testUpload() {
   Wrapper_DropboxIntegration.DropboxRequest dropboxRequest = new Wrapper_DropboxIntegration.DropboxRequest();
   dropboxRequest.path = 'x/y.z';
   dropboxRequest.mode='add';
   dropboxRequest.autorename = true;
   dropboxRequest.mute = false;
   dropboxRequest.strict_conflict = false;

   Account acc = new Account();
   acc.Name = 'Cliente Muestra';
   insert acc;

   Opportunity opp = new Opportunity();
   Date myDate = Date.newInstance(2019, 2, 17);
   opp.CloseDate = myDate;
   opp.Name = 'Algo Incre';
   opp.ID_Oportunidad_Enkontrol__c = '5';
   opp.ID_Prospecto_Enkontrol__c = '4';
   opp.StageName = 'Nueva';
   opp.AccountId = acc.Id;
   insert opp;

   Attachment attachment_2 = new Attachment();
   Blob bodyBloby = Blob.valueOf('body blob');
   attachment_2.Body = bodyBloby;
   attachment_2.Name = '123456.pdf';
   attachment_2.ParentId = opp.Id;
   insert attachment_2;

   Historial_dropbox__c sffile = new Historial_dropbox__c();
   sffile.Name = attachment_2.Name;
   sffile.Oportunidad__c = opp.Id;
   sffile.Id_attach__c = attachment_2.Id;
   sffile.Id_dropbox__c = 'iddrop';
   sffile.Tipo_de_documento__c = 'RFC';
   sffile.Url_dropbox__c = '/url/drop/algo.pdf';
   sffile.Url_Salesforce__c = '/url/salesforce/algo.pdf';
   insert sffile;

   DocumentoxEsquema__c documentoxEsquema = new DocumentoxEsquema__c();
   documentoxEsquema.Documento__c = 'RFC';
   documentoxEsquema.Etapa__c = 'Apartado';
   documentoxEsquema.Name = 'RFC';
   documentoxEsquema.Nivel__c = 1;
   documentoxEsquema.Obligatorio__c =  true;
   documentoxEsquema.Tipo_de_persona__c = 'Persona Moral';
   insert documentoxEsquema;

   Wrapper_DropboxIntegration.Metadata metadata = new Wrapper_DropboxIntegration.Metadata();
   metadata.x_tag = 'file';
   metadata.name = 'test.jpeg';
   metadata.path_lower = '/TEST' + '/test.jpeg';
   metadata.path_display = '/TEST' + '/test.jpeg';
   metadata.id = 'id:O02JXj0ZtTAAAAAAAAADSQ';
   metadata.rev = '137e35c9e90';
   metadata.size = 4049992;
   metadata.client_modified = '2018-08-27T20:51:41Z';
   metadata.server_modified = '2018-08-27T20:51:41Z';
   metadata.content_hash = '3347590b17b5e10f30795867308a568d73e4ae5b1db594cb65278f1a8619ed6c';

   Wrapper_DropboxIntegration.DropboxFile dropboxFile = new Wrapper_DropboxIntegration.DropboxFile();
   dropboxFile.metadata = metadata;
   dropboxFile.link = 'www.test.test';

   String payloadDropbox = JSON.serialize(dropboxFile);

   Map<String, Object> mapaMocks = new Map<String, Object>();
   mapaMocks.put(Service_DropboxIntegration.DROPBOX_CONTENT_ENDPOINT + Service_DropboxIntegration.FILE_UPLOAD_ENDPOINT, payloadDropbox);
   Test_WS_DropboxMock wsMock = new Test_WS_DropboxMock(mapaMocks);
   Test.setMock(HttpCalloutMock.class, wsMock);


   Test.startTest();

   Service_DropboxIntegration.crearArchivosDeDropbox(sffile);

   Test.stopTest();
 }

 public static testMethod void testObtenerLinkTemporal() {
  Map<String, Object> mapaMocks = new Map<String, Object>();

  /** Build the mock objects **/
  Wrapper_DropboxIntegration.DropboxFile dropboxFile = new Wrapper_DropboxIntegration.DropboxFile();

  Wrapper_DropboxIntegration.Metadata metadata = new Wrapper_DropboxIntegration.Metadata();
  metadata.x_tag = 'file';
  metadata.name = 'test.jpeg';
  metadata.path_lower = '/TEST' + '/test.jpeg';
  metadata.path_display = '/TEST' + '/test.jpeg';
  metadata.id = 'id:O02JXj0ZtTAAAAAAAAADSQ';
  metadata.rev = '137e35c9e90';
  metadata.size = 4049992;
  metadata.client_modified = '2018-08-27T20:51:41Z';
  metadata.server_modified = '2018-08-27T20:51:41Z';
  metadata.content_hash = '3347590b17b5e10f30795867308a568d73e4ae5b1db594cb65278f1a8619ed6c';

  dropboxFile.metadata = metadata;
  dropboxFile.link = 'www.test.test';

  Blob fileBlob = EncodingUtil.base64Decode(Test_Utilities.imagenMuestraBase64);
  String payloadDropbox = JSON.serialize(dropboxFile);

  mapaMocks.put(Service_DropboxIntegration.DROPBOX_API_ENDPOINT + Service_DropboxIntegration.TEMPORARY_URL_ENDPOINT, payloadDropbox);

  Test_WS_DropboxMock wsMock = new Test_WS_DropboxMock(mapaMocks);
  Test.setMock(HttpCalloutMock.class, wsMock);

  Test.startTest();

  Service_DropboxIntegration.obtenerLinkTemporalDropbox(metadata.path_lower);

  Test.stopTest();
}

public static testMethod void testObtenerListaArchivosDropboxYDescargar(){

  Map<String, Object> mapaMocks = new Map<String, Object>();

  /** Build the mock objects **/
  Wrapper_DropboxIntegration.ElementList elementList = new Wrapper_DropboxIntegration.ElementList();

  List<Wrapper_DropboxIntegration.Metadata> metadatos = new List<Wrapper_DropboxIntegration.Metadata>();
  Wrapper_DropboxIntegration.Metadata metadata = new Wrapper_DropboxIntegration.Metadata();
  metadata.x_tag = 'file';
  metadata.name='test.jpeg';
  metadata.path_lower = '/TEST' + '/test.jpeg';
  metadata.path_display = '/TEST' + '/test.jpeg';
  metadata.id='id:O02JXj0ZtTAAAAAAAAADSQ';
  metadata.rev = '137e35c9e90';
  metadata.size = 4049992;
  metadata.client_modified = '2018-08-27T20:51:41Z';
  metadata.server_modified = '2018-08-27T20:51:41Z';
  metadata.content_hash = '3347590b17b5e10f30795867308a568d73e4ae5b1db594cb65278f1a8619ed6c';

  metadatos.add(metadata);
  elementList.entries = metadatos;
  elementList.cursor = 'cursor';
  elementList.has_more = false;

  Blob fileBlob = EncodingUtil.base64Decode(Test_Utilities.imagenMuestraBase64);
  String payloadDropbox = JSON.serialize(elementList);

  mapaMocks.put(Service_DropboxIntegration.DROPBOX_CONTENT_ENDPOINT + Service_DropboxIntegration.FILE_DOWNLOAD_ENDPOINT, fileBlob);
  mapaMocks.put(Service_DropboxIntegration.DROPBOX_API_ENDPOINT + Service_DropboxIntegration.LIST_FOLDER_ENDPOINT, payloadDropbox);

  Test_WS_DropboxMock wsMock = new Test_WS_DropboxMock(mapaMocks);

  Test.setMock(HttpCalloutMock.class, wsMock);

  Test.startTest();

  Service_DropboxIntegration.obtenerArchivosDeDropbox(metadata.path_lower);
  Service_DropboxIntegration.descargarArchivosDropbox(metadata.name, metadata.path_lower);

  Test.stopTest();

}
}