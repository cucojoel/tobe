public with sharing class CONTROL_MapaContactacion {

	public Lead prospecto;
	public Map<Integer,List<String>> diasActividades {get; set;}
	public Map<Integer,String> coloresDias {get; set;}
	//public String colorMapa {get; set;}

	public CONTROL_MapaContactacion(ApexPages.StandardController stdController){
		
		Lead prospectoAux = (Lead)stdController.getRecord();
		System.debug('*****prospectoAux:' + prospectoAux);
		prospecto = [SELECT Id, Status, CreatedDate FROM Lead WHERE Id =: prospectoAux.Id];
		System.debug('*****prospecto:' + prospecto);
		
		/*if(prospecto.Status == 'Contactado'){
			colorMapa = '#4BCA81';
		}
		else{
			colorMapa = '#005FB2';
		}*/
		
		diasActividades = new Map<Integer,List<String>>();
		coloresDias = new Map<Integer,String>();
		obtenerDiasActividades();
	}
	
	public void obtenerDiasActividades(){
		
		//Se obtienen todas las bitacoras del prospecto
		List<Bitacora__c> bitacorasProspecto = [SELECT Id, Name, CreatedDate, Forma_de_Contacto__c FROM Bitacora__c WHERE Lead__c =: prospecto.Id ORDER BY CreatedDate];
		
		//La fecha de creacion del prospecto es la fecha de inicio, dia 1
		Date fechaCalculada = prospecto.CreatedDate.date();
		Date fechaCreacionBitacora = null;
		Integer contadorDias = 1;
		Integer contadorLlamadas = 0;
		Integer contadorCorreos = 0;
		Integer contadorWhatsapp = 0;
		List<String> listaActividades = new List<String>();
		String colorDia = '';
		
		//Se hace un ciclo por cada uno de los dias desde la creacion del prospecto hasta el dia actual
		while(fechaCalculada <= Date.today() && contadorDias <= 10){			
			
			listaActividades = new List<String>();
			
			//Se recorre cada una de las bitacoras del prospecto
			for(Bitacora__c bitacora : bitacorasProspecto){
				
				fechaCreacionBitacora = bitacora.CreatedDate.date();
				System.debug('*****bitacora.Name:' + bitacora.Name);
				System.debug('*****fechaCalculada:' + fechaCalculada);
				System.debug('*****fechaCreacionBitacora:' + fechaCreacionBitacora);
				
				if(fechaCalculada == fechaCreacionBitacora){
					
					if(bitacora.Forma_de_Contacto__c == 'Llamada'){
						contadorLlamadas++;
						listaActividades.add('Llamada ' + contadorLlamadas);
					}
					else if(bitacora.Forma_de_Contacto__c == 'Correo'){
						contadorCorreos++;
						listaActividades.add('Correo ' + contadorCorreos);
					}
					else if(bitacora.Forma_de_Contacto__c == 'WhatsApp'){
						contadorWhatsapp++;
						listaActividades.add('WhatsApp ' + contadorWhatsapp);
					}
				}
			}
			
			colorDia = '';
			//Si el prospecto ya ha sido contactado toda la linea se pinta de color verde
			if(prospecto.Status == 'Contactado'){
				colorDia = '#4BCA81';
			}
			//Si el prospecto aun no se ha contactado se pinta la linea por cada dia dependiendo el color del dia actual
			else{
				//Si el dia es del 1 al 5 se pinta verde
				if(contadorDias >= 1 && contadorDias <= 5){
					colorDia = '#4BCA81';
				}
				//Si el dia es del 6 al 8 se pinta amarillo
				else if(contadorDias >= 6 && contadorDias <= 8){
					colorDia = '#F2CF5B';
				}
				//Si el dia es del 9 al 10 se pinta rojo
				else if(contadorDias >= 9 && contadorDias <= 10){
					colorDia = '#ED1C24';
				}
			}
			
			System.debug('*****colorDia:' + colorDia);
			System.debug('*****contadorDias:' + contadorDias);
			System.debug('*****listaActividades:' + listaActividades);
			diasActividades.put(contadorDias,listaActividades);
			coloresDias.put(contadorDias,colorDia);
			contadorDias++;
			fechaCalculada = fechaCalculada.addDays(1);
		}
		 
		for(Integer d : diasActividades.keySet()){

            System.debug('*****Dia:' + d + ', bitacora:' + diasActividades.get(d));
        }
        
        for(Integer d : coloresDias.keySet()){

            System.debug('*****Dia:' + d + ', color:' + coloresDias.get(d));
        }
        
		//System.debug('*****coloresDias:' + coloresDias);
		//System.debug('*****diasActividades:' + diasActividades);		
	}
	
	/*public void obtenerDiasActividades(){
		
		//Se obtienen todas las bitacoras del prospecto
		List<Bitacora__c> bitacorasProspecto = [SELECT Id, Name, CreatedDate, Forma_de_Contacto__c FROM Bitacora__c WHERE Lead__c =: prospecto.Id ORDER BY CreatedDate];
		
		//La fecha de creacion del prospecto es la fecha de inicio, dia 1
		Date fechaCalculada = prospecto.CreatedDate.date();
		Date fechaCreacionBitacora = null;
		Integer contadorDias = 1;
		Integer contadorLlamadas = 0;
		Integer contadorCorreos = 0;
		Integer contadorWhatsapp = 0;
		List<String> listaActividades = new List<String>();
		
		//Se hace un ciclo por cada uno de los dias desde la creacion del prospecto hasta el dia actual
		while(contadorDias <= 10) {			
			
			listaActividades = new List<String>();
			
			//Se recorre cada una de las bitacoras del prospecto
			for(Bitacora__c bitacora : bitacorasProspecto){
				
				fechaCreacionBitacora = bitacora.CreatedDate.date();
				System.debug('*****bitacora.Name:' + bitacora.Name);
				System.debug('*****fechaCalculada:' + fechaCalculada);
				System.debug('*****fechaCreacionBitacora:' + fechaCreacionBitacora);
				
				if(fechaCalculada == fechaCreacionBitacora){
					
					if(bitacora.Forma_de_Contacto__c == 'Llamada'){
						contadorLlamadas++;
						listaActividades.add('Llamada ' + contadorLlamadas);
					}
					else if(bitacora.Forma_de_Contacto__c == 'Correo'){
						contadorCorreos++;
						listaActividades.add('Correo ' + contadorCorreos);
					}
					else if(bitacora.Forma_de_Contacto__c == 'WhatsApp'){
						contadorWhatsapp++;
						listaActividades.add('WhatsApp ' + contadorWhatsapp);
					}
				}
			}
			
			System.debug('*****contadorDias:' + contadorDias);
			System.debug('*****listaActividades:' + listaActividades);
			diasActividades.put(contadorDias,listaActividades);
			contadorDias++;
			fechaCalculada = fechaCalculada.addDays(1);
		}
		
		System.debug('*****diasActividades:' + diasActividades);		
	}*/
}