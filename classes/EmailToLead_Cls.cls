/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        EmailToLead_Cls
* @Autor       Joel Soto
* @Date        05/12/2019
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global class EmailToLead_Cls implements Messaging.InboundEmailHandler {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        05/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env) {
    String emailTextBody;
    emailTextBody = cleanString(email);
    List<Desarrollo_Bdc__mdt> listDesarrollos = [SELECT Label, ApiWeb__c FROM Desarrollo_Bdc__mdt WHERE ActivoNicho__c = true];
    Map<String, String> mapDesarrollos = new Map<String, String>();
    mapDesarrollos = getMapDes(listDesarrollos);
    List<Bg_Nicho__mdt> listNichos = [SELECT EmailEntre__c, EmailY__c, IdNicho__c, Medio__c, NombreEntre__c, NombreY__c, LeadSource__c, TelefonoEntre__c, TelefonoY__c, TestString__c FROM Bg_Nicho__mdt];
    List<Lead> leadsToInsert = new List<Lead>();
    for (Bg_Nicho__mdt nichito : listNichos) {
      if (emailTextBody.contains(nichito.IdNicho__c)) {
        String firstName;
        String lastName;
        String nombreApellido = emailTextBody.substringBetween(nichito.NombreEntre__c, nichito.NombreY__c).trim();
        if(String.isNotBlank(nombreApellido)) {
          String[] nombreApellidoSplit = nombreApellido.split(' ', 2);
          firstName = String.isNotBlank(nombreApellidoSplit.get(0)) ? nombreApellidoSplit.get(0) : 'portal';
          lastName  = nombreApellidoSplit.size() > 1 ? nombreApellidoSplit.get(1) : '[No proporcionado]';
        }
        String emailLead = emailTextBody.substringBetween(nichito.EmailEntre__c, nichito.EmailY__c).trim();
        String phoneLead = emailTextBody.substringBetween(nichito.TelefonoEntre__c, nichito.TelefonoY__c).trim();
        String cleanPhoneLead = cleanPhone(phoneLead);
        String desarrolloWeb;
        for (String desKey:mapDesarrollos.keySet()) {
          desarrolloWeb = emailTextBody.contains(desKey) ? mapDesarrollos.get(desKey) : desarrolloWeb;
        }
        Lead nichoLead = new Lead();
        nichoLead.FirstName = firstName.capitalize();
        nichoLead.LastName = lastName.capitalize();
        nichoLead.Email = emailLead;
        nichoLead.Phone = cleanPhoneLead;
        nichoLead.LeadSource = nichito.LeadSource__c;
        nichoLead.Medio__c = nichito.Medio__c;
        nichoLead.Desarrollo_Web__c = desarrolloWeb;
        nichoLead.Email2lead__c = emailTextBody;
        leadsToInsert.add(nichoLead);
      }
    }
    List<Database.SaveResult> dbSvResults = Database.insert(leadsToInsert, false);
    for (Integer idx = 0; idx < dbSvResults.size(); idx++) {
      Database.SaveResult dbSvResult = dbSvResults.get(idx);
      System.debug(dbSvResult);
    }
    return null;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        05/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String cleanString(Messaging.inboundEmail email) {
    String emailTextBody;
    if (String.isNotBlank(email.plainTextBody)) {
      emailTextBody = email.plainTextBody.stripHtmlTags();
    } else {
      emailTextBody = email.htmlBody.stripHtmlTags();
    }
    emailTextBody = emailTextBody.replaceAll(':', ' ').replaceAll('\\+', ' ');
    emailTextBody = emailTextBody.replaceAll(',', ' ').replaceAll('\\*', ' ');
    emailTextBody = emailTextBody.replaceAll('®', ' ').replaceAll(' ', ' ');
    emailTextBody = emailTextBody.replaceAll('\\n', ' ').replaceAll('�', ' ');
    emailTextBody = emailTextBody.replaceAll('\\s+', ' ');
    emailTextBody = emailTextBody.toLowerCase().trim();
    return emailTextBody;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        05/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String cleanPhone(String phoneLead) {
    String strCleanPhone = null;
    if(String.isNotBlank(phoneLead)) {
      phoneLead = phoneLead.replaceAll('[^0-9]', '');
      phoneLead = '00000' + phoneLead;
      strCleanPhone = phoneLead.right(10);
    }
    return strCleanPhone;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        05/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static Map<String, String> getMapDes(List<Desarrollo_Bdc__mdt> listDesarrollos) {
    Map<String, String> mapDesarrollos = new Map<String, String>();
    for (Desarrollo_Bdc__mdt desa : listDesarrollos) {
      mapDesarrollos.put((desa.Label).toLowerCase(), desa.ApiWeb__c);
    }
    return mapDesarrollos;
  }
}