/**
 * Created by scarlettcastillo on 8/28/18.
 */

global class Test_WS_DropboxMock implements HttpCalloutMock {

    Map<String, Object> calloutMocks = new Map<String, Object>();

    global Test_WS_DropboxMock(Map<String, Object> callouts) {
        this.calloutMocks = callouts;
    }


    global HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);

        System.debug('The callout maps on the mock =>' + calloutMocks);
        System.debug('The endpoint on the mock =>' + req.getEndpoint());

        if(calloutMocks.containsKey(req.getEndpoint())) {
            Object response = calloutMocks.get(req.getEndpoint());

            System.debug('The content on the mock =>' + calloutMocks.get(req.getEndpoint()));

            if(response instanceof String){

                System.debug('Its a string =>' + response);

                res.setBody((String) response);
                res.setHeader('Content-Type', 'application/json');

            }else if(response instanceof Blob){

                System.debug('Its a blob =>' + response);

                res.setBodyAsBlob((Blob) response);
            }
        }

        System.debug('returned response =>'+ res);
        return res;
    }
}