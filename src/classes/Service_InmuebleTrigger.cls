public without sharing class Service_InmuebleTrigger {
  public static void crearEntrega(List<Inmueble__c> TriggerNew){
    List<Carta_Oferta__c> entregaToInsert = new List<Carta_Oferta__c>();
    for(Inmueble__c inm : TriggerNew){
        Carta_Oferta__c entrega = new Carta_Oferta__c();
        entrega.Inmueble__c = inm.Id;
        entrega.Recibio_Supervision__c = 'NO';
        entregaToInsert.add(entrega);
    }
    if(!entregaToInsert.isEmpty()){
      system.debug(entregaToInsert);
      insert entregaToInsert;
    }
  }
}