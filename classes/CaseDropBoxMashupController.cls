global with sharing class CaseDropBoxMashupController {

	public CaseDropBoxMashupController(ApexPages.StandardController stdController) {

    }

	@RemoteAction
	global static String customSettingInformation(){
		Map<String,DropboxIntegration__c> customSettingRecordMap = new Map<String,DropboxIntegration__c>([SELECT AccessCode__c
		 FROM DropboxIntegration__c where Name='DropBoxIntegration (Perfil)' limit 1]);
		return JSON.serialize(customSettingRecordMap.values());
	}

	@RemoteAction
	global static String getCaseResources(String caseId){
		Map<String,Recursos_Garantias__c> customSettingRecordMap = new Map<String,Recursos_Garantias__c>([SELECT Id,Descripcion__c,Fecha__c,Garantia__c,Id_Enkontrol__c,Name,SystemModstamp,URL__c, Extension__c 
			FROM Recursos_Garantias__c where Garantia__c=:caseId ]);
		return JSON.serialize(customSettingRecordMap.values());
	}
}