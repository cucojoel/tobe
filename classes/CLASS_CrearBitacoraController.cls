public class CLASS_CrearBitacoraController {

    @AuraEnabled
    public static Bitacora__c identificaTipo (String IdRecord){
       	
        List<Lead> lstLeads = new List<Lead>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        
        lstLeads = [SELECT Id, Desarrollo_Comercial__c, Name  FROM Lead WHERE Id =: IdRecord];
        lstOpp = [SELECT Id, Desarrollo__r.Desarrollo_Comercial__c,Name FROM Opportunity WHERE Id =: IdRecord];
        
        integer leads = lstLeads.size();
        integer opps = lstOpp.size();
        
       Bitacora__c record = new Bitacora__c();
        
        if(leads > 0){
           
           record.RecordTypeId = Schema.SObjectType.Bitacora__c.getRecordTypeInfosByName().get('Prospecto').getRecordTypeId();
           String maxInt = (String) [SELECT max(Intento__c) FROM Bitacora__c WHERE Lead__c =: IdRecord].get(0).get('expr0');
            
            if(maxInt != null){
              record.Intento__c = String.valueOf(Integer.valueOf(maxInt) + 1);  
            }else{
              record.Intento__c = '1';
            } 
            record.NombreOppTemp__c = lstLeads.get(0).Name; 
            record.Lead__c = IdRecord;
           record.Desarrollo_Comercial__c = lstLeads.get(0).Desarrollo_Comercial__c;
            
        }else if(opps > 0){
           
           record.RecordTypeId = Schema.SObjectType.Bitacora__c.getRecordTypeInfosByName().get('Oportunidad').getRecordTypeId();
           
           String maxInt = (String) [SELECT max(Intento__c) FROM Bitacora__c WHERE Oportunidad__c =: IdRecord].get(0).get('expr0');
            
            if(maxInt != null){
              record.Intento__c = String.valueOf(Integer.valueOf(maxInt) + 1);  
            }else{
              record.Intento__c = '1';
            }  
            
            
           record.Oportunidad__c = lstOpp.get(0).Id;
           record.NombreOppTemp__c = lstOpp.get(0).Name; 
           record.Desarrollo_Comercial__c = lstOpp.get(0).Desarrollo__r.Desarrollo_Comercial__c;
            
        }
        
        return record;
    }
    
    @AuraEnabled  
    public static List<Map<String, String>> getResultados(String formaDeContacto){
		
        List<Map<String, String>> response = new List<Map<String, String>>();
		System.debug('*****bitacora.Forma_de_Contacto__c: ' + formaDeContacto);
		response.add(new Map<String, String>{'value' => '', 'label' => '--Ninguno--'});
		
        Schema.DescribeFieldResult fieldResult = Bitacora__c.Resultado__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
        String[] arregloFormaContacto;
        String formaContacto = '';
		for(Schema.PicklistEntry f : ple){
			
			System.debug('*****f.getValue(): ' + f.getValue());
			System.debug('*****f.getValue().contains(-): ' + f.getValue().contains('-'));
			if(f.getValue().contains('-')){				
				
				arregloFormaContacto = f.getValue().split('-');
				formaContacto = arregloFormaContacto.get(0);
				System.debug('*****formaContacto: ' + formaContacto);
				if(formaContacto == formaDeContacto){
					System.debug('*****agregado: ' + f.getValue());
                    response.add(new Map<String, String>{'value' => f.getValue(), 'label' => f.getLabel()});
				}
			}
		}
		
        return response;
	}
    
    @AuraEnabled 
    public static List<Map<String, String>> getEstatusNoAgendaCita(String formaDeContacto){
		
        List<Map<String, String>> response = new List<Map<String, String>>();
        response.add(new Map<String, String>{'value' => '', 'label' => '--Ninguno--'});
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Estatus_No_Agenda_Cita__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
        String[] arregloFormaContacto;
        String formaContacto = '';
		for(Schema.PicklistEntry f : ple){
			
			if(f.getValue().contains('-')){
				
				arregloFormaContacto = f.getValue().split('-');
				formaContacto = arregloFormaContacto.get(0);
				System.debug('*****formaContacto: ' + formaContacto);
				
				if(formaContacto == formaDeContacto){
					System.debug('*****agregado: ' + f.getValue());
                    response.add(new Map<String, String>{'value' => f.getValue(), 'label' => f.getLabel()});
				}
			}
			else{
				response.add(new Map<String, String>{'value' => f.getValue(), 'label' => f.getLabel()});
			}
		}
		return response;
	}
    
    
}