/**
 * Created by scarlettcastillo on 8/31/18.
 */

trigger Trigger_Contact on Contact (before insert, before update) {

    if(Trigger.isBefore){
        Handler_ContactTrigger.actualizacionDeEmailEnCuentaPadre(Trigger.new);
    }
}