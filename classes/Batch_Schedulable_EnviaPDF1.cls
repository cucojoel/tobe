global class Batch_Schedulable_EnviaPDF1 implements Schedulable {
	global void execute(SchedulableContext SC) {
		set<Id> cuentasId = new set<Id>();
		list<Account> cuentasObjeto = new list<Account>();
		
		// select Id, AccountId 
		// from Opportunity Where 
		// Account.Edo_Cta_Ejecutado__c = false and 
		// Account.Edo_Cta_EjecutadoU__c = false and 
		// Departamento__r.Estatus__c IN ('VENDIDO','BIENVENIDA','ESCRITURADO') and 
		// Departamento__r.Edo_Cta_Ejecutado__c = true and 
		// AccountId <> null limit 10
		for(Opportunity forData : Database.query(system.label.Batch_Schedulable_EnviaPDF)) {
			cuentasId.add(forData.AccountId);
		}
		
		if(!cuentasId.isEmpty()) {	
			cuentasObjeto = [select Id, Edo_Cta_Ejecutado__c from Account where Id IN : cuentasId];
			for(Account forData : cuentasObjeto) {
				forData.Edo_Cta_Ejecutado__c = true;
			}
			Database.update(cuentasObjeto, false);				// "false" indica que si falla un registro los restantes continuan actualizandose
				
			// Mata el proceso de envio
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDF'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
				
			// Mata el proceso de envio
			list<CronTrigger> ct1 = new list<CronTrigger>();
			ct1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCteX1'];
			if(!ct1.isEmpty()) { System.abortJob(ct1[0].Id); }

			// Mata el proceso de envio
			list<CronTrigger> ct2 = new list<CronTrigger>();
			ct2 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCteX'];
			if(!ct2.isEmpty()) { System.abortJob(ct2[0].Id); }
				
			// Genera proceso BatchNuevo
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_EnviaPDF bsPDF = new Batch_Schedulable_EnviaPDF();
			String job = System.schedule('EnviaPDF', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
		else {
			// Mata el proceso de envio
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDF'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Mata el proceso de envio
			list<CronTrigger> ct1 = new list<CronTrigger>();
			ct1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCteX1'];
			if(!ct1.isEmpty()) { System.abortJob(ct1[0].Id); }

			// Mata el proceso de envio
			list<CronTrigger> ct2 = new list<CronTrigger>();
			ct2 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCteX'];
			if(!ct2.isEmpty()) { System.abortJob(ct2[0].Id); }
			
			// Genera el batch final			
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_KillAll bsPDF = new Batch_Schedulable_KillAll();
			String job = System.schedule('KillAll', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
	}
}