/**
 * Created by scarlettcastillo on 8/31/18.
 */

public with sharing class QueueableService_CaseEnkontrol implements Queueable, Database.AllowsCallouts {
    public final List<Case> casosAMandar;

    @TestVisible
    public boolean hacerCallout = true;

    public QueueableService_CaseEnkontrol(List<Case> casos){
        this.casosAMandar = casos;
    }

    public void execute(QueueableContext context) {
        if(hacerCallout){
           hacerCalloutEnkontrol();
        }
    }

    public void hacerCalloutEnkontrol(){
        // Hacer callout
        Set<Id> casos = new Set<Id>();
        List<Case> casosParaActualizar = new List<Case>();

        //Traer configuración enkontrol
        Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();

        Utils_WS_EnkontrolRequestBuilder.SecurityWrapper securityData = new Utils_WS_EnkontrolRequestBuilder.SecurityWrapper();
        securityData.password = configuracionEnkontrol.Password__c;
        securityData.username = configuracionEnkontrol.Username__c;

        for(Case c : casosAMandar){
            casos.add(c.Id);
        }

        List<Case> casosPorSincronizar = [
                SELECT Id, Entrega__r.Inmueble__r.Etapa__r.Id_Etapa_Enkontrol__c,
                        Entrega__r.Inmueble__r.Name,
                        Folio_Enkontrol__c, CaseNumber,
                (SELECT Id, Name, Id_Falla_WS__c FROM Deficiencias__r)
                FROM Case
                WHERE Id IN :casos
                LIMIT 50
        ];


        System.debug('**** CASOS EN QUEUEABLE ****' + casosPorSincronizar);

        for(Case unCaso : casosPorSincronizar) {

            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(configuracionEnkontrol.Service_Endpoint__c);
            req.setMethod('POST');
            req.setHeader('SOAPAction', Utils_WS_EnkontrolRequestBuilder.SOAPActionFailReportExecute);
            req.setHeader('Content-Type', 'text/xml');

            Utils_WS_EnkontrolRequestBuilder.CasoStatusWrapper enkontrolRequest = new Utils_WS_EnkontrolRequestBuilder.CasoStatusWrapper();

            enkontrolRequest.etapa = unCaso.Entrega__r.Inmueble__r.Etapa__r.Id_Etapa_Enkontrol__c;
            enkontrolRequest.numeroSalesforceCaso = unCaso.CaseNumber;
            enkontrolRequest.folioEnkontrolCaso = String.valueOf(unCaso.Folio_Enkontrol__c);
            enkontrolRequest.inmueble = unCaso.Entrega__r.Inmueble__r.Name;

            enkontrolRequest.fallas = new List<Utils_WS_EnkontrolRequestBuilder.FallaStatusWrapper>();

            for (Detalle_del_Caso__c det : unCaso.Deficiencias__r) {
                Utils_WS_EnkontrolRequestBuilder.FallaStatusWrapper f = new Utils_WS_EnkontrolRequestBuilder.FallaStatusWrapper();
                f.folioSalesforceFalla = det.Name;
                f.idEnkontrolFalla = det.Id_Falla_WS__c;
                enkontrolRequest.fallas.add(f);
            }

            String requestBody = Utils_WS_EnkontrolRequestBuilder.construirCasosRequest(enkontrolRequest, securityData);

            System.debug('**** REQUEST ****' + requestBody);

            req.setBody(requestBody);

            HttpResponse res = http.send(req);

            System.debug('**** RESPONSE ****' + res.getBody());

            // Actualizar caso con el status
            /*
            String result = Utils_WS_EnkontrolRequestBuilder.parseCasosResponse(res);
            Boolean serviceResult = Boolean.valueOf(result);

            System.debug('**** RESULTADO DEL SERVICIO ****' + serviceResult);

            unCaso.Cierre_Sincronizado_En_Enkontrol__c = serviceResult;
            casosParaActualizar.add(unCaso);*/
        }

        if(!casosParaActualizar.isEmpty()){
            update casosParaActualizar;
        }
    }
}