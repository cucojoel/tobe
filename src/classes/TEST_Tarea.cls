@isTest
private class TEST_Tarea {

	private static Desarrollo_Comercial__c desarrolloComercial;
	private static User usuario;
	private static Lead prospecto;
	private static Task cita;
	
	private static void init(){
		
		desarrolloComercial = new Desarrollo_Comercial__c();
		desarrolloComercial.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercial.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);
				
		usuario = new User();
		usuario.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id;
		usuario.Alias = 'usreje';
		usuario.Country = 'México';
		usuario.Email = 'ejecutivo@email.com';
		usuario.EmailEncodingKey = 'UTF-8';
		usuario.LastName = 'ejecutivo';
		usuario.LanguageLocaleKey = 'es_MX';
		usuario.LocaleSidKey = 'es_MX';
		usuario.TimeZoneSidKey = 'America/Mexico_City';
		usuario.UserName='ejecutivo@email.com';
		usuario.Country = 'Mexico';
		
		prospecto = new Lead();
		prospecto.FirstName = 'prospecto';
		prospecto.LastName = 'prospecto';
		prospecto.Email = 'prospecto@test.com';
		
		cita = new Task();
		cita.Subject = 'Cita';
		cita.Fecha_Cita__c = Date.today().addDays(1);
		cita.ActivityDate = Date.today().addDays(1);
		cita.Estatus_de_Visita__c = 'Pendiente';
	}
	
	static testMethod void testCambiaPropietarioProspecto(){
		
		init();
		
		insert desarrolloComercial;
		
		insert usuario;
		
		prospecto.Desarrollo_Comercial__c = desarrolloComercial.Id;
		prospecto.Agente__c = usuario.Id;
		insert prospecto;
		
		cita.WhoId = prospecto.Id;
		cita.OwnerId = usuario.Id;
		insert cita;
		
		cita.Estatus_de_Visita__c = 'No Visitó';
		update cita;
	}
}