global class Service_HistorialdropTrigger {
  public static void crearDropfile(List<Historial_dropbox__c> TriggerNew){
    List<ID> recordIds = new List<ID>();
    for (Historial_dropbox__c historialDropbox : TriggerNew){
      recordIds.add(historialDropbox.Id);
    }
    if(recordIds != null){
      processRecords(recordIds);
    }
  }
  @future (callout=true)
  public static void processRecords(List<ID> recordIds){
    List<Historial_dropbox__c> newListHistDrop = new List<Historial_dropbox__c>();
    List<Historial_dropbox__c> listHistDrop = [SELECT Id, Name, Id_compania__c, Desarrollo__c, Id_prospecto__c, Id_lote__c, Tipo_de_documento__c,Id_attach__c,Url_dropbox__c,Id_oportunidad_enkontrol__c,Url_Salesforce__c
    FROM Historial_dropbox__c
    WHERE Id IN :recordIds];
    Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();
    String body = '<soapenv:Envelope xmlns:beg="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:enk="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">'
    +'<soapenv:Header>'
    +'<Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
    +'<UsernameToken>'
    +'<Username>'+configuracionEnkontrol.Username__c+'</Username>'
    +'<Password>'+configuracionEnkontrol.Password__c+'</Password>'
    +'</UsernameToken>'
    +'</Security>'
    +'</soapenv:Header>'
    +'<soapenv:Body>'
    +'<tem:FileExecute>'
    +'<tem:request>'
    +'<beg:Item>';
    for (Historial_dropbox__c histDrop:listHistDrop){
      DocumentoxEsquema__c cf = new DocumentoxEsquema__c();
      try {
        cf = [SELECT Documento__c,Etapa__c,Id,Name,Nivel__c,Obligatorio__c,Tipo_de_persona__c FROM DocumentoxEsquema__c WHERE Documento__c = :histDrop.Tipo_de_documento__c];
      }
      catch (Exception ex){
        System.debug('fallo');
      }
      Wrapper_DropboxIntegration.Metadata elements =
      Service_DropboxIntegration.crearArchivosDeDropbox(histDrop);
      histDrop.Url_dropbox__c = elements.path_lower;
      histDrop.Id_dropbox__c = elements.id;
      body +='<enk:FileDTO>'
      +'<enk:CompanyId>'+histDrop.Id_compania__c+'</enk:CompanyId>'
      +'<enk:DevelopmentKey>'+histDrop.Desarrollo__c+'</enk:DevelopmentKey>'
      +'<enk:DocumentId>'+cf.Nivel__c+'</enk:DocumentId>'
      +'<enk:LoteId>'+histDrop.Id_lote__c+'</enk:LoteId>'
      +'<enk:Name>'+histDrop.Name+'</enk:Name>'
      +'<enk:Path>'+histDrop.Url_dropbox__c+'</enk:Path>'
      +'<enk:ProspectId>'+histDrop.Id_prospecto__c+'</enk:ProspectId>'
      +'<enk:SaleOpportunityId>'+histDrop.Id_oportunidad_enkontrol__c+'</enk:SaleOpportunityId>'
      +'</enk:FileDTO>';
      histDrop.Sincronizado__c = true;
      newListHistDrop.add(histDrop);
    }
    body +='</beg:Item>'
    +'<beg:Operation>Create</beg:Operation>'
    +'</tem:request>'
    +'</tem:FileExecute>'
    +'</soapenv:Body>'
    +'</soapenv:Envelope>';
    System.debug(body);
    Http http = new Http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc');
    req.setMethod('POST');
    req.setHeader('Content-Type', 'text/xml');
    req.setHeader('SOAPAction', 'http://tempuri.org/IEnKontrolService/FileExecute');
    req.setBody(body);
    HTTPResponse res = http.send(req);
    System.debug(res.getBody());
    if(res.getStatusCode() == 200){
      System.debug(res.getBody());
    }
    else {
      System.debug(res);
    }
    update newListHistDrop;
  }
}