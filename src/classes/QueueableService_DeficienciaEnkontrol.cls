public class QueueableService_DeficienciaEnkontrol implements Queueable, Database.AllowsCallouts {
	
    public List<Detalle_del_Caso__c> ddcAMandar;
    
    @TestVisible
    public boolean ejecutarCallout = true;
    
    public QueueableService_DeficienciaEnkontrol(List<Detalle_del_Caso__c> deficiencias){
        this.ddcAMandar = deficiencias;
    }
    
    public void execute(QueueableContext context) {
        if(ejecutarCallout){
            ejecutarCalloutAEnkontrol();
        }
    }
    
    public void ejecutarCalloutAEnkontrol() {
        Set<Id> ddc = new Set<Id>();
        List<Detalle_del_Caso__c> ddcParaActualizar = new List<Detalle_del_Caso__c>();

        Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();

        Utils_WS_EnkontrolRequestBuilder.SecurityWrapper securityData = new Utils_WS_EnkontrolRequestBuilder.SecurityWrapper();
        securityData.password = configuracionEnkontrol.Password__c;
        securityData.username = configuracionEnkontrol.Username__c;
        securityData.soapOperation = Utils_WS_EnkontrolRequestBuilder.OPERATION_CHANGE_STATUS;

        for (Detalle_del_Caso__c def : ddcAMandar) {
            ddc.add(def.Id);
        }

        List<Detalle_del_Caso__c> ddcPorMandar = [
            SELECT Id, OwnerId, Id_Falla_WS__c, No_Sello__c, Proveedor_Emergente__c, Casos__r.Folio_Enkontrol__c, Name,
            Casos__r.Entrega__r.Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c,
            Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c, Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c,
            Matriz_de_Deficiencias__r.Id_Subespecialidad_Enkontrol__c, Matriz_de_Deficiencias__r.Especialidad__c,
            Matriz_de_Deficiencias__r.Cuadrilla__c, Matriz_de_Deficiencias__r.Proveedor__r.Id_Proveedor_Enkontrol__c,
            Estatus__c, Tipo_Falla__c, Descripcion__c
            FROM Detalle_del_Caso__c
            WHERE Id IN :ddc
            LIMIT 50
        ];
        
        Set<Id> idSet = new Set<Id>();
        for(Detalle_del_Caso__c undc: ddcPorMandar) {
            idSet.add(undc.OwnerId);
        }
        
        Map<Id, String> usuarios = new Map<Id, String>();
        for(User u : [SELECT Id, Id_Enkontrol__c FROM User WHERE Id IN :idSet]){
            usuarios.put(u.Id, u.Id_Enkontrol__c);
        }

        System.debug('**** Deficiencias a mandar a enkontrol ****');

        for (Detalle_del_Caso__c unddc : ddcPorMandar) {

            System.debug('**** Datos de deficiencia ya en queueable ****');
            System.debug('**** Deficiencia ****'+unddc);
            try{
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(configuracionEnkontrol.Service_Endpoint__c);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'text/xml');
                
                String requestBody = '';
                
                WS_EnkontrolIntegration.Fallas deficiencia = new WS_EnkontrolIntegration.Fallas();
                
                //deficiencia.idFalla = unddc.Id_Falla_WS__c;/***/
                deficiencia.idFallaAnterior = unddc.Name;/**ID SF*/
                deficiencia.idDesarrollo = unddc.Casos__r.Entrega__r.Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c;/***/
                //deficiencia.proveedor = unddc.Name;/**Folio*/
                deficiencia.folio = (integer)unddc.Casos__r.Folio_Enkontrol__c;/**Folio*/
                deficiencia.numeroSello = (integer)unddc.No_Sello__c;/***/
                deficiencia.idPiso = usuarios.get(unddc.OwnerId);/**IdEmpleado*/
                deficiencia.idZona = (integer)unddc.Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c;/***/
                deficiencia.Especialidad = unddc.Matriz_de_Deficiencias__r.Especialidad__c;//////////
                deficiencia.idEspecialidad = (integer)unddc.Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c;/***/
                deficiencia.idSubespecialidad = (integer)unddc.Matriz_de_Deficiencias__r.Id_Subespecialidad_Enkontrol__c;///////
                deficiencia.esCuadrilla = unddc.Matriz_de_Deficiencias__r.Cuadrilla__c;/***/
                deficiencia.idProveedor = (integer)unddc.Matriz_de_Deficiencias__r.Proveedor__r.Id_Proveedor_Enkontrol__c;/***/
                deficiencia.estatus = unddc.Estatus__c;/***/
                deficiencia.tipo = unddc.Tipo_Falla__c;/***/
                deficiencia.descripcion = unddc.Descripcion__c;/***/
                deficiencia.esProveedorEmergente = unddc.Proveedor_Emergente__c;/***/
                System.debug('*** Deficiencia ***'+deficiencia);
                req.setHeader('SOAPAction', Utils_WS_EnkontrolRequestBuilder.SOAPActionFailDetailExecute);
                requestBody = Utils_WS_EnkontrolRequestBuilder.construirDeficienciasRequest(deficiencia, securityData);
                
                System.debug('**** REQUEST ****' + requestBody);
                req.setBody(requestBody);
                HttpResponse res = http.send(req);
                System.debug('**** RESPONSE ****' + res.getBody());

                
            }catch(Exception e){
                System.debug('Error ocurrido:'+e);
            }
            
        }
    }
    
}