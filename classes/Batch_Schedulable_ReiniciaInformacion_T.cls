@isTest
public class Batch_Schedulable_ReiniciaInformacion_T {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	static TestMethod void test_01() {
		SFDC_BG_EdoCta_temp__c temporalPDF = new SFDC_BG_EdoCta_temp__c(Name = 'temporalPDF');
		insert temporalPDF;
		
		Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'BGR',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
        
		Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'VENDIDO',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = true,
			Id_Cliente_Enkontrol__c = '5108',
			Id_Prospecto_Enkontrol__c = '15810',
			OportunidadTexto__c = '0062f000001ffTuAAI'
		);
		insert departamento;
		
		Account cliente = new Account(
			FirstName = 'firstname',
			LastName = 'lastname',
			RFC__pc = '1234567890123',
			Edo_Cta_Ejecutado__c = true,
			Edo_Cta_EjecutadoU__c = true,
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId());
		insert cliente;
		
		Test.startTest();
			String jobId = System.schedule('ScheduledApexTest', CRON_EXP, new Batch_Schedulable_ReiniciaInformacion());						
		Test.stopTest();
	}    
}