/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        OpportunityTriggerClass
* @Autor       Joel Soto
* @Date        28/11/2019
*
* @Group       BeGrand
* @Description Se migra clase anterior de oportunidad y se añaden nuevas funciones
* @Changes
*/
public with sharing class OpportunityTriggerClass {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description elimina registros relacionados a la oportunidad
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void eliminaAttachments(Map<Id, Opportunity> oportunidadesEliminadas) {
    Set<Id> idsOportunidadesEliminadas = oportunidadesEliminadas.keySet();
    Set<Id> idsCartasOferta = new Set<Id>();
    for(C_Oferta__c cartaOferta : [SELECT Id, Oportunidad__c FROM C_Oferta__c WHERE Oportunidad__c IN : idsOportunidadesEliminadas]) {
      idsCartasOferta.add(cartaOferta.Id);
    }
    List<Attachment> attachmentsCartaOfertaEliminar = [SELECT Id FROM Attachment WHERE ParentId IN : idsCartasOferta];
    delete attachmentsCartaOfertaEliminar;
    Set<Id> idsCarpetas = new Set<Id>();
    for(Carpeta__c carpeta : [SELECT Id, Oportunidad__c FROM Carpeta__c WHERE Oportunidad__c IN : idsOportunidadesEliminadas]) {
      idsCarpetas.add(carpeta.Id);
    }
    List<Attachment> attachmentsCarpertaEliminar = [SELECT Id FROM Attachment WHERE ParentId IN : idsCarpetas];
    delete attachmentsCarpertaEliminar;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description valida el cambio de etapa segun la configuracion de metadata
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void validaCambioEtapa(List<Opportunity> triggerOpps, Map<Id, Opportunity> mapTriggerOpps) {
    List<TriggerOpp__mdt> listTOpp = [SELECT Id, NombreEtapa__c, NumeroEtapa__c, ValidaDocs__c FROM TriggerOpp__mdt WHERE Activo__c = true ORDER BY NumeroEtapa__c];
    List<Attachment> lstAttachment = [SELECT Id, Name, Description, ParentId FROM Attachment WHERE ParentId in :mapTriggerOpps.keySet()];
    String stageValidate = '';
    Map<String, TriggerOpp__mdt> mapStageNameNum = new Map<String, TriggerOpp__mdt>();
    for(TriggerOpp__mdt custConf : listTOpp) {
      mapStageNameNum.put(custConf.NombreEtapa__c, custConf);
      stageValidate = custConf.ValidaDocs__c == true ? custConf.NombreEtapa__c : stageValidate;
    }
    List<DocumentoxEsquema__c> listDocsEsq = [SELECT Id, Etapa__c, Documento__c, Obligatorio__c, Tipo_de_persona__c FROM DocumentoxEsquema__c];
    Map<String, List<Attachment>> getMapAttachoppid = OpportunityHelper.getMapAttachbyoppid(lstAttachment);
    Map<String, List<DocumentoxEsquema__c>> mapTipoPna = OpportunityHelper.listToMapdocxp(listDocsEsq);
    for(Opportunity opp : triggerOpps) {
      SObject oldOpp = mapTriggerOpps.get(opp.Id);
      if(mapStageNameNum.containsKey(opp.StageName) && mapStageNameNum.containsKey((String)oldOpp.get('StageName'))) {
        TriggerOpp__mdt currentNewConf = mapStageNameNum.get(opp.StageName);
        TriggerOpp__mdt currentOldConf = mapStageNameNum.get((String)oldOpp.get('StageName'));
        if(currentNewConf.NumeroEtapa__c + 1 == currentOldConf.NumeroEtapa__c || currentNewConf.NumeroEtapa__c - 1 == currentOldConf.NumeroEtapa__c) {
          if(opp.StageName == stageValidate ) {
            List<DocumentoxEsquema__c> listDocsEsqxp = mapTipoPna.get(opp.Tipo_de_persona__c);
            List<Attachment> listAttachThis = getMapAttachoppid.get(opp.Id);
            Set<String> setAttachName = OpportunityHelper.getSetOppAttach(listAttachThis);
            Set<String> setConfEtapa = OpportunityHelper.getSetConfEtapa(listDocsEsqxp);
            Boolean isSuccess = OpportunityHelper.validaDocs(setConfEtapa, setAttachName, opp);
          }
        } else {
          opp.StageName.addError('Salto no permitido');
        }
      }
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description manda a enkontrol la actualizacion de cierre
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void sendEnkontrol(List<Opportunity> triggerOpps) {
  OpportunityTriggerQueueable caseJob = new OpportunityTriggerQueueable(triggerOpps);
      Id jobID2 = System.enqueueJob(caseJob);
  }
}