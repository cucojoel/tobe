/**
 * Created by scarlettcastillo on 8/27/18.
 */
public class Wrapper_DropboxIntegration {
  public class DropboxRequest {
    public String path {get; set;}
    public String mode {get; set;}
    public boolean autorename {get; set;}
    public boolean mute {get; set;}
    public boolean strict_conflict {get; set;}
  }
  public class ElementList {
    public List<Metadata> entries {get; set;}
    public String cursor {get; set;}
    public boolean has_more {get; set;}
  }
  public class Metadata {
    public String x_tag {get; set;}
    public String name {get; set;}
    public String path_lower {get; set;}
    public String path_display {get; set;}
    public String id {get; set;}
    public String client_modified {get; set;}
    public String server_modified {get; set;}
    public String rev {get; set;}
    public Integer size {get; set;}
    public String content_hash {get; set;}
  }
  public enum Tag {FOLDER, FILE}
  public class DropboxFile {
    public Metadata metadata;
    public String link;
  }
}