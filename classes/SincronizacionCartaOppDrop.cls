/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        SincronizacionCartaOppDrop
* @Autor       Joel Soto
* @Date        07/01/2020
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global with sharing class SincronizacionCartaOppDrop {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        07/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  @future (callout = true)
  public static void processRecords(Set<String> recordIds) {
    System.debug('log - SincronizacionRecursoOrdenDrop: ' + 'future SincronizacionCartaOppDrop');
    if (!recordIds.isEmpty()) {
      Map<Id, C_Oferta__c> mapCoferta = new Map<Id, C_Oferta__c>();
      List<Attachment> listAttach = new List<Attachment>();
      List<C_Oferta__c> listCoferta = [SELECT Id, URL__c, Sincronizado__c
        FROM C_Oferta__c
        WHERE Sincronizado__c = false AND URL__c != null
        AND Oportunidad__c IN :recordIds];
      for (C_Oferta__c reg : listCoferta) {
        System.debug('log - SincronizacionCartaOppDrop: ' + reg.URL__c);
        mapCoferta.put(reg.Id, reg);
        string[] filenametmp = reg.URL__c.split('/');
        string filename = filenametmp.get(filenametmp.size() - 1);
        String contentType = 'application/pdf';
        if (filename.split('\\.').get(1) == 'jpg') {
          contentType = 'image/jpeg';
        }
        string pathLower = reg.URL__c.toLowerCase();
        Blob file;
        file = Service_DropboxIntegration.descargarArchivosDropbox(filename, pathLower);
        if (file == null) {
          file = Blob.valueOf('Error de carga de datos');
        }
        Attachment attach = new Attachment();
        attach.Body = file;
        attach.Name = filename;
        attach.IsPrivate = false;
        attach.ParentId = reg.Id;
        attach.ContentType = contentType;
        attach.Description = 'false';
        listAttach.add(attach);
      }
      insert listAttach;
      List<C_Oferta__c> listCartasToUp = new List<C_Oferta__c>();
      String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
      for (Attachment attach : listAttach) {
        C_Oferta__c cOferta = new C_Oferta__c();
        cOferta.Id = attach.ParentId;
        cOferta.Url_Salesforce__c = sfdcURL + '/servlet/servlet.FileDownload?file=' + attach.Id;
        cOferta.Sincronizado__c = true;
        listCartasToUp.add(cOferta);
      }
      update listCartasToUp;
    }
  }
}