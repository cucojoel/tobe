@isTest
public class Test_InmuebleTrigger {

    static TestMethod void myUnitTest(){

        List<Account> cuentas = Test_Service.CrearCuenta(1);
        insert cuentas;

        system.assertNotEquals(0, cuentas.size());

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        List<Desarrollo__c> desarrollos = Test_Service.CrearDesarrollo(1);

        Time myTime = Time.newInstance(1, 2, 3, 4);

        for(Desarrollo__c des : desarrollos){
            des.Desarrollo_Comercial__c = desarrolloComercial.Id;
            des.Inicio__c = myTime;
        }

        insert desarrollos;

        system.assertNotEquals(0, desarrollos.size());

        List<Etapa__c> etapas = Test_Service.CrearEtapa(1, desarrollos[0].Id);
        insert etapas;

        system.assertNotEquals(0, etapas.size());

        List<Inmueble__c> inmuebles = Test_Service.CrearInmueble(1, etapas[0].Id);
        insert inmuebles;

        system.assertNotEquals(0, inmuebles.size());

        Test.startTest();

        inmuebles[0].Programar_entrega__c = true;

        update inmuebles;

        Carta_Oferta__c EntregaCreada = [SELECT Id FROM Carta_Oferta__c WHERE Inmueble__c =: inmuebles[0].Id ORDER BY CreatedDate DESC LIMIT 1];

        system.assertNotEquals(null, EntregaCreada);

        Test.stopTest();

    }

}