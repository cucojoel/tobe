@isTest
private class TEST_CrearCita {

	private static Desarrollo_Comercial__c desarrolloComercial;
	private static Lead prospecto;
	private static User usuario;
	private static Guardia__c guardia1;
	private static Guardia__c guardia2;
	private static Task tarea;
	private static Event evento;
	
	private static void init(){
		
		desarrolloComercial = new Desarrollo_Comercial__c();
		desarrolloComercial.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercial.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);
				
		usuario = new User();
		usuario.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id;
		usuario.Alias = 'usreje';
		usuario.Country = 'México';
		usuario.Email = 'ejecutivo@email.com';
		usuario.EmailEncodingKey = 'UTF-8';
		usuario.LastName = 'ejecutivo';
		usuario.LanguageLocaleKey = 'es_MX';
		usuario.LocaleSidKey = 'es_MX';
		usuario.TimeZoneSidKey = 'America/Mexico_City';
		usuario.UserName='ejecutivo@email.com';
		usuario.Country = 'Mexico';
		
		prospecto = new Lead();
		prospecto.FirstName = 'prospecto';
		prospecto.LastName = 'prospecto';
		prospecto.Email = 'prospecto@test.com';
		
		guardia1 = new Guardia__c();
		guardia1.Fecha_Guardia__c = Date.today().addDays(1);
		guardia1.Roll__c = 1;
		
		guardia2 = new Guardia__c();
		guardia2.Fecha_Guardia__c = Date.today().addDays(1);
		guardia2.Roll__c = 2;
		
		tarea = new Task();
		tarea.Fecha_Cita__c = Date.today().addDays(1);
		tarea.ActivityDate = Date.today().addDays(1);
		
		evento = new Event();
		evento.StartDateTime = DateTime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day(), 10, 0, 0);
		evento.EndDateTime = DateTime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day(), 11, 0, 0);
		evento.ActivityDate = Date.today().addDays(1);
		evento.DurationInMinutes = 60;
	}
	
	static testMethod void testBuscarHorariosDisponibles(){
		
		init();
		
		insert desarrolloComercial;
		
		insert usuario;
		
		prospecto.Desarrollo_Comercial__c = desarrolloComercial.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		insert prospecto;
		
		guardia1.Desarrollo_Comercial__c = desarrolloComercial.Id;
		guardia1.Nombre_Asesor__c = usuario.Id;
		insert guardia1;
		
		tarea.OwnerId = usuario.Id;
		insert tarea;
		
		evento.OwnerId = usuario.Id;
		insert evento;
		
		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearCita controller = new CONTROL_CrearCita();
		controller.prospecto.F_CM__c = Date.today().addDays(1);
		controller.buscarHorariosDisponibles();
	}
	
	static testMethod void testGuardar1(){
		
		init();
		
		prospecto.Desarrollo_Comercial__c = desarrolloComercial.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		insert prospecto;
		
		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearCita controller = new CONTROL_CrearCita();
		controller.prospecto.F_CM__c = null;
		controller.horarioSeleccionado = null;
		controller.guardar();
	}
	
	static testMethod void testGuardar2(){
		
		init();
		
		insert desarrolloComercial;
		
		insert usuario;
		
		prospecto.Desarrollo_Comercial__c = desarrolloComercial.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		insert prospecto;
		
		guardia1.Desarrollo_Comercial__c = desarrolloComercial.Id;
		guardia1.Nombre_Asesor__c = usuario.Id;
		insert guardia1;
		
		guardia2.Desarrollo_Comercial__c = desarrolloComercial.Id;
		guardia2.Nombre_Asesor__c = usuario.Id;
		insert guardia2;
		
		tarea.OwnerId = usuario.Id;
		insert tarea;
		
		evento.OwnerId = usuario.Id;
		insert evento;
		
		Set<Id> idsAsesores = new Set<Id>();
		idsAsesores.add(usuario.Id);
		
		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearCita controller = new CONTROL_CrearCita();
		controller.prospecto.F_CM__c = Date.today().addDays(1);
		controller.horarioSeleccionado = '12:00';
		controller.idsAsesores = idsAsesores;
		controller.guardar();
	}
	
	static testMethod void testCancelar(){
		
		init();
		
		insert desarrolloComercial;
		
		insert usuario;
		
		prospecto.Desarrollo_Comercial__c = desarrolloComercial.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
		insert prospecto;
		
		ApexPages.currentPage().getParameters().put('id',prospecto.Id);
		CONTROL_CrearCita controller = new CONTROL_CrearCita();
		controller.cancelar();
	}
}