global class Batch_Batchable_GeneraPDF implements Schedulable {	
	global void execute(SchedulableContext SC) {
		PreparaEstadosCuenta__c tiempoGenerarPDF = [select Id, Name, Inicio_hora__c, Fin_hora__c from PreparaEstadosCuenta__c where Name = 'HoraInicioBatchGeneraPDF'];
		String horaLocal = String.valueOf(system.now());
		String [] diaPartes = horaLocal.split(' ');
		String [] horaPartes = diaPartes[1].split(':');
		// Detecta los pdf en blanco y si la hora de ejecucion sigue siendo valida, borra y procesa de nuevo
		if(Decimal.valueOf(horaPartes[0]) < tiempoGenerarPDF.Fin_hora__c) {
			list<String> oppsEdoCtaNull = new list<String>();
			list<Inmueble__c> departamentos = new list<Inmueble__c>();
			list<Attachment> adjunto = Database.query(system.label.Batch_Batchable_GeneraPDF);
			if(!adjunto.isEmpty()) {
				for(Attachment forData : adjunto) {
					String tmp = forData.Name;
					tmp = tmp.replace('ECtaBG_','');
					tmp = tmp.subString(0,18);
					oppsEdoCtaNull.add(tmp);
				}
			}
			if(!oppsEdoCtaNull.isEmpty()) {
				for(Inmueble__c forData : [select Id, OportunidadTexto__c, Edo_Cta_Ejecutado__c from Inmueble__c where OportunidadTexto__c IN : oppsEdoCtaNull]) {
					forData.Edo_Cta_Ejecutado__c = false;
					departamentos.add(forData);
				}
			}
			if(!departamentos.isEmpty()) { Database.update(departamentos, false); }
			if(!adjunto.isEmpty())       { Database.delete(adjunto, false); }
			
			// Mata el proceso Schedulable
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'GeneraPDFSchedulable'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Genera proceso Schedulable
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_GeneraPDF bsPDF = new Batch_Schedulable_GeneraPDF();
			String job = System.schedule('GeneraPDFSchedulable', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
		else {
			// Mata el ultimo proceso que genero PDF
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'GeneraPDFSchedulable'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Activa el proceso que evaluara PDF es blanco
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_ReportaPDFBlanco bsPDFBlanco = new Batch_Schedulable_ReportaPDFBlanco();
			String job = System.schedule('BorrandoPDFBlanco', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDFBlanco);
		}
	}
}