public with sharing class CerrarDeficienciasController {
    public Case thisCase {get;set;}
    public CerrarDeficienciasController(ApexPages.StandardController stdController){
        thisCase = (Case)stdController.getRecord();
    }
    
    public PageReference updateRecords(){
        System.debug(thisCase);
        List<Detalle_del_Caso__c> detalleCaso = new List<Detalle_del_Caso__c>();
        List<Detalle_del_Caso__c> detalleCasoUpdate = new List<Detalle_del_Caso__c>();
        detalleCaso = [SELECT Id FROM Detalle_del_Caso__c WHERE Estatus__c != 'Reparado' AND Pendiente__c = false AND Casos__c =:thisCase.Id];
        system.debug(detalleCaso);
        for(Detalle_del_Caso__c caso:detalleCaso){
            caso.Estatus__c = 'Reparado';
            detalleCasoUpdate.add(caso);
        }
        system.debug(detalleCasoUpdate);
        if(!detalleCasoUpdate.isEmpty()){
            update detalleCasoUpdate;
        }
        return new ApexPages.StandardController(thisCase).view();
    }
    
}