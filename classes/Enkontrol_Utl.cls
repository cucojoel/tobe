/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Enkontrol_Utl
* @Autor       Joel Soto
* @Date        02/01/2020
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global class Enkontrol_Utl {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static RegistroValidoWrapper checkOpp(Enkontrol_Ws.Oportunidad oppWs) {
    RegistroValidoWrapper validez = new RegistroValidoWrapper();
    validez.valido = true;
    validez.camposFallidos = new List<String>();
    if(oppWs.idEnkontrol == null) {
      validez.valido = false;
      validez.camposFallidos.add('idEnkontrol');
    }
    if(oppWs.idProspecto == null) {
      validez.valido = false;
      validez.camposFallidos.add('idProspecto');
    }
    if(oppWs.compania == null) {
      validez.valido = false;
      validez.camposFallidos.add('compania');
    }
    if(oppWs.agente == null) {
      validez.valido = false;
      validez.camposFallidos.add('agente');
    }
    if(oppWs.desarrollo == null) {
      validez.valido = false;
      validez.camposFallidos.add('desarrollo');
    }
    if (oppWs.dbxUrls == null) {
      oppWs.dbxUrls = new List<Enkontrol_Ws.Documento>();
    }
    if (oppWs.esRenta == null) {
      oppWs.esRenta = false;
    }
    return validez;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static RegistroValidoWrapper checkDepto(Enkontrol_Ws.ActualizarEstatusDepto depto) {
    RegistroValidoWrapper validez = new RegistroValidoWrapper();
    validez.valido = true;
    validez.camposFallidos = new List<String>();
    if(depto.loteId == null) {
      validez.valido = false;
      validez.camposFallidos.add('loteId');
    }
    if(depto.desarrollo == null) {
      validez.valido = false;
      validez.camposFallidos.add('desarrollo');
    }
    return validez;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static RegistroValidoWrapper checkLead(Enkontrol_Ws.Prospecto prospecto) {
    RegistroValidoWrapper validez = new RegistroValidoWrapper();
    validez.valido = true;
    validez.camposFallidos = new List<String>();
    if(prospecto.nombre == null) {
      validez.valido = false;
      validez.camposFallidos.add('nombre');
    }
    if(prospecto.apellidoPaterno == null) {
      validez.valido = false;
      validez.camposFallidos.add('apellidoPaterno');
    }
    if(prospecto.email1 == null) {
      validez.valido = false;
      validez.camposFallidos.add('email1');
    }
    if(prospecto.idEnkontrol == null) {
      validez.valido = false;
      validez.camposFallidos.add('idEnkontrol');
    }
    if(prospecto.agente == null) {
      validez.valido = false;
      validez.camposFallidos.add('agente');
    }
    if(prospecto.puntoProspectacion == null) {
      validez.valido = false;
      validez.camposFallidos.add('puntoProspectacion');
    }
    if(prospecto.desarrollo == null) {
      validez.valido = false;
      validez.camposFallidos.add('desarrollo');
    }
    if(prospecto.compania == null) {
      validez.valido = false;
      validez.camposFallidos.add('compania');
    }

    if(prospecto.personaMoral == null){
      prospecto.personaMoral = false;
    }
    return validez;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        25/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static RegistroValidoWrapper checkCase(Enkontrol_Ws.Caso newCase) {
    RegistroValidoWrapper validez = new RegistroValidoWrapper();
    validez.valido = true;
    validez.camposFallidos = new List<String>();
    if(String.isBlank(newCase.idLote)) {
      validez.valido = false;
      validez.camposFallidos.add('idLote');
    }
    if(String.isBlank(String.valueof(newCase.folio))) {
      validez.valido = false;
      validez.camposFallidos.add('folio');
    }
    if(String.isBlank(newCase.idEtapa)) {
      validez.valido = false;
      validez.camposFallidos.add('idEtapa');
    }
    if(String.isBlank(newCase.idDesarrollo)) {
      validez.valido = false;
      validez.camposFallidos.add('idDesarrollo');
    }
    if(String.isBlank(newCase.idPiso)) {
      validez.valido = false;
      validez.camposFallidos.add('idPiso');
    }
    if(String.isBlank(newCase.idInmueble)) {
      validez.valido = false;
      validez.camposFallidos.add('idInmueble');
    }
    if(newCase.fallas == null){
      newCase.fallas = new List<Enkontrol_Ws.Falla>();
    }
    return validez;
  }
    /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna sfId del registro si encuentra la llave
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       String custKey
  * @Param       Map<String, SObject> mapObj
  * @Return      String sfId
  * @example     validateKeyObj('PL2-01', mapObj)
  **/
  public static String validateKeyObj(String custKey, Map<String, SObject> mapObj) {
    String retKey = mapObj.containsKey(custKey) ? mapObj.get(custKey).Id : null;
    return retKey;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String validateKeyObjDec(Decimal custKey, Map<Decimal, SObject> mapObj) {
    String retKey = mapObj.containsKey(custKey) ? mapObj.get(custKey).Id : null;
    return retKey;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static SObject validateKeyDesa(String custKey, Map<String, SObject> mapObj) {
    SObject retKey = mapObj.containsKey(custKey) ? mapObj.get(custKey) : null;
    return retKey;
  }
    /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        02/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String procesErrorDb(List<Database.Error> listErrors) {
    String errorMsg = '';
    if(!listErrors.isEmpty()) {
      for (Database.Error dbError : listErrors) {
        errorMsg += dbError.getMessage() + ' - ';
      }
      errorMsg = errorMsg.removeEnd(' - ');
    }
    return errorMsg;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        02/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String procesErrorField(List<String> listCampos) {
    String errorMsg = 'Campo(s) requerido(s): ';
    for (String campo : listCampos) {
      errorMsg += campo + ', ';
    }
    errorMsg = errorMsg.removeEnd(', ');
    return errorMsg;
  }
}