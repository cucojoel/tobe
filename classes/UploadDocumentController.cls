public with sharing class UploadDocumentController {
  public List<CustomAttachment> lstCustomAttachment {get;set;}
  public String objId {get;set;}
  public String esquema {get;set;}
  public String tipoPersona {get;set;}
  public List<customStage> lstCustomStage {get;set;}
  public Opportunity mainOpp {get;set;}
  public UploadDocumentController(ApexPages.StandardController stdController)
  {
    this.mainOpp = (Opportunity)stdController.getRecord();
    System.debug('*****this.mainOpp:' + this.mainOpp);
    if(mainOpp.Id != null) {
      mainOpp = [
                 SELECT Id, Name, StageName, Esquema_Financiamiento__c, Tipo_de_persona__c
                 FROM Opportunity
                 WHERE Id =: mainOpp.Id
      ];
    } else   {
      Id idOpp = ApexPages.currentPage().getParameters().get('Id');
      System.debug('*****idOpp:' + idOpp);
      mainOpp = [
                 SELECT Id, Name, StageName, Esquema_Financiamiento__c, Tipo_de_persona__c
                 FROM Opportunity
                 WHERE Id =: idOpp
      ];
    }
    objId = mainOpp.Id;
    esquema = mainOpp.StageName;
    tipoPersona = mainOpp.Tipo_de_persona__c;
    cargarDocumentos();
  }
  public void cargarDocumentos()
  {
    //Obtenemos todos los documento por esquema que se requieren en esta oportunidad
    List<DocumentoxEsquema__c> lstDocumentoEsquema = [
                                                      SELECT Id, Name, Documento__c, Etapa__c, Obligatorio__c, Esquema_financiero__c, Tipo_de_persona__c
                                                      FROM DocumentoxEsquema__c
                                                      WHERE Tipo_de_persona__c =:tipoPersona
                                                                                 ORDER BY Nivel__c
    ];
    List<Attachment> lstAttachment = [
                                      SELECT Id,Name,Description
                                      FROM Attachment WHERE ParentId =: objId
    ];
    List<String> lstEtapas = new List<String>();
    Set<string> setEtapas = new Set<string>();
    //creamos una lista de customStage
    lstCustomStage = new  List<customStage>();
    lstCustomAttachment = new List<CustomAttachment>();
    for(DocumentoxEsquema__c docEsquema: lstDocumentoEsquema) {
      setEtapas.add( docEsquema.Etapa__c );
    }
    lstEtapas.addAll( setEtapas );
    for(String etapa: lstEtapas) {
      customStage customstage = new customStage();
      customstage.stage = etapa;
      customStage.lstCustomAttachment = new List<CustomAttachment>();
      lstCustomStage.add(customstage);
    }
    Map<String,Attachment> mapAttachment = new Map<String,Attachment>();
    if(lstAttachment != null && lstAttachment.size() > 0) {
      for(Attachment attchment:lstAttachment) {
        String strName = attchment.Name;
        String documentName = strName.split('\\.')[0];
        mapAttachment.put( documentName,attchment );
      }
    }
    for(customStage customStageTemp: lstCustomStage) {
      for(DocumentoxEsquema__c documentoEsquema:lstDocumentoEsquema) {
        if( documentoEsquema.Etapa__c == customStageTemp.stage || customStageTemp.stage == 'Vendido'){
        CustomAttachment customAttachment = new CustomAttachment();
        if(lstAttachment != null && lstAttachment.size() > 0) {
          if( mapAttachment.containsKey( documentoEsquema.Documento__c )) {
            customAttachment.uploaded = true;
            customAttachment.attachmentId = mapAttachment.get( documentoEsquema.Documento__c ).Id;
              if(mapAttachment.get( documentoEsquema.Documento__c ).Description.indexOf(';') != -1){
                  if(mapAttachment.get( documentoEsquema.Documento__c ).Description.split(';').size() > 2){
                      customAttachment.Comentarios = mapAttachment.get( documentoEsquema.Documento__c ).Description.split(';').get(2);
                  }
                  
            	
                customAttachment.estatus = mapAttachment.get( documentoEsquema.Documento__c ).Description.split(';').get(1);
            	customAttachment.Descrip = mapAttachment.get( documentoEsquema.Documento__c ).Description.split(';').get(0);
              }else{
                customAttachment.Descrip = mapAttachment.get( documentoEsquema.Documento__c ).Description;
              }
          } else  {
            customAttachment.uploaded = false;
          }
        } else  {
          customAttachment.uploaded = false;
        }
        customAttachment.documentoPorEsquema = documentoEsquema;
        customStageTemp.lstCustomAttachment.add(customAttachment);
        }
      }
    }
    //return null;
    for(customStage cs : lstCustomStage) {
      for(CustomAttachment ca : cs.lstCustomAttachment) {
        System.debug('****ca.uploaded:' + ca.uploaded);
        System.debug('****ca.attachmentId:' + ca.attachmentId);
        System.debug('****ca.documentoPorEsquema:' + ca.documentoPorEsquema);
      }
    }
  }
  public class customStage {
    public String stage {get;set;}
    public List<CustomAttachment> lstCustomAttachment {get;set;}
  }
  public class CustomAttachment {
    public Boolean uploaded {get;set;}
    public String attachmentId {get;set;}
    public DocumentoxEsquema__c documentoPorEsquema {get;set;}
    public String estatus {get;set;}
    public String comentarios {get;set;}
    public String Descrip {get;set;}
  }
  public void createObjectClass()
  {
    String name;
    String objectId;
    Boolean isActive;
    Integer amount;
    String language;
    String request;
    Boolean requestValid;
    String idRequest;
    Integer requestAttempts;
    String requestResponse;
    String requestBody;
    String requestParameter;
    Id idUserRequest;
    String messageSuccess;
    String messageFailure;
    Integer enkontrolId;
    String stage;
    String operation;
    Id idProspect;
    String prospectAgent;
    String prospectCompany;
    String prospectDes;
    String prospectEmail;
    String prospectName;
    String prospectGender;
    String prospectPhone;
    Integer prospectIdEnkontrol;
    String prospectMoral;
    String prospectLastName;
    String prospectPuntoProspectacion;
    Id prospectIdSalesforce;
    String prospectFirstName;
    String prospectStreet;
    String prospectBirthdate;
    String prospectCity;
    String prospectCountry;
    String prospectColony;
    String prospectEmail2;
    String prospectNacionality;
    String prospectPhone2;
    String prospectPhone3;
    Integer prospectZipCode;
    String prospectRFC;
    String prospectState;
    String origin;
    Integer errorCode;
    String errorMessage;
    Integer successCode;
    String successMessage;
    String opportunity;
    Id opportunityId;
    Date createdDate;
    String description;
    Boolean delivery;
    String source;
    String appartment;
    Id idAppartment;
    String assignatedToName;
    Id assignatedToId;
    String ownerName;
    Id owner;
    Id externalId;
    String reference;
    Date requestDate;
    Id parentId;
    Decimal actualCost;
    String typeRequest;
    String idDropbox;
    String specialTerms;
    String contractTerm;
    Id accountId;
    Date deadLine;
    Integer access;
    String zone;
    Integer maxTime;
    Integer minTime;
    Integer averageTime;
    Integer duration;
    String contractNumber;
    String title;
    Id contractId;
    String development;
    Boolean completed;
    String nextStep;
    Integer probability;
    String subStatus;
    String bill;
    Date activatedDate;
    Integer orderReferenceNumber;
    Id originalOrderId;
    Integer discount;
    Date expirationDate;
    String quoteToName;
    Boolean isSync;
    String alias;
    String divisionName;
    String signature;
    Id managerId;
    Id userProfileId;
    Id managerProfileId;
    String name0;
    name = (String.isBlank(name) == true ? null : name);
    String objectId0;
  }
}