/**
 * Created by scarlettcastillo on 8/31/18.
 */

@isTest
public with sharing class Test_ContactTrigger {
    public static testMethod void testContactTrigger(){
        Account unProveedor = Test_Utilities.crearProveedorPrueba(1293, 'Test Proveedor');
        Contact unContacto = Test_Utilities.crearContactoDeProveedorPrueba(unProveedor.Id);

        unContacto.Contacto_Notificacion__c = true;

        Test.startTest();

        insert unContacto;

        Test.stopTest();
    }
}