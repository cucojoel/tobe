public with sharing class LC_Cerrar_Deficiencias_Cls {
	@AuraEnabled
    public static List<Detalle_del_Caso__c> getDeficiencias(String sfId){
        List<Detalle_del_Caso__c> detalleCaso = new List<Detalle_del_Caso__c>();
        detalleCaso = [SELECT Id, Name FROM Detalle_del_Caso__c WHERE Casos__c =:sfId];
        system.debug(detalleCaso);
        return detalleCaso;
    }
}