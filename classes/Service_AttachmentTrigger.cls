public with sharing class Service_AttachmentTrigger {
  public static void crearHistorialDrop(List<Attachment> TriggerNew){
    List<Historial_dropbox__c> listhistorialDropbox = new List<Historial_dropbox__c>();
    for(Attachment attachment : TriggerNew){
      try{
        Opportunity oppExist = [Select Id From Opportunity Where Id = : attachment.ParentId];
        string[] tipoDocumentotmp = attachment.Name.split('\\.');
        string tipoDocumento = tipoDocumentotmp.get(0);
        String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
        Historial_dropbox__c historialDropbox = new Historial_dropbox__c();
        historialDropbox.Name = attachment.Name;
        historialDropbox.Id_attach__c = attachment.Id;
        historialDropbox.Oportunidad__c = attachment.ParentId;
        historialDropbox.Tipo_de_documento__c = tipoDocumento;
        historialDropbox.Url_Salesforce__c = sfdcURL+'/servlet/servlet.FileDownload?file='+attachment.Id;
        listhistorialDropbox.add(historialDropbox);
      }
      catch(Exception ex){
        system.debug('Errores aqui');
        system.debug(ex.getMessage());
        system.debug(listhistorialDropbox);
      }
    }
    if  (listhistorialDropbox != null) {
      insert listhistorialDropbox;
    }
  }

  public void createObjectClass(){
    String name;
    String objectId;
    Boolean isActive;
    Integer amount;
    String language;
    String request;
    Boolean requestValid;
    String idRequest;
    Integer requestAttempts;
    String requestResponse;
    String requestBody;
    String requestParameter;
    Id idUserRequest;
    String messageSuccess;
    String messageFailure;
    Integer enkontrolId;
    String stage;
    String operation;
    Id idProspect;
    String prospectAgent;
    String prospectCompany;
    String prospectDes;
    String prospectEmail;
    String prospectName;
    String prospectGender;
    String prospectPhone;
    Integer prospectIdEnkontrol;
    String prospectMoral;
    String prospectLastName;
    String prospectPuntoProspectacion;
    Id prospectIdSalesforce;
    String prospectFirstName;
    String prospectStreet;
    String prospectBirthdate;
    String prospectCity;
    String prospectCountry;
    String prospectColony;
    String prospectEmail2;
    String prospectNacionality;
    String prospectPhone2;
    String prospectPhone3;
    Integer prospectZipCode;
    String prospectRFC;
    String prospectState;
    String origin;
    Integer errorCode;
    String errorMessage;
    Integer successCode;
    String successMessage;
    String opportunity;
    Id opportunityId;
    Date createdDate;
    String description;
    Boolean delivery;
    String source;
    String appartment;
    Id idAppartment;
    String assignatedToName;
    Id assignatedToId;
    String ownerName;
    Id owner;
    Id externalId;
    String reference;
    Date requestDate;
    Id parentId;
    Decimal actualCost;
    String typeRequest;
    String idDropbox;
    String specialTerms;
    String contractTerm;
    Id accountId;
    Date deadLine;
    Integer access;
    String zone;
    Integer maxTime;
    Integer minTime;
    Integer averageTime;
    Integer duration;
    String contractNumber;
    String title;
    Id contractId;
    String development;
    Boolean completed;
    String nextStep;
    Integer probability;
    String subStatus;
    String bill;
    Date activatedDate;
    Integer orderReferenceNumber;
    Id originalOrderId;
    Integer discount;
    Date expirationDate;
    String quoteToName;
    Boolean isSync;
    String alias;
    String divisionName;
    String signature;
    Id managerId;
    Id userProfileId;
    Id managerProfileId;
    String name0;
    name = (String.isBlank(name) == true ? null : name);
    String objectId0;
  }
}