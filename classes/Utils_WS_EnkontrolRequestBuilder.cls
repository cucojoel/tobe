/**
* ----------------------------------------------------------------------------------------------------------------------
* @NameBEG_NAMESPACE      Utils_WS_EnkontrolRequestBuilder
* @Autor       Joel Soto
* @Date        10/01/2020
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
public class Utils_WS_EnkontrolRequestBuilder {
  public static final String SOAP_ENVELOPE = 'http://schemas.xmlsoap.org/soap/envelope/';
  public static final String TEM_NAMESPACE = 'http://tempuri.org/';
  public static final String BEG_NAMESPACE = 'http://schemas.datacontract.org/2004/07/BeGrand.Common.Services';
  public static final String ENK_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO';
  public static final String XMLNS_NAMESPACE = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
  public static final String SOAPACTIONBASICPROSPECTEXECUTE = 'http://tempuri.org/IEnKontrolService/BasicProspectExecute';
  public static final String SOAPACTIONPROSPECTEXECUTE = 'http://tempuri.org/IEnKontrolService/ProspectExecute';
  public static final String SOAPACTION_CLOSEDPROSPECTEXECUTE = 'http://tempuri.org/IEnKontrolM22Service/ClosedProspectExecute';
  public static final String PASSWORD_TYPE = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';
  public static final String TEM_PREFIX = 'tem';
  public static final String BEG_PREFIX = 'beg';
  public static final String ENK_PREFIX = 'enk';
  public static final String SOAPENV_PREFIX = 'soapenv';
  public static final String OPERATION_CREATE = 'Create';
  public static final String OPERATION_UPDATE = 'Update';
  public static final String OPERATION_CHANGE_STATUS = 'ChangeStatus';
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String construirProspectosRequestCompleto(WS_EnkontrolIntegration.Prospectos prospecto, SecurityWrapper securityData) {
    Dom.Document doc = new Dom.Document();
    dom.XmlNode envelope = doc.createRootElement('Envelope', SOAP_ENVELOPE, SOAPENV_PREFIX);
    envelope.setNamespace(TEM_PREFIX, TEM_NAMESPACE);
    envelope.setNamespace(BEG_PREFIX, BEG_NAMESPACE);
    envelope.setNamespace(ENK_PREFIX, ENK_NAMESPACE);
    dom.XmlNode header = envelope.addChildElement('Header', SOAP_ENVELOPE, SOAPENV_PREFIX);
    dom.XmlNode body = envelope.addChildElement('Body', SOAP_ENVELOPE, SOAPENV_PREFIX);
    dom.XmlNode secNode = header.addChildElement('Security', null, null);
    secNode.setAttribute('xmlns', XMLNS_NAMESPACE);
    dom.XmlNode usernameToken = secNode.addChildElement('UsernameToken', null, null);
    usernameToken.addChildElement('Username', null, null).addTextNode(securityData.username);
    usernameToken.addChildElement('Password', null, null).addTextNode(securityData.password);
    dom.XmlNode requestNode = body
      .addChildElement('ProspectExecute', TEM_NAMESPACE, TEM_PREFIX)
      .addChildElement('request', TEM_NAMESPACE, TEM_PREFIX);
    dom.XmlNode item = requestNode.addChildElement('Item', BEG_NAMESPACE, BEG_PREFIX);
    item.addChildElement('AgentKey', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(String.valueOf(prospecto.agente)));
    item.addChildElement('CompanyId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.compania));
    item.addChildElement('DevelopmentKey', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.desarrollo));
    item.addChildElement('Email', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.email1));
    item.addChildElement('FirstName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.nombre));
    if (String.isNotBlank(prospecto.sexo)) item.addChildElement('Gender', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.sexo));
    if (String.isNotBlank(prospecto.telefono1)) item.addChildElement('HomePhoneNumber', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.telefono1));
    item.addChildElement('Id', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(String.valueof(prospecto.idEnkontrol)));
    item.addChildElement('IsMoralPerson', ENK_NAMESPACE, ENK_PREFIX).addTextNode(String.valueof(prospecto.personaMoral));
    item.addChildElement('LastName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.apellidoPaterno));
    item.addChildElement('Medio', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.medio));
    if (prospecto.personaMoral) {
      if (prospecto.empresa != null) item.addChildElement('ProspectCompanyName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(prospecto.empresa);
    }
    item.addChildElement('ProspectingTypeId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(String.valueOf(prospecto.puntoProspectacion)));
    item.addChildElement('SalesForceId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.idSalesforce));
    item.addChildElement('SecondLastName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.apellidoMaterno));
    item.addChildElement('UserId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.userId));
    item.addChildElement('Address', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.calleYNumero));
    if (String.isNotBlank(prospecto.fechaDeNacimiento)) item.addChildElement('BirthDate', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.fechaDeNacimiento));
    itemComplement(item, prospecto);
    dom.XmlNode operation = requestNode.addChildElement('Operation', BEG_NAMESPACE, BEG_PREFIX).addTextNode(securityData.soapOperation);
    return doc.toXmlString();
  }
  public static dom.XmlNode itemComplement(dom.XmlNode item, WS_EnkontrolIntegration.Prospectos prospecto) {
    item.addChildElement('City', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.ciudad));
    item.addChildElement('Country', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.pais));
    item.addChildElement('District', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.colonia));
    item.addChildElement('Email2', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.email2));
    item.addChildElement('IdEstatus', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.idEstatus));
    item.addChildElement('Nationality', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.nacionalidad));
    item.addChildElement('OfficePhoneNumber', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.telefono2));
    item.addChildElement('OtherPhoneNumber', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.telefono3));
    item.addChildElement('PostalCode', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.cp));
    item.addChildElement('RFC', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.rfc));
    item.addChildElement('State', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.estado));
    return item;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String construirProspectosRequestBasico(WS_EnkontrolIntegration.Prospectos prospecto, SecurityWrapper securityData) {
    Dom.Document doc = new Dom.Document();
    dom.XmlNode envelope = doc.createRootElement('Envelope', SOAP_ENVELOPE, SOAPENV_PREFIX);
    envelope.setNamespace(TEM_PREFIX, TEM_NAMESPACE);
    envelope.setNamespace(BEG_PREFIX, BEG_NAMESPACE);
    envelope.setNamespace(ENK_PREFIX, ENK_NAMESPACE);
    dom.XmlNode header = envelope.addChildElement('Header', SOAP_ENVELOPE, SOAPENV_PREFIX);
    dom.XmlNode body = envelope.addChildElement('Body', SOAP_ENVELOPE, SOAPENV_PREFIX);
    dom.XmlNode secNode = header.addChildElement('Security', null, null);
    secNode.setAttribute('xmlns', XMLNS_NAMESPACE);
    dom.XmlNode usernameToken = secNode.addChildElement('UsernameToken', null, null);
    usernameToken.addChildElement('Username', null, null).addTextNode(securityData.username);
    usernameToken.addChildElement('Password', null, null).addTextNode(securityData.password);
    dom.XmlNode requestNode = body
      .addChildElement('BasicProspectExecute', TEM_NAMESPACE, TEM_PREFIX)
      .addChildElement('request', TEM_NAMESPACE, TEM_PREFIX);
    dom.XmlNode item = requestNode.addChildElement('Item', BEG_NAMESPACE, BEG_PREFIX);
    item.addChildElement('AgentKey', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(String.valueOf(prospecto.agente)));
    item.addChildElement('CompanyId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.compania));
    item.addChildElement('DevelopmentKey', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.desarrollo));
    item.addChildElement('Email', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.email1));
    item.addChildElement('FirstName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.nombre));
    if (String.isNotBlank(prospecto.sexo)) item.addChildElement('Gender', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.sexo));
    if (String.isNotBlank(prospecto.telefono1)) item.addChildElement('HomePhoneNumber', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.telefono1));
    item.addChildElement('Id', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(String.valueOf(prospecto.idEnkontrol)));
    item.addChildElement('IsMoralPerson', ENK_NAMESPACE, ENK_PREFIX).addTextNode(String.valueof(prospecto.personaMoral));
    item.addChildElement('LastName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.apellidoPaterno));
    item.addChildElement('Medio', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.medio));
    if (prospecto.personaMoral) {
      if (prospecto.empresa != null) item.addChildElement('ProspectCompanyName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(prospecto.empresa);
    }
    item.addChildElement('ProspectingTypeId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(String.valueOf(prospecto.puntoProspectacion)));
    item.addChildElement('SalesForceId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.idSalesforce));
    item.addChildElement('SecondLastName', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.apellidoMaterno));
    item.addChildElement('UserId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(Bg_Utils_Cls.validateStringWs(prospecto.userId));
    dom.XmlNode operation = requestNode.addChildElement('Operation', BEG_NAMESPACE, BEG_PREFIX).addTextNode(securityData.soapOperation);
    return doc.toXmlString();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static ProspectResponseWrapper parseProspectosResponse(HttpResponse response) {
    ProspectResponseWrapper responseWrapper = new ProspectResponseWrapper();
    Dom.Document doc = response.getBodyDocument();
    Dom.XmlNode envelope = doc.getRootElement();
    List<Dom.XmlNode> children = envelope.getChildren();
    Dom.XmlNode body = envelope.getChildElement('Body', SOAP_ENVELOPE);
    try {
      if(body.getChildElement('ProspectExecuteResponse', TEM_NAMESPACE) == null) {
        Dom.XmlNode faultNode = body.getChildElement('Fault', SOAP_ENVELOPE);
        String errorMessage = faultNode.getChildElement('faultstring', null).getText();
        responseWrapper.success = false;
        responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
      } else {
        Dom.XmlNode prospectExecute = body.getChildElement('ProspectExecuteResponse', TEM_NAMESPACE);
        Dom.XmlNode prospectExecuteResult = prospectExecute.getChildElement('ProspectExecuteResult', TEM_NAMESPACE);
        String success = prospectExecuteResult.getChildElement('Success', BEG_NAMESPACE).getText();
        responseWrapper.success = Boolean.valueOf(success);
        if (!responseWrapper.success) {
          Dom.XmlNode errorList = prospectExecuteResult.getChildElement('ErrorList', BEG_NAMESPACE);
          Dom.XmlNode errorNode = errorList.getChildElement('ErrorDTO', BEG_NAMESPACE);
          String errorMessage = errorNode.getChildElement('Message', BEG_NAMESPACE).getText();
          responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
        } else {
          Dom.XmlNode listNode = prospectExecuteResult.getChildElement('List', BEG_NAMESPACE);
          Dom.XmlNode prospectDTO = listNode.getChildElement('ProspectDTO', ENK_NAMESPACE);
          String idEnkontrol = prospectDTO.getChildElement('Id', ENK_NAMESPACE).getText();
          String idGp = prospectDTO.getChildElement('IdGroup', ENK_NAMESPACE).getText();
          responseWrapper.enkontrolId = Integer.valueOf(idEnkontrol);
          responseWrapper.idGroup = Integer.valueOf(idGp);
        }
      }
      return responseWrapper;
    } catch (Exception exp) {
      System.debug('log - Utils_WS_EnkontrolRequestBuilder: ' + exp);
      Dom.XmlNode faultNode = body.getChildElement('Fault', SOAP_ENVELOPE);
      String errorMessage = faultNode.getChildElement('faultstring', null).getText();
      responseWrapper.success = false;
      responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
      return responseWrapper;
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static ProspectResponseWrapper parseProspectosBasicResponse(HttpResponse response) {
    ProspectResponseWrapper responseWrapper = new ProspectResponseWrapper();
    Dom.Document doc = response.getBodyDocument();
    Dom.XmlNode envelope = doc.getRootElement();
    Dom.XmlNode body = envelope.getChildElement('Body', SOAP_ENVELOPE);
    try {
      if(body.getChildElement('BasicProspectExecuteResponse', TEM_NAMESPACE) == null) {
        Dom.XmlNode faultNode = body.getChildElement('Fault', SOAP_ENVELOPE);
        String errorMessage = faultNode.getChildElement('faultstring', null).getText();
        responseWrapper.success = false;
        responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
      } else {
        Dom.XmlNode responseExe = body.getChildElement('BasicProspectExecuteResponse', TEM_NAMESPACE);
        Dom.XmlNode resultExe = responseExe.getChildElement('BasicProspectExecuteResult', TEM_NAMESPACE);
        String success = resultExe.getChildElement('Success', BEG_NAMESPACE).getText();
        responseWrapper.success = Boolean.valueOf(success);
        if (!responseWrapper.success) {
          Dom.XmlNode errorList = resultExe.getChildElement('ErrorList', BEG_NAMESPACE);
          Dom.XmlNode errorNode = errorList.getChildElement('ErrorDTO', BEG_NAMESPACE);
          String errorMessage = errorNode.getChildElement('Message', BEG_NAMESPACE).getText();
          responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
        } else {
          Dom.XmlNode listNode = resultExe.getChildElement('List', BEG_NAMESPACE);
          Dom.XmlNode prospectDTO = listNode.getChildElement('ProspectDTO', ENK_NAMESPACE);
          String idEnkontrol = prospectDTO.getChildElement('Id', ENK_NAMESPACE).getText();
          responseWrapper.enkontrolId = Integer.valueOf(idEnkontrol);
          String idGp = prospectDTO.getChildElement('IdGroup', ENK_NAMESPACE).getText();
          responseWrapper.idGroup = Integer.valueOf(idGp);
        }
      }
    }  catch (Exception exp) {
      System.debug('log - Utils_WS_EnkontrolRequestBuilder: ' + exp);
      Dom.XmlNode faultNode = body.getChildElement('Fault', SOAP_ENVELOPE);
      String errorMessage = faultNode.getChildElement('faultstring', null).getText();
      responseWrapper.success = false;
      responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
    }
    return responseWrapper;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static final String ENK1_PREFIX = 'enk1';
  public static final String ENK1_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Request';
  public static String closedProspectExecuteReq(Opportunity oppSf, SecurityWrapper securityData) {
    Dom.Document doc = new Dom.Document();
    dom.XmlNode envelope = doc.createRootElement('Envelope', SOAP_ENVELOPE, SOAPENV_PREFIX);
    envelope.setNamespace(TEM_PREFIX, TEM_NAMESPACE);
    envelope.setNamespace(BEG_PREFIX, BEG_NAMESPACE);
    envelope.setNamespace(ENK_PREFIX, ENK_NAMESPACE);
    dom.XmlNode header = envelope.addChildElement('Header', SOAP_ENVELOPE, SOAPENV_PREFIX);
    dom.XmlNode body = envelope.addChildElement('Body', SOAP_ENVELOPE, SOAPENV_PREFIX);
    dom.XmlNode secNode = header.addChildElement('Security', null, null);
    secNode.setAttribute('xmlns', XMLNS_NAMESPACE);
    dom.XmlNode usernameToken = secNode.addChildElement('UsernameToken', null, null);
    usernameToken.addChildElement('Username', null, null).addTextNode(securityData.username);
    usernameToken.addChildElement('Password', null, null).addTextNode(securityData.password);
    dom.XmlNode requestNode = body.addChildElement('ClosedProspectExecute', TEM_NAMESPACE, TEM_PREFIX).addChildElement('request', TEM_NAMESPACE, TEM_PREFIX);
    dom.XmlNode item = requestNode.addChildElement('Item', BEG_NAMESPACE, BEG_PREFIX);
    dom.XmlNode closedProspectDTO = item.addChildElement('ClosedProspectDTO', ENK_NAMESPACE, ENK_PREFIX);
    closedProspectDTO.addChildElement('Id', ENK_NAMESPACE, ENK_PREFIX).addTextNode(oppSf.Id_Prospecto_Enkontrol__c);
    closedProspectDTO.addChildElement('OportunityId', ENK_NAMESPACE, ENK_PREFIX).addTextNode(oppSf.ID_Oportunidad_Enkontrol__c);
    dom.XmlNode operation = requestNode.addChildElement('Operation', BEG_NAMESPACE, BEG_PREFIX).addTextNode(securityData.soapOperation);
    dom.XmlNode developmentType = requestNode.addChildElement('DevelopmentType', ENK1_NAMESPACE, ENK1_PREFIX).addTextNode('Office');
    dom.XmlNode userId = requestNode.addChildElement('UserId', ENK1_NAMESPACE, ENK1_PREFIX).addTextNode(oppSf.Usuario_Enk__c);
    return doc.toXmlString();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        22/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static ProspectClosedResponseWrapper closedProspectExecuteRes(HttpResponse response) {
    ProspectClosedResponseWrapper responseWrapper = new ProspectClosedResponseWrapper();
    Dom.Document doc = response.getBodyDocument();
    Dom.XMLNode envelope = doc.getRootElement();
    Dom.XmlNode body = envelope.getChildElement('Body', SOAP_ENVELOPE);
    try {
      if(body.getChildElement('ClosedProspectExecuteResponse', TEM_NAMESPACE) == null) {
        Dom.XmlNode faultNode = body.getChildElement('Fault', SOAP_ENVELOPE);
        String errorMessage = faultNode.getChildElement('faultstring', null).getText();
        responseWrapper.success = false;
        responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
      } else {
        Dom.XmlNode responseExe = body.getChildElement('ClosedProspectExecuteResponse', TEM_NAMESPACE);
        Dom.XmlNode resultExe = responseExe.getChildElement('ClosedProspectExecuteResult', TEM_NAMESPACE);
        String success = resultExe.getChildElement('Success', BEG_NAMESPACE).getText();
        responseWrapper.success = Boolean.valueOf(success);
        if (!responseWrapper.success) {
          Dom.XmlNode errorList = resultExe.getChildElement('ErrorList', BEG_NAMESPACE);
          Dom.XmlNode errorNode = errorList.getChildElement('ErrorDTO', BEG_NAMESPACE);
          String errorMessage = errorNode.getChildElement('Message', BEG_NAMESPACE).getText();
          responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
        } else {
          Dom.XmlNode listNode = resultExe.getChildElement('List', BEG_NAMESPACE);
          Dom.XmlNode closedProspectDTO = listNode.getChildElement('ClosedProspectDTO', ENK_NAMESPACE);
          responseWrapper.errorMessage = closedProspectDTO.getChildElement('Error', ENK_NAMESPACE).getText();
          responseWrapper.enkontrolId = closedProspectDTO.getChildElement('Id', ENK_NAMESPACE).getText();
        }
      }
    }  catch (Exception exp) {
      System.debug('log - Utils_WS_EnkontrolRequestBuilder: ' + exp);
      Dom.XmlNode faultNode = body.getChildElement('Fault', SOAP_ENVELOPE);
      String errorMessage = faultNode.getChildElement('faultstring', null).getText();
      responseWrapper.success = false;
      responseWrapper.errorMessage = Bg_Utils_Cls.cleanString(errorMessage);
    }
    return responseWrapper;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        22/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public class CasoStatusWrapper {
    public List<FallaStatusWrapper> fallas {get; set;}
    public String folioEnkontrolCaso {get; set;}
    public String etapa {get; set;}
    public String numeroSalesforceCaso {get; set;}
    public String inmueble {get; set;}
  }
  public class FallaStatusWrapper {
    public String idEnkontrolFalla {get; set;}
    public String folioSalesforceFalla {get; set;}
  }
  public class SecurityWrapper {
    public String username {get; set;}
    public String password {get; set;}
    public String soapOperation {get; set;}
  }
  public class ProspectResponseWrapper {
    public Integer enkontrolId {get; set;}
    public Integer idGroup {get; set;}
    public Boolean success {get; set;}
    public String errorMessage {get; set;}
  }
  public class ProspectClosedResponseWrapper {
    public String enkontrolId {get; set;}
    public Boolean success {get; set;}
    public String errorMessage {get; set;}
  }
  public void createObjectClass() {
    String alias0;
    String alias1;
    String alias2;
    String alias3;
    String alias4;
    String alias5;
    String alias6;
    String alias7;
    String appartment0;
    String appartment1;
    String appartment2;
    String appartment3;
    String appartment4;
    String appartment5;
    String appartment6;
    String appartment7;
    String assignatedToName0;
    String assignatedToName1;
    String assignatedToName2;
    String assignatedToName3;
    String assignatedToName4;
    String assignatedToName5;
    String assignatedToName6;
    String assignatedToName7;
    String bill0;
    String bill1;
    String bill2;
    String bill3;
    String bill4;
    String bill5;
    String bill6;
    String bill7;
    String contractNumber0;
    String contractNumber1;
    String contractNumber2;
    String contractNumber3;
    String contractNumber4;
    String contractNumber5;
    String contractNumber6;
    String contractNumber7;
    String contractTerm0;
    String contractTerm1;
    String contractTerm2;
    String contractTerm3;
    String contractTerm4;
    String contractTerm5;
    String contractTerm6;
    String contractTerm7;
    String description0;
    String description1;
    String description2;
    String description3;
    String description4;
    String description5;
    String description6;
    String description7;
    String development0;
    String development1;
    String development2;
    String development3;
    String development4;
    String development5;
    String development6;
    String development7;
    String divisionName0;
    String divisionName1;
    String divisionName2;
    String divisionName3;
    String divisionName4;
    String divisionName5;
    String divisionName6;
    String divisionName7;
    String errorMessage0;
    String errorMessage1;
    String errorMessage2;
    String errorMessage3;
    String errorMessage4;
    String errorMessage5;
    String errorMessage6;
    String errorMessage7;
    String idDropbox0;
    String idDropbox1;
    String idDropbox2;
    String idDropbox3;
    String idDropbox4;
    String idDropbox5;
    String idDropbox6;
    String idDropbox7;
    String idRequest0;
    String idRequest1;
    String idRequest2;
    String idRequest3;
    String idRequest4;
    String idRequest5;
    String idRequest6;
    String idRequest7;
    String language0;
    String language1;
    String language2;
    String language3;
    String language4;
    String language5;
    String language6;
    String language7;
    String messageFailure0;
    String messageFailure1;
    String messageFailure2;
    String messageFailure3;
    String messageFailure4;
    String messageFailure5;
    String messageFailure6;
    String messageFailure7;
    String messageSuccess0;
    String messageSuccess1;
    String messageSuccess2;
    String messageSuccess3;
    String messageSuccess4;
    String messageSuccess5;
    String messageSuccess6;
    String messageSuccess7;
    String name0;
    String name1;
    String name2;
    String name3;
    String name4;
    String name5;
    String name6;
    String name7;
    String nextStep0;
    String nextStep1;
    String nextStep2;
    String nextStep3;
    String nextStep4;
    String nextStep5;
    String nextStep6;
    String nextStep7;
    String objectId0;
    String objectId1;
    String objectId2;
    String objectId3;
    String objectId4;
    String objectId5;
    String objectId6;
    String objectId7;
    String operation0;
    String operation1;
    String operation2;
    String operation3;
    String operation4;
    String operation5;
    String operation6;
    String operation7;
    String opportunity0;
    String opportunity1;
    String opportunity2;
    String opportunity3;
    String opportunity4;
    String opportunity5;
    String opportunity6;
    String opportunity7;
    String origin0;
    String origin1;
    String origin2;
    String origin3;
    String origin4;
    String origin5;
    String origin6;
    String origin7;
    String ownerName0;
    String ownerName1;
    String ownerName2;
    String ownerName3;
    String ownerName4;
    String ownerName5;
    String ownerName6;
    String ownerName7;
    String prospectAgent0;
    String prospectAgent1;
    String prospectAgent2;
    String prospectAgent3;
    String prospectAgent4;
    String prospectAgent5;
    String prospectAgent6;
    String prospectAgent7;
    String prospectBirthdate0;
    String prospectBirthdate1;
    String prospectBirthdate2;
    String prospectBirthdate3;
    String prospectBirthdate4;
    String prospectBirthdate5;
    String prospectBirthdate6;
    String prospectBirthdate7;
    String prospectCity0;
    String prospectCity1;
    String prospectCity2;
    String prospectCity3;
    String prospectCity4;
    String prospectCity5;
    String prospectCity6;
    String prospectCity7;
    String prospectColony0;
    String prospectColony1;
    String prospectColony2;
    String prospectColony3;
    String prospectColony4;
    String prospectColony5;
    String prospectColony6;
    String prospectColony7;
    String prospectCompany0;
    String prospectCompany1;
    String prospectCompany2;
    String prospectCompany3;
    String prospectCompany4;
    String prospectCompany5;
    String prospectCompany6;
    String prospectCompany7;
    String prospectCountry0;
    String prospectCountry1;
    String prospectCountry2;
    String prospectCountry3;
    String prospectCountry4;
    String prospectCountry5;
    String prospectCountry6;
    String prospectCountry7;
    String prospectDes0;
    String prospectDes1;
    String prospectDes2;
    String prospectDes3;
    String prospectDes4;
    String prospectDes5;
    String prospectDes6;
    String prospectDes7;
    String prospectFirstName0;
    String prospectFirstName1;
    String prospectFirstName2;
    String prospectFirstName3;
    String prospectFirstName4;
    String prospectFirstName5;
    String prospectFirstName6;
    String prospectFirstName7;
    String prospectGender0;
    String prospectGender1;
    String prospectGender2;
    String prospectGender3;
    String prospectGender4;
    String prospectGender5;
    String prospectGender6;
    String prospectGender7;
    String prospectLastName0;
    String prospectLastName1;
    String prospectLastName2;
    String prospectLastName3;
    String prospectLastName4;
    String prospectLastName5;
    String prospectLastName6;
    String prospectLastName7;
    String prospectMoral0;
    String prospectMoral1;
    String prospectMoral2;
    String prospectMoral3;
    String prospectMoral4;
    String prospectMoral5;
    String prospectMoral6;
    String prospectMoral7;
    String prospectNacionality0;
    String prospectNacionality1;
    String prospectNacionality2;
    String prospectNacionality3;
    String prospectNacionality4;
    String prospectNacionality5;
    String prospectNacionality6;
    String prospectNacionality7;
    String prospectName0;
    String prospectName1;
    String prospectName2;
    String prospectName3;
    String prospectName4;
    String prospectName5;
    String prospectName6;
    String prospectName7;
    String prospectPhone0;
    String prospectPhone1;
    String prospectPhone2;
    String prospectPhone3;
    String prospectPhone4;
    String prospectPhone5;
    String prospectPhone6;
    String prospectPhone7;
    String prospectPuntoProspectacion0;
    String prospectPuntoProspectacion1;
    String prospectPuntoProspectacion2;
    String prospectPuntoProspectacion3;
    String prospectPuntoProspectacion4;
    String prospectPuntoProspectacion5;
    String prospectPuntoProspectacion6;
    String prospectPuntoProspectacion7;
    String prospectRFC0;
    String prospectRFC1;
    String prospectRFC2;
    String prospectRFC3;
    String prospectRFC4;
    String prospectRFC5;
    String prospectRFC6;
    String prospectRFC7;
    String prospectState0;
    String prospectState1;
    String prospectState2;
    String prospectState3;
    String prospectState4;
    String prospectState5;
    String prospectState6;
    String prospectState7;
    String prospectStreet0;
    String prospectStreet1;
    String prospectStreet2;
    String prospectStreet3;
    String prospectStreet4;
    String prospectStreet5;
    String prospectStreet6;
    String prospectStreet7;
    String quoteToName0;
    String quoteToName1;
    String quoteToName2;
    String quoteToName3;
    String quoteToName4;
    String quoteToName5;
    String quoteToName6;
    String quoteToName7;
    String reference0;
    String reference1;
    String reference2;
    String reference3;
    String reference4;
    String reference5;
    String reference6;
    String reference7;
    String request0;
    String request1;
    String request2;
    String request3;
    String request4;
    String request5;
    String request6;
    String request7;
    String requestBody0;
    String requestBody1;
    String requestBody2;
    String requestBody3;
    String requestBody4;
    String requestBody5;
    String requestBody6;
    String requestBody7;
    String requestParameter0;
    String requestParameter1;
    String requestParameter2;
    String requestParameter3;
    String requestParameter4;
    String requestParameter5;
    String requestParameter6;
    String requestParameter7;
    String requestResponse0;
    String requestResponse1;
    String requestResponse2;
    String requestResponse3;
    String requestResponse4;
    String requestResponse5;
    String requestResponse6;
    String requestResponse7;
    String signature0;
    String signature1;
    String signature2;
    String signature3;
    String signature4;
    String signature5;
    String signature6;
    String signature7;
    String source0;
    String source1;
    String source2;
    String source3;
    String source4;
    String source5;
    String source6;
    String source7;
    String specialTerms0;
    String specialTerms1;
    String specialTerms2;
    String specialTerms3;
    String specialTerms4;
    String specialTerms5;
    String specialTerms6;
    String specialTerms7;
    String stage0;
    String stage1;
    String stage2;
    String stage3;
    String stage4;
    String stage5;
    String stage6;
    String stage7;
    String subStatus0;
    String subStatus1;
    String subStatus2;
    String subStatus3;
    String subStatus4;
    String subStatus5;
    String subStatus6;
    String subStatus7;
    String successMessage0;
    String successMessage1;
    String successMessage2;
    String successMessage3;
    String successMessage4;
    String successMessage5;
    String successMessage6;
    String successMessage7;
    String title0;
    String title1;
    String title2;
    String title3;
    String title4;
    String title5;
    String title6;
    String title7;
    String typeRequest0;
    String typeRequest1;
    String typeRequest2;
    String typeRequest3;
    String typeRequest4;
    String typeRequest5;
    String typeRequest6;
    String typeRequest7;
    String zone0;
    String zone1;
    String zone2;
    String zone3;
    String zone4;
    String zone5;
    String zone6;
    String zone7;
  }
}