@isTest
private class UploadDocumentController_TEST {
  static testMethod void unitTest() {
    //Id devRecordTypeIdFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Fisica').getRecordTypeId(); //->LINEA COMENTADA POR JCS CLOUDMOBILE 211118
    Account cuenta = new Account();
    cuenta.LastName = 'Melquides';
    cuenta.FirstName = 'Marco';
    cuenta.PersonEmail = 'test@gmail.com';
    cuenta.RFC__pc = 'CASJ811111RD9';
    insert cuenta;
    Contact contacto = [SELECT Id FROM Contact WHERE AccountId =: cuenta.Id LIMIT 1];
    Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
    Time myTime = Time.newInstance(1, 2, 3, 4);
    String idEnkontrolDesarrollo = 'AP12';
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Alto Polanco';
    desarrollo.Id_Desarrollo_Enkontrol__c = idEnkontrolDesarrollo;
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Inicio__c = myTime;
    insert desarrollo;
    /*Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'desarrollo';
    //desarrollo.Nombre_corto__c = 'des';
    //desarrollo.Nombre_largo__c = 'desarrollo';
    insert desarrollo;*/
    Opportunity opp = new Opportunity();
    opp.Name = 'test';
    opp.AccountId = cuenta.Id;
    //opp.StageName = 'Separado';
    //opp.Esquema__c = 'Recurso propio';
    opp.CloseDate = Date.today().addDays(10);
    opp.Desarrollo__c = desarrollo.Id;
    opp.StageName = 'Apartado';
    insert opp;
    DocumentoxEsquema__c documentoEsquTest = new DocumentoxEsquema__c();
    documentoEsquTest.Documento__c = 'Test';
    //documentoEsquTest.Esquema_financiero__c = 'Recurso propio';
    documentoEsquTest.Name = 'AB01';
    documentoEsquTest.Tipo_de_persona__c = 'Persona Moral';
    documentoEsquTest.Etapa__c = 'Apartado';
    insert documentoEsquTest;
    DocumentoxEsquema__c documentoEsquTest2 = new DocumentoxEsquema__c();
    documentoEsquTest2.Documento__c = 'Test2';
    //documentoEsquTest2.Esquema_financiero__c = 'Recurso propio';
    documentoEsquTest2.Name = 'AB02';
    documentoEsquTest2.Tipo_de_persona__c = 'Persona Física';
    documentoEsquTest2.Etapa__c = 'Apartado';
    insert documentoEsquTest2;
    List<DocumentoxEsquema__c> listDocumentoxEsquema = new List<DocumentoxEsquema__c>();
    listDocumentoxEsquema.add(documentoEsquTest);
    listDocumentoxEsquema.add(documentoEsquTest2);
    ApexPages.currentPage().getParameters().put('objId',opp.Id);
    ApexPages.currentPage().getParameters().put('stage','Apartado');
    Attachment documentoPrevio = new Attachment();
    documentoPrevio.Body = Blob.valueOf('Some Text');
    documentoPrevio.ParentId = opp.Id;
    documentoPrevio.Name = 'Test';
    //documentoPrevio.Description = 'true';
    insert documentoPrevio;
    /*
    UploadDocumentController.CustomAttachment customAttachment = new UploadDocumentController.CustomAttachment();
    customAttachment.uploaded = true;
    customAttachment.attachmentId = documentoPrevio.Id;
    customAttachment.documentoPorEsquema = documentoEsquTest;
    List<UploadDocumentController.CustomAttachment> listdocumentoPrevio = new List<UploadDocumentController.CustomAttachment>();
    listdocumentoPrevio.add(customAttachment);
    UploadDocumentController.customStage customStage = new UploadDocumentController.customStage();
    customStage.stage = 'stage';
    customStage.lstCustomAttachment = listdocumentoPrevio;
    */
    ApexPages.StandardController sc = new ApexPages.StandardController( opp );
    UploadDocumentController upcontroller = new UploadDocumentController(sc);
  }
  static testMethod void unitTest2() {
    //Id devRecordTypeIdFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Fisica').getRecordTypeId(); //->LINEA COMENTADA POR JCS CLOUDMOBILE 211118
    Account cuenta = new Account();
    cuenta.LastName = 'Melquides';
    cuenta.FirstName = 'Marco';
    cuenta.PersonEmail = 'test@gmail.com';
    cuenta.RFC__pc = 'CASJ811111RD9';
    insert cuenta;
    Contact contacto = [SELECT Id FROM Contact WHERE AccountId =: cuenta.Id LIMIT 1];
    Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
    Time myTime = Time.newInstance(1, 2, 3, 4);
    String idEnkontrolDesarrollo = 'AP12';
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Alto Polanco';
    desarrollo.Id_Desarrollo_Enkontrol__c = idEnkontrolDesarrollo;
    desarrollo.Inicio__c = myTime;
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    insert desarrollo;
    /*Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'desarrollo';
    //desarrollo.Nombre_corto__c = 'des';
    //desarrollo.Nombre_largo__c = 'desarrollo';
    insert desarrollo;*/
    Opportunity opp = new Opportunity();
    opp.Name = 'test';
    opp.AccountId = cuenta.Id;
    opp.StageName = 'Apartado';
    //opp.Esquema__c = 'Recurso propio';
    opp.CloseDate = Date.today().addDays(10);
    opp.Desarrollo__c = desarrollo.Id;
    insert opp;
    Opportunity opp2 = new Opportunity();
    opp2.Name = 'test';
    opp2.AccountId = cuenta.Id;
    opp2.StageName = 'Nueva';
    //opp2.Esquema__c = 'Recurso propio';
    opp2.CloseDate = Date.today().addDays(10);
    opp2.Desarrollo__c = desarrollo.Id;
    DocumentoxEsquema__c documentoEsquTest = new DocumentoxEsquema__c();
    documentoEsquTest.Documento__c = 'Test';
    //documentoEsquTest.Esquema_financiero__c = 'Recurso propio';
    documentoEsquTest.Name = 'AB01';
    //documentoEsquTest.Etapa__c = 'Separado';
    insert documentoEsquTest;
    DocumentoxEsquema__c documentoEsquTest2 = new DocumentoxEsquema__c();
    documentoEsquTest2.Documento__c = 'Test2';
    //documentoEsquTest2.Esquema_financiero__c = 'Recurso propio';
    documentoEsquTest2.Name = 'AB02';
    documentoEsquTest2.Etapa__c = 'Apartado';
    insert documentoEsquTest2;
    List<DocumentoxEsquema__c> listDocumentoxEsquema = new List<DocumentoxEsquema__c>();
    listDocumentoxEsquema.add(documentoEsquTest);
    listDocumentoxEsquema.add(documentoEsquTest2);
    Attachment documentoPrevio = new Attachment();
    documentoPrevio.Body = Blob.valueOf('Some Text');
    documentoPrevio.ParentId = opp.Id;
    documentoPrevio.Name = 'Test';
    insert documentoPrevio;
    ApexPages.currentPage().getParameters().put('Id',opp.Id);
    ApexPages.currentPage().getParameters().put('stage','Apartado');
    ApexPages.StandardController sc = new ApexPages.StandardController( opp2 );
    /*
    UploadDocumentController.CustomAttachment customAttachment = new UploadDocumentController.CustomAttachment();
    customAttachment.uploaded = true;
    customAttachment.attachmentId = documentoPrevio.Id;
    customAttachment.documentoPorEsquema = documentoEsquTest;
    UploadDocumentController.CustomAttachment customAttachment_2 = new UploadDocumentController.CustomAttachment();
    customAttachment_2.uploaded = false;
    customAttachment_2.attachmentId = documentoPrevio.Id;
    customAttachment_2.documentoPorEsquema = documentoEsquTest;
    List<UploadDocumentController.CustomAttachment> listdocumentoPrevio = new List<UploadDocumentController.CustomAttachment>();
    listdocumentoPrevio.add(customAttachment);
    listdocumentoPrevio.add(customAttachment_2);
    UploadDocumentController.customStage customStage = new UploadDocumentController.customStage();
    customStage.stage = 'Apartado';
    customStage.lstCustomAttachment = listdocumentoPrevio;
    */
    UploadDocumentController upcontroller = new UploadDocumentController(sc);
    //Handler_AttachmentTrigger controller = new Handler_AttachmentTrigger();
    //controller.createObjectClass();
    //UploadDocumentController udc = new UploadDocumentController();
    upcontroller.createObjectClass();
  }
}