/**
* Created by scarlettcastillo on 8/31/18.
*/

public with sharing class Handler_CaseTrigger {
    
    public static void mandarCasosCerradosAEnkontrol(List<Case> casos){
        
        QueueableService_CaseEnkontrol caseJob = new QueueableService_CaseEnkontrol(casos);
        
        if(Test.isRunningTest()){
            ID jobID = System.enqueueJob(caseJob);
        }
    }
}