/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Bg_Utils_Cls
* @Autor       Joel Soto
* @Date        21/11/2019
*
* @Group       BeGrand
* @Description Clase para crear registros preelaborados.
* @Changes
*/
public class Bg_Utils_Cls {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una compañia a partir del niombre e idEnk
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String idEnk
  * @Return      Compania__c compania
  * @example     genCompania('name', '001');
  **/
  public static Compania__c genCompania(String name, String idEnk) {
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = idEnk;
    compania.Name = name;
    return compania;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna un usuario a partir de un alias y perfil existente
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String alias
  * @Param       String profile
  * @Return      User sfUser
  * @example     genUser('aliasAsesor','Asesor');
  **/
  public static User genUser(String alias, String profile) {
    Profile userProfile   = [SELECT Id FROM Profile WHERE Name = :profile];
    User sfUser = new User();
    sfUser.Alias = ('alias' + alias).left(8);
    sfUser.Desarrollo__c = 'Coapa';
    sfUser.Email = alias + '@begrand.com';
    sfUser.EmailEncodingKey = 'UTF-8';
    sfUser.Id_Enkontrol__c = '1';
    sfUser.LanguageLocaleKey = 'en_US';
    sfUser.LastName = 'LastName';
    sfUser.LocaleSidKey = 'en_US';
    sfUser.ProfileId = userProfile.Id;
    sfUser.TimeZoneSidKey = 'America/Los_Angeles';
    sfUser.Username = alias + '@begrand.com';
    return sfUser;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna un desarrollo comercial a partir del nombre, compañia y el gerente asignado
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String companiaId
  * @Param       String gerenteId
  * @Return      Desarrollo_Comercial__c desCom
  * @example     genDesComercial('name','020LAOE93UC7A9D0C','001KKSS9093OLX')
  **/
  public static Desarrollo_Comercial__c genDesComercial(String name, String companiaId, String gerenteId) {
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = name;
    desarrolloComercial.Compania__c = CompaniaId;
    desarrolloComercial.Gerente_Ventas__c = gerenteId;
    desarrolloComercial.Gerente_Administrativo__c = gerenteId;
    return desarrolloComercial;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna guardia a partir del id de desarrollo comercial, id id2 des com, id asesor y roll de guardia
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String desComUnoId
  * @Param       String desComDosId
  * @Param       String asesorId
  * @Param       Decimal roll
  * @Return      Guardia__c guardia
  * @example     genGuardia('030J7UJMNB567YG','040J7UJMNB5YYYG','001J7FFMNBYY7YG',1)
  **/
  public static Guardia__c genGuardia(String desComUnoId, String desComDosId, String asesorId, Decimal roll) {
    Guardia__c guardia = new Guardia__c();
    guardia.Nombre_Asesor__c = asesorId;
    guardia.Fecha_Guardia__c = Date.today();
    guardia.Roll__c = roll;
    guardia.Desarrollo_Comercial__c = desComUnoId;
    guardia.Desarrollo_Comercial_Primario__c = desComDosId;
    guardia.Desarrollo_Comercial_Secundario__c = desComDosId;
    return guardia;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una etapa de desarrollo a partir del nombre, idenk y un id de desarrollo comercial
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String enkId
  * @Param       String desComId
  * @Return      Desarrollo__c etapa
  * @example     genDesarrollo('name','001','030JIKMBV5678KG89P')
  **/
  public static Desarrollo__c genDesarrollo(String name, String enkId, String desComId) {
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = name;
    desarrollo.Id_Desarrollo_Enkontrol__c = enkId;
    desarrollo.Desarrollo_Comercial__c = desComId;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    return desarrollo;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna un prospecto a partir del nombre, id desarrollo a asignar y un id asesor
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String desarrolloId
  * @Param       String sfUserId
  * @Return      Lead lead
  * @example     genLead('name','005A8J20CJE30DL','001A8J20CJFF0DL')
  **/
  public static Lead genLead(String name, String desarrolloId, String sfUserId) {
    Lead lead = new Lead();
    lead.FirstName = name;
    lead.LastName = 'testLead';
    lead.Email = name + '@testLead.com';
    lead.Phone = '1234567890';
    lead.LeadSource = '1';
    lead.Agente__c = sfUserId;
    lead.Desarrollo__c = desarrolloId;
    lead.Medio__c = 'Conoce la marca Be Grand';
    return lead;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una cuenta a partir del nombre, id enkontrol y record type
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String enkId
  * @Param       String recordTypeId
  * @Return      Account acc
  * @example     genAccount('name','001','002KIE8403OCI81L')
  **/
  public static Account genAccount(String name, String enkId, String recordTypeId) {
    Account acc = new Account();
    acc.LastName = name;
    acc.ID_Prospecto__pc = enkId;
    acc.RecordTypeId = recordTypeId;
    acc.PersonEmail = name.trim().replaceAll(' ', '') + '@test.com';
    return acc;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una oportunidad a partir del nombre, etapa, desarrollo y cuenta asociada
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String stageName
  * @Param       String desarrolloId
  * @Param       String accId
  * @Return      Opportunity opp
  * @example     genOpportunity('name','stageName','010JDNC883OK120','001JDNC883OK120');
  **/
  public static Opportunity genOpportunity(String name, String stageName, String desarrolloId, String accId) {
    Opportunity opp = new Opportunity();
    opp.Name = name;
    opp.AccountId = accId;
    opp.CloseDate = Date.today().addDays(10);
    opp.Desarrollo__c = desarrolloId;
    opp.StageName = stageName;
    return opp;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una attach a partir del nombre y parent id
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String name
  * @Param       String parentId
  * @Return      Attachment attach
  * @example     genAttachment('carta oferta', '003JDNC883OK120');
  **/
  public static Attachment genAttachment(String name, String parentId) {
    Attachment attach = new Attachment();
    attach.Body = Blob.valueOf('Some Text');
    attach.ParentId = parentId;
    attach.Name = name;
    attach.Description = 'description;comentario;observacion';
    return attach;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna un recordType a partir del nombre del rType y el objeto
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String recorTypeName
  * @Param       String objName
  * @Return      RecordType rType
  * @example     getRtype('personAccount', 'Account')
  **/
  public static RecordType getRtype(String rtypeName, String rtypeObject) {
    return [SELECT DeveloperName, Id, IsActive, SobjectType FROM RecordType WHERE SobjectType = : rtypeObject AND DeveloperName = : rtypeName limit 1];
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna el nombre de n objeto a partir de un registro
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String recordId
  * @Return      String objName
  * @example     getObjectName('003KSUDN837NDC9')
  **/
  public static String getObjectName(String recordId) {
    Map<String, String> keys = new Map<String, String>();
    Map<String, Schema.SobjectType> describe = Schema.getGlobalDescribe();
    for(String s:describe.keyset()) {
      keys.put(describe.get(s).getDescribe().getKeyPrefix(), s);
    }
    return keys.get(recordId.left(3));
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una fecha formateada.
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String 04/12/2019
  * @Return      Date sfDate
  * @example     formatDdMmAaaa('04/12/2019')
  **/
  public static Date formatDdMmAaaa(String stringDate) {
    Date formatdate = null;
    if(String.isNotBlank(stringDate)) {
      String[] dateparse = String.valueOf(stringDate).split('/');
      formatdate = Date.newInstance(Integer.valueOf(dateparse.get(2)), Integer.valueOf(dateparse.get(1)), Integer.valueOf(dateparse.get(0)));
    }
    return formatdate;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Valida si la cadena de texto no es vacia.
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String myString
  * @Return      String myString
  * @example     validateString('myString');
  **/
  public static String validateString(String myString) {
    return String.isBlank(String.valueof(myString)) ? null : myString;
  }
  /**
* --------------------------------------------------------------------------------------------------------------------
* @Description Valida si la cadena de texto no es vacia.
*
* @Autor       Joel Soto
* @Date        04/12/2019
* @Param       String myString
* @Return      String myString
* @example     validateString('myString');
**/
  public static String validateStringWs(String myString) {
    return String.isBlank(String.valueof(myString)) ? '' : myString;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Transforma un string a decimal si este no es nulo.
  *
  * @Autor       Joel Soto
  * @Date        04/12/2019
  * @Param       String myString
  * @Return      Decimal myString
  * @example     validateString('myString');
  **/
  public static Decimal stringToDecimal(String myDecimal) {
    return String.isBlank(myDecimal) ? 0 : Decimal.valueOf(myDecimal);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una etapa a partir del nombre, id enkontrol y el id de desarrollo.
  *
  * @Autor       Joel Soto
  * @Date        11/12/2019
  * @Param       String name
  * @Param       String idEnk
  * @Return      String idDesarrollo
  * @example     genEtapa('Etapa','1','005KOLMA19D02KS');
  **/
  public static Etapa__c genEtapa(String name, String idEnk, String idDesarrollo) {
    Etapa__c etapaSf = new Etapa__c();
    etapaSf.Name = name;
    etapaSf.Id_Etapa_Enkontrol__c = idEnk;
    etapaSf.No_de_Pisos__c = 23;
    etapaSf.Desarrollo__c = idDesarrollo;
    return etapaSf;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retrona departamento a partir de id de etapa, estatus y lote id
  *
  * @Autor       Joel Soto
  * @Date        11/12/2019
  * @Param       String idEtapa
  * @Param       String status
  * @Param       String idLote
  * @Return      Inmueble__c depto
  * @example     genDepto('009KHFSXVNLIH37', 'Disponible', '1');
  **/
  public static Inmueble__c genDepto(String idEtapa, String status, String idLote) {
    Inmueble__c depto = new Inmueble__c();
    depto.Etapa__c = idEtapa;
    depto.Estatus__c = status;
    depto.Lote_Id__c = idLote;
    return depto;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna un registro de equivalencia de estado a partir del nombre y su equivalencia
  *
  * @Autor       Joel Soto
  * @Date        11/12/2019
  * @Param       String name
  * @Param       String api
  * @Return      EquivalenciaEstados__c eqEstados
  * @example     genEstados('Aguascalientes','Ags');
  **/
  public static EquivalenciaEstados__c genEstados(String name, String api) {
    EquivalenciaEstados__c eqEstados = new EquivalenciaEstados__c();
    eqEstados.apiEnk__c = api;
    eqEstados.Name = name;
    return eqEstados;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna una carta oferta (Entrega) ligada a un depto
  *
  * @Autor       Joel Soto
  * @Date        11/12/2019
  * @Param       String idDepto
  * @Return      Carta_Oferta__c cOferta
  * @example     genEntrega('094SDFERT40HCSW');
  **/
  public static Carta_Oferta__c genEntrega(String idDepto) {
    Carta_Oferta__c cOferta = new Carta_Oferta__c();
    cOferta.Inmueble__c = idDepto;
    return cOferta;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Obtiene la lista de campos de algun objeto
  *
  * @Autor       Joel Soto
  * @Date        11/12/2019
  * @Param       Schema.SObjectType targetObject
  * @Return      List<String> listCampos
  * @example     getListCampos(Case.SObjectType);
  **/
  public static List<String> getListCampos(String objName) {
    final List<String> listCampos = new List<String>();
    final Set<String> setCpsExcl = new Set<String> {'CreatedById', 'CreatedDate', 'SystemModstamp', 'LastModifiedDate', 'LastModifiedById', 'LastReferencedDate', 'LastViewedDate', 'IsDeleted', 'et4ae5__HasOptedOutOfMobile__pc', 'Contacto_Notificacion__pc'};
    Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    final List<Schema.sObjectField> lSobjectFields = schemaMap.get(objName).getDescribe().fields.getMap().values();
    for(Schema.sObjectField sob : lSobjectFields) {
      final Schema.DescribeFieldResult sofd = sob.getDescribe();
      final String nombreCampo = sofd.getName();
      if(!setCpsExcl.contains(nombreCampo)) {
        listCampos.add(nombreCampo);
      }
    }
    return listCampos;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Obtiene la lista de campos de algun objeto y lo convierte a String para utilizar en Query
  *
  * @Autor       Joel Soto
  * @Date        11/12/2019
  * @Param       Schema.SObjectType targetObject
  * @Return      String targetObject
  * @example     getListCampos(Case.SObjectType);
  **/
  public static String getStrCampos(String objName) {
    return String.join(getListCampos(objName), ', ');
  }
  public static String cleanString(String sendString) {
    String mystring = sendString;
    mystring = mystring.replaceAll('\\+', ' ').replaceAll('\\*', ' ').replaceAll('®', ' ').replaceAll(' ', ' ');
    mystring = mystring.replaceAll('\\n', ' ').replaceAll('�', ' ').replaceAll('\\s+', ' ').trim();
    return mystring;
  }
  public static final String RESPUESTADROP = '' +
    '{' +
    '  "entries": [' +
    '    {' +
    '      ".tag": "file",' +
    '      "name": "Prime_Numbers.txt",' +
    '      "id": "id:a4ayc_80_OEAAAAAAAAAXw",' +
    '      "client_modified": "2015-05-12T15:50:38Z",' +
    '      "server_modified": "2015-05-12T15:50:38Z",' +
    '      "rev": "a1c10ce0dd78",' +
    '      "size": 7212,' +
    '      "path_lower": "/homework/math/prime_numbers.txt",' +
    '      "path_display": "/Homework/math/Prime_Numbers.txt",' +
    '      "sharing_info": {' +
    '        "read_only": true,' +
    '        "parent_shared_folder_id": "84528192421",' +
    '        "modified_by": "dbid:AAH4f99T0taONIb-OurWxbNQ6ywGRopQngc"' +
    '      },' +
    '      "is_downloadable": true,' +
    '      "property_groups": [' +
    '        {' +
    '          "template_id": "ptid:1a5n2i6d3OYEAAAAAAAAAYa",' +
    '          "fields": [' +
    '            {' +
    '              "name": "Security Policy",' +
    '              "value": "Confidential"' +
    '            }' +
    '          ]' +
    '        }' +
    '      ],' +
    '      "has_explicit_shared_members": false,' +
    '      "content_hash": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",' +
    '      "file_lock_info": {' +
    '        "is_lockholder": true,' +
    '        "lockholder_name": "Imaginary User",' +
    '        "created": "2015-05-12T15:50:38Z"' +
    '      }' +
    '    },' +
    '    {' +
    '      ".tag": "folder",' +
    '      "name": "math",' +
    '      "id": "id:a4ayc_80_OEAAAAAAAAAXz",' +
    '      "path_lower": "/homework/math",' +
    '      "path_display": "/Homework/math",' +
    '      "sharing_info": {' +
    '        "read_only": false,' +
    '        "parent_shared_folder_id": "84528192421",' +
    '        "traverse_only": false,' +
    '        "no_access": false' +
    '      },' +
    '      "property_groups": [' +
    '        {' +
    '          "template_id": "ptid:1a5n2i6d3OYEAAAAAAAAAYa",' +
    '          "fields": [' +
    '            {' +
    '              "name": "Security Policy",' +
    '              "value": "Confidential"' +
    '            }' +
    '          ]' +
    '        }' +
    '      ]' +
    '    }' +
    '  ],' +
    '  "cursor": "ZtkX9_EHj3x7PMkVuFIhwKYXEpwpLwyxp9vMKomUhllil9q7eWiAu",' +
    '  "has_more": false' +
    '}';
  public static String imagenMuestraBase64 = '' +
    'EBMQGhcbFhUWGxcpIBwcICkvJyUnLzkzMzlHREddXX0BBQUFBQUFBQYGBQgIBwgICwoJCQoLEQwNs' +
    'DA0MERoQExAQExAaFxsWFRYbFykgHBwgKS8nJScvOTMzOUdER11dff/CABEIASwBLAMBIgACEQED' +
    'EQH/xAAdAAEAAwADAQEBAAAAAAAAAAAABwgJAwQGBQEC/9oACAEBAAAAALlgAAAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB834wAB6Dt+' +
    'b8L9733H5oDs+iAq9QgAA0yhin/U/ZhuvmMBKeooFXqG6A+MpZbWWqDTf0YP0E5XkaO3MppZizPl' +
    'aMeujC3kqZ/S3aaqHR1FAq9Q3YCMsx9DrE1FlOvlW9gu6j7NGbq0at+9Z/1q/jRie8gbEX7r18O0' +
    'IHz/AIvqYgzH0OsUUUq1rX2nY+nG2XEiWym/sfM/fR93IGxF+wAIfzH0OsUUUqlyE66Pq5Ui8jyz' +
    'JeOTHFkDYi/YAEP5j6HWKKKVZvpyPVzd43xMuxVBFXWq/ouLIGxF+wAIfzH0OsUUUq3sF3RUilGw' +
    'PdQNnBonYXiyBsRfsACH8x9DrFFFKt7Bd0VIo9eW2X7Uqjmks58WQNiL9gArVTyP9DrFFFKqyX/Q' +
    '875H9k3vRTLOmXjM8Y1svfsAEJV5WflQrpC4HLKUUPZWu+n5KnH7Lll35+gAAAAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/8QA' +
    'FAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAB//8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAxAAAAAAAAAAAAAA' +
    'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//8QATRAAAAQDAQcOCgcHBQAA' +
    'AAAAAQIDBQAEBgcIEBESVpSyExQWGDE2N1JVcnR1kdEXICEiMDM0NUFRMkJhYnOxsxUjJHGBgsJA' +
    'U4CQo//aAAgBAQABPwD/AJ4uTw1NCZFnJxlpRI5sUp11ATKI/Lzo2dUblU152n3xs6o3KprztPvj' +
    'Z1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xs6o3KprztPvjZ1RuV' +
    'TXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xs6o3KprztPvjZ1RuVTXnaf' +
    'fElOyTlKpzcjOJTEup9BVI4HIPNEt57rGlqb853fpOTHiKKgBx/pBrebK0zmIaqCfzBFQwQ0WqWe' +
    'PqiaMlVkkKp/IBTn1PSxYKYpygYpgEB3BAd2JmYl5RBVddUiSCZcY5zjigUOMYY2dUXlS152n3xs' +
    '6o3KprztPvjZ1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xKVbS0/' +
    'MJS0rUbcsuoOKmklMEOc4/YAD6C6x3q0/wBY/wCH+gsBEAsopsekfqRbBdAzSU3N0/SEwCYJCZOZ' +
    'cS6KUTc5Nzy55iamVF1VBwmUUMJzCP8AMb3lAYoG2ir6FWQSLOHnmwB8+SXNoDukGHyrmWt7Iqnd' +
    '2lfHRUalynJ9dI+J5SHD0Fi3CfSXS/QXWO9Wn+sf8IIAGOQB3BEAhvubLO5mQkF1AcMdaXTUHAvx' +
    'wi1Gwih6Qod6em7XmvJUiYp463GOBb1i1jdH15SJ3Z3Cb1yE4sj+5VxAwFgbmWzgCj7xziHqVSkH' +
    'h1k0sOpITSyRMO7gIYShFhFmtN2hrVCR7BfBKEQFLUT4n04t1s8YbPHlmk2UF9TmJQVVNWPjjCBQ' +
    'OskQdwxygPbErc02cqyssofX+FRJM/tHHCNrLZx8nHOI2slm/wAnHOIr+5/oOmqNqB4kdfa5lJUV' +
    'Esde8hVi9KXNTYeUUEk3PnWk0jfEmqKGwiWBHCOEYsgsGUrSSSfH2YUlms/s6KfrV4SufrK0kgT2' +
    'OmU++ZdSKjuZqFcJVb9jHmW2axfMNjiqnFS0840s9uLM4pYkzKKYhvkPyMH2GCLPavmmJGqGgVR1' +
    'i7tUyioQR8gKlIJiHvWOWLUbXFGJu7sE3roZlVMdSWxAwFhS5ms3KRT3juGH18OSCcs4zyCWHESX' +
    'UTLh+RTYIsQoVjr6pp9tedX1BKSMsXUj4g4+MBY2stnHycc4i2qi2agquSaGfVtbDIorDqpsccJ4' +
    'sW4T6S6X6C6x3q0/1j/hCPrU+eWGT3O19ER0Ai3ngqqrmo/qXrl7g6V61XgS4QMHzhzuWQcXJwnd' +
    'mGJrhdRXE1rxhiyeyLwYKvR/23r7XxEg9TqeJqUXV2+WmugH04lvaZf8Qv5w3+wSX4BNEL9qsrMz' +
    'lnlVoSyB1llJIwEIQuE5h+6EbCawyWc81U7orhtcZGwWgE5uUVQOi5rgomoQSCGNjYMIGvWYT0lP' +
    'WfUirInIKRW9JPAX4HIGAwX7qOUQQr6SXIQAOu2JmV5xTiEFMYo4SjgG9c1cGaHT14V9UrzDaMPf' +
    'vh26WtpjFyvv6duqVdMt60+wototRketkWs8EsmjqWoY/wBAYoy5xCkamaH3ZTrnWS2PqWtsTG9A' +
    '5szS8IpouTdKzaZRxiEXTBUCjxvOjYNRmSrXmqXdBEyJkAhAApSlwAAbgBFvPBVVXNR/UvXL3B0r' +
    '1qv4l1dvlproB9OJYcEzLiP+4X84kajp4shJAL5JAJUSYf4gny/nGySn+XJLO';
  public static final String RESPUESTA_ENKONTROL = '' +
    '<?xml version="1.0" encoding="UTF-8"?>' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" s:mustUnderstand="1">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-04T04:13:26.532Z</u:Created>' +
    '        <u:Expires>2018-09-04T04:18:26.532Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <FailReportExecuteResponse xmlns="http://tempuri.org/">' +
    '      <FailReportExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" ' +
    '        xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" />' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" ' +
    '          xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO" i:nil="true" />' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </FailReportExecuteResult>' +
    '    </FailReportExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T18:23:13.461Z</u:Created>' +
    '        <u:Expires>2018-09-20T18:28:13.461Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <ProspectExecuteResponse xmlns="http://tempuri.org/">' +
    '      <ProspectExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">' +
    '          <b:ProspectDTO>' +
    '            <b:AgentKey>1</b:AgentKey>' +
    '            <b:AgentName>0</b:AgentName>' +
    '            <b:CompanyId>11</b:CompanyId>' +
    '            <b:CompanyName i:nil="true"/>' +
    '            <b:DevelopmentKey>AP2</b:DevelopmentKey>' +
    '            <b:DevelopmentName>0</b:DevelopmentName>' +
    '            <b:Email>kim.jaime@test.tst</b:Email>' +
    '            <b:FirstName>Kim</b:FirstName>' +
    '            <b:Gender>M</b:Gender>' +
    '            <b:HomePhoneNumber>7788990099</b:HomePhoneNumber>' +
    '            <b:Id>49568</b:Id>' +
    '            <b:IdGroup>80045</b:IdGroup>' +
    '            <b:IsMoralPerson>false</b:IsMoralPerson>' +
    '            <b:LastName>Jaime</b:LastName>' +
    '            <b:ProspectingTypeId>1</b:ProspectingTypeId>' +
    '            <b:ProspectingTypeName i:nil="true"/>' +
    '            <b:SecondLastName>Lopez</b:SecondLastName>' +
    '            <b:Address>Calzada Ignacio Zaragoza</b:Address>' +
    '            <b:BirthDate>0001-01-01T00:00:00</b:BirthDate>' +
    '            <b:City>Ciudad de México</b:City>' +
    '            <b:Country>Mexico</b:Country>' +
    '            <b:CreatedDate>0001-01-01T00:00:00</b:CreatedDate>' +
    '            <b:District>Agricola Oriental</b:District>' +
    '            <b:Email2>kim.jaime1@test.tst</b:Email2>' +
    '            <b:Nationality>MEXICANA</b:Nationality>' +
    '            <b:OfficePhoneNumber>7788990098</b:OfficePhoneNumber>' +
    '            <b:OtherPhoneNumber>7788990097</b:OtherPhoneNumber>' +
    '            <b:PostalCode>15000</b:PostalCode>' +
    '            <b:PropertyTypeName i:nil="true"/>' +
    '            <b:RFC i:nil="true"/>' +
    '            <b:SaleOpportunityList i:nil="true"/>' +
    '            <b:SalesForceId i:nil="true"/>' +
    '            <b:State>Ciudad de México</b:State>' +
    '          </b:ProspectDTO>' +
    '        </List>' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </ProspectExecuteResult>' +
    '    </ProspectExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_FAIL = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T17:55:58.239Z</u:Created>' +
    '        <u:Expires>2018-09-20T18:00:58.239Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <s:Fault>' +
    '      <faultcode>s:Client</faultcode>' +
    '      <faultstring xml:lang="es-MX">Service operation ProspectExecute failed due to validation errors: &#xD; &#xD;Identificador del punto de prospeccion no valido &#xD;' +
    '      </faultstring>' +
    '    </s:Fault>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_BASICO_SUCCESS = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T17:46:47.269Z</u:Created>' +
    '        <u:Expires>2018-09-20T17:51:47.269Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <BasicProspectExecuteResponse xmlns="http://tempuri.org/">' +
    '      <BasicProspectExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">' +
    '          <b:ProspectDTO>' +
    '            <b:AgentKey>1</b:AgentKey>' +
    '            <b:AgentName>0</b:AgentName>' +
    '            <b:CompanyId>11</b:CompanyId>' +
    '            <b:CompanyName i:nil="true"/>' +
    '            <b:DevelopmentKey>AP2</b:DevelopmentKey>' +
    '            <b:DevelopmentName>0</b:DevelopmentName>' +
    '            <b:Email i:nil="true"/>' +
    '            <b:FirstName>Jimena</b:FirstName>' +
    '            <b:Gender>M</b:Gender>' +
    '            <b:HomePhoneNumber>1223334455</b:HomePhoneNumber>' +
    '            <b:Id>80045</b:Id>' +
    '            <b:IdGroup>80045</b:IdGroup> +' +
    '            <b:IsMoralPerson>false</b:IsMoralPerson>' +
    '            <b:LastName>Sanchez</b:LastName>' +
    '            <b:ProspectingTypeId>1</b:ProspectingTypeId>' +
    '            <b:ProspectingTypeName i:nil="true"/>' +
    '            <b:SecondLastName>Martinez</b:SecondLastName>' +
    '            <b:Address i:nil="true"/>' +
    '            <b:BirthDate>0001-01-01T00:00:00</b:BirthDate>' +
    '            <b:City i:nil="true"/>' +
    '            <b:Country i:nil="true"/>' +
    '            <b:CreatedDate>0001-01-01T00:00:00</b:CreatedDate>' +
    '            <b:District i:nil="true"/>' +
    '            <b:Email2 i:nil="true"/>' +
    '            <b:Nationality i:nil="true"/>' +
    '            <b:OfficePhoneNumber i:nil="true"/>' +
    '            <b:OtherPhoneNumber i:nil="true"/>' +
    '            <b:PostalCode i:nil="true"/>' +
    '            <b:PropertyTypeName i:nil="true"/>' +
    '            <b:RFC i:nil="true"/>' +
    '            <b:SaleOpportunityList i:nil="true"/>' +
    '            <b:SalesForceId i:nil="true"/>' +
    '            <b:State i:nil="true"/>' +
    '          </b:ProspectDTO>' +
    '        </List>' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </BasicProspectExecuteResult>' +
    '    </BasicProspectExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_PROSPECTOS_BASICO_FAIL = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-09-20T17:39:45.174Z</u:Created>' +
    '        <u:Expires>2018-09-20T17:44:45.174Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <s:Fault>' +
    '      <faultcode>s:Client</faultcode>' +
    '      <faultstring xml:lang="es-MX"> ' +
    '        Service operation BasicProspectExecute failed due to validation errors: &#xD; &#xD; Numero de teléfono de casa no valido (10 digitos) &#xD; ' +
    '      </faultstring>' +
    '    </s:Fault>' +
    '  </s:Body>' +
    '</s:Envelope>';
  public static final String RESPUESTA_ENKONTROL_DEFICIENCIAS_SUCCESS = '' +
    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
    '  <s:Header>' +
    '    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">' +
    '      <u:Timestamp u:Id="_0">' +
    '        <u:Created>2018-10-23T17:58:38.519Z</u:Created>' +
    '        <u:Expires>2018-10-23T18:03:38.519Z</u:Expires>' +
    '      </u:Timestamp>' +
    '    </o:Security>' +
    '  </s:Header>' +
    '  <s:Body>' +
    '    <FailDetailExecuteResponse xmlns="http://tempuri.org/">' +
    '      <FailDetailExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
    '        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>' +
    '        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">' +
    '          <b:FailReportDetailDTO>' +
    '            <b:Comments i:nil="true"/>' +
    '            <b:DetailOrderNumber>1</b:DetailOrderNumber>' +
    '            <b:EmployeeId>0</b:EmployeeId>' +
    '            <b:FailId>1</b:FailId>' +
    '            <b:FailLocationId>4</b:FailLocationId>' +
    '            <b:FailProviderId>0</b:FailProviderId>' +
    '            <b:FailStatus>Reasign</b:FailStatus>' +
    '            <b:FailTypeId>12</b:FailTypeId>' +
    '            <b:Folio>1863</b:Folio>' +
    '            <b:IsAlternativeProvider>true</b:IsAlternativeProvider>' +
    '            <b:IsLocalProvider>true</b:IsLocalProvider>' +
    '            <b:PreviousOrderNumber>0</b:PreviousOrderNumber>' +
    '            <b:PropertyDevelopmentKey>AP2</b:PropertyDevelopmentKey>' +
    '            <b:SalesForceId>FDC-00000671</b:SalesForceId>' +
    '            <b:Type i:nil="true"/>' +
    '          </b:FailReportDetailDTO>' +
    '        </List>' +
    '        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>' +
    '      </FailDetailExecuteResult>' +
    '    </FailDetailExecuteResponse>' +
    '  </s:Body>' +
    '</s:Envelope>';


     public static final String RESPUESTA_ENKONTROL_CIERRE_PROSPECTO = '' +
   '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
'  <s:Header>'+
'    <o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
'      <u:Timestamp u:Id="_0">'+
'        <u:Created>2020-01-29T17:45:45.887Z</u:Created>'+
'        <u:Expires>2020-01-29T17:50:45.887Z</u:Expires>'+
'      </u:Timestamp>'+
'    </o:Security>'+
'  </s:Header>'+
'  <s:Body>'+
'    <ClosedProspectExecuteResponse xmlns="http://tempuri.org/">'+
'      <ClosedProspectExecuteResult xmlns:a="http://schemas.datacontract.org/2004/07/EnKontrol.Response" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'+
'        <ErrorList xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services"/>'+
'        <List xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services" '+
'          xmlns:b="http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO">'+
'          <b:ClosedProspectDTO>'+
'            <b:Error>E5 El prospecto enviado no existe en el catálogo del usuario 236.</b:Error>'+
'            <b:Id>0</b:Id>'+
'            <b:IsClosed>false</b:IsClosed>'+
'            <b:OportunityId>0</b:OportunityId>'+
'          </b:ClosedProspectDTO>'+
'        </List>'+
'        <Success xmlns="http://schemas.datacontract.org/2004/07/BeGrand.Common.Services">true</Success>'+
'      </ClosedProspectExecuteResult>'+
'    </ClosedProspectExecuteResponse>'+
'  </s:Body>'+
'</s:Envelope>';


}