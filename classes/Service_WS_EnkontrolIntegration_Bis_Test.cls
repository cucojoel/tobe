@IsTest
public class Service_WS_EnkontrolIntegration_Bis_Test {
  static testMethod void wsService2()
  {
    List<WS_EnkontrolIntegration.Fallas> listFallas = new List<WS_EnkontrolIntegration.Fallas>();
    Map<String, Object> mapaMocks = new Map<String, Object>();
    Desarrollo_Comercial__c unDesarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
    Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba('102', unDesarrolloComercial.Id, '1', 'AP12');
    unInmueble.Nivel__c = '11';
    update unInmueble;
    Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
    Account cliente = Test_Utilities.crearClientePrueba();
    Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);
    entrega.Inmueble__c = unInmueble.Id;
    entrega.Cliente__c = cliente.Id;
    entrega.Estatus__c = 'Entrega';
    entrega.Estatus__c = 'Inventario';
    upsert entrega;
    Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
    Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);
    detalle.Id_Falla_WS__c = '12345';
    update detalle;
    Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);
    Test_Utilities.testCrearConfiguracionPersonalizada();
    Profile supervisionObra = [SELECT Id FROM Profile WHERE Name = 'Gerente supervisión de obra'];
    User unGerenteSo = new User(Alias = 'standt', Email = 'gerentesupervision@begrand.test',
                                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = supervisionObra.Id,
                                TimeZoneSidKey = 'America/Los_Angeles', Username = 'gerentesupervision@begrand.test',
                                Id_Enkontrol__c ='99011',  Desarrollo__c = 'ALTO PEDREGAL');
    insert unGerenteSo;
    User servClienteUs = new User(Alias = 'scanus', Email = 'coordinadoruser@begrand.test',
                                  EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                                  LocaleSidKey = 'en_US', ProfileId = supervisionObra.Id,
                                  TimeZoneSidKey = 'America/Los_Angeles', Username = 'coordinadoruser@begrand.test',
                                  Id_Enkontrol__c ='99011',  Desarrollo__c = 'ALTO PEDREGAL');
    insert servClienteUs;
    User asignObra = new User(Alias = 'scanus', Email = 'asignadoObra@begrand.test',
                              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US', ProfileId = supervisionObra.Id,
                              TimeZoneSidKey = 'America/Los_Angeles', Username = 'asignadoObra@begrand.test',
                              Id_Enkontrol__c ='99011',  Desarrollo__c = 'ALTO PEDREGAL');
    insert asignObra;
    /** Crear datos de prueba */
    List<User> supervision = [
                              SELECT Id, Name, Username, Id_Enkontrol__c
                              FROM User
                              WHERE Username = 'gerentesupervision@begrand.test'
                                               LIMIT 1
    ];
    List<User> servicioCliente = [
                                  SELECT Id, Name, Username, Id_Enkontrol__c
                                  FROM User
                                  WHERE Username = 'coordinadoruser@begrand.test'
                                                   LIMIT 1
    ];
    List<User> asignadoObra = [
                               SELECT Id, Name, Username, Id_Enkontrol__c
                               FROM User
                               WHERE Username = 'asignadoObra@begrand.test'
                                                LIMIT 1
    ];
    WS_EnkontrolIntegration.Fallas falla0 = new WS_EnkontrolIntegration.Fallas();
    falla0.idFalla                = null;
    falla0.zona                   = null;
    falla0.idZona                 = null;
    falla0.especialidad           = null;
    falla0.idEspecialidad         = null;
    falla0.subespecialidad        = null;
    falla0.idSubespecialidad      = null;
    falla0.idInmueble             = null;
    falla0.idEtapa                = null;
    falla0.idDesarrollo           = null;
    falla0.idLote                 = null;
    falla0.idPiso                 = null;
    falla0.numeroSello            = null;
    falla0.estatus                = null;
    falla0.existe                 = null;
    falla0.proveedor              = null;
    falla0.idProveedor            = null;
    falla0.esCuadrilla            = null;
    falla0.esProveedorEmergente   = null;
    falla0.idFallaAnterior        = null;
    falla0.tipo                   = null;
    falla0.descripcion            = null;
    falla0.dropboxFolderUrl       = null;
    falla0.checklistDropboxUrl    = null;
    falla0.folio                  = null;
    falla0.idEmpleado             = null;
    falla0.idAplicacionOrigen     = null;
    falla0.idEtapaEntrega         = null;
    falla0.sellosDropboxFolderUrl = null;
    falla0.idSupervisor           = null;
    falla0.PersonaEntrega         = null;
    falla0.correoPersonaEntrega   = null;
    listFallas.add(falla0);
    //FALLA 1
    WS_EnkontrolIntegration.Fallas falla1 = new WS_EnkontrolIntegration.Fallas();
    falla1.idFalla = 'FALLA-001';
    falla1.zona = 'Cocina';
    falla1.idZona = 1223;
    falla1.especialidad = 'GRANITO';
    falla1.idEspecialidad = 3314;
    falla1.subespecialidad = 'CUBIERTA';
    falla1.idSubespecialidad = 9902;
    falla1.idInmueble = '1408';
    falla1.idEtapa = '1';
    falla1.idDesarrollo = 'AP12';
    falla1.idLote = '102';
    falla1.idPiso = '11';
    falla1.proveedor = proveedor.Name;
    falla1.idProveedor = Integer.valueOf(proveedor.Id_Proveedor_Enkontrol__c);
    falla1.esCuadrilla = false;
    falla1.esProveedorEmergente = false;
    falla1.idFallaAnterior = null;
    falla1.tipo = 'D';
    falla1.descripcion = 'Description test';
    falla1.dropboxFolderUrl = '/TEST/001';
    falla1.checklistDropboxUrl = '/TEST/001';
    falla1.folio = 122;
    falla1.idEmpleado = 111;
    falla1.idAplicacionOrigen = 1;
    falla1.idEtapaEntrega = 1;
    falla1.sellosDropboxFolderUrl = '/TEST/001';
    falla1.PersonaEntrega = 'Prueba Prueba';
    falla1.correoPersonaEntrega = 'prueba@prueba.com';
    if(!supervision.isEmpty()) {
      falla1.idSupervisor = Integer.valueOf(supervision.get(0).Id_Enkontrol__c);
    }
    if(!asignadoObra.isEmpty()) {
      falla1.idAsignadoObra = Integer.valueOf(asignadoObra.get(0).Id_Enkontrol__c);
    }
    listFallas.add(falla1);
    //FALLA 2
    WS_EnkontrolIntegration.Fallas falla2 = new WS_EnkontrolIntegration.Fallas();
    falla2.idFalla = 'FALLA-002';
    falla2.zona = 'Cocina';
    falla2.idZona = 1223;
    falla2.especialidad = 'GAS';
    falla2.idEspecialidad = 9912;
    falla2.subespecialidad = 'TUBERIAS';
    falla2.idSubespecialidad = 9921;
    falla2.idInmueble = '1408';
    falla2.idEtapa = '1';
    falla2.idDesarrollo = 'AP12';
    falla2.idLote = '102';
    falla2.idPiso = '11';
    falla2.proveedor = 'test proveedor';
    falla2.idProveedor = 0;
    falla2.esCuadrilla = true;
    falla2.esProveedorEmergente = true;
    falla2.idFallaAnterior = 'true';
    falla2.tipo = 'D';
    falla2.descripcion = 'Description test';
    falla2.folio = 122;
    falla2.idAplicacionOrigen = 1;
    falla2.idEtapaEntrega = 2;
    falla2.sellosDropboxFolderUrl = '/TEST/001';
    falla2.checklistDropboxUrl = '/TEST/001';
    falla2.PersonaEntrega = 'Prueba Prueba';
    falla2.correoPersonaEntrega = 'prueba@prueba.com';
    if(!servicioCliente.isEmpty()) {
      falla2.idCoordinadorSC = Integer.valueOf(servicioCliente.get(0).Id_Enkontrol__c);
    }
    listFallas.add(falla2);
    //FALLA 3
    WS_EnkontrolIntegration.Fallas falla3 = new WS_EnkontrolIntegration.Fallas();
    falla3.idFalla = 'FALLA-002';
    falla3.zona = 'Cocina';
    falla3.idZona = 1223;
    falla3.especialidad = 'GAS';
    falla3.idEspecialidad = 9912;
    falla3.subespecialidad = 'TUBERIAS';
    falla3.idSubespecialidad = 9921;
    falla3.idInmueble = '1408';
    falla3.idEtapa = '1';
    falla3.idDesarrollo = 'AP12';
    falla3.idLote = '102';
    falla3.idPiso = '11';
    falla3.proveedor = 'test proveedor';
    falla3.idProveedor = 0;
    falla3.esCuadrilla = true;
    falla3.esProveedorEmergente = false;
    falla3.idFallaAnterior = null;
    falla3.tipo = 'D';
    falla3.descripcion = 'Description test';
    falla3.folio = 122;
    falla3.idAplicacionOrigen = 1;
    falla3.idEtapaEntrega = 3;
    falla3.sellosDropboxFolderUrl = '/TEST/001';
    falla3.checklistDropboxUrl = '/TEST/001';
    falla3.personaEntrega = 'Prueba Prueba';
    falla3.correoPersonaEntrega = 'prueba@prueba.com';
    listFallas.add(falla3);
    //List<WS_EnkontrolIntegration.SaveResult> results = WS_EnkontrolIntegration.bienvenidaCliente(listFallas);
    falla2.idInmueble = '1222';
    falla2.idLote = '102';
    Test.startTest();
    WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas> {});

    WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas> {falla2});
    falla1.idEmpleado = 554;
    WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas> {falla1});
    Set<String> casosId = new Set<String>();
    Case nCases = new Case();
    nCases.ChecklistUrl__c = '/TEST/001';
    nCases.Entrega__c = entrega.Id;
    insert nCases;
    casosId.add(String.valueOf(nCases.Id));
    Test.stopTest();
    Service_WS_EnkontrolIntegration_Bis serviceCtrl = new Service_WS_EnkontrolIntegration_Bis();
    serviceCtrl.createObjectClassFifth();
    serviceCtrl.createObjectClassFifth2();
    serviceCtrl.createObjectClassFifth3();
    serviceCtrl.createObjectClassFifth4();
    System.assertNotEquals(null, casosId);
  }
}