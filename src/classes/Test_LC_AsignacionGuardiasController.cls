@isTest
public class Test_LC_AsignacionGuardiasController {

    @isTest static void obtenerGuardiasExistentesTest(){
        Compania__c compania = Test_Utilities.crearCompania();
        User usuario = Test_Utilities.crearUsuarioPrueba('99999');
        Desarrollo_Comercial__c dcm = Test_Utilities.crearDesarrolloComercial(compania.Id, usuario.Id, usuario.Id);
        List<Guardia__c> guardList = new List<Guardia__c>();
        Test.startTest();
        guardList = LC_AsignacionGuardiasController.obtenerGuardiasExistentes(dcm.Id);
        Test.stopTest();
    }

    @isTest static void eliminarGuardiaTest(){
        Compania__c compania = Test_Utilities.crearCompania();
        User usuario = Test_Utilities.crearUsuarioPrueba('99999');
        Desarrollo_Comercial__c dcm = Test_Utilities.crearDesarrolloComercial(compania.Id, usuario.Id, usuario.Id);
        Guardia__c nuevaGuardia = new Guardia__c();
        List<Guardia__c> guardias = [SELECT Id, Nombre_Asesor__c FROM Guardia__c WHERE Nombre_Asesor__c = :usuario.Id];

        if(guardias.isEmpty()){
            nuevaGuardia.Desarrollo_Comercial__c = dcm.Id;
            nuevaGuardia.Nombre_Asesor__c = usuario.Id;
            nuevaGuardia.Fecha_Guardia__c = Date.today();
            nuevaGuardia.Roll__c = 0;
            insert nuevaGuardia;
        }
        Boolean exito;

        Test.startTest();
        exito = LC_AsignacionGuardiasController.eliminarGuardia(nuevaGuardia.Id);
        exito = LC_AsignacionGuardiasController.eliminarGuardia(nuevaGuardia.Id);
        Test.stopTest();
    }

    @isTest static void agregarUsuarioAGuardiaTest(){
        Compania__c compania = Test_Utilities.crearCompania();
        User usuario = Test_Utilities.crearUsuarioPrueba('99999');
        Desarrollo_Comercial__c dcm = Test_Utilities.crearDesarrolloComercial(compania.Id, usuario.Id, usuario.Id);
        Guardia__c guardia = new Guardia__c();
        String fechaaplicar = String.valueof(System.today());
        String rollid='1';
        Test.startTest();
        guardia = LC_AsignacionGuardiasController.agregarUsuarioAGuardia(usuario.Id, dcm.Id,fechaaplicar, rollid);
        Test.stopTest();
    }

    @isTest static void borrarTodasGuardiasTest(){
        Compania__c compania = Test_Utilities.crearCompania();
        User usuario = Test_Utilities.crearUsuarioPrueba('99999');
        Desarrollo_Comercial__c dcm = Test_Utilities.crearDesarrolloComercial(compania.Id, usuario.Id, usuario.Id);
        Test.startTest();
        LC_AsignacionGuardiasController.borrarTodasGuardias(dcm.Id);
        Test.stopTest();
    }

    @isTest static void busquedaUsuariosTest(){
        Compania__c compania = Test_Utilities.crearCompania();
        User u;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            u = new User(
                    ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id,
                    LastName = 'last',
                    Email = 'puser000@test.com',
                    Username = 'puser000@test.com' + System.currentTimeMillis(),
                    CompanyName = 'TEST',
                    Title = 'title',
                    Alias = 'alias',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    Id_Enkontrol__c = '99999'
            );
            insert u;
            Group  grupo = new Group(Name = 'Alto Polanco', Type = 'Regular');
            insert grupo;
            GroupMember memberAdmin = new GroupMember(
                GroupId = grupo.Id,
                UserOrGroupId = u.Id
            );
            insert memberAdmin;
        }
        Desarrollo_Comercial__c dcm = Test_Utilities.crearDesarrolloComercial(compania.Id, u.Id, u.Id);
        List<User> userList = new List<User>();
        Test.startTest();
        userList = LC_AsignacionGuardiasController.busquedaUsuarios(' ', dcm.Id);
        Test.stopTest();
    }

    /*@isTest static void obtenerGruposParaUsuarioTest(){
        User usuario = Test_Utilities.crearUsuarioPrueba('99999');
        List<Group> grupo = new List<Group>();
        Test.startTest();
        grupo = LC_AsignacionGuardiasController.obtenerGruposParaUsuario(usuario.Id);
        Test.stopTest();
    }

    @isTest static void obtenerGruposPorIdsTest(){
        User usuario = Test_Utilities.crearUsuarioPrueba('99999');
        List<User> lista = new List<User>([SELECT Id, Name FROM User WHERE Name = 'Scarlett Castillo']);
        Set<Id> ids = new Set<Id>{lista[0].Id};
        Set<Id> salida = new Set<Id>();
        Test.startTest();
        salida = LC_AsignacionGuardiasController.obtenerGruposPorIds(ids);
        Test.stopTest();
    }*/

}