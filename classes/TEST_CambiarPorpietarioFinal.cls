@isTest
public with sharing class TEST_CambiarPorpietarioFinal {

	public static testMethod void unitTest(){

		Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
		User gerenteUser = new User();
		gerenteUser.Alias = 'gereUser';
		gerenteUser.Desarrollo__c = 'Coapa';
		gerenteUser.Email = 'gereUser@begrand.com';
		gerenteUser.EmailEncodingKey = 'UTF-8';
		gerenteUser.Id_Enkontrol__c ='00001';
		gerenteUser.LanguageLocaleKey = 'en_US';
		gerenteUser.LastName = 'gereUser';
		gerenteUser.LocaleSidKey = 'en_US';
		gerenteUser.ProfileId = profileGerente.Id;
		gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
		gerenteUser.Username = 'gereUser@begrand.com';
		insert gerenteUser;

		Lead lead_1 = new Lead();
    lead_1.FirstName = 'lead_1';
		lead_1.LastName = 'lead_1';
		lead_1.Email = 'lead_test00@test.com';
		lead_1.Phone = '1234567890';
		//lead_1.Desarrollo_Comercial__c = desarrolloComercial.Id;
		lead_1.LeadSource = '1';
		lead_1.Web_to_lead__c = false;
		lead_1.Medio__c = 'Conoce la marca Be Grand';
		lead_1.Propietario_asignado__c = gerenteUser.Id;
		lead_1.OwnerId = gerenteUser.Id;
		insert lead_1;

		CambiarPorpietarioFinal.asignarPropietario();

		Handler_LeadTrigger hg = new Handler_LeadTrigger();
		hg.createObjectClass();
	}

}