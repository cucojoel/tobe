/**
 * Created by scarlettcastillo on 8/28/18.
 */
global class Test_WS_DropboxMock implements HttpCalloutMock {
  Map<String, Object> calloutMocks = new Map<String, Object>();
  global Test_WS_DropboxMock(Map<String, Object> callouts) {
    this.calloutMocks = callouts;
  }
  global HTTPResponse respond(HTTPRequest req) {
    HttpResponse res = new HttpResponse();
    res.setStatusCode(200);
    System.debug('log - Test_WS_DropboxMock The endpoint on the mock: '+ req.getEndpoint());
    if(calloutMocks.containsKey(req.getEndpoint())) {
      Object response = calloutMocks.get(req.getEndpoint());
      if(response instanceof String) {
        res.setBody((String) response);
        res.setHeader('Content-Type', 'application/json');
      } else if(response instanceof Blob)  {
        res.setBodyAsBlob((Blob) response);
      }
    }
    return res;
  }
}