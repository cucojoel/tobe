/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        SincronizacionCarpetasOppDrop
* @Autor       Joel Soto
* @Date        07/01/2020
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global with sharing class SincronizacionCarpetasOppDrop {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        07/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  @future (callout = true)
  public static void processRecords(Set<String> recordIds) {
    System.debug('log - SincronizacionRecursoOrdenDrop: ' + 'future');
    if (!recordIds.isEmpty()) {
      Map<Id, Carpeta__c> mapCarpetas = new Map<Id, Carpeta__c>();
      List<Attachment> listAttach = new List<Attachment>();
      List<Carpeta__c> listCarpetas = [SELECT Id, URL__c, Documento_enviado__c, Sincronizado__c, Url_Salesforce__c
        FROM Carpeta__c
        WHERE Id IN  :recordIds AND Sincronizado__c = false
        AND URL__c != null];
      for (Carpeta__c reg : listCarpetas) {
        System.debug('log - SincronizacionCartaOppDrop: ' + reg.URL__c);
        mapCarpetas.put(reg.Id, reg);
        String[] filenametmp = reg.URL__c.split('/');
        String filename = filenametmp.get(filenametmp.size() - 1);
        String contentType = 'application/pdf';
        if (filename.split('\\.').get(1) == 'jpg') {
          contentType = 'image/jpeg';
        }
        String pathLower = reg.URL__c.toLowerCase();
        Blob file;
        file = Service_DropboxIntegration.descargarArchivosDropbox(filename, pathLower);
        if (file == null) {
          file = Blob.valueOf('Error de carga de datos');
        }
        Schema.DescribeFieldResult fieldResult = Carpeta__c.Documento_enviado__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        Map<String, String> mapValueLabel = new Map<String, String>();
        for( Schema.PicklistEntry v : values) {
          mapValueLabel.put(v.getValue(), v.getLabel());
        }
        String publicLabel = mapValueLabel.get(reg.Documento_enviado__c);
        DateTime d = System.now();
        String timestamp = '' + d.year() + '-' + d.month() + '-' + d.day() + ' ' + d.hour() + ':' + d.minute() + ':' + d.second() + '.' + d.millisecondGMT();
        Attachment attach = new Attachment();
        attach.Body = file;
        attach.Name = publicLabel + ' ' + timestamp;
        attach.IsPrivate = false;
        attach.ParentId = reg.Id;
        attach.ContentType = contentType;
        attach.Description = 'false';
        listAttach.add(attach);
      }
      insert listAttach;
      List<Carpeta__c> listCarpetasToUp = new List<Carpeta__c>();
      String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
      for (Attachment attach : listAttach) {
        Carpeta__c carpetaOpp = new Carpeta__c();
        carpetaOpp.Id = attach.ParentId;
        carpetaOpp.Url_Salesforce__c = sfdcURL + '/servlet/servlet.FileDownload?file=' + attach.Id;
        carpetaOpp.Sincronizado__c = true;
        listCarpetasToUp.add(carpetaOpp);
      }
      update listCarpetasToUp;
    }
  }
}