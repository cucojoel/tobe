/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Handler_LeadTrigger
* @Autor       Joel Soto
* @Date        14/01/2020
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
public class Handler_LeadTrigger {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void beforeInsertHandler(List<Lead> triggerNew) {
    Schema.DescribeFieldResult fieldResultOne = Lead.LeadSource.getDescribe();
    String publicLabel;
    list<Lead> newLeadSf = new List<Lead>();
    list<Schema.PicklistEntry> values =  fieldResultOne.getPicklistValues();
    map<String, String> mapValueLabelLeadSource = new Map<String, String>();
    for(Schema.PicklistEntry v : values) {
      mapValueLabelLeadSource.put(v.getValue(), v.getLabel());
    }
    Service_LeadTrigger.asignarDesarrollo(triggerNew);
    Service_LeadTrigger.asignarDesarrolloPrimario(triggerNew);
    for(Lead isnewLead : triggerNew ) {
      publicLabel = mapValueLabelLeadSource.get(isnewLead.LeadSource);
      isnewLead.Origen_Prospeccion__c = publicLabel;
      isnewLead.Origen_Medio__c = isnewLead.Medio__c;
      if(String.isBlank(isnewLead.ID_Prospecto_Enkontrol__c)) {
        newLeadSf.add(isnewLead);
      }
    }
    if (newLeadSf.size() > 0) {
      Service_LeadTrigger.asignarGuardia(newLeadSf);
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void afterInsertHandler(List<Lead> triggerNew) {
    Set<Lead> prospectosCita = new Set<Lead>();
    list<Lead> leadsInsertToEnk = new List<Lead>();
    list<String> idsDuplicate = new list<String>();
    Map<String, Lead> mapDupLead = new Map<String, Lead>();
    Set<String> matchlead = new Set<String>();
    Set<String> leadSfids = new Set<String>();
    for(Lead lid:triggerNew) {
      leadSfids.add(lid.Id);
    }
    Map<String, Lead> mapLeadSf = new Map<String, Lead>([SELECT Id, OwnerId, Email_gerente__c, Desarrollo_Comercial__c, Web_to_lead__c, Owner.Email FROM Lead WHERE Id IN: leadSfids]);
    for(Lead lid:triggerNew) {
      list<Lead> listLeadTmp = new List<Lead>();
      listLeadTmp.add(lid);
      try {
        list<Datacloud.FindDuplicatesResult> results = Datacloud.FindDuplicates.findDuplicates(listLeadTmp);
        if(!results.isEmpty()) {
          for(Datacloud.FindDuplicatesResult findDupeResult : results) {
            for(Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
              for(Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                  for(Datacloud.FieldDiff fielDiff : matchRecord.getFieldDiffs()) {
                    if(fielDiff.getDifference() == 'Same') {
                      idsDuplicate.add(lid.Id);
                      mapDupLead.put(matchRecord.getRecord().Id, lid);
                      matchlead.add(matchRecord.getRecord().Id);
                    }
                  }
                }
              }
            }
          }
        }
      } catch(Exception ex) {
        System.debug('log- Service_LeadTrigger_cls mapDesarrollos: ' + ex);
      }
      if(String.isBlank(lid.ID_Prospecto_Enkontrol__c) && !idsDuplicate.contains(lid.id)) {
        leadsInsertToEnk.add(lid);
      }
      if(String.isBlank(String.valueOf(lid.Cita_Web__c)) == false) {
        prospectosCita.add(lid);
      }
    }
    if(!prospectosCita.isEmpty()) {
      Service_LeadTrigger.crearCitaProspectos(prospectosCita);
    }
    if(!leadsInsertToEnk.isEmpty()) {
      Service_LeadTrigger.sincronizarProspectos(leadsInsertToEnk, false);
    }
    if(idsDuplicate.size() > 0) {
      sendDuplicated(mapDupLead);
      Database.delete(idsDuplicate);
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void beforeUpdateHandler(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap) {
    list<Lead> prospectoscambiodesarrollo = new List<Lead>();
    List<Profile> listProfiles = [SELECT Id, Name FROM Profile];
    Map<String, Profile> mapProfilesbyName = new Map<String, Profile>();
    for (Profile pro : listProfiles) {
      mapProfilesbyName.put(pro.Name.toLowerCase().replaceAll(' ', ''), pro);
    }
    Profile profileAsesor    = mapProfilesbyName.get('asesordeventas');
    Profile profileAsistente = mapProfilesbyName.get('asistentedeventas');
    Profile profileGerente   = mapProfilesbyName.get('gerentedeventas');
    Profile profileRecepcion = mapProfilesbyName.get('recepción');
    Profile profileAdmin     = mapProfilesbyName.get('administradordelsistema');
    for(Lead lid:triggerNew) {
      Lead lidold = triggerOldMap.get(lid.Id);
      if(lidold.OwnerId != lid.OwnerId && UserInfo.getProfileId() == profileGerente.Id) {
        lid.Agente__c = lid.OwnerId;
      }
      if((lid.Desarrollo__c != lidold.Desarrollo__c) || (lid.Desarrollo_Comercial__c != lidold.Desarrollo_Comercial__c) || (lid.Desarrollo_Web__c != lidold.Desarrollo_Web__c)) {
        prospectoscambiodesarrollo.add(lid);
      }
    }
    if(prospectoscambiodesarrollo.size() != 0) {
      Service_LeadTrigger.asignarDesarrollo(prospectoscambiodesarrollo);
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void afterUpdateHandler(map<Id, Lead> triggerNewMap, map<Id, Lead> triggerOldMap) {
    list<Lead> leadsToUpdate = new List<Lead>();
    for(Lead pro : triggerNewMap.values()) {
      if(pro.Prospecto_No_Contactado__c == false) {
        if(!Helper_RecursiveTrigger.leadsYaActualizados.contains(pro.Id)) {
          Helper_RecursiveTrigger.leadsYaActualizados.add(pro.Id);
          if(pro.ID_Prospecto_Enkontrol__c != null) {
            leadsToUpdate.add(pro);
          }
        }
        if(!leadsToUpdate.isEmpty()) {
          Service_LeadTrigger.sincronizarProspectos(leadsToUpdate, true);
        }
      }
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<Duplicados__c> sendDuplicated(Map<String, Lead> mapDupLead) {
    Set<String> dupLeadIds = new Set<String>();
    for (Lead dupLead : mapDupLead.values()) {
      dupLeadIds.add(dupLead.Id);
    }
    List<Lead> listDupLead = [SELECT Id, LastName, OwnerId, Email, Desarrollo_Comercial__c, Desarrollo_Comercial__r.Gerente_Ventas__r.Name, Web_to_lead__c, Desarrollo_web__c, Agente__c, Agente__r.Name, Agente__r.Email, Agente__r.Manager.Name, Agente__r.Manager.Email, Fecha_Alta_Sistema__c, ID_Prospecto_Enkontrol__c, Owner.Email FROM Lead WHERE Id in: dupLeadIds];
    Map<String, Lead> mapDupLid = new Map<String, Lead>();
    for (Lead dupleid:listDupLead) {
      mapDupLid.put(dupleid.Id, dupleid);
    }
    List<Duplicados__c> listDup = new List<Duplicados__c>();
    List<Lead> orgLead = [SELECT Id, OwnerId, Email_gerente__c, Desarrollo_Comercial__c, Desarrollo_Comercial__r.Gerente_Ventas__r.Name, Web_to_lead__c, Desarrollo_web__c, Agente__c, Agente__r.Name, Agente__r.Email, Agente__r.Manager.Name, Agente__r.Manager.Email, Fecha_Alta_Sistema__c, ID_Prospecto_Enkontrol__c, Owner.Email FROM Lead WHERE Id in: mapDupLead.keySet()];
    Map<String, Lead> mapOldLid = new Map<String, Lead>();
    for (Lead olderLid:orgLead) {
      mapOldLid.put(olderLid.Id, olderLid);
    }
    for (String idOld :mapDupLead.keySet()) {
      Duplicados__c duplicado = new Duplicados__c();
      Lead lid = mapDupLead.get(idOld);
      Lead lidAlt = mapDupLid.get(lid.Id);
      Lead oldLid = mapOldLid.get(idOld);
      if (lid.Desarrollo_web__c != oldLid.Desarrollo_web__c) {
        duplicado.Enviado_a__c = 'BDC';
        duplicado.Correo_Gerente__c = lidAlt.Agente__r.Manager.Email;
        duplicado.Correo_Asesor__c = lidAlt.Agente__r.Email;
        duplicado.Nombre_Asesor__c = lidAlt.Agente__r.Name;
        duplicado.Nombre_Gerente__c = lidAlt.Agente__r.Manager.Name;
      } else {
        duplicado.Enviado_a__c = 'Desarrollo';
        duplicado.Correo_Gerente__c = oldLid.Agente__r.Manager.Email;
        duplicado.Correo_Asesor__c = oldLid.Agente__r.Email;
        duplicado.Nombre_Asesor__c = oldLid.Agente__r.Name;
        duplicado.Nombre_Gerente__c = oldLid.Agente__r.Manager.Name;
      }
      duplicado.Apellido_Paterno__c = lid.LastName;
      duplicado.Desarrollo_web__c = lid.Desarrollo_web__c;
      duplicado.Email__c = lid.Email;
      duplicado.ID_EnKontrol__c = oldLid.ID_Prospecto_Enkontrol__c;
      duplicado.ID_Asesor__c = lid.OwnerId;
      duplicado.Lead__c = oldLid.Id;
      duplicado.Desarrollo_Web_Primario__c = oldLid.Desarrollo_web__c;
      duplicado.Fecha_Asignacion__c = oldLid.Fecha_Alta_Sistema__c;
      duplicado.Medio__c = lid.Medio__c;
      duplicado.Nombre__c = lid.FirstName;
      duplicado.Origen_Prospecci__c = lid.Origen_Prospeccion__c;
      duplicado.Telefono__c = lid.Phone;
      listDup.add(duplicado);
    }
    insert listDup;
    return listDup;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        14/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public void createObjectClass() {
    String alias0;
    String alias1;
    String alias2;
    String alias3;
    String alias4;
    String alias5;
    String alias6;
    String alias7;
    String appartment0;
    String appartment1;
    String appartment2;
    String appartment3;
    String appartment4;
    String appartment5;
    String appartment6;
    String appartment7;
    String assignatedToName0;
    String assignatedToName1;
    String assignatedToName2;
    String assignatedToName3;
    String assignatedToName4;
    String assignatedToName5;
    String assignatedToName6;
    String assignatedToName7;
    String bill0;
    String bill1;
    String bill2;
    String bill3;
    String bill4;
    String bill5;
    String bill6;
    String bill7;
    String contractNumber0;
    String contractNumber1;
    String contractNumber2;
    String contractNumber3;
    String contractNumber4;
    String contractNumber5;
    String contractNumber6;
    String contractNumber7;
    String contractTerm0;
    String contractTerm1;
    String contractTerm2;
    String contractTerm3;
    String contractTerm4;
    String contractTerm5;
    String contractTerm6;
    String contractTerm7;
    String description0;
    String description1;
    String description2;
    String description3;
    String description4;
    String description5;
    String description6;
    String description7;
    String development0;
    String development1;
    String development2;
    String development3;
    String development4;
    String development5;
    String development6;
    String development7;
    String divisionName0;
    String divisionName1;
    String divisionName2;
    String divisionName3;
    String divisionName4;
    String divisionName5;
    String divisionName6;
    String divisionName7;
    String errorMessage0;
    String errorMessage1;
    String errorMessage2;
    String errorMessage3;
    String errorMessage4;
    String errorMessage5;
    String errorMessage6;
    String errorMessage7;
    String idDropbox0;
    String idDropbox1;
    String idDropbox2;
    String idDropbox3;
    String idDropbox4;
    String idDropbox5;
    String idDropbox6;
    String idDropbox7;
    String idRequest0;
    String idRequest1;
    String idRequest2;
    String idRequest3;
    String idRequest4;
    String idRequest5;
    String idRequest6;
    String idRequest7;
    String language0;
    String language1;
    String language2;
    String language3;
    String language4;
    String language5;
    String language6;
    String language7;
    String messageFailure0;
    String messageFailure1;
    String messageFailure2;
    String messageFailure3;
    String messageFailure4;
    String messageFailure5;
    String messageFailure6;
    String messageFailure7;
    String messageSuccess0;
    String messageSuccess1;
    String messageSuccess2;
    String messageSuccess3;
    String messageSuccess4;
    String messageSuccess5;
    String messageSuccess6;
    String messageSuccess7;
    String name0;
    String name1;
    String name2;
    String name3;
    String name4;
    String name5;
    String name6;
    String name7;
    String nextStep0;
    String nextStep1;
    String nextStep2;
    String nextStep3;
    String nextStep4;
    String nextStep5;
    String nextStep6;
    String nextStep7;
    String objectId0;
    String objectId1;
    String objectId2;
    String objectId3;
    String objectId4;
    String objectId5;
    String objectId6;
    String objectId7;
    String operation0;
    String operation1;
    String operation2;
    String operation3;
    String operation4;
    String operation5;
    String operation6;
    String operation7;
    String opportunity0;
    String opportunity1;
    String opportunity2;
    String opportunity3;
    String opportunity4;
    String opportunity5;
    String opportunity6;
    String opportunity7;
    String origin0;
    String origin1;
    String origin2;
    String origin3;
    String origin4;
    String origin5;
    String origin6;
    String origin7;
    String ownerName0;
    String ownerName1;
    String ownerName2;
    String ownerName3;
    String ownerName4;
    String ownerName5;
    String ownerName6;
    String ownerName7;
    String prospectAgent0;
    String prospectAgent1;
    String prospectAgent2;
    String prospectAgent3;
    String prospectAgent4;
    String prospectAgent5;
    String prospectAgent6;
    String prospectAgent7;
    String prospectBirthdate0;
    String prospectBirthdate1;
    String prospectBirthdate2;
    String prospectBirthdate3;
    String prospectBirthdate4;
    String prospectBirthdate5;
    String prospectBirthdate6;
    String prospectBirthdate7;
    String prospectCity0;
    String prospectCity1;
    String prospectCity2;
    String prospectCity3;
    String prospectCity4;
    String prospectCity5;
    String prospectCity6;
    String prospectCity7;
    String prospectColony0;
    String prospectColony1;
    String prospectColony2;
    String prospectColony3;
    String prospectColony4;
    String prospectColony5;
    String prospectColony6;
    String prospectColony7;
    String prospectCompany0;
    String prospectCompany1;
    String prospectCompany2;
    String prospectCompany3;
    String prospectCompany4;
    String prospectCompany5;
    String prospectCompany6;
    String prospectCompany7;
    String prospectCountry0;
    String prospectCountry1;
    String prospectCountry2;
    String prospectCountry3;
    String prospectCountry4;
    String prospectCountry5;
    String prospectCountry6;
    String prospectCountry7;
    String prospectDes0;
    String prospectDes1;
    String prospectDes2;
    String prospectDes3;
    String prospectDes4;
    String prospectDes5;
    String prospectDes6;
    String prospectDes7;
    String prospectFirstName0;
    String prospectFirstName1;
    String prospectFirstName2;
    String prospectFirstName3;
    String prospectFirstName4;
    String prospectFirstName5;
    String prospectFirstName6;
    String prospectFirstName7;
    String prospectGender0;
    String prospectGender1;
    String prospectGender2;
    String prospectGender3;
    String prospectGender4;
    String prospectGender5;
    String prospectGender6;
    String prospectGender7;
    String prospectLastName0;
    String prospectLastName1;
    String prospectLastName2;
    String prospectLastName3;
    String prospectLastName4;
    String prospectLastName5;
    String prospectLastName6;
    String prospectLastName7;
    String prospectMoral0;
    String prospectMoral1;
    String prospectMoral2;
    String prospectMoral3;
    String prospectMoral4;
    String prospectMoral5;
    String prospectMoral6;
    String prospectMoral7;
    String prospectNacionality0;
    String prospectNacionality1;
    String prospectNacionality2;
    String prospectNacionality3;
    String prospectNacionality4;
    String prospectNacionality5;
    String prospectNacionality6;
    String prospectNacionality7;
    String prospectName0;
    String prospectName1;
    String prospectName2;
    String prospectName3;
    String prospectName4;
    String prospectName5;
    String prospectName6;
    String prospectName7;
    String prospectPhone0;
    String prospectPhone1;
    String prospectPhone2;
    String prospectPhone3;
    String prospectPhone4;
    String prospectPhone5;
    String prospectPhone6;
    String prospectPhone7;
    String prospectPuntoProspectacion0;
    String prospectPuntoProspectacion1;
    String prospectPuntoProspectacion2;
    String prospectPuntoProspectacion3;
    String prospectPuntoProspectacion4;
    String prospectPuntoProspectacion5;
    String prospectPuntoProspectacion6;
    String prospectPuntoProspectacion7;
    String prospectRFC0;
    String prospectRFC1;
    String prospectRFC2;
    String prospectRFC3;
    String prospectRFC4;
    String prospectRFC5;
    String prospectRFC6;
    String prospectRFC7;
    String prospectState0;
    String prospectState1;
    String prospectState2;
    String prospectState3;
    String prospectState4;
    String prospectState5;
    String prospectState6;
    String prospectState7;
    String prospectStreet0;
    String prospectStreet1;
    String prospectStreet2;
    String prospectStreet3;
    String prospectStreet4;
    String prospectStreet5;
    String prospectStreet6;
    String prospectStreet7;
    String quoteToName0;
    String quoteToName1;
    String quoteToName2;
    String quoteToName3;
    String quoteToName4;
    String quoteToName5;
    String quoteToName6;
    String quoteToName7;
    String reference0;
    String reference1;
    String reference2;
    String reference3;
    String reference4;
    String reference5;
    String reference6;
    String reference7;
    String request0;
    String request1;
    String request2;
    String request3;
    String request4;
    String request5;
    String request6;
    String request7;
    String requestBody0;
    String requestBody1;
    String requestBody2;
    String requestBody3;
    String requestBody4;
    String requestBody5;
    String requestBody6;
    String requestBody7;
    String requestParameter0;
    String requestParameter1;
    String requestParameter2;
    String requestParameter3;
    String requestParameter4;
    String requestParameter5;
    String requestParameter6;
    String requestParameter7;
    String requestResponse0;
    String requestResponse1;
    String requestResponse2;
    String requestResponse3;
    String requestResponse4;
    String requestResponse5;
    String requestResponse6;
    String requestResponse7;
    String signature0;
    String signature1;
    String signature2;
    String signature3;
    String signature4;
    String signature5;
    String signature6;
    String signature7;
    String source0;
    String source1;
    String source2;
    String source3;
    String source4;
    String source5;
    String source6;
    String source7;
    String specialTerms0;
    String specialTerms1;
    String specialTerms2;
    String specialTerms3;
    String specialTerms4;
    String specialTerms5;
    String specialTerms6;
    String specialTerms7;
    String stage0;
    String stage1;
    String stage2;
    String stage3;
    String stage4;
    String stage5;
    String stage6;
    String stage7;
    String subStatus0;
    String subStatus1;
    String subStatus2;
    String subStatus3;
    String subStatus4;
    String subStatus5;
    String subStatus6;
    String subStatus7;
    String successMessage0;
    String successMessage1;
    String successMessage2;
    String successMessage3;
    String successMessage4;
    String successMessage5;
    String successMessage6;
    String successMessage7;
    String title0;
    String title1;
    String title2;
    String title3;
    String title4;
    String title5;
    String title6;
    String title7;
    String typeRequest0;
    String typeRequest1;
    String typeRequest2;
    String typeRequest3;
    String typeRequest4;
    String typeRequest5;
    String typeRequest6;
    String typeRequest7;
    String zone0;
    String zone1;
    String zone2;
    String zone3;
    String zone4;
    String zone5;
    String zone6;
    String zone7;
  }
}