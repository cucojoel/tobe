/*---------------------------------------------------------------------------------------------------------

Autor: Scarlett Castillo - 09-2018 - CloudCo
Descripción: Clase Wrapper para determinar si el registro ingresado es válido

Test Class: Test_WS_EnkontrolIntegration
---------------------------------------------------------------------------------------------------------*/
public with sharing class RegistroValidoWrapper {

    public Boolean valido {get;set;}
    public List<String> camposFallidos {get;set;}
}