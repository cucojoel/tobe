public class EnviarPorCorreo {
	public Id parentid {get;set;}

	public EnviarPorCorreo() {
		/* Va recibir un Id de Cuenta */
		parentid = ApexPages.currentPage().getParameters().get('id');
	}
	
	public static void SavePdf(String oppId) {
		Date deit = System.today();
		set<String> departamentos = new set<String>();		
		Opportunity oppID_query = [SELECT Id, AccountId, ApartmentLocation__c, Id_Prospecto_Enkontrol__c From Opportunity WHERE Id =: oppId];
		
		/* */
		String SOAP_ENVELOPE = 'http://schemas.xmlsoap.org/soap/envelope/';
		String TEM_NAMESPACE = 'http://tempuri.org/';
		String BEG_NAMESPACE = 'http://schemas.datacontract.org/2004/07/BeGrand.Common.Services';
		String DTO_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO';
		
		Configuracion_Enkontrol__c configuracion = [SELECT Id, Username__c, Password__c, Service_Endpoint__c FROM Configuracion_Enkontrol__c];
		User usuario = [SELECT Id, Id_Enkontrol__c FROM User WHERE Id =: UserInfo.getUserId()];
		  		
  		String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:enk="http://schemas.datacontract.org/2004/07/EnKontrol.Request">'
			+'<soapenv:Header>'
			+'<Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
			+'<UsernameToken>'
			+'<Username>' + configuracion.Username__c + '</Username>'
			+'<Password>' + configuracion.Password__c + '</Password>'
			+'</UsernameToken>'
			+'</Security>'
			+'</soapenv:Header>'
			+'<soapenv:Body>'
			+'<tem:AccountStatementExecute>'
			+'<tem:request>'
			+'<enk:AccountStatementType>ByProspect</enk:AccountStatementType>'
			+'<enk:AgentId>' + usuario.Id_Enkontrol__c + '</enk:AgentId>'
			+'<enk:ProspectId>' + oppID_query.Id_Prospecto_Enkontrol__c+ '</enk:ProspectId>'
			+'<enk:ValidateAgent>true</enk:ValidateAgent>'
			+'</tem:request>'
			+'</tem:AccountStatementExecute>'
			+'</soapenv:Body>'
			+'</soapenv:Envelope>';
			
		Http http = new Http();
		HttpRequest req = new HttpRequest();
		req.setEndpoint(configuracion.Service_Endpoint__c);
		req.setMethod('GET');
		req.setHeader('Content-Type', 'text/xml');
		req.setHeader('SOAPAction', 'http://tempuri.org/IEnKontrolService/AccountStatementExecute');
		req.setBody(body);
			
		HTTPResponse res = http.send(req);
			
		Dom.Document doc = res.getBodyDocument();
		Dom.XMLNode  nodeRoot = doc.getRootElement();
		
    	if(res.getStatusCode() == 200) {
    		Dom.XmlNode nodeBody = nodeRoot.getChildElement('Body', SOAP_ENVELOPE);
    		Dom.XmlNode nodeStatementExeRespon = nodeBody.getChildElement('AccountStatementExecuteResponse', TEM_NAMESPACE);
    		Dom.XmlNode nodeStatementExeResult = nodeStatementExeRespon.getChildElement('AccountStatementExecuteResult', TEM_NAMESPACE);
    		Dom.XmlNode errorList = nodeStatementExeResult.getChildElement('ErrorList', BEG_NAMESPACE);
    		Dom.XmlNode nodeList = nodeStatementExeResult.getChildElement('List', BEG_NAMESPACE);
    		Dom.XmlNode isSuccess = nodeStatementExeResult.getChildElement('Success', BEG_NAMESPACE);
    		for(Dom.XmlNode nodeStatementDto: nodeList.getChildren()) {
    			departamentos.add(nodeStatementDto.getChildElement('ApartmentLocation',DTO_NAMESPACE).getText());
      		}
    	}
      	/* */
      	
      	for(String forData : departamentos) {
			PageReference pdf = Page.EstadoCuenta;
			pdf.getParameters().put('id', oppID_query.Id);
			pdf.getParameters().put('render', 'PDF');
			pdf.getParameters().put('edocta', forData);

			Blob bodyBlob;
			try { bodyBlob = pdf.getContentAsPDF(); }
			catch (VisualforceException e) { bodyBlob = Blob.valueOf('Error de carga de datos'); }
			
			Attachment attach = new Attachment();	
			attach.Body = bodyBlob;
			attach.Name = deit.format() + ' ' + forData + '.pdf';
			attach.IsPrivate = false;
			attach.Description = 'false';
			attach.ParentId = oppID_query.Id;
			attach.ContentType = 'application/pdf';
		
			//try { insert attach; } catch(Exception ex) { system.debug(ex.getMessage()); }
			insert attach;
      	}
	}
  
  	public Pagereference enviarMail() {
  		Attachment adjuntos = new Attachment();
  		Id idOpp = [select Id from Opportunity where AccountId =: parentid limit 1].Id;
	  	SavePdf(idOpp);
	  	
  		if(parentid != NULL) {
	  		Opportunity oppRecord = [SELECT Id, Name, Account.Name, Account.PersonEmail, Desarrollo__c, Desarrollo__r.Name, Desarrollo__r.Id_Desarrollo_Enkontrol__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c, ID_Cliente_Enkontrol__c, Id_Prospecto_Enkontrol__c, AccountId, ApartmentLocation__c FROM Opportunity WHERE Id =: idOpp];
  			Pagereference pageRef = null;
  			adjuntos = [SELECT Id, Body FROM Attachment WHERE ParentId =: idOpp ORDER BY CreatedDate DESC LIMIT 1];
  			Date deit = System.today();
  		
  			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
  			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
  			efa.setFileName('Estado de cuenta - ' + deit.format() + ' ' + oppRecord.ApartmentLocation__c + '.pdf');
  			efa.setBody(adjuntos.Body);
  		
  			//string[] toAddresses = New String[] {oppRecord.Account.PersonEmail};
  			string[] toAddresses = New String[] {'mario.quevedo@cloudmobile.com.mx'};
  		
  			email.setSubject( 'Estado de cuenta ' + deit.format());
  			email.setToAddresses( toAddresses );
  			email.setPlainTextBody( 'Estimado: ' + oppRecord.Account.Name + ' hemos generado un nuevo estado de cuenta para usted la fecha cual se generó el estado de cuenta es ' + deit.format() + '.');
  			email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
  			/*
	  		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
  			for(Messaging.SendEmailResult rr : r) {
	  			if(rr.isSuccess() == true) {
  					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Mensaje enviado'));
  				}
  			}
  			return pageRef;
  			*/
  		}
  		return null;
  	}
}