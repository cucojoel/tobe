@isTest
private class OpportunityTriggerTest {
  @testSetup static void testSetup() {
    Configuracion_Enkontrol_Cierre__c confEnk = new Configuracion_Enkontrol_Cierre__c();
    confEnk.Name = 'Cierre';
    confEnk.Service_Endpoint__c = 'https://e-pago.begrand.mx/prospectoTest/EnkontrolM22Service.svc';
    confEnk.Username__c = 'Username';
    confEnk.Password__c = 'Password';
    insert confEnk;
    Compania__c companiaUno = Bg_Utils_Cls.genCompania('Be Grand', '1');
    insert companiaUno;
    User asesorUno = Bg_Utils_Cls.genUser('asesorUno', 'Asesor de Ventas');
    insert asesorUno;
    Desarrollo_Comercial__c desComUno = Bg_Utils_Cls.genDesComercial('Coapa', companiaUno.Id, asesorUno.Id );
    insert desComUno;
    Guardia__c guardiaUno = Bg_Utils_Cls.genGuardia(desComUno.Id, desComUno.Id, asesorUno.Id, 1);
    insert guardiaUno;
    Desarrollo__c desarrollo = Bg_Utils_Cls.genDesarrollo('Coapa', '1', desComUno.Id);
    insert desarrollo;
    RecordType rType = Bg_Utils_Cls.getRtype('PersonAccount', 'Account');
    Account acc = Bg_Utils_Cls.genAccount('persona fisica', '1', rType.Id);
    insert acc;
    Contact contacto = [SELECT Id FROM Contact WHERE AccountId =: acc.Id LIMIT 1];
    Opportunity opp = Bg_Utils_Cls.genOpportunity('name', 'Nueva', desarrollo.Id, acc.Id);
    opp.Id_Prospecto_Enkontrol__c = '1';
    opp.ID_Oportunidad_Enkontrol__c = '1';
    opp.DocumentacionCompleta__c = false;
    insert opp;
    opp.StageName = 'Cotizacion';
    update opp;
    C_Oferta__c cartaOferta = new C_Oferta__c();
    cartaOferta.Oportunidad__c = opp.Id;
    insert cartaOferta;
    Attachment attach = Bg_Utils_Cls.genAttachment('name', cartaOferta.Id);
    insert attach;
    DocumentoxEsquema__c docEsq = new DocumentoxEsquema__c();
    docEsq.Documento__c = 'Carta oferta';
    docEsq.Name = 'Carta oferta pf';
    docEsq.Tipo_de_persona__c = 'Persona Física';
    docEsq.Etapa__c = 'Expediente';
    insert docEsq;
    DocumentoxEsquema__c docEsq0 = new DocumentoxEsquema__c();
    docEsq0.Documento__c = 'CURP';
    docEsq0.Name = 'CURP pf';
    docEsq0.Tipo_de_persona__c = 'Persona Física';
    docEsq0.Etapa__c = 'Expediente';
    insert docEsq0;
  }
  static testMethod void testOne() {
    System.debug('log - OpportunityTriggerTest: ####1');
    Desarrollo__c desarrollo = [SELECT Id FROM Desarrollo__c LIMIT 1];
    Account acc = [SELECT Id FROM Account LIMIT 1];
    Opportunity opp = Bg_Utils_Cls.genOpportunity('name', 'Expediente', desarrollo.Id, acc.Id);
    opp.Id_Prospecto_Enkontrol__c = '2';
    opp.ID_Oportunidad_Enkontrol__c = '2';
    opp.DocumentacionCompleta__c = false;
    insert opp;
    Attachment attach = Bg_Utils_Cls.genAttachment('Carta oferta', opp.Id);
    attach.Description = '';
    insert attach;
    Attachment attach0 = Bg_Utils_Cls.genAttachment('CURP', opp.Id);
    attach0.Description = '';
    insert attach0;
    delete opp;
  }
  static testMethod void testTwo() {
    System.debug('log - OpportunityTriggerTest: ####2');
    Opportunity opp = [SELECT Id, Id_Prospecto_Enkontrol__c, ID_Oportunidad_Enkontrol__c, Usuario_Enk__c, DocumentacionCompleta__c FROM Opportunity LIMIT 1];
    Boolean docscompletos = false;
    if(!opp.DocumentacionCompleta__c && docscompletos || opp.DocumentacionCompleta__c && !docscompletos) {
      OpportunityHelper.updateMyOpp(opp, docscompletos);
    }
  }
  static testMethod void testThree() {
    System.debug('log - OpportunityTriggerTest: ####3');
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/prospectoTest/EnkontrolM22Service.svc', Bg_Utils_Cls.RESPUESTA_ENKONTROL_CIERRE_PROSPECTO);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    Opportunity opp = [SELECT Id, Id_Prospecto_Enkontrol__c,StageName, ID_Oportunidad_Enkontrol__c, Usuario_Enk__c FROM Opportunity LIMIT 1];
    System.debug('log - OpportunityTriggerTest: ' + opp);
    opp.StageName = 'Cierre de Prospecto';
    update opp;
  }
}