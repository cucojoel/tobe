global with sharing class CONTROL_CrearBitacoraCuenta {

	public Bitacora__c bitacora {get; set;}
	public Account cliente{get;set;}
	public String crearCita {get; set;}
	public Set<Id> idsAsesores;
	public Map<String,Task> mapaAsesorHorasTareas;
	public Map<String,Event> mapaAsesorHorasEventos;
	public List<SelectOption> horariosDisponibles {get; set;}
	public String horarioSeleccionado {get; set;}
	public Integer numeroIntentosGeneral {get; set;}
	
	public CONTROL_CrearBitacoraCuenta() {
		
		bitacora = new Bitacora__c();
		
		crearCita = '';
		idsAsesores = new Set<Id>();
		mapaAsesorHorasTareas = new Map<String,Task>();
		mapaAsesorHorasEventos = new Map<String,Event>();
		horariosDisponibles = new List<SelectOption>();
		horariosDisponibles.add(new SelectOption('','--Ninguna--'));
		
		horarioSeleccionado = null;
		
		obtenerDatosIniciales();
		
		/*****desarrolloComercialOriginal*****/
		/*****statusProspectoOriginal*****/
	}
	
	//Metodo para inicializar las variables necesarias
	public void obtenerDatosIniciales(){

		cliente = [SELECT Id, Name, F_CM__c, DC_CM__c, DC_CM__r.Name, DC_CM__r.Hora_Inicio__c, DC_CM__r.Hora_Fin__c, Grado_de_interes__c, OwnerId FROM Account WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
		numeroIntentosGeneral = [SELECT COUNT() FROM Bitacora__c WHERE Cliente__c =: cliente.Id];
	}
	
	//Metodo para obtener las formas de contacto de un cliente al crear una bitacora
	public List<SelectOption> getFormasContacto(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Forma_de_Contacto__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para obtener las contestaciones del contacto al crear una bitacora
	public List<SelectOption> getContestaciones(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Contesta__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para obtener los resultados del contacto al crear una bitacora en base a la forma de contacto elegida cuando no contesto
	public List<SelectOption> getResultados(){
		
		System.debug('*****bitacora.Forma_de_Contacto__c: ' + bitacora.Forma_de_Contacto__c);
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Resultado__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
        String[] arregloFormaContacto;
        String formaContacto = '';
		for(Schema.PicklistEntry f : ple){
			
			System.debug('*****f.getValue(): ' + f.getValue());
			System.debug('*****f.getValue().contains(-): ' + f.getValue().contains('-'));
			if(f.getValue().contains('-')){				
				
				arregloFormaContacto = f.getValue().split('-');
				formaContacto = arregloFormaContacto.get(0);
				System.debug('*****formaContacto: ' + formaContacto);
				if(formaContacto == bitacora.Forma_de_Contacto__c){
					System.debug('*****agregado: ' + f.getValue());
					optionList.add(new SelectOption(f.getValue(), f.getLabel()));
				}
			}
		}
		
		System.debug('*****optionList: ' + optionList);
		return optionList;
	}
	
	//Metodo para obtener las opciones para crear o no una cita cuando un cliente si a contestado
	public List<SelectOption> getOpcionesCrearCita(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		optionList.add(new SelectOption('Si','Si'));
		optionList.add(new SelectOption('No','No'));
		return optionList;
	}
	
	//Metodo para obtener los estatus cuando un cliente ha contestado pero no desea agendar cita
	public List<SelectOption> getEstatusNoAgendaCita(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Bitacora__c.Estatus_No_Agenda_Cita__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
        String[] arregloFormaContacto;
        String formaContacto = '';
		for(Schema.PicklistEntry f : ple){
			
			if(f.getValue().contains('-')){
				
				arregloFormaContacto = f.getValue().split('-');
				formaContacto = arregloFormaContacto.get(0);
				System.debug('*****formaContacto: ' + formaContacto);
				
				if(formaContacto == bitacora.Forma_de_Contacto__c){
					System.debug('*****agregado: ' + f.getValue());
					optionList.add(new SelectOption(f.getValue(), f.getLabel()));
				}
			}
			else{
				optionList.add(new SelectOption(f.getValue(), f.getLabel()));
			}
		}
		return optionList;
	}
	
	//Metodo para obtener los estatus cuando un cliente ha contestado pero no desea agendar cita
	public List<SelectOption> getGradosInteres(){
		
		List<SelectOption> optionList = new List<SelectOption>(); 
		optionList.add(new SelectOption('','--Ninguno--'));
		
		Schema.DescribeFieldResult fieldResult = Lead.Grado_de_interes__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        	
		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}
	
	//Metodo para buscar los horarios disponibles de los asesores en base a la fecha seleccionada y las guardias existentes
	public void buscarHorariosDisponibles(){
		
		idsAsesores = new Set<Id>();
		mapaAsesorHorasTareas = new Map<String,Task>();
		mapaAsesorHorasEventos = new Map<String,Event>();		
		horariosDisponibles = new List<SelectOption>();
		horariosDisponibles.add(new SelectOption('','--Ninguna--'));
		horarioSeleccionado = null;
		
		//Se obtienen las guardias disponibles para el dia seleccionado
		List<Guardia__c> guardias = new List<Guardia__c>();

		//Se toman en cuenta las guardias existentes dependiendo del desarrollo comercial seleccionado
		System.debug('*****cliente.F_CM__c: ' + cliente.F_CM__c);
		System.debug('*****cliente.DC_CM__c: ' + cliente.DC_CM__c);
		guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: cliente.DC_CM__c AND Fecha_Guardia__c =: cliente.F_CM__c];

		System.debug('*****guardias: ' + guardias);
		
		//Se obtienen los asesores asignados en las guardias disponibles para el dia seleccionado
		for(Guardia__c g : guardias){
			idsAsesores.add(g.Nombre_Asesor__c);
		}
		System.debug('*****idsAsesores: ' + idsAsesores);
		
		//Se obtienen todas las tareas (citas) de los asesores obtenidos previamente
		List<Task> tareasAsesores = [SELECT Id, OwnerId, Fecha_Cita__c, ActivityDate FROM Task WHERE OwnerId IN : idsAsesores AND DAY_ONLY(Fecha_Cita__c) =: cliente.F_CM__c];
		System.debug('*****tareasAsesores: ' + tareasAsesores);
		
		//En base a las tareas obtenidas se crea un mapa con el asesor y la hora de la tarea
		for(Task ta : tareasAsesores){
			mapaAsesorHorasTareas.put(ta.OwnerId + '-' + ta.Fecha_Cita__c.hour(),ta);
		}
		System.debug('*****mapaAsesorHorasTareas: ' + mapaAsesorHorasTareas);		
		
		//Se obtienen todos los eventos de los asesores obtenidos previamente
		List<Event> eventosAsesores = [SELECT Id, OwnerId, StartDateTime, EndDateTime, ActivityDate, DurationInMinutes FROM Event WHERE OwnerId IN : idsAsesores AND ActivityDate =: cliente.F_CM__c];
		System.debug('*****eventosAsesores: ' + eventosAsesores);
		
		//En base a los eventos obtenidos se crea un mapa con el asesor y la hora del evento
		Integer horasDuracionEvento;
		Integer j;
		Integer sumaHora;
		for(Event ea : eventosAsesores){
			
			//Se obtienen cuantas horas dura el evento
			horasDuracionEvento = Integer.valueOf(Math.ceil(Decimal.valueOf(ea.DurationInMinutes) / 60.0));
			
			//En base al numero de horas que dura el evento se crean elementos en el mapa comenzando con la hora de inicio del evento
			j = 1;
			sumaHora = ea.StartDateTime.hour();
			while(horasDuracionEvento >= j){
				mapaAsesorHorasEventos.put(ea.OwnerId + '-' + sumaHora,ea);
				j++;
				sumaHora++;
			}
		}
		System.debug('*****mapaAsesorHorasEventos: ' + mapaAsesorHorasEventos);
		
		System.debug('*****cliente.DC_CM__c: ' + cliente.DC_CM__c);
		Desarrollo_Comercial__c desarrolloComercialElegido = [SELECT Id, Hora_Inicio__c, Hora_Fin__c FROM Desarrollo_Comercial__c WHERE Id =: cliente.DC_CM__c];
		System.debug('*****desarrolloComercialElegido.Hora_Inicio__c: ' + desarrolloComercialElegido.Hora_Inicio__c);
		System.debug('*****desarrolloComercialElegido.Hora_Inicio__c.hour(): ' + desarrolloComercialElegido.Hora_Inicio__c.hour());
		System.debug('*****desarrolloComercialElegido.Hora_Fin__c.hour(): ' + desarrolloComercialElegido.Hora_Fin__c.hour());
		Integer horaInicio = desarrolloComercialElegido.Hora_Inicio__c.hour();
		Integer horaFin = desarrolloComercialElegido.Hora_Fin__c.hour() - 1;
		Boolean horarioDisponible = false;
		String llaveAsesorHora = '';
		/*List<Integer> horariosDisponiblesInt = new List<Integer>();*/
		//Se hace un ciclo desde la hora de inicio hasta la hora de fin de actividades del desarrollo comercial
		for(Integer i = horaInicio; i <= horaFin; i++){
			
			System.debug('*****i:' + i);
			//Se hace un ciclo por cada uno de los asesores
			horarioDisponible = false;
			for(Id ida : idsAsesores){
				
				//Si ni el mapa de tareas ni el mapa de eventos contienen el asesor y la hora es una hora disponible a elegir 
				llaveAsesorHora = ida + '-' + i;
				System.debug('*****llaveAsesorHora: ' + llaveAsesorHora);
				if(mapaAsesorHorasTareas.containsKey(llaveAsesorHora) == false && mapaAsesorHorasEventos.containsKey(llaveAsesorHora) == false){
					horarioDisponible = true;
				}
				
				System.debug('*****horarioDisponible: ' + horarioDisponible);
			}
			
			if(horarioDisponible == true){
				/*horariosDisponiblesInt.add(i);*/				
				horariosDisponibles.add(new SelectOption(String.valueOf(i),(String.valueOf(i).length() == 1 ? '0'+i+':00' : i+':00')));
			}
		}		
		/*System.debug('*****horariosDisponiblesInt:' + horariosDisponiblesInt);*/
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la forma de contacto seleccionada
	public void limpiaVariablesFormaContacto(){
		
		bitacora.Contesta__c = null;
		bitacora.Resultado__c = null;
		crearCita = null;
		bitacora.Estatus_No_Agenda_Cita__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		horarioSeleccionado = null;
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la contestacion seleccionada
	public void limpiaVariablesContesta(){
		
		bitacora.Resultado__c = null;
		crearCita = null;
		bitacora.Estatus_No_Agenda_Cita__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		horarioSeleccionado = null;
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la opcion de crear cita seleccionada
	public void limpiaVariablesCrearCita(){
		
		bitacora.Resultado__c = null;
		bitacora.Estatus_No_Agenda_Cita__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		horarioSeleccionado = null;
	}
	
	//Metodo para limpiar las variables de la bitacora y cita cuando se cambia la opcion de crear cita seleccionada
	public void limpiaVariablesEstatusNoAgendaCita(){
		
		bitacora.Resultado__c = null;
		bitacora.Comentarios__c = null;
		bitacora.Fecha_futura_de_contacto__c = null;
		cliente.F_CM__c = null;
		horarioSeleccionado = null;
	}
	
		//Metodo para guardar una bitacora y en su caso una cita
	public PageReference guardar(){
		
		//Validaciones
		Boolean errorCantidadCitasPendientes = false;
		Boolean errorFormaContacto = false;
		Boolean errorContesta = false;
		Boolean errorResultado = false;
		Boolean errorCrearCita = false;
		Boolean errorEstatusNoCrearCita = false;
		Boolean errorFechaCita = false;
		Boolean errorHorarioCita = false;
		Boolean errorGradoInteres = false;
		Boolean existeAlgunError = false;
		
		Integer cantidadCitasPendientes = [SELECT count() FROM Task WHERE Subject = 'Cita' AND WhatId =: cliente.Id AND Estatus_de_Visita__c = 'Pendiente'];
		
		if(cantidadCitasPendientes >= 1){
			errorCantidadCitasPendientes = true;
		}			
		if(String.isBlank(bitacora.Forma_de_Contacto__c) == true){
			errorFormaContacto = true;
		}
		if(String.isBlank(bitacora.Contesta__c) == true){
			errorContesta = true;
		}		
		if(String.isBlank(bitacora.Resultado__c) == true){
			errorResultado = true;
		}
		if(String.isBlank(crearCita) == true){
			errorCrearCita = true;
		}
		if(String.isBlank(bitacora.Estatus_No_Agenda_Cita__c) == true){
			errorEstatusNoCrearCita = true;
		}
		if(String.isBlank(String.valueOf(cliente.F_CM__c)) == true){
			errorFechaCita = true;
		}
		if(String.isBlank(horarioSeleccionado) == true){
			errorHorarioCita = true;
		}
		if(String.isBlank(cliente.Grado_de_interes__c) == true){
			errorGradoInteres = true;
		}
		
		//Si no se selecciono forma de contacto y una contestacion se manda error
		if(errorFormaContacto == true || errorContesta == true){
			if(errorFormaContacto == true){
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar una forma de contacto.');
	    		ApexPages.addMessage(mensaje);
			}
			if(errorContesta == true){
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar si ha habido contestación o no por parte del cliente.');
	    		ApexPages.addMessage(mensaje);
			}
			existeAlgunError = true;
		}
		//Si si se selecciono una forma de contacto y una contestacion
		else{	
			//Si la contestacion fue No
			if(bitacora.Contesta__c == 'No'){	
				//Si no se selecciono un resultado se manda error
				if(errorResultado == true){
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un resultado.');
	    			ApexPages.addMessage(mensaje);	    			
	    			existeAlgunError = true;
				}
			}
			//Si la contestacion fue Si
			else if(bitacora.Contesta__c == 'Si'){
				//Si no se selecciono una opcion de crear una cita se manda error
				if(errorCrearCita == true){
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar si se creará o no una cita.');
	    			ApexPages.addMessage(mensaje);	    			
	    			existeAlgunError = true;
				}
				//Si si se selecciono una opcion de crear una cita
				else{
					//Si no se desea crear una cita
					if(crearCita == 'No'){	
						//Si no se selecciono un estatus se manda error
						if(errorEstatusNoCrearCita == true){
							ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar el estatus de la no creación de la cita.');
		    				ApexPages.addMessage(mensaje);		    			
		    				existeAlgunError = true;
						}
					}
					//Si si se desea crear una cita
					else if(crearCita == 'Si'){
						//Si el prospecto ya tiene una cita con estatus pendiente no se puede crear una cita mas
						if(errorCantidadCitasPendientes == true){
							ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'No se le puede generar la cita al cliente pues ya tiene una cita pendiente.');
					    	ApexPages.addMessage(mensaje);
							existeAlgunError = true;
						}						
						//Si no se selecciono una fecha y un horario para la cita se manda error
						else if(errorFechaCita == true || errorHorarioCita == true || errorGradoInteres == true){
							if(errorFechaCita == true){
								ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar una fecha para la cita.');
					    		ApexPages.addMessage(mensaje);					    		
					    		existeAlgunError = true;
							}
							if(errorHorarioCita == true){
								ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un horario para la cita.');
					    		ApexPages.addMessage(mensaje);					    		
					    		existeAlgunError = true;
							}
							if(errorGradoInteres == true){
								ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un grado de interés del prospecto.');
					    		ApexPages.addMessage(mensaje);					    		
					    		existeAlgunError = true;
							}
						}
					}
				}
			}
		}
		
		//Si hubo error(es) en las validaciones se muestra mensaje de error y se regresa a la pagina
		if(existeAlgunError == true){
			return null;
		}
		//Si no hubo error(es) en las validaciones se procede con el guardado de la bitacora y en su caso con el guardado de la cita
		else{
			
			Integer cantidadIntentos = 0;
			cantidadIntentos = [SELECT COUNT() FROM Bitacora__c WHERE Cliente__c =: cliente.Id];

			bitacora.Cliente__c = cliente.Id;
			cliente.Cantidad_Bitacoras__c = cantidadIntentos + 1;

			bitacora.Intento__c = String.valueOf(cantidadIntentos + 1);
			bitacora.Desarrollo_Comercial__c = cliente.DC_CM__c;
			
			//Si el prospecto no contesto se guarda la bitacora y se actualiza la etapa del prospecto a En Proceso
			if(bitacora.Contesta__c == 'No'){
				insert bitacora;
			}		
				
			//Si el prospecto si contesto se guarda la bitacora y se actualiza el estatus del prospecto BDC a Contactado o Citado
			if(bitacora.Contesta__c == 'Si'){
				
				if(crearCita == 'No'){
					
					insert bitacora;
					
					if(bitacora.Estatus_No_Agenda_Cita__c == 'No interesado por el momento' || bitacora.Estatus_No_Agenda_Cita__c == 'Interesado a futuro' || bitacora.Estatus_No_Agenda_Cita__c == 'Contesta y pide llamar después'){
						Event evento = new Event();
						//evento.OwnerId = cliente.OwnerId;
						evento.OwnerId = UserInfo.getUserId();
						evento.Desarrollo_Comercial__c = cliente.DC_CM__c;
					    evento.WhatId = cliente.Id;
					    evento.StartDateTime = DateTime.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day(),bitacora.Fecha_futura_de_contacto__c.hour(),bitacora.Fecha_futura_de_contacto__c.minute(),bitacora.Fecha_futura_de_contacto__c.second());
					    evento.EndDateTime = DateTime.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day(),bitacora.Fecha_futura_de_contacto__c.addHours(1).hour(),bitacora.Fecha_futura_de_contacto__c.minute(),bitacora.Fecha_futura_de_contacto__c.second());
					    evento.ActivityDateTime = DateTime.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day(),bitacora.Fecha_futura_de_contacto__c.hour(),bitacora.Fecha_futura_de_contacto__c.minute(),bitacora.Fecha_futura_de_contacto__c.second());
					    evento.IsReminderSet = true;
					    evento.ReminderDateTime = DateTime.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day(),bitacora.Fecha_futura_de_contacto__c.hour(),bitacora.Fecha_futura_de_contacto__c.addMinutes(-10).minute(),bitacora.Fecha_futura_de_contacto__c.second());
					    evento.Subject = 'Contacto Futuro';					    
					    insert evento;
					    
					    /* Agrego MQD : 02-08-2019 */
					    list<Task> tareas = new list<Task>();
					    /* *********************** */
					    Task tarea = new Task();
						tarea.Subject = 'Contacto Futuro';
						//tarea.OwnerId = cliente.OwnerId;
						tarea.OwnerId = UserInfo.getUserId();
						tarea.WhatId = cliente.Id;
						tarea.Desarrollo_Comercial__c = cliente.DC_CM__c;
						tarea.Fecha_Cita__c = DateTime.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day(),bitacora.Fecha_futura_de_contacto__c.hour(),bitacora.Fecha_futura_de_contacto__c.minute(),bitacora.Fecha_futura_de_contacto__c.second());
						tarea.ActivityDate = Date.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day());
						tarea.Cita_para_cuenta__c = true;
						//tarea.Nombre_Prospecto__c = prospecto.Name;
						tareas.add(tarea);
						
						/* Agrego MQD : 02-08-2019 */
						/*Task tareaRecepcion1 = new Task();
						tareaRecepcion1.Subject = 'Contacto Futuro';
						tareaRecepcion1.OwnerId = usuarioRecepcion.get(desarrolloComercial.get(prospecto.Desarrollo_Comercial__c));
						tareaRecepcion1.WhoId = prospecto.Id;
						tareaRecepcion1.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
						tareaRecepcion1.Fecha_Cita__c = DateTime.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day(),bitacora.Fecha_futura_de_contacto__c.hour(),bitacora.Fecha_futura_de_contacto__c.minute(),bitacora.Fecha_futura_de_contacto__c.second());
						tareaRecepcion1.ActivityDate = Date.newInstance(bitacora.Fecha_futura_de_contacto__c.year(),bitacora.Fecha_futura_de_contacto__c.month(),bitacora.Fecha_futura_de_contacto__c.day());
						tareaRecepcion1.Nombre_Prospecto__c = prospecto.Name;
						tareaRecepcion1.Cita_para_cuenta__c = true;
						tareas.add(tareaRecepcion1);*/
						insert tareas;
						/* *********************** */
					}
				}
				else if(crearCita == 'Si'){
					
					bitacora.Resultado__c = 'Agendo Cita';
					insert bitacora;
					
					String[] arregloHorarioElegido = horarioSeleccionado.split('\\:');
					Integer horaElegida = Integer.valueOf(arregloHorarioElegido[0]);
					System.debug('*****horaElegida:' + horaElegida);
					
					//Se recorren los asesores para verificar cuales estan disponibles en el horario seleccionado
					String llaveAsesorHora = '';
					Set<Id> idsAsesoresDisponibles = new Set<Id>();
					System.debug('*****idsAsesores Guardar:' + idsAsesores);
					
					for(Id ida : idsAsesores){	
						//Si ni el mapa de tareas ni el mapa de eventos contienen el asesor y la hora es un asesor al que se le puede asignar la cita
						llaveAsesorHora = ida + '-' + horaElegida;
						System.debug('*****llaveAsesorHora: ' + llaveAsesorHora);
						if(mapaAsesorHorasTareas.containsKey(llaveAsesorHora) == false && mapaAsesorHorasEventos.containsKey(llaveAsesorHora) == false){
							idsAsesoresDisponibles.add(ida);
						}
					}
					System.debug('*****idsAsesoresDisponibles: ' + idsAsesoresDisponibles);
					
					for(User testAsesor : [SELECT Id, Name FROM User WHERE Id IN : idsAsesoresDisponibles]){
						System.debug('*****Asesor Disponible: ' + testAsesor.Name);
					}
		
					//Se buscan las guardias de los asesores disponibles para la comparacion de roll y contador de citas para determinar a cual asesor se
					//le asigna la cita
					List<Guardia__c> guardiasAsesoresDisponibles = [SELECT Nombre_Asesor__c, Roll__c, Contador_Citas__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: cliente.DC_CM__c AND Fecha_Guardia__c =: cliente.F_CM__c AND Nombre_Asesor__c IN : idsAsesoresDisponibles ORDER BY Roll__c];
					System.debug('*****guardiasAsesoresDisponibles: ' + guardiasAsesoresDisponibles);
	
					Guardia__c guardiaActual = new Guardia__c();
					Guardia__c guardiaSiguiente = new Guardia__c();
					Integer contadorCitasGuardiaActual = 0;
					Integer contadorCitasGuardiaSiguiente = 0;
					for(Integer i = 0; i < guardiasAsesoresDisponibles.size(); i++){
						if(i==0){
							guardiaActual = guardiasAsesoresDisponibles.get(i);	
						}							
						
						if(i < (guardiasAsesoresDisponibles.size() - 1)){
							
							guardiaSiguiente = guardiasAsesoresDisponibles.get(i + 1);
							
							System.debug('*****guardiaActual:' + guardiaActual);
							System.debug('*****guardiaSiguiente:' + guardiaSiguiente);
							
							contadorCitasGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c));
							contadorCitasGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Citas__c));
							System.debug('*****contadorCitasGuardiaActual:' + contadorCitasGuardiaActual);
							System.debug('*****contadorCitasGuardiaSiguiente:' + contadorCitasGuardiaSiguiente);
							
							if(contadorCitasGuardiaActual <= contadorCitasGuardiaSiguiente){
								guardiaActual = guardiaActual;
								System.debug('****entro a if');
							}
							else{
								guardiaActual = guardiaSiguiente;
								System.debug('****entro a else');
							}
						}
						else{
							break;
						}
					}
					
					System.debug('*****guardiaActual: ' + guardiaActual);
					
					/* Agrego MQD : 02-08-2019 */
					//list<Task> tareas = new list<Task>();
					/* *********************** */
					    
					Task cita = new Task();
					cita.Subject = 'Cita';
					cita.OwnerId = guardiaActual.Nombre_Asesor__c;
					cita.WhatId = cliente.Id;
					cita.Desarrollo_Comercial__c = cliente.DC_CM__c;
					cita.Fecha_Cita__c = DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0);
					cita.ActivityDate = Date.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day());
					cita.Estatus_de_Visita__c = 'Pendiente';
					//cita.Nombre_Prospecto__c = prospecto.Name;
					cita.Cita_para_cuenta__c = true;
					insert cita;
					System.debug('*****Cita Asesor de Ventas (tarea) creada:' + cita);
					
					/* Agrego MQD : 02-08-2019 */
					/*Task tareaRecepcion2 = new Task();
					tareaRecepcion2.Subject = 'Cita';
					tareaRecepcion2.OwnerId = usuarioRecepcion.get(desarrolloComercial.get(prospecto.Desarrollo_Comercial__c));
					tareaRecepcion2.WhoId = prospecto.Id;
					tareaRecepcion2.Desarrollo_Comercial__c = prospecto.Desarrollo_Comercial__c;
					tareaRecepcion2.Fecha_Cita__c = DateTime.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day(),horaElegida,0,0);
					tareaRecepcion2.ActivityDate = Date.newInstance(prospecto.F_CM__c.year(),prospecto.F_CM__c.month(),prospecto.F_CM__c.day());
					tareaRecepcion2.Estatus_de_Visita__c = 'Pendiente';
					tareaRecepcion2.Nombre_Prospecto__c = prospecto.Name;
					tareaRecepcion2.Id_Cita_Relacionada__c = cita.Id;
					tareaRecepcion2.Cita_para_cuenta__c = true;
					insert tareaRecepcion2;
					System.debug('*****Cita Recepcion (tarea) creada:' + tareaRecepcion2);*/
					/* *********************** */
					
					/*cita.Id_Cita_Relacionada__c = tareaRecepcion2.Id;
					update cita;*/
					
					/*
					try{
						//Agrego MQD : 02-08-2019 
						insert tareas;
						//*********************** 
					}
					catch(Exception e){
						System.debug('*****Error al crear la cita:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    		ApexPages.addMessage(mensaje);
					}*/
					System.debug('*****Cita (tarea) creada:' + cita);

					//Emoran craete  new Event
					Event evento = new Event();
					evento.OwnerId = cliente.OwnerId;
					evento.Desarrollo_Comercial__c = cliente.DC_CM__c;
				    evento.WhatId = bitacora.Id;
				    evento.StartDateTime =  DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0);
				    evento.EndDateTime =  DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida +  1,0,0);
				    evento.ActivityDateTime = DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0);
				    evento.IsReminderSet = true;
				    evento.ReminderDateTime = DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0).addDays(-1);
				    evento.Subject = 'Cita';					    

				    try{
						insert evento;
					}
					catch(Exception e){
						System.debug('*****Error al crear el evento:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    		ApexPages.addMessage(mensaje);
					}
					System.debug('*****Cita (evento) creada:' + evento);	
					
					guardiaActual.Contador_Citas__c = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c)) + 1;
					try{
						update guardiaActual;
					}
					catch(Exception e){
						System.debug('*****Error al actualizar la guardia:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
			    		ApexPages.addMessage(mensaje);
					}				
				}
				
				System.debug('*****guardar cliente.OwnerId: ' + cliente.OwnerId);
				System.debug('*****guardar cliente.DC_CM__r: ' + cliente.DC_CM__r);
			}
			
			PageReference pageRef = new PageReference('/' + bitacora.Id);
			return pageRef;
		}
	}
	
	global class GuardiaWrapper implements Comparable {

    public Guardia__c oppy;
    
    // Constructor
    public GuardiaWrapper(Guardia__c op) {
        oppy = op;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        GuardiaWrapper compareToOppy = (GuardiaWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (oppy.Roll__c > compareToOppy.oppy.Roll__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (oppy.Roll__c < compareToOppy.oppy.Roll__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}
	
	//Metodo para cancelar la creacion de una bitacora
	public PageReference cancelar(){
		
		PageReference pageRef = new PageReference('/' + cliente.Id);
		return pageRef;
	}
}