/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        OpportunityTriggerHandler
* @Autor       Joel Soto
* @Date        28/11/2019
*
* @Group       BeGrand
* @Description handler para el trigger de oportunidad
* @Changes
*/
public class OpportunityTriggerHandler extends TriggerHandler {
  /** mapa del registro antes del cambio que entran al trigger*/
  private Map<Id, Opportunity> newTOldMap;
  /** lista de oportunidades que entran al trigger*/
  private List<Opportunity> newTNew;
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description antes de borrar
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public override void beforeDelete() {
    this.newTOldMap = (Map<Id, Opportunity>) Trigger.oldMap;
    OpportunityTriggerClass.eliminaAttachments(this.newTOldMap);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description antes de actualizar
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public override void beforeUpdate() {

    this.newTNew = (List<Opportunity>) Trigger.new;
    this.newTOldMap = (Map<Id, Opportunity>) Trigger.oldMap;
    List<Opportunity> oppToUpdate = new List<Opportunity>();
    Map<String, String> mapRtype = new Map<String, String>();
    for(Opportunity opp : this.newTNew) {
      SObject oldOpp = this.newTOldMap.get(opp.Id);
      if(opp.StageName != (String)oldOpp.get('StageName')) {
        oppToUpdate.add(opp);
      }
    }
    OpportunityTriggerClass.validaCambioEtapa(oppToUpdate, this.newTOldMap);

  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description despues de actualizar
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public override void afterUpdate() {

    this.newTNew = (List<Opportunity>) Trigger.new;
    this.newTOldMap = (Map<Id, Opportunity>) Trigger.oldMap;
    List<Opportunity> oppToCierre = new List<Opportunity>();
    for(Opportunity opp : this.newTNew) {
      SObject oldOpp = this.newTOldMap.get(opp.Id);
      if(opp.StageName != (String)oldOpp.get('StageName') && (opp.StageName == Label.CierreVenta || opp.StageName == Label.CierreRenta)) {
        oppToCierre.add(opp);
      }
    }
    if(!oppToCierre.isEmpty()){
    OpportunityTriggerClass.sendEnkontrol(oppToCierre);

    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description despues de actualizar
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public override void afterInsert() {
    CLASS_TriggerConvertedLead convert = new CLASS_TriggerConvertedLead();
    this.newTNew = (List<Opportunity>) Trigger.new;
    convert.relacionaBitacoras(this.newTNew);
  }
}