trigger Trigger_Entrega on Carta_Oferta__c (after update) {

    if(Trigger.isAfter && Trigger.isUpdate){
        Handler_EntregaTrigger.isAfterUpdate(Trigger.New, Trigger.oldMap);
    }
  
}