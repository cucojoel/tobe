public class OpportunityHelper {
  public static Map<String, List<DocumentoxEsquema__c>> listToMapdocxp(List<DocumentoxEsquema__c> listDocsEsq) {
    Map<String, List<DocumentoxEsquema__c>> mapTipoPna = new Map<String, List<DocumentoxEsquema__c>>();
    for(DocumentoxEsquema__c docEsq : listDocsEsq) {
      if (mapTipoPna.containsKey(docEsq.Tipo_de_persona__c)) {
        List<DocumentoxEsquema__c> listDocsEsqtmp = mapTipoPna.get(docEsq.Tipo_de_persona__c);
        listDocsEsqtmp.add(docEsq);
        mapTipoPna.put(docEsq.Tipo_de_persona__c, listDocsEsqtmp);
      } else {
        List<DocumentoxEsquema__c> listDocsEsqtmp = new List<DocumentoxEsquema__c>();
        listDocsEsqtmp.add(docEsq);
        mapTipoPna.put(docEsq.Tipo_de_persona__c, listDocsEsqtmp);
      }
    }
    return mapTipoPna;
  }
  public static Map<String, List<Attachment>> getMapAttachbyoppid(List<Attachment> listAttach) {
    Map<String, List<Attachment>> mappOppAttach = new Map<String, List<Attachment>>();
    for (Attachment att : listAttach) {
      if(mappOppAttach.containsKey(att.ParentId)) {
        List<Attachment> listTmp = mappOppAttach.get(att.ParentId);
        listTmp.add(att);
        mappOppAttach.put(att.ParentId,listTmp);
      } else {
        List<Attachment> listTmp = new List<Attachment>();
        listTmp.add(att);
        mappOppAttach.put(att.ParentId,listTmp);
      }
    }
    return mappOppAttach;
  }
  public static Set<String> getSetOppAttach(List<Attachment> listAttach) {
    Set<String> setNametAttach = new Set<String>();
    for (Attachment att : listAttach) {
      setNametAttach.add(att.Name.split('\\.').get(0));
    }
    return setNametAttach;
  }
  public static Set<String> getSetConfEtapa(List<DocumentoxEsquema__c> listDocsEsqxp) {
    Set<String> setConfEtapa = new Set<String>();
    for (DocumentoxEsquema__c docEx : listDocsEsqxp) {
      setConfEtapa.add(docEx.Documento__c);
    }
    return setConfEtapa;
  }
  public static Boolean validaDocs(Set<String> setConfEtapa, Set<String> setAttachName, Opportunity opp) {
    String stringFilter = '';
    for (String etta : setConfEtapa) {
      if (!setAttachName.contains(etta)) {
        stringFilter += etta + ', ';
      }
    }
    stringFilter = stringFilter.removeEnd(', ');
    if (String.isNotBlank(stringFilter)) {
      opp.addError('Faltan documentos por cargar: ' + stringFilter);
      return false;
    }
    return true;
  }
  public static void updateMyOpp(Opportunity opp, Boolean bol) {
    system.debug(opp);
    system.debug(bol);
    opp.DocumentacionCompleta__c = bol;
    update opp;
  }
}