/**
  * ----------------------------------------------------------------------------------------------------------------------
  * @Name        Enkontrol_Tst
  * @Autor       Joel Soto
  * @Date        02/01/2020
  *
  * @Group       BeGrand
  * @Description Enkontrol_Cls - 96  %
  *              Enkontrol_Hdr - 100 %
  *              Enkontrol_Hlp - 92  %
  *              Enkontrol_Utl - 92  %
  *              Enkontrol_Ws  - 100 %
  * @Changes
  */
@IsTest
public class Enkontrol_Tst {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Test Setup para generar regisros basicos
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019  **/
  @testSetup static void testSetup() {
    Compania__c compania = Bg_Utils_Cls.genCompania('companiaName', '1');
    insert compania;
    User gerenteUser = Bg_Utils_Cls.genUser('gereUser', 'Gerente de ventas');
    insert gerenteUser;
    Desarrollo_Comercial__c desarrolloComercial = Bg_Utils_Cls.genDesComercial('test', compania.Id, gerenteUser.Id );
    insert desarrolloComercial;
    Desarrollo_Comercial__c desarrolloComercialBDC = Bg_Utils_Cls.genDesComercial('BDC', compania.Id, gerenteUser.Id );
    insert desarrolloComercialBDC;
    Guardia__c guardiaUno = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial.Id, gerenteUser.Id, 1);
    insert guardiaUno;
    Desarrollo__c desarrollo = Bg_Utils_Cls.genDesarrollo('test', 'BO4', desarrolloComercial.Id);
    insert desarrollo;
    Etapa__c etapaSf = Bg_Utils_Cls.genEtapa('Torre 2', '1', desarrollo.Id);
    insert etapaSf;
    Inmueble__c depto = Bg_Utils_Cls.genDepto(etapaSf.Id, 'Disponible', '1409');
    insert depto;
    RecordType pAccountrType = Bg_Utils_Cls.getRtype('PersonAccount', 'Account');
    Account acc = Bg_Utils_Cls.genAccount('TestCommunityUser', '1', pAccountrType.Id);
    insert acc;
    EquivalenciaEstados__c eqEstados = Bg_Utils_Cls.genEstados('Aguascalientes', 'Ags');
    insert eqEstados;
    Lead leadSf = Bg_Utils_Cls.genLead('name', desarrollo.Id, gerenteUser.Id);
    leadSf.ID_Prospecto_Enkontrol__c = '123';
    insert leadSf;
    Carta_Oferta__c cOferta = Bg_Utils_Cls.genEntrega(depto.Id);
    insert cOferta;
    Contact contactsf = [SELECT Id, AccountId from Contact WHERE AccountId = :acc.Id LIMIT 1];
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    System.runAs(thisUser) {
      User coordinador = Bg_Utils_Cls.genUser('coordinador', 'Coordinador Servicio a Cliente');
      coordinador.Desarrollo__c = 'ALTO PEDREGAL';
      coordinador.Id_Enkontrol__c = '2';
      insert coordinador;
      User supervisionObra = Bg_Utils_Cls.genUser('supervisionObra', 'Gerente supervisión de obra');
      supervisionObra.Desarrollo__c = 'ALTO PEDREGAL';
      supervisionObra.Id_Enkontrol__c = '3';
      insert supervisionObra;
      User commUser = Bg_Utils_Cls.genUser('commUser', 'Gerente supervisión de obra');
      commUser.Desarrollo__c = 'ALTO PEDREGAL';
      commUser.Id_Enkontrol__c = '4';
      insert commUser;
      User standardUser = Bg_Utils_Cls.genUser('standardUser', 'Jefe de Cuadrilla');
      standardUser.Desarrollo__c = 'ALTO PEDREGAL';
      standardUser.Id_Enkontrol__c = '5';
      insert standardUser;
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Enkontrol_Cls - 16  %
  *              Enkontrol_Hdr - 21  %
  *              Enkontrol_Hlp - 41  %
  *              Enkontrol_Utl - 41  %
  *              Enkontrol_Ws  - 45  %
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  **/
  static testMethod void testActualizacionProspectos() {
    List<Enkontrol_Ws.Prospecto> listaProspectosZero = new List<Enkontrol_Ws.Prospecto>();
    Enkontrol_Ws.Prospecto prospectoWsnoDes = new Enkontrol_Ws.Prospecto();
    prospectoWsnoDes.apellidoPaterno = null;
    prospectoWsnoDes.nombre = null;
    prospectoWsnoDes.email1 = null;
    prospectoWsnoDes.idEnkontrol = null;
    prospectoWsnoDes.agente = null;
    prospectoWsnoDes.puntoProspectacion = null;
    prospectoWsnoDes.desarrollo = null;
    prospectoWsnoDes.compania = null;
    listaProspectosZero.add(prospectoWsnoDes);
    Enkontrol_Ws.actualizacionProspectos(listaProspectosZero);
    List<Enkontrol_Ws.Prospecto> listaProspectos = new List<Enkontrol_Ws.Prospecto>();
    Enkontrol_Ws.Prospecto prospectoWs = new Enkontrol_Ws.Prospecto();
    prospectoWs.personaMoral = false;
    prospectoWs.agente = 1;
    prospectoWs.idEnkontrol = 1;
    prospectoWs.puntoProspectacion = 6;
    prospectoWs.apellidoMaterno = '';
    prospectoWs.apellidoPaterno = 'apellidoPaterno';
    prospectoWs.compania = '1';
    prospectoWs.desarrollo = 'BO4';
    prospectoWs.email1 = 'prospecto001@test.com';
    prospectoWs.email2 = '';
    prospectoWs.empresa = '';
    prospectoWs.estado = 'Ags';
    prospectoWs.fechaAlta = '10/10/2010';
    prospectoWs.fechaDeNacimiento = '01/01/1980';
    prospectoWs.idSalesforce = '';
    prospectoWs.medio = '';
    prospectoWs.nombre = 'nombre';
    prospectoWs.rfc = '';
    prospectoWs.sexo = '';
    prospectoWs.telefono1 = '';
    prospectoWs.telefono2 = '';
    prospectoWs.telefono3 = '';
    prospectoWs.idGroup = '';
    prospectoWs.calleYNumero = '';
    prospectoWs.ciudad = '';
    prospectoWs.colonia = '';
    prospectoWs.cp = '';
    prospectoWs.nacionalidad = '';
    prospectoWs.pais = 'Mexico';
    listaProspectos.add(prospectoWs);
    Enkontrol_Ws.Prospecto prospectoWsDup = new Enkontrol_Ws.Prospecto();
    prospectoWsDup.personaMoral = false;
    prospectoWsDup.agente = 1;
    prospectoWsDup.idEnkontrol = 1;
    prospectoWsDup.puntoProspectacion = 6;
    prospectoWsDup.apellidoPaterno = 'apellidoPaterno';
    prospectoWsDup.compania = '1';
    prospectoWsDup.desarrollo = 'BO5';
    prospectoWsDup.email1 = 'prospecto001@test.com';
    prospectoWsDup.estado = 'Ags';
    prospectoWsDup.fechaAlta = '10/10/2010';
    prospectoWsDup.fechaDeNacimiento = '01/01/1980';
    prospectoWsDup.nombre = 'nombre';
    prospectoWsDup.pais = 'Mexico';
    //listaProspectos.add(prospectoWsDup);
    Enkontrol_Ws.actualizacionProspectos(listaProspectos);
    Lead prospectoSfRec = [SELECT Id FROM Lead LIMIT 1];
    prospectoSfRec.Asignado_BDC__c = true;
    update prospectoSfRec;
    List<Enkontrol_Ws.Prospecto> listaProspectosRecov = new List<Enkontrol_Ws.Prospecto>();
    Enkontrol_Ws.Prospecto prospectoWsRecov = new Enkontrol_Ws.Prospecto();
    prospectoWsRecov.personaMoral = false;
    prospectoWsRecov.agente = 1;
    prospectoWsRecov.idEnkontrol = 1;
    prospectoWsRecov.puntoProspectacion = 6;
    prospectoWsRecov.apellidoPaterno = 'apellidoPaterno';
    prospectoWsRecov.compania = '1';
    prospectoWsRecov.desarrollo = 'BO4';
    prospectoWsRecov.email1 = 'prospecto001@test.com';
    prospectoWsRecov.estado = 'Ags';
    prospectoWsRecov.fechaAlta = '10/10/2010';
    prospectoWsRecov.fechaDeNacimiento = '01/01/1980';
    prospectoWsRecov.idSalesforce = prospectoSfRec.Id;
    prospectoWsRecov.nombre = 'nombre';
    prospectoWsRecov.pais = 'Mexico';
    listaProspectosRecov.add(prospectoWsRecov);
    Enkontrol_Ws.actualizacionProspectos(listaProspectosRecov);
    List<Enkontrol_Ws.Prospecto> listaProspectosRecno = new List<Enkontrol_Ws.Prospecto>();
    Enkontrol_Ws.Prospecto prospectoWsRecNo = new Enkontrol_Ws.Prospecto();
    prospectoWsRecNo.personaMoral = false;
    prospectoWsRecNo.agente = 1;
    prospectoWsRecNo.idEnkontrol = 2;
    prospectoWsRecNo.puntoProspectacion = 6;
    prospectoWsRecNo.apellidoPaterno = 'apellidoPaterno';
    prospectoWsRecNo.compania = '1';
    prospectoWsRecNo.desarrollo = 'BO4';
    prospectoWsRecNo.email1 = 'prospecto001@test.com';
    prospectoWsRecNo.estado = 'Ags';
    prospectoWsRecNo.fechaAlta = '10/10/2010';
    prospectoWsRecNo.fechaDeNacimiento = '01/01/1980';
    prospectoWsRecNo.idSalesforce = null;
    prospectoWsRecNo.nombre = 'nombre';
    listaProspectosRecno.add(prospectoWsRecNo);
    Enkontrol_Ws.actualizacionProspectos(listaProspectosRecno);
    Test.startTest();
    System.assertNotEquals(null, listaProspectos);
    Test.stopTest();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Enkontrol_Cls - 10  %
  *              Enkontrol_Hdr - 57  %
  *              Enkontrol_Hlp - 22  %
  *              Enkontrol_Utl - 24  %
  *              Enkontrol_Ws  - 54  %
  *
  * @Autor       Joel Soto
  * @Date        09/12/2019
  **/
  static testMethod void testUpdateDepto() {
    Enkontrol_Ws.ActualizarEstatusDepto updateDepto = new Enkontrol_Ws.ActualizarEstatusDepto();
    updateDepto.desarrollo = 'BO5';
    updateDepto.loteId = '1409';
    updateDepto.estatus = 'Apartado';
    Enkontrol_Ws.ActualizarEstatusDepto updateDeptoCh = new Enkontrol_Ws.ActualizarEstatusDepto();
    updateDeptoCh.desarrollo = 'BO4';
    updateDeptoCh.loteId = '1409';
    updateDeptoCh.estatus = 'Apartado';
    Enkontrol_Ws.ActualizarEstatusDepto updateDeptoNull = new Enkontrol_Ws.ActualizarEstatusDepto();
    updateDeptoNull.desarrollo = null;
    updateDeptoNull.loteId = '1409';
    updateDeptoNull.estatus = 'Apartado';
    Enkontrol_Ws.ActualizarEstatusDepto updateDeptoNullLot = new Enkontrol_Ws.ActualizarEstatusDepto();
    updateDeptoNullLot.desarrollo = 'BO4';
    updateDeptoNullLot.loteId = null;
    updateDeptoNullLot.estatus = 'Apartado';
    List<Enkontrol_Ws.ActualizarEstatusDepto> listUpdateDepto = new List<Enkontrol_Ws.ActualizarEstatusDepto>();
    listUpdateDepto.add(updateDepto);
    listUpdateDepto.add(updateDeptoCh);
    listUpdateDepto.add(updateDeptoNull);
    listUpdateDepto.add(updateDeptoNullLot);
    Enkontrol_Ws.actualizaEstatusDepto(listUpdateDepto);
    Enkontrol_Ws.ActualizarEstatusDepto updateDeptoWoCh = new Enkontrol_Ws.ActualizarEstatusDepto();
    updateDeptoWoCh.desarrollo = 'BO4';
    updateDeptoWoCh.loteId = '1409';
    updateDeptoWoCh.estatus = 'Apartado';
    List<Enkontrol_Ws.ActualizarEstatusDepto> listUpdateDeptoCh = new List<Enkontrol_Ws.ActualizarEstatusDepto>();
    listUpdateDeptoCh.add(updateDeptoWoCh);
    Enkontrol_Ws.actualizaEstatusDepto(listUpdateDeptoCh);
    Enkontrol_Ws.ActualizarEstatusDepto updateDeptoJa = new Enkontrol_Ws.ActualizarEstatusDepto();
    updateDeptoJa.desarrollo = 'BO4';
    updateDeptoJa.loteId = '1409';
    updateDeptoJa.estatus = 'Ja';
    List<Enkontrol_Ws.ActualizarEstatusDepto> listUpdateDeptoJa = new List<Enkontrol_Ws.ActualizarEstatusDepto>();
    listUpdateDeptoJa.add(updateDeptoJa);
    Enkontrol_Ws.actualizaEstatusDepto(listUpdateDeptoJa);
    Test.startTest();
    System.assertNotEquals(null, listUpdateDeptoJa);
    Test.stopTest();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Enkontrol_Cls - 10  %
  *              Enkontrol_Hdr - 21  %
  *              Enkontrol_Hlp - 40  %
  *              Enkontrol_Utl - 28  %
  *              Enkontrol_Ws  - 42  %
  *
  * @Autor       A. Martinez
  * @Date        11/12/2019
  **/
  static testMethod void testBienvenidaCliente() {
    List<Enkontrol_Ws.Caso> casosWs = new List<Enkontrol_Ws.Caso>();
    Enkontrol_Ws.Caso casoWs = new Enkontrol_Ws.Caso();
    casosWs.add(casoWs);
    Enkontrol_Ws.bienvenidaCliente(casosWs);
    Test.startTest();
    System.assertNotEquals(null, casosWs);
    Test.stopTest();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Enkontrol_Cls - 36  %
  *              Enkontrol_Hdr - 42  %
  *              Enkontrol_Hlp - 58  %
  *              Enkontrol_Utl - 35  %
  *              Enkontrol_Ws  - 60  %
  *
  * @Autor       A. Martinez
  * @Date        11/12/2019
  **/
  static testMethod void testOportunidades() {
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-test.begrand.mx/EnKontrol/EnkontrolService.svc', Bg_Utils_Cls.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    Enkontrol_Ws.CartaOferta cofee = new Enkontrol_Ws.CartaOferta();
    List<Enkontrol_Ws.Documento> lDocumentos = new List<Enkontrol_Ws.Documento>();
    Enkontrol_Ws.Documento oDocumento1 = new Enkontrol_Ws.Documento();
    oDocumento1.docType = 1;
    oDocumento1.urlCarpeta = '/Pruebas/Leads/91618/2/1/5e1e0104cd1d5.pdf';
    Enkontrol_Ws.Documento oDocumento4 = new Enkontrol_Ws.Documento();
    oDocumento4.docType = 4;
    oDocumento4.urlCarpeta = 'Pruebas/Leads/91618/2/1/5e1e0104cd1d5.pdf';
    lDocumentos.add(oDocumento1);
    lDocumentos.add(oDocumento4);
    /*
    List<Enkontrol_Ws.Oportunidad> listOppsWsRenta = new List<Enkontrol_Ws.Oportunidad>();
    Enkontrol_Ws.Oportunidad oppWsRenta = new Enkontrol_Ws.Oportunidad();
    oppWsRenta.agente = 2;
    oppWsRenta.compania = '1';
    oppWsRenta.desarrollo = 'BO4';
    oppWsRenta.idEnkontrol = 1;
    oppWsRenta.idProspecto = 123;
    oppWsRenta.loteId = '1409';
    oppWsRenta.esRenta = true;
    oppWsRenta.numeroCliente = 67286;
    oppWsRenta.referencia = 'BO400000083595';
    listOppsWsRenta.add(oppWsRenta);
    Enkontrol_Ws.registroOportunidades(listOppsWsRenta);
    */
    List<Enkontrol_Ws.Oportunidad> listOppsWs = new List<Enkontrol_Ws.Oportunidad>();
    Enkontrol_Ws.Oportunidad oppWs = new Enkontrol_Ws.Oportunidad();
    oppWs.agente = 2;
    oppWs.compania = '1';
    oppWs.dbxUrls = lDocumentos;
    oppWs.desarrollo = 'BO4';
    oppWs.esRenta = false;
    oppWs.estatus = '0';
    oppWs.etapa = '1';
    oppWs.idEnkontrol = 1;
    oppWs.idGroup = '123';
    oppWs.idProspecto = 123;
    oppWs.loteId = '1409';
    oppWs.metodoPago = 3;
    oppWs.montoApartado = '5000';
    oppWs.numeroCliente = 67286;
    oppWs.precioFianza = '14365';
    oppWs.precioRenta = '14365';
    oppWs.referencia = 'BO400000083595';
    listOppsWs.add(oppWs);
    Enkontrol_Ws.actualizacionOportunidades(listOppsWs);
    Enkontrol_Ws.registroOportunidades(listOppsWs);
    //Enkontrol_Ws.actualizacionOportunidades(listOppsWs);
    List<Enkontrol_Ws.Oportunidad> listOppsWsFalse = new List<Enkontrol_Ws.Oportunidad>();
    Enkontrol_Ws.Oportunidad oppWsFalse = new Enkontrol_Ws.Oportunidad();
    oppWsFalse.agente = 3;
    oppWsFalse.compania = '1';
    oppWsFalse.dbxUrls = lDocumentos;
    oppWsFalse.desarrollo = 'BO5';
    oppWsFalse.etapa = '1';
    oppWsFalse.idEnkontrol = 2;
    oppWsFalse.idProspecto = 124;
    oppWsFalse.loteId = '1472';
    oppWsFalse.numeroCliente = 67287;
    oppWsFalse.referencia = 'BO400000083598';
    listOppsWsFalse.add(oppWsFalse);
    listOppsWsFalse.add(oppWs);
    Enkontrol_Ws.registroOportunidades(listOppsWsFalse);
    //Enkontrol_Ws.actualizacionOportunidades(listOppsWsFalse);
    List<Enkontrol_Ws.Oportunidad> listOppsNull = new List<Enkontrol_Ws.Oportunidad>();
    Enkontrol_Ws.Oportunidad oppWsNull = new Enkontrol_Ws.Oportunidad();
    oppWsNull.agente = null;
    oppWsNull.compania = null;
    oppWsNull.desarrollo = null;
    oppWsNull.idEnkontrol = null;
    oppWsNull.idProspecto = null;
    oppWsNull.loteId = null;
    oppWsNull.esRenta = null;
    oppWsNull.dbxUrls = null;
    oppWsNull.numeroCliente = null;
    listOppsNull.add(oppWsNull);
    listOppsNull.add(oppWs);
    Enkontrol_Ws.registroOportunidades(listOppsNull);
    //Enkontrol_Ws.actualizacionOportunidades(listOppsNull);
    Test.startTest();
    System.assertNotEquals(null, listOppsWs);
    Test.stopTest();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Enkontrol_Cls - 12  %
  *              Enkontrol_Hdr - 57  %
  *              Enkontrol_Hlp - 20  %
  *              Enkontrol_Utl - 9   %
  *              Enkontrol_Ws  - 51  %
  *
  * @Autor       A. Martinez
  * @Date        11/12/2019
  **/
  static testMethod void testProveedores() {
    List<Enkontrol_Ws.Proveedor> listProvsWs = new List<Enkontrol_Ws.Proveedor>();
    Enkontrol_Ws.Proveedor provWs = new Enkontrol_Ws.Proveedor();
    provWs.idProveedor = 1222;
    provWs.nombreProveedor = 'Test Proveedor';
    listProvsWs.add(provWs);
    Enkontrol_Ws.registroProveedores(listProvsWs);
    List<Enkontrol_Ws.Proveedor> proveedoresDup = new List<Enkontrol_Ws.Proveedor>();
    Enkontrol_Ws.Proveedor prooveedorDup = new Enkontrol_Ws.Proveedor();
    prooveedorDup.idProveedor = 1222;
    prooveedorDup.nombreProveedor = 'Test Proveedor';
    proveedoresDup.add(prooveedorDup);
    Enkontrol_Ws.registroProveedores(proveedoresDup);
    List<Enkontrol_Ws.Proveedor> proveedoresNull = new List<Enkontrol_Ws.Proveedor>();
    Enkontrol_Ws.Proveedor prooveedorNull = new Enkontrol_Ws.Proveedor();
    prooveedorNull.idProveedor = null;
    prooveedorNull.nombreProveedor = null;
    proveedoresNull.add(prooveedorNull);
    Enkontrol_Ws.registroProveedores(proveedoresNull);
    List<Enkontrol_Ws.Proveedor> listProvsWsNs = new List<Enkontrol_Ws.Proveedor>();
    Enkontrol_Ws.Proveedor provWsNs = new Enkontrol_Ws.Proveedor();
    provWsNs.idProveedor = 1224;
    provWsNs.nombreProveedor = 'Test Proveedor';
    listProvsWsNs.add(provWsNs);
    listProvsWsNs.add(provWs);
    Enkontrol_Ws.actualizacionProveedores(listProvsWsNs);
    Test.startTest();
    System.assertNotEquals(null, proveedoresDup);
    Test.stopTest();
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Enkontrol_Cls - 33  %
  *              Enkontrol_Hdr - 14  %
  *              Enkontrol_Hlp - 39  %
  *              Enkontrol_Utl - 35  %
  *              Enkontrol_Ws  - 44  %
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020  **/
  static testMethod void oneTtest() {
    List<Enkontrol_Ws.Falla> listwsFallaNv = new List<Enkontrol_Ws.Falla>();
    Enkontrol_Ws.Falla fallaWs = new Enkontrol_Ws.Falla();
    fallaWs.esCuadrilla = false;
    fallaWs.esEmergente = false;
    fallaWs.especialidad = 'CANCELERIA Y ALUMINIO';
    fallaWs.idEspecialidad = 62;
    fallaWs.idProveedor = 3575;
    fallaWs.idSubespecialidad = 16;
    fallaWs.idZona = 6;
    fallaWs.numeroSello = 1;
    fallaWs.proveedor = 'VERTIQUITECTURA S.A. DE C.V.';
    fallaWs.subespecialidad = 'VIDRIOS';
    fallaWs.tipo = 'D';
    fallaWs.zona = 'RECAMARA 2';
    listwsFallaNv.add(fallaWs);
    List<Enkontrol_Ws.Caso> listwscasos = new List<Enkontrol_Ws.Caso>();
    Enkontrol_Ws.Caso casoWs = new Enkontrol_Ws.Caso();
    casoWs.fallas = listwsFallaNv;
    casoWs.folio = 1855;
    casoWs.idAppOrigen = 1;
    casoWs.idAsignadoObra = 0;
    casoWs.idCoordinadorSC = 0;
    casoWs.idDesarrollo = 'BO4';
    casoWs.idEmpleado = 9999;
    casoWs.idEtapa = 'A';
    casoWs.idEtapaEntrega = 3;
    casoWs.idInmueble = '2405';
    casoWs.idLote = '1409';
    casoWs.idPiso = '24';
    casoWs.idSupervisor = 1;
    listwscasos.add(casoWs);
    Enkontrol_Ws.bienvenidaCliente(listwscasos);
    List<Enkontrol_Ws.Caso> listwscasosNv = new List<Enkontrol_Ws.Caso>();
    Enkontrol_Ws.Caso casoWsNv = new Enkontrol_Ws.Caso();
    casoWsNv.folio = 1856;
    casoWsNv.idAppOrigen = 2;
    casoWsNv.idAsignadoObra = 0;
    casoWsNv.idCoordinadorSC = 0;
    casoWsNv.idDesarrollo = 'BO4';
    casoWsNv.idEmpleado = 9999;
    casoWsNv.idEtapa = 'A';
    casoWsNv.idEtapaEntrega = 3;
    casoWsNv.idInmueble = '2405';
    casoWsNv.idLote = '1409';
    casoWsNv.idPiso = '24';
    casoWsNv.idSupervisor = 0;
    //casoWsNv.fallas = null;
    listwscasosNv.add(casoWsNv);
    Enkontrol_Ws.Caso casoWsNd = new Enkontrol_Ws.Caso();
    casoWsNd.folio = 1856;
    casoWsNd.idAppOrigen = 2;
    casoWsNd.idAsignadoObra = 0;
    casoWsNd.idCoordinadorSC = 0;
    casoWsNd.idDesarrollo = 'BO4';
    casoWsNd.idEmpleado = 9999;
    casoWsNd.idEtapa = 'A';
    casoWsNd.idEtapaEntrega = 3;
    casoWsNd.idInmueble = '2405';
    casoWsNd.idLote = '1409';
    casoWsNd.idPiso = '24';
    casoWsNd.idSupervisor = 0;
    listwscasosNv.add(casoWsNd);
    Enkontrol_Ws.bienvenidaCliente(listwscasosNv);
    List<Enkontrol_Ws.Caso> listwscasosExist = new List<Enkontrol_Ws.Caso>();
    Enkontrol_Ws.Caso casoWsExist = new Enkontrol_Ws.Caso();
    casoWsExist.folio = 1856;
    casoWsExist.idAppOrigen = 2;
    casoWsExist.idAsignadoObra = 0;
    casoWsExist.idCoordinadorSC = 0;
    casoWsExist.idDesarrollo = 'BO4';
    casoWsExist.idEmpleado = 9999;
    casoWsExist.idEtapa = 'A';
    casoWsExist.idEtapaEntrega = 3;
    casoWsExist.idInmueble = '2405';
    casoWsExist.idLote = '1409';
    casoWsExist.idPiso = '24';
    casoWsExist.idSupervisor = 0;
    listwscasosExist.add(casoWsExist);
    Enkontrol_Ws.bienvenidaCliente(listwscasosExist);
    List<Enkontrol_Ws.Caso> listwscasosNull = new List<Enkontrol_Ws.Caso>();
    Enkontrol_Ws.Caso casoWsNull = new Enkontrol_Ws.Caso();
    casoWsNull.idLote = null;
    casoWsNull.folio = null;
    casoWsNull.idEtapa = null;
    casoWsNull.idDesarrollo = null;
    casoWsNull.idPiso = null;
    casoWsNull.idInmueble = null;
    listwscasosNull.add(casoWsNull);
    Enkontrol_Ws.bienvenidaCliente(listwscasosNull);
    System.assertEquals(1, 1);
  }
}