/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        BatchOldLead
* @Autor       Joel Soto
* @Date        07/11/2019
*
* @Group       BeGrand
* @Description Batch para reasignar el owner de los lead con antiguedad de 31 dias
* @Changes
*/
global class BatchOldLead implements Database.Batchable<SObject> {
  /** String SOQLQUERY**/
  public static final String SOQLQUERY = 'SELECT Id, Web_to_lead__c, Agente__c, LeadSource, Cita_Web__c, Medio__c, Desarrollo_Web__c, Desarrollo_Comercial__c, Desarrollo__c, Desarrollo_Comercial_Primario__c, Dias_Desde_Creacion__c, Asignado_BDC__c, Asesor_Primario__c, OwnerId, Email_gerente__c, Dia_reasignacion__c, Subestatus__c, Con_Cita_Concretada__c FROM Lead WHERE Dias_Desde_Creacion__c = 31 AND Desarrollo_Comercial__r.Name = \'Contadero\' AND Subestatus__c IN (\'No localizado\',\'Sin Atender\',\'Datos Falsos\',\'Fuera de Presupuesto\',\'No interesado\',\'No interesado por el momento\') AND isConverted = false AND Estatus_Prospecto_BDC__c != \'Rescate\' AND OwnerId IN (SELECT Id From User WHERE Desarrollo__c = \'BDC\')';
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        07/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  global Database.QueryLocator start(Database.BatchableContext bc) {
    return Database.getquerylocator(SOQLQUERY);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        07/11/2019
  * @Param       List<Lead> prospectoscambiodesarrollo
  **/
  global void execute(Database.BatchableContext bc, List<Lead> prospectoscambiodesarrollo) {
    Bg_LeadUtil_Cls.reasignarGuardia(prospectoscambiodesarrollo);
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        07/11/2019
  * @Param       Database.BatchableContext bc
  **/
  global void finish(Database.BatchableContext bc) {
  }
}