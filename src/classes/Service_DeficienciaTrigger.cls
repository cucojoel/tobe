public without sharing class Service_DeficienciaTrigger {
    
    public static void compruebaEstado(List<Detalle_del_Caso__c> TriggerNew, Map<id,Detalle_del_Caso__c> oldMap){
        
        for(Detalle_del_Caso__c ddc: TriggerNew){
            if(ddc.Proveedor_Emergente__c == false && ddc.Nombre_proveedor_emergente__c != null){
                ddc.Proveedor_Emergente__c = true;
            }
        }
        
        Set<id> casosInvolucrados = new Set<id>();
        
        for(Detalle_del_caso__c caso: TriggerNew){
            casosInvolucrados.add(caso.Casos__c);
        }
        
        Map<Id, Case> casosPorId = new Map<Id,Case>([SELECT Id, Status FROM Case Where Id IN :casosInvolucrados]);
           
        for(Detalle_del_Caso__c ddc: TriggerNew){
            
            if(casosPorId.containsKey(ddc.Casos__c)){
                
                Case casoDeLaDeficiencia = casosPorId.get(ddc.Casos__c);
                
                if((casoDeLaDeficiencia.Status == 'Resuelto') &&
                   (ddc.Estatus__c != oldMap.get(ddc.Id).Estatus__c) &&
                   (oldMap.get(ddc.Id).Estatus__c == 'Reparado') &&
                   (ddc.Estatus__c != 'Reparado')
                  ){
                      ddc.addError('No se puede cambiar el estatus a '+ddc.Estatus__c+' debido a que el Caso está '+casoDeLaDeficiencia.Status);
                  }
                
            }
        } 
    }
    
    public static void actualizaCaso(List<Detalle_del_Caso__c> TriggerNew) {

        Set<id> casosInvolucrados = new Set<id>();

        for (Detalle_del_caso__c caso : TriggerNew) {
            casosInvolucrados.add(caso.Casos__c);
        }

        Map<Id, Decimal> mapPendientes = new Map<Id, Decimal>();
        Map<Id, Decimal> mapReparados = new Map<Id, Decimal>();
        Map<Id, Decimal> mapCancelados = new Map<Id, Decimal>();
        Map<Id, Case> casosActualizar = new Map<Id, Case>([SELECT Id, Total_FallasDetectadas__c, FallasPendientes__c, FallasReparadas__c FROM Case WHERE Id IN:casosInvolucrados]);
        AggregateResult[] conteoTotal = [
                SELECT Casos__c, COUNT(Id)Conteo
                FROM Detalle_del_Caso__c
                WHERE Casos__c IN :casosInvolucrados
                GROUP BY Casos__c
        ];
        AggregateResult[] conteoPendientes = [
                SELECT Casos__c, COUNT(Id)Conteo
                FROM Detalle_del_Caso__c
                WHERE Casos__c IN :casosInvolucrados AND
                (Estatus__c = 'Nuevo' OR Estatus__c = 'Por Validar' OR Estatus__c = 'En Revisión' OR Estatus__c = 'Reincidencia')
                GROUP BY Casos__c
        ];
        AggregateResult[] conteoReparados = [
                SELECT Casos__c, COUNT(Id)Conteo
                FROM Detalle_del_Caso__c
                WHERE Casos__c IN :casosInvolucrados AND
                Estatus__c = 'Reparado'
                GROUP BY Casos__c
        ];
        AggregateResult[] conteoCancelados = [
                SELECT Casos__c, COUNT(Id)Conteo
                FROM Detalle_del_Caso__c
                WHERE Casos__c IN :casosInvolucrados AND
                Estatus__c = 'Cancelado'
                GROUP BY Casos__c
        ];
        for (AggregateResult ar : conteoPendientes) {
            mapPendientes.put((Id) ar.get('Casos__c'), (Decimal) ar.get('Conteo'));
        }
        for (AggregateResult ar : conteoReparados) {
            mapReparados.put((Id) ar.get('Casos__c'), (Decimal) ar.get('Conteo'));
        }
        for (AggregateResult ar : conteoCancelados) {
            mapCancelados.put((Id) ar.get('Casos__c'), (Decimal) ar.get('Conteo'));
        }
        List<Case> casosActualizables = new List<Case>();
        for (AggregateResult ar : conteoTotal) {
            if (casosActualizar.containsKey((Id) ar.get('Casos__c'))) {
                Case camposActualizables = casosActualizar.get((Id) ar.get('Casos__c'));
                camposActualizables.Total_FallasDetectadas__c = (Decimal) ar.get('Conteo');
                if (mapPendientes.containsKey((Id) ar.get('Casos__c'))) {
                    camposActualizables.FallasPendientes__c = mapPendientes.get((Id) ar.get('Casos__c'));
                } else {
                    camposActualizables.FallasPendientes__c = 0;
                }
                if (mapReparados.containsKey((Id) ar.get('Casos__c'))) {
                    camposActualizables.FallasReparadas__c = mapReparados.get((Id) ar.get('Casos__c'));
                } else {
                    camposActualizables.FallasReparadas__c = 0;
                }
                if (mapCancelados.containsKey((Id) ar.get('Casos__c'))) {
                    camposActualizables.Fallas_Canceladas__c = mapCancelados.get((Id) ar.get('Casos__c'));
                } else {
                    camposActualizables.Fallas_Canceladas__c = 0;
                }
                casosActualizables.add(camposActualizables);
            }
        }
        update casosActualizables;
    }
    
    public static void sincronizarDeficienciaInsert(List<Detalle_del_Caso__c> TriggerNew){
        
        Set<Id> detallesParaSincronizar = new Set<Id>();
        
        for(Detalle_del_Caso__c ddc : triggerNew){
            if(ddc.Id_Falla_WS__c == null){
                detallesParaSincronizar.add(ddc.Id);
            }
        }
        
        if(!detallesParaSincronizar.isEmpty()){
            System.debug('*** Detalles a sincronizar ***' + detallesParaSincronizar);
            if(!Test.isRunningTest()){
                //QueueableService_DeficienciaEnkontrol caseJob = new QueueableService_DeficienciaEnkontrol(detallesParaSincronizar);
                //Id jobID = System.enqueueJob(caseJob);
            }
        }
        
    }
    
    public static void sincronizarDeficienciaUpdate(List<Detalle_del_Caso__c> TriggerNew, Map<id,Detalle_del_Caso__c> oldMap){
        
        List<Detalle_del_Caso__c> detallesParaSincronizar = new List<Detalle_del_Caso__c>();

        System.debug('*** Deficiencias en trigger new ***' + triggerNew);
		
        Set<Id> actualizadosId = new Set<Id>();
        
        for(Detalle_del_Caso__c ddc : triggerNew){
			
            if(!actualizadosId.contains(ddc.Id)){
                Detalle_del_Caso__c oldDdc = oldMap.get(ddc.Id);
                if(oldDdc.Id_Falla_WS__c != null){
                    if(oldDdc.Id == ddc.Id && oldDdc.Estatus__c != 'Cancelado' && ddc.Estatus__c == 'Cancelado'){
                        detallesParaSincronizar.add(ddc);
                        actualizadosId.add(ddc.Id);
                    }
                }
            }
        }

        if(!detallesParaSincronizar.isEmpty()){
            System.debug('*** Detalles a sincronizar ***' + detallesParaSincronizar);
            if(!Test.isRunningTest()){
                QueueableService_DeficienciaEnkontrol caseJob = new QueueableService_DeficienciaEnkontrol(detallesParaSincronizar);
                Id jobID = System.enqueueJob(caseJob);
            }
        }
    }
    
}