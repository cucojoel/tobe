@isTest
public class Test_Service {

    public static List<Account> CrearCuenta(Integer NumeroCuentas){

        List<Account> ListCuentas = new List<Account>();
		
        Map<String,String> recordTypesAccount = getRecordTypeByName('Account');
        
        for(Integer i = 1; i <= NumeroCuentas; i++){
            Account NuevaCuenta = new Account();
            NuevaCuenta.FirstName = 'Account';
            NuevaCuenta.LastName = 'Test' + i;
			NuevaCuenta.RFC__pc = '1234567891234'; 
            NuevaCuenta.RecordTypeId = recordTypesAccount.get('PersonAccount');
            ListCuentas.add(NuevaCuenta);
        }
        
        System.assertNotEquals(0, ListCuentas.size());

        return ListCuentas;        

    }
    
    public static List<Desarrollo__c > CrearDesarrollo(Integer NumeroDesarrollo){
        
        List<Desarrollo__c> ListDesarrollos = new List<Desarrollo__c>();

        for(Integer i = 1; i <= NumeroDesarrollo; i++){
            Desarrollo__c NuevaDesarrollo = new Desarrollo__c();
			NuevaDesarrollo.Name = 'Desarrollo Test ' + i;
            ListDesarrollos.add(NuevaDesarrollo);
        }
        
        System.assertNotEquals(0, ListDesarrollos.size());

        return ListDesarrollos; 
        
    }
    
    public static List<Etapa__c> CrearEtapa(Integer NumeroEtapa, String DesarrolloId){
        
        List<Etapa__c> ListEtapas = new List<Etapa__c>();

        for(Integer i = 1; i <= NumeroEtapa; i++){
            Etapa__c NuevaEtapa = new Etapa__c();
			NuevaEtapa.Desarrollo__c = DesarrolloId;
            NuevaEtapa.Tipo__c = 'Torre';
            ListEtapas.add(NuevaEtapa);
        }
        
        System.assertNotEquals(0, ListEtapas.size());

        return ListEtapas; 
        
    }
    
    public static List<Inmueble__c> CrearInmueble(Integer NumeroInmueble, String EtapaId){
        
        List<Inmueble__c> ListInmuebles = new List<Inmueble__c>();

        for(Integer i = 1; i <= NumeroInmueble; i++){
            Inmueble__c NuevoInmueble = new Inmueble__c();
			NuevoInmueble.Name = 'Departamento Test ' + i;
            NuevoInmueble.Etapa__c = EtapaId;
            ListInmuebles.add(NuevoInmueble);
        }
        
        System.assertNotEquals(0, ListInmuebles.size());

        return ListInmuebles; 
        
    }
    
    public static List<Carta_Oferta__c> CrearEntrega(Integer NumeroEntrega, String InmuebleId){
        
        List<Carta_Oferta__c> ListEntregas = new List<Carta_Oferta__c>();

        for(Integer i = 1; i <= NumeroEntrega; i++){
            Carta_Oferta__c NuevaEntrega = new Carta_Oferta__c();
            NuevaEntrega.Inmueble__c = InmuebleId;
            ListEntregas.add(NuevaEntrega);
        }
        
        System.assertNotEquals(0, ListEntregas.size());

        return ListEntregas; 
        
    }
    
    public static Map<String,Id> getRecordTypeByName(String sObjectType){
        Map<String,Id> result = new Map<String,Id>();
        for(RecordType rt : [Select id, Name, DeveloperName from RecordType where sObjectType=:sObjectType]){
            result.put(rt.DeveloperName, rt.id); 
        }
        return result;
    }
    
}