trigger Trigger_Attachment on Attachment (after insert) {
  if(Trigger.isAfter && Trigger.isInsert){
    System.debug('*** Entrando a after insert***');
    Handler_AttachmentTrigger.isAfterInsert(Trigger.New);
  }
}