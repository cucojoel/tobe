/**
 * Created by scarlettcastillo on 8/31/18.
 */

public with sharing class Handler_ContactTrigger {

    public static void actualizacionDeEmailEnCuentaPadre(List<Contact> triggerNew){
        List<Contact> cons = new List<Contact>();
        List<Account> accountsToUpdate = new List<Account>();
        Map<Id, String> emailsPorIdAccount = new Map<Id, String>();

        for(Contact c: triggerNew){
            if(c.AccountId != null){
                if(c.Contacto_Notificacion__c){
                    emailsPorIdAccount.put(c.AccountId, c.Email);
                }
            }
        }

        for (Id accId : emailsPorIdAccount.keySet()){
            Account newAccount = new Account(Id = accId);
            newAccount.Email_Proveedor__c = emailsPorIdAccount.get(accId);
            accountsToUpdate.add(newAccount);
        }

        System.debug('Accounts =>' + accountsToUpdate);
        
        if(!accountsToUpdate.isEmpty()){
            update accountsToUpdate;
        }
    }
}