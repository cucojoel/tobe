@isTest
public class LC_DetalleDeficienciaControllerTest {

    public static testMethod void buscarZonasTest() {

        String inmueble = '102';
        String idEnkontrolEtapa = '1';
        String idEnkontrolDesarrollo  = 'AP12';

        /** Crear datos de prueba */
        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba(inmueble, desarrolloComercial.Id, idEnkontrolEtapa, idEnkontrolDesarrollo);

        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        Account cliente = Test_Utilities.crearClientePrueba();

        Carta_Oferta__c cartaOferta = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);

        Case unCaso = Test_Utilities.crearCaso(cartaOferta.Id, cliente.Id);
        Detalle_del_Caso__c detalleDelCaso = Test_Utilities.crearDeficiencia(unCaso.Id);

        Matriz_Deficiencias__c matriz = Test_Utilities.crearMatrizPrueba(
                1,'Test Zona', 2, 'Test Especialidad', 3 , 'Test Subespecialidad', unInmueble.Etapa__c, proveedor.Id, false
        );

        detalleDelCaso.Matriz_de_Deficiencias__c = matriz.Id;

        Test_Utilities.testCrearConfiguracionPersonalizada();

        Test.startTest();

        LC_DetalleDeficienciaController.obtenerZonas(detalleDelCaso.Id);

        Test.stopTest();
    }

    public static testMethod void buscarEspecialidadesTest() {

        String inmueble = '102';
        String idEnkontrolEtapa = '1';
        String idEnkontrolDesarrollo  = 'AP12';

        /** Crear datos de prueba */
        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba(inmueble, desarrolloComercial.Id, idEnkontrolEtapa, idEnkontrolDesarrollo);

        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        Account cliente = Test_Utilities.crearClientePrueba();

        Carta_Oferta__c cartaOferta = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);

        Case unCaso = Test_Utilities.crearCaso(cartaOferta.Id, cliente.Id);
        Detalle_del_Caso__c detalleDelCaso = Test_Utilities.crearDeficiencia(unCaso.Id);

        Matriz_Deficiencias__c matriz = Test_Utilities.crearMatrizPrueba(
                1,'Test Zona', 2, 'Test Especialidad', 3 , 'Test Subespecialidad', unInmueble.Etapa__c, proveedor.Id, false
        );

        detalleDelCaso.Matriz_de_Deficiencias__c = matriz.Id;

        Test_Utilities.testCrearConfiguracionPersonalizada();

        Test.startTest();

        LC_DetalleDeficienciaController.obtenerEspecialidadesRelacionadas(1 , detalleDelCaso.Id);

        Test.stopTest();

    }

    public static testMethod void buscarSubespecialidadesTest() {

        String inmueble = '102';
        String idEnkontrolEtapa = '1';
        String idEnkontrolDesarrollo  = 'AP12';

        /** Crear datos de prueba */
        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba(inmueble, desarrolloComercial.Id, idEnkontrolEtapa, idEnkontrolDesarrollo);

        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        Account cliente = Test_Utilities.crearClientePrueba();

        Carta_Oferta__c cartaOferta = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);

        Case unCaso = Test_Utilities.crearCaso(cartaOferta.Id, cliente.Id);
        Detalle_del_Caso__c detalleDelCaso = Test_Utilities.crearDeficiencia(unCaso.Id);

        Matriz_Deficiencias__c matriz = Test_Utilities.crearMatrizPrueba(
                1,'Test Zona', 2, 'Test Especialidad', 3 , 'Test Subespecialidad', unInmueble.Etapa__c, proveedor.Id, false
        );

        detalleDelCaso.Matriz_de_Deficiencias__c = matriz.Id;

        Test_Utilities.testCrearConfiguracionPersonalizada();

        Test.startTest();

        LC_DetalleDeficienciaController.obtenerSubEspecialidadesRelacionadas(2, detalleDelCaso.Id);

        Test.stopTest();

    }

    public static testMethod void asignarMatrizTest() {

        String inmueble = '102';
        String idEnkontrolEtapa = '1';
        String idEnkontrolDesarrollo  = 'AP12';

        /** Crear datos de prueba */
        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba(inmueble, desarrolloComercial.Id, idEnkontrolEtapa, idEnkontrolDesarrollo);

        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        Account cliente = Test_Utilities.crearClientePrueba();

        Carta_Oferta__c cartaOferta = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);

        Case unCaso = Test_Utilities.crearCaso(cartaOferta.Id, cliente.Id);
        Detalle_del_Caso__c detalleDelCaso = Test_Utilities.crearDeficiencia(unCaso.Id);

        Matriz_Deficiencias__c matriz = Test_Utilities.crearMatrizPrueba(
                1,'Test Zona', 2, 'Test Especialidad', 3 , 'Test Subespecialidad', unInmueble.Etapa__c, proveedor.Id, false
        );

        Test_Utilities.testCrearConfiguracionPersonalizada();

        Test.startTest();

        LC_DetalleDeficienciaController.asignarMatriz( detalleDelCaso.Id, 1, 2, 3);

        Test.stopTest();

    }

    public static testMethod void obtenerMatrizTest() {

        String inmueble = '102';
        String idEnkontrolEtapa = '1';
        String idEnkontrolDesarrollo  = 'AP12';

        /** Crear datos de prueba */
        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba(inmueble, desarrolloComercial.Id, idEnkontrolEtapa, idEnkontrolDesarrollo);

        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        Account cliente = Test_Utilities.crearClientePrueba();

        Carta_Oferta__c cartaOferta = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);

        Case unCaso = Test_Utilities.crearCaso(cartaOferta.Id, cliente.Id);
        Detalle_del_Caso__c detalleDelCaso = Test_Utilities.crearDeficiencia(unCaso.Id);

        Matriz_Deficiencias__c matriz = Test_Utilities.crearMatrizPrueba(
                1,'Test Zona', 2, 'Test Especialidad', 3 , 'Test Subespecialidad', unInmueble.Etapa__c, proveedor.Id, false
        );

        detalleDelCaso.Matriz_de_Deficiencias__c = matriz.Id;

        update detalleDelCaso;

        Test_Utilities.testCrearConfiguracionPersonalizada();

        Test.startTest();

        LC_DetalleDeficienciaController.obtenerMatrizRelacionada( detalleDelCaso.Id );

        Test.stopTest();

    }

}