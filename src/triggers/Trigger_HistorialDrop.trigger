trigger Trigger_HistorialDrop on Historial_dropbox__c (after insert, after update) {
  if(Trigger.isAfter && Trigger.isInsert){
    System.debug('*** Entrando a after insert***');
    Handler_HistorialdropTrigger.isAfterInsert(Trigger.New);
  }
}