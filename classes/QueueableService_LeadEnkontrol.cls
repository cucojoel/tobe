public class QueueableService_LeadEnkontrol implements Queueable, Database.AllowsCallouts {
  public List<Lead> leadsToEnk;
  public Boolean esActualizacion;
  @TestVisible
  public QueueableService_LeadEnkontrol(List<Lead> listLeadsSf, Boolean esActualizacion) {
    this.leadsToEnk = listLeadsSf;
    this.esActualizacion = esActualizacion;
  }
  public void execute(QueueableContext context) {
    ejecutarCalloutAEnkontrol();
  }
  public void ejecutarCalloutAEnkontrol() {
    Set<Id> setLeadsSfIds = new Set<Id>();
    Set<Id> serUsersSfIds = new Set<Id>();
    String userIdEnkontrol = [SELECT Id, Id_Enkontrol__c FROM User WHERE Id =: UserInfo.getUserId()].Id_Enkontrol__c;
    List<Lead> leadsParaActualizar = new List<Lead>();
    Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();
    Utils_WS_EnkontrolRequestBuilder.SecurityWrapper securityData = new Utils_WS_EnkontrolRequestBuilder.SecurityWrapper();
    securityData.password = configuracionEnkontrol.Password__c;
    securityData.username = configuracionEnkontrol.Username__c;
    if (esActualizacion) {
      securityData.soapOperation = Utils_WS_EnkontrolRequestBuilder.OPERATION_UPDATE;
    } else {
      securityData.soapOperation = Utils_WS_EnkontrolRequestBuilder.OPERATION_CREATE;
    }
    for (Lead leadSf : this.leadsToEnk) {
      setLeadsSfIds.add(leadSf.Id);
    }
    Map<String, String> mapEstados = Enkontrol_Hlp.getMapEstados();
    List<Lead> leadsPorSincronizar = [SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId, Medio__c,/* <-- se agrego campo */
      Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
      Telefono2__c, Telefono_3__c, Email, Email2__c, Street, City, Country, Nacionalidad__c, Pais_Nacimiento__c, ID_Prospecto_Enkontrol__c, Status,
      PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
      CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c, Error_Sincronizacion__c
      FROM Lead
      WHERE Id IN :setLeadsSfIds
      LIMIT 49];
    for (Lead leadSf : leadsPorSincronizar) {
      serUsersSfIds.add(leadSf.OwnerId);
      serUsersSfIds.add(leadSf.Agente__c);
    }
    Map<Id, User> mapUsersSf = new Map<Id, User>([SELECT Id, Name, Id_Enkontrol__c, ProfileId
        FROM User
        WHERE Id_Enkontrol__c != null AND Id IN: serUsersSfIds]);
    for (Lead leadSf : leadsPorSincronizar) {
      Utils_WS_EnkontrolRequestBuilder.ProspectResponseWrapper responseWrapper = new Utils_WS_EnkontrolRequestBuilder.ProspectResponseWrapper();
      WS_EnkontrolIntegration.Prospectos leadEnk = retProspecto(leadSf);
      if (mapUsersSf.containsKey(leadSf.OwnerId)) {
        leadEnk.agente = Integer.valueof(mapUsersSf.get(leadSf.OwnerId).Id_Enkontrol__c);
      }
      leadEnk.estado = mapEstados.containsKey(leadSf.State) ? mapEstados.get(leadSf.State) : null;
      leadEnk.userId = userIdEnkontrol;
      String requestBody = Utils_WS_EnkontrolRequestBuilder.construirProspectosRequestCompleto(leadEnk, securityData);
      Http http = new Http();
      HttpRequest req = new HttpRequest();
      req.setEndpoint(configuracionEnkontrol.Service_Endpoint__c);
      req.setMethod('POST');
      req.setHeader('Content-Type', 'text/xml');
      req.setHeader('SOAPAction', Utils_WS_EnkontrolRequestBuilder.SOAPACTIONPROSPECTEXECUTE);
      req.setBody(requestBody);
      HttpResponse res = http.send(req);
      responseWrapper = Utils_WS_EnkontrolRequestBuilder.parseProspectosResponse(res);
      System.debug('log - QueueableService_LeadEnkontrol: ' + leadEnk);
      System.debug('log - QueueableService_LeadEnkontrol: ' + res);
      System.debug('log - QueueableService_LeadEnkontrol: ' + responseWrapper);
      if (esActualizacion) {
        if (responseWrapper.success) {
          if(leadSf.Sincronizado_en_Enkontrol__c == false) {
            leadSf.Sincronizado_en_Enkontrol__c = true;
            leadSf.Error_Sincronizacion__c = '';
            leadSf.Id_Agrupador__c = String.valueOf(responseWrapper.idGroup);
            leadSf.Status = leadSf.Status == 'Bloqueado' ? 'Asignado' : leadSf.Status;
            leadsParaActualizar.add(leadSf);
            Helper_RecursiveTrigger.leadsYaActualizados.add(leadSf.Id);
          }
        } else {
          if(leadSf.Sincronizado_en_Enkontrol__c == true && String.isBlank(leadSf.Error_Sincronizacion__c)) {
            leadSf.Sincronizado_en_Enkontrol__c = false;
            leadSf.Error_Sincronizacion__c = responseWrapper.errorMessage;
            leadSf.Status = leadSf.Status == 'Asignado' ? 'Bloqueado' : leadSf.Status;
            leadsParaActualizar.add(leadSf);
            Helper_RecursiveTrigger.leadsYaActualizados.add(leadSf.Id);
          }
        }
      } else {
        if (responseWrapper.success) {
          leadSf.Sincronizado_en_Enkontrol__c = true;
          leadSf.ID_Prospecto_Enkontrol__c = String.valueOf(responseWrapper.enkontrolId);
          leadSf.Error_Sincronizacion__c = '';
          leadSf.Id_Agrupador__c = String.valueOf(responseWrapper.idGroup);
          leadSf.Status = leadSf.Status == 'Bloqueado' ? 'Asignado' : leadSf.Status;
          leadsParaActualizar.add(leadSf);
          Helper_RecursiveTrigger.leadsYaActualizados.add(leadSf.Id);
        } else {
          leadSf.Sincronizado_en_Enkontrol__c = false;
          leadSf.Error_Sincronizacion__c = responseWrapper.errorMessage;
          leadSf.Status = 'Bloqueado';
          leadsParaActualizar.add(leadSf);
          Helper_RecursiveTrigger.leadsYaActualizados.add(leadSf.Id);
        }
      }
    }
    update leadsParaActualizar;
  }
  public WS_EnkontrolIntegration.Prospectos retProspecto(Lead leadSf) {
    String idEstatus = '0';
    if (leadSf.Status == 'Contactado' || leadSf.Status == 'Cotizacion' || leadSf.Status == 'Cotizado') {
      idEstatus = '1';
    }
    WS_EnkontrolIntegration.Prospectos leadEnk = new WS_EnkontrolIntegration.Prospectos();
    leadEnk.apellidoMaterno = leadSf.Apellido_Materno__c;
    leadEnk.apellidoPaterno = leadSf.LastName;
    leadEnk.calleYNumero = leadSf.Street;
    leadEnk.ciudad = leadSf.City;
    leadEnk.colonia = leadSf.Colonia__c;
    leadEnk.compania = leadSf.Compania__r.Id_Compania_Enkontrol__c;
    leadEnk.cp = leadSf.PostalCode;
    leadEnk.desarrollo = leadSf.Desarrollo__r.Id_Desarrollo_Enkontrol__c;
    leadEnk.email1 = leadSf.Email;
    leadEnk.email2 = leadSf.Email2__c;
    leadEnk.idEstatus = idEstatus;
    leadEnk.empresa = leadSf.Company;
    leadEnk.fechaDeNacimiento = String.valueOf(leadSf.Fecha_Nacimiento__c);
    leadEnk.idEnkontrol = String.isNotBlank(leadSf.ID_Prospecto_Enkontrol__c) ? Integer.valueOf(leadSf.ID_Prospecto_Enkontrol__c) : 0;
    //leadEnk.idSalesforce = leadSf.Id;
    leadEnk.medio = leadSf.Medio__c;
    leadEnk.nacionalidad = leadSf.Pais_Nacimiento__c;
    leadEnk.nombre = leadSf.FirstName;
    leadEnk.pais = leadSf.Country;
    leadEnk.personaMoral = leadSf.Persona_Moral__c;
    leadEnk.puntoProspectacion = Integer.valueOf(leadSf.LeadSource);
    leadEnk.rfc = leadSf.RFC__c;
    leadEnk.sexo = leadSf.Sexo__c;
    leadEnk.telefono1 = leadSf.Phone;
    leadEnk.telefono2 = leadSf.Telefono2__c;
    leadEnk.telefono3 = leadSf.Telefono_3__c;
    return leadEnk;
  }
  public void createObjectClass() {
    String alias0;
    String alias1;
    String appartment0;
    String appartment1;
    String assignatedToName0;
    String assignatedToName1;
    String bill0;
    String bill1;
    String contractNumber0;
    String contractNumber1;
    String contractTerm0;
    String contractTerm1;
    String description0;
    String description1;
    String development0;
    String development1;
    String divisionName0;
    String divisionName1;
    String errorMessage0;
    String errorMessage1;
    String idDropbox0;
    String idDropbox1;
    String idRequest0;
    String idRequest1;
    String language0;
    String language1;
    String messageFailure0;
    String messageFailure1;
    String messageSuccess0;
    String messageSuccess1;
    String name0;
    String name1;
    String nextStep0;
    String nextStep1;
    String objectId0;
    String objectId1;
    String operation0;
    String operation1;
    String opportunity0;
    String opportunity1;
    String origin0;
    String origin1;
    String ownerName0;
    String ownerName1;
    String prospectAgent0;
    String prospectAgent1;
    String prospectBirthdate0;
    String prospectBirthdate1;
    String prospectCity0;
    String prospectCity1;
    String prospectColony0;
    String prospectColony1;
    String prospectCompany0;
    String prospectCompany1;
    String prospectCountry0;
    String prospectCountry1;
    String prospectDes0;
    String prospectDes1;
    String prospectEmail0;
    String prospectEmail1;
    String prospectFirstName0;
    String prospectFirstName1;
    String prospectGender0;
    String prospectGender1;
    String prospectLastName0;
    String prospectLastName1;
    String prospectMoral0;
    String prospectMoral1;
    String prospectNacionality0;
    String prospectNacionality1;
    String prospectName0;
    String prospectName1;
    String prospectPhone0;
    String prospectPhone1;
    String prospectPuntoProspectacion0;
    String prospectPuntoProspectacion1;
    String prospectRFC0;
    String prospectRFC1;
    String prospectState0;
    String prospectState1;
    String prospectStreet0;
    String prospectStreet1;
    String quoteToName0;
    String quoteToName1;
    String reference0;
    String reference1;
    String request0;
    String request1;
    String requestBody0;
    String requestBody1;
    String requestParameter0;
    String requestParameter1;
    String requestResponse0;
    String requestResponse1;
    String signature0;
    String signature1;
    String source0;
    String source1;
    String specialTerms0;
    String specialTerms1;
    String stage0;
    String stage1;
    String subStatus0;
    String subStatus1;
    String successMessage0;
    String successMessage1;
    String title0;
    String title1;
    String typeRequest0;
    String typeRequest1;
    String zone0;
    String zone1;
  }
}