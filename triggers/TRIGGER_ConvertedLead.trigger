trigger TRIGGER_ConvertedLead on Lead (after update) {
	
    map<String,String> convertedOpportunityIds = new map<String,String>();

    
    for (Lead l : Trigger.new) {
        if (!trigger.oldMap.get(l.Id).IsConverted && l.IsConverted && l.convertedOpportunityId != null) {
			convertedOpportunityIds.put(l.Id,l.convertedOpportunityId);
		}
	}
    
    List<Bitacora__c> lstBitacoras  = new List<Bitacora__c>();
    List<Bitacora__c> lstBitacorasUp  = new List<Bitacora__c>();
   
    if(!convertedOpportunityIds.IsEmpty()){
        lstBitacoras=[SELECT Id, Lead__c, Oportunidad__c FROM Bitacora__c WHERE Lead__c IN: convertedOpportunityIds.keySet()];
    }
    
    if(!lstBitacoras.isEmpty()){
        for(Bitacora__c bt : lstBitacoras){
            if(convertedOpportunityIds.containsKey(bt.Lead__c)){
                Bitacora__c btTemp = new Bitacora__c();
                btTemp.Id = bt.Id;
                btTemp.Oportunidad__c = convertedOpportunityIds.get(bt.Lead__c);
                lstBitacorasUp.add(btTemp);
            }
        }
    }
    
    update lstBitacorasUp;
}