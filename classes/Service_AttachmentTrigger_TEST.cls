@isTest
public with sharing class Service_AttachmentTrigger_TEST {
  public static testMethod void testOne() {
  /*
  *
  */
  Profile profileSupervision = [SELECT Id FROM Profile WHERE Name = 'Supervisión'];
  User supervisionUser = new User();
  supervisionUser.Alias = 'supeUser';
  supervisionUser.Desarrollo__c = 'Coapa';
  supervisionUser.Email = 'supervisionUser@begrand.com';
  supervisionUser.EmailEncodingKey = 'UTF-8';
  supervisionUser.Id_Enkontrol__c ='00001';
  supervisionUser.LanguageLocaleKey = 'en_US';
  supervisionUser.LastName = 'supervisionUser';
  supervisionUser.LocaleSidKey = 'en_US';
  supervisionUser.ProfileId = profileSupervision.Id;
  supervisionUser.TimeZoneSidKey = 'America/Los_Angeles';
  supervisionUser.Username = 'supervisionUser@begrand.com';
  insert supervisionUser;

  List<Attachment> triggerNew = new List<Attachment>();
  Account cuenta = new Account();
  cuenta.LastName = 'Melquides';
  cuenta.FirstName = 'Marco';
  cuenta.PersonEmail = 'test@gmail.com';
  cuenta.RFC__pc = 'CASJ811111RD9';
  insert cuenta;

  /*
  *
  */
  Compania__c compania = new Compania__c();
  compania.Id_Compania_Enkontrol__c = '1';
  compania.Name = 'companiaItem';
  insert compania;
  /*
  *
  */
  Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
  desarrolloComercial.Name = 'Coapa';
  desarrolloComercial.Compania__c = compania.Id;
  desarrolloComercial.Gerente_Ventas__c = supervisionUser.Id;
  desarrolloComercial.Gerente_Administrativo__c = supervisionUser.Id ;
  insert desarrolloComercial;

  Desarrollo__c desarrollo = new Desarrollo__c();
  desarrollo.Name = 'Alto Polanco';
  desarrollo.Id_Desarrollo_Enkontrol__c = 'AP2';
  desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
  desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
  insert desarrollo;

  Opportunity opp = new Opportunity();
  opp.Name = 'test';
  opp.AccountId = cuenta.Id;
  opp.StageName = 'Apartado';
  //opp.Esquema__c = 'Recurso propio';
  opp.CloseDate = Date.today().addDays(10);
  opp.Desarrollo__c = desarrollo.Id;
  insert opp;

  Attachment documentoPrevio = new Attachment();
  documentoPrevio.Body = Blob.valueOf('Some Text');
  documentoPrevio.ParentId = opp.Id;
  documentoPrevio.Name = 'Test';
  insert documentoPrevio;
  triggerNew.add(documentoPrevio);
  //Service_AttachmentTrigger.crearHistorialDrop(triggerNew);
  Service_AttachmentTrigger satach = new Service_AttachmentTrigger();
  satach.createObjectClass();

  Handler_AttachmentTrigger hatach = new Handler_AttachmentTrigger();
  hatach.createObjectClass();
}
}