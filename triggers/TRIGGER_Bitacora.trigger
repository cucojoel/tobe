trigger TRIGGER_Bitacora on Bitacora__c (after insert) {
	
    CLASS_TriggerBitacora classBitacora = new CLASS_TriggerBitacora();
    
    if(trigger.isInsert && trigger.isAfter) {
		classBitacora.creaTareasEventos(Trigger.new);
	}
    
}