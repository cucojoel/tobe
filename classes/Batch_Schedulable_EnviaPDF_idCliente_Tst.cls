/*
Este test cubre las clases Batch_Schedulable_EnviaPDF_idCliente y Batch_Schedulable_EnviaPDF1_idCliente
*/

@isTest
public class Batch_Schedulable_EnviaPDF_idCliente_Tst {
	static TestMethod void test_01() {
		String CRON_EXP = '0 0 0 15 3 ? 2022';
    
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
        
        Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'BGR',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
		
        Opportunity oportunidad = new Opportunity(
			Name = '15810-8',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			Desarrollo__c = desarrollo.Id,
			Etapa__c = etapa.Id,
			Departamento__c = null,
			Compania__c = compania.Id,
			Lote_Id__c = '322',
			Id_Prospecto_Enkontrol__c = '15810',
			ID_Cliente_Enkontrol__c = '5108',
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'VENDIDO',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = false,
			Id_Cliente_Enkontrol__c = '5108',
			Id_Prospecto_Enkontrol__c = '15810',
			OportunidadTexto__c = oportunidad.Id
		);
		insert departamento;
		
		oportunidad.Departamento__c = departamento.Id;
		update oportunidad;
		
    	Attachment attach = new Attachment();       
        attach.Name = 'ECtaBG_' + oportunidad.Id;
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = cliente.Id;
        insert attach;
        
        SFDC_BG_EdoCta_temp__c temporal1 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'BGR',
			InteresMoratorio__c = '0.00',
			Email__c = 'a@a.a',
			IsGroupedAccountStatement__c = 'false',
			IdGroup__c = '0'
		);
		insert temporal1;
		
		Test.startTest();
			String jobId = System.schedule('EnviaPDFCte', CRON_EXP, new Batch_Schedulable_EnviaPDF_idCliente());						
		Test.stopTest();		
	}
	
	static TestMethod void test_02() {
		String CRON_EXP = '0 0 0 15 3 ? 2022';
    
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
        
        Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'BGR',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
		
        Opportunity oportunidad = new Opportunity(
			Name = '15810-8',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			Desarrollo__c = desarrollo.Id,
			Etapa__c = etapa.Id,
			Departamento__c = null,
			Compania__c = compania.Id,
			Lote_Id__c = '322',
			Id_Prospecto_Enkontrol__c = '15810',
			ID_Cliente_Enkontrol__c = '5108',
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'VENDIDO',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = false,
			Id_Cliente_Enkontrol__c = '5108',
			Id_Prospecto_Enkontrol__c = '15810',
			OportunidadTexto__c = oportunidad.Id
		);
		insert departamento;
		
		oportunidad.Departamento__c = departamento.Id;
		update oportunidad;
		
    	Attachment attach = new Attachment();       
        attach.Name = 'ECtaBG_' + oportunidad.Id;
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = cliente.Id;
        insert attach;
        
        SFDC_BG_EdoCta_temp__c temporal1 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'BGR',
			InteresMoratorio__c = '0.00',
			Email__c = 'a@a.a',
			IsGroupedAccountStatement__c = 'false',
			IdGroup__c = '0'
		);
		insert temporal1;
		
		Test.startTest();
			String jobId = System.schedule('EnviaPDFCte1', CRON_EXP, new Batch_Schedulable_EnviaPDF1_idCliente());						
		Test.stopTest();		
	}
	
	static TestMethod void test_03() {
		String CRON_EXP = '0 0 0 15 3 ? 2022';		
		Test.startTest();
			String jobId = System.schedule('EnviaPDFCte', CRON_EXP, new Batch_Schedulable_EnviaPDF_idCliente());						
		Test.stopTest();		
	}
	
	static TestMethod void test_04() {
		String CRON_EXP = '0 0 0 15 3 ? 2022';		
		Test.startTest();
			String jobId = System.schedule('EnviaPDFCte1', CRON_EXP, new Batch_Schedulable_EnviaPDF1_idCliente());						
		Test.stopTest();		
	}
}