public class OpportunityTriggerQueueable implements Queueable, Database.AllowsCallouts {
  public List<Opportunity> listOppsSfQu;
  @TestVisible
  public OpportunityTriggerQueueable(List<Opportunity> listOppsSf) {
    System.debug('log - OpportunityTriggerQueueable: ' + 'Entra que');
    this.listOppsSfQu = listOppsSf;
  }
  public void execute(QueueableContext context) {
    ejecutarCalloutAEnkontrol();
  }
  public void ejecutarCalloutAEnkontrol() {
    System.debug('log - OpportunityTriggerQueueable: ' + 'ejecutarCalloutAEnkontrol');
    List<Opportunity> oppsParaActualizar = new List<Opportunity>();
    Configuracion_Enkontrol_Cierre__c configuracionEnkontrol = [SELECT Service_Endpoint__c, Password__c, Username__c FROM Configuracion_Enkontrol_Cierre__c LIMIT 1];
    Utils_WS_EnkontrolRequestBuilder.SecurityWrapper securityData = new Utils_WS_EnkontrolRequestBuilder.SecurityWrapper();
    securityData.password = configuracionEnkontrol.Password__c;
    securityData.username = configuracionEnkontrol.Username__c;
    securityData.soapOperation = 'ChangeStatus';
    Set<String> setOppSf = new Set<String>();
    for (Opportunity oppSf : this.listOppsSfQu) {
      setOppSf.add(oppSf.Id);
    }
    List<Opportunity> oppsToUpdate = [SELECT Id, ErrorCierre__c, StageName, Id_Prospecto_Enkontrol__c, ID_Oportunidad_Enkontrol__c, Usuario_Enk__c FROM Opportunity WHERE Id IN :setOppSf];
    for (Opportunity oppSf : oppsToUpdate) {
      Utils_WS_EnkontrolRequestBuilder.ProspectClosedResponseWrapper responseWrapper = new Utils_WS_EnkontrolRequestBuilder.ProspectClosedResponseWrapper();
      String requestBody = Utils_WS_EnkontrolRequestBuilder.closedProspectExecuteReq(oppSf, securityData);
      Http http = new Http();
      HttpRequest req = new HttpRequest();
      req.setEndpoint(configuracionEnkontrol.Service_Endpoint__c);
      req.setMethod('POST');
      req.setHeader('Content-Type', 'text/xml');
      req.setHeader('SOAPAction', Utils_WS_EnkontrolRequestBuilder.SOAPACTION_CLOSEDPROSPECTEXECUTE);
      req.setBody(requestBody);
      System.debug('log - OpportunityTriggerQueueable: ' + req);
      HttpResponse res = http.send(req);
      System.debug('log - OpportunityTriggerQueueable: ' + res);
      responseWrapper = Utils_WS_EnkontrolRequestBuilder.closedProspectExecuteRes(res);
      System.debug('log - OpportunityTriggerQueueable: ' + responseWrapper);
      if(String.isNotBlank(responseWrapper.errorMessage)) {
        oppSf.ErrorCierre__c = responseWrapper.errorMessage;
        oppSf.StageName = 'Expediente';
        oppsParaActualizar.add(oppSf);
      }
      if (String.isBlank(responseWrapper.errorMessage) && String.isNotBlank(oppSf.ErrorCierre__c)) {
        oppSf.ErrorCierre__c = '';
        oppsParaActualizar.add(oppSf);
      }
    }
    update oppsParaActualizar;
  }
}