public with sharing class CLASS_Tarea {
	// Metodo para cerrar la Tarea de Cita si se cambio de estatus de visita a 'Visito' o 'No Visito'
	public void cerrarTareaCita(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales) {
		
		Set<String> idsCitasRelacionadas = new Set<String>();
		Map<String,String> statusCitasPadres = new Map<String,String>();
		
		for(Task tareaActual : tareasActuales.values()) {
			Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
			if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && (tareaActual.Estatus_de_Visita__c == 'No Visitó' || tareaActual.Estatus_de_Visita__c == 'Visitó')) {
				tareaActual.Status = 'Completed';
				/*idsCitasRelacionadas.add(tareaActual.Id_Cita_Relacionada__c);
				statusCitasPadres.put(tareaActual.Id,tareaActual.Estatus_de_Visita__c);*/
			}
		}
		
		/*List<Task> citasRelacionadas = [SELECT Id, Status, Id_Cita_Relacionada__c, Estatus_de_Visita__c FROM Task WHERE Id IN : idsCitasRelacionadas];
		for(Task citaRelacionada : citasRelacionadas){
			citaRelacionada.Status = 'Completed';
			citaRelacionada.Estatus_de_Visita__c = statusCitasPadres.get(citaRelacionada.Id_Cita_Relacionada__c);
		}
		update citasRelacionadas;*/
	}

	// Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'Confirmada' para crear un evento al asesor de ventas, 
	// cambiar el propietario del prospecto asociado a la cita y el estatus de este
	public void cambioEstatusCitaConfirmada(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales) {
		
		Map<Id,Task> tareasActualesLead = new Map<Id,Task>();
		for(Task tareaActual : tareasActuales.values()){
			
			if(tareaActual.Cita_para_cuenta__c == false){
				tareasActualesLead.put(tareaActual.Id,tareaActual);
			}
		}
		
		if(tareasActualesLead.size() > 0){
			
			tareasActuales = tareasActualesLead;
		
			Set<Id> idsProspectos = new Set<Id>();
			
			for(Task tareaActual : tareasActuales.values()){
				idsProspectos.add(tareaActual.WhoId);
			}
		
			/* Agrego MQD : 02-08-2019 */
			Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Status, Estatus_Cita__c, OwnerId, Agente__c, IsConverted FROM Lead WHERE Id IN : idsProspectos]);
			/*Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Status, Estatus_Cita__c, OwnerId, Agente__c FROM Lead WHERE Id IN : idsProspectos and IsConverted = false]);*/
			/* *********************** */
			Lead prospecto = new Lead();
			List<Lead> prospectosActualizar = new List<Lead>();
		
			for(Task tareaActual : tareasActuales.values()) {
				Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
				if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'Confirmada') {
					Event evento = new Event();
					evento.OwnerId = tareaActual.OwnerId;
					evento.Desarrollo_Comercial__c = tareaActual.Desarrollo_Comercial__c;
					evento.WhoId = tareaActual.WhoId;
					evento.StartDateTime = tareaActual.Fecha_Cita__c;
					evento.EndDateTime = tareaActual.Fecha_Cita__c.addHours(1);
					evento.ActivityDateTime = tareaActual.Fecha_Cita__c;
					evento.IsReminderSet = true;
					evento.ReminderDateTime = DateTime.newInstance(tareaActual.Fecha_Cita__c.year(),tareaActual.Fecha_Cita__c.month(),tareaActual.Fecha_Cita__c.day(),tareaActual.Fecha_Cita__c.hour(),tareaActual.Fecha_Cita__c.addMinutes(-10).minute(),tareaActual.Fecha_Cita__c.second());
					evento.Subject = 'Cita Confirmada';
					
					try {
						insert evento;
					}
					catch(Exception e) {
						System.debug('*****Error al crear el evento:' + e.getMessage());
						ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
				    	ApexPages.addMessage(mensaje);
					}
					System.debug('*****Cita (evento) creada:' + evento);            	
	            	
	            	if(prospectosCitas.containsKey(tareaActual.WhoId) == true){
	            		prospecto = prospectosCitas.get(tareaActual.WhoId);
	            		prospecto.Estatus_Prospecto_BDC__c = 'Confirmado';
	            		prospecto.Estatus_Cita__c = 'Confirmada';
	            		prospecto.OwnerId = tareaActual.OwnerId; 
	            		prospectosActualizar.add(prospecto);
	            	}            	
	            }
			}
			update prospectosActualizar;
		}
	}
	
	// Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'No Confirmada' para cambiar el estatus del prospecto
	public void cambioEstatusCitaNoConfirmada(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales) {
		
		Map<Id,Task> tareasActualesLead = new Map<Id,Task>();
		for(Task tareaActual : tareasActuales.values()){
			
			if(tareaActual.Cita_para_cuenta__c == false){
				tareasActualesLead.put(tareaActual.Id,tareaActual);
			}
		}
		
		if(tareasActualesLead.size() > 0){
			
			tareasActuales = tareasActualesLead;
		
			Set<Id> idsProspectos = new Set<Id>();
			for(Task tareaActual : tareasActuales.values()) {
				idsProspectos.add(tareaActual.WhoId);
			}
		
			/* Agrego MQD : 02-08-2019 */
			Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Status, Estatus_Cita__c, OwnerId, Agente__c, IsConverted FROM Lead WHERE Id IN : idsProspectos]);
			/*Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Status, Estatus_Cita__c, OwnerId, Agente__c FROM Lead WHERE Id IN : idsProspectos and IsConverted = false]);*/
			/* *********************** */
			Lead prospecto = new Lead();
			List<Lead> prospectosActualizar = new List<Lead>();
			
			for(Task tareaActual : tareasActuales.values()) {	
	            Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
	            if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'No Confirmada') {
	            	            	
	            	if(prospectosCitas.containsKey(tareaActual.WhoId) == true){
	            		prospecto = prospectosCitas.get(tareaActual.WhoId);
	            		prospecto.Estatus_Cita__c = 'Cancelada';
	            		prospectosActualizar.add(prospecto);
	            	}
	            }
			}
			update prospectosActualizar;
		}
	}
	
	// Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'No Visito' para cambiar el propietario del prospecto asociado a la cita
	public void cambioEstatusCitaNoAsistio(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales) {
		
		Map<Id,Task> tareasActualesLead = new Map<Id,Task>();
		for(Task tareaActual : tareasActuales.values()){
			
			if(tareaActual.Cita_para_cuenta__c == false){
				tareasActualesLead.put(tareaActual.Id,tareaActual);
			}
		}
		
		if(tareasActualesLead.size() > 0){
			
			tareasActuales = tareasActualesLead;
		
			Set<Id> idsProspectos = new Set<Id>();
			
			for(Task tareaActual : tareasActuales.values()){
				idsProspectos.add(tareaActual.WhoId);
			}
			
			/* Agrego MQD : 02-08-2019 */
			Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, OwnerId, Agente__c, Cantidad_No_Shows__c, Status, IsConverted FROM Lead WHERE Id IN : idsProspectos]);
			/*Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, OwnerId, Agente__c, Cantidad_No_Shows__c, Status FROM Lead WHERE Id IN : idsProspectos and IsConverted = false]);*/
			/* *********************** */
			Lead prospecto = new Lead();
			List<Lead> prospectosActualizar = new List<Lead>();
			
			for(Task tareaActual : tareasActuales.values()){	
	            Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
	            if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'No Visitó') {
	            	
	            	if(prospectosCitas.containsKey(tareaActual.WhoId) == true){
	            		
	            		prospecto = prospectosCitas.get(tareaActual.WhoId);
	            		prospecto.OwnerId = prospecto.Agente__c;
		            	prospecto.Cantidad_No_Shows__c = (String.isBlank(String.valueOf(prospecto.Cantidad_No_Shows__c)) == true ? 0 : Integer.valueOf(prospecto.Cantidad_No_Shows__c)) + 1;
		            	
		            	if(prospecto.Cantidad_No_Shows__c >= 4) {
		            		prospecto.Estatus_Prospecto_BDC__c = 'No Contactar';
		            	}
		            	else {
		            		prospecto.Estatus_Prospecto_BDC__c = 'Rescate';
		            	}
		            	prospectosActualizar.add(prospecto);
	            	}            	
	            }
			}
			update prospectosActualizar;
		}
	}
	
	// Metodo para verificar si una cita (tarea) cambio de estatus de visita a 'Visito' para cambiar el estatus del prospecto
	public void cambioEstatusCitaVisito(Map<Id,Task> tareasAnteriores, Map<Id,Task> tareasActuales) {
		
		Map<Id,Task> tareasActualesLead = new Map<Id,Task>();
		for(Task tareaActual : tareasActuales.values()){
			
			if(tareaActual.Cita_para_cuenta__c == false){
				tareasActualesLead.put(tareaActual.Id,tareaActual);
			}
		}
		
		if(tareasActualesLead.size() > 0){
			
			tareasActuales = tareasActualesLead;
		
			Set<Id> idsProspectos = new Set<Id>();
			for(Task tareaActual : tareasActuales.values()){
				idsProspectos.add(tareaActual.WhoId);
			}
			
			/* Agrego MQD : 02-08-2019 */
			Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Con_Cita_Concretada__c, Fecha_Cita_Concretada__c, IsConverted FROM Lead WHERE Id IN : idsProspectos]);
			/*Map<Id,Lead> prospectosCitas = new Map<Id,Lead>([SELECT Id, Con_Cita_Concretada__c, Fecha_Cita_Concretada__c FROM Lead WHERE Id IN : idsProspectos and IsConverted = false]);*/
			/* *********************** */
			Lead prospecto = new Lead();
			List<Lead> prospectosActualizar = new List<Lead>();
			
			for(Task tareaActual : tareasActuales.values()) {
				Task tareaAnterior = tareasAnteriores.get(tareaActual.Id);
				
				if((tareaActual.Estatus_de_Visita__c != tareaAnterior.Estatus_de_Visita__c ) && tareaActual.Estatus_de_Visita__c == 'Visitó') {
					
	            	if(prospectosCitas.containsKey(tareaActual.WhoId) == true){
	            		
	            		prospecto = prospectosCitas.get(tareaActual.WhoId);
		            	prospecto.Con_Cita_Concretada__c = true;
		            	prospecto.Fecha_Cita_Concretada__c = DateTime.now();
		            	prospectosActualizar.add(prospecto);
	            	}
	            }
			}
			update prospectosActualizar;
		}
	}
}