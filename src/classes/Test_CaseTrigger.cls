/**
 * Created by scarlettcastillo on 9/3/18.
 */

@IsTest
public with sharing class Test_CaseTrigger {

    public static testMethod void testCaseTrigger(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);

        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);

        Test.startTest();

        unCaso.Folio_Enkontrol__c = 1001;
        unCaso.Status = 'Resuelto';
        unCaso.FallasPendientes__c = 0; //-> LINEA AGREGADA POR JCS CLOUDMOBILE 211118

        update unCaso;

        Test.stopTest();
    }

    public static testMethod void testCaseTriggerQueuable(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);

        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);

        Test.startTest();

        QueueableService_CaseEnkontrol queueableCase = new QueueableService_CaseEnkontrol(new List<Case>{unCaso});
        queueableCase.hacerCallout = false;
        ID jobID = System.enqueueJob(queueableCase);

        Test.stopTest();
    }

    public static testMethod void testCaseTriggerCallout(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);

        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);

        unCaso.Folio_Enkontrol__c = 1001;
        unCaso.Status = 'Resuelto';
		unCaso.FallasPendientes__c = 0; //-> LINEA AGREGADA POR JCS CLOUDMOBILE 211118
		
        update unCaso;

        Test.startTest();

        QueueableService_CaseEnkontrol queueableCase = new QueueableService_CaseEnkontrol(new List<Case>{unCaso});
        queueableCase.hacerCalloutEnkontrol();

        Test.stopTest();
    }
}