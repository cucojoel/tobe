/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Bg_LeadUtil_Cls
* @Autor       Joel Soto
* @Date        28/11/2019
*
* @Group       BeGrand
* @Description Bg_LeadUtil_Cls contiene las funciones para reasignar agente a los prospectos
* @Changes
*/
global class Bg_LeadUtil_Cls {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description reasigna guardia la lista de prospectos
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static void reasignarGuardia(List<Lead> listLeadsTrigger) {
    List<Lead> leadToUpdate = new List<Lead>();





    Set<Id> setDesComFLead = new Set<Id>();
    for(Lead leadTrigger : listLeadsTrigger) {
      setDesComFLead.add(leadTrigger.Desarrollo_Comercial__c);
    }
    if(setDesComFLead.size() > 0) {
      Map<String, Desarrollo_Comercial__c> mapDesarrolloComIds = new Map<String, Desarrollo_Comercial__c>();
      List<Desarrollo_Comercial__c> listDesarrolloComercial = new List<Desarrollo_Comercial__c>();
      listDesarrolloComercial = [SELECT Id, Gerente_Ventas__r.Email, Asig_Automatica__c, Gerente_Ventas__c
        FROM Desarrollo_Comercial__c
        WHERE Id IN: setDesComFLead];
      for(Desarrollo_Comercial__c desacom :listDesarrolloComercial) {
        mapDesarrolloComIds.put(desacom.Id, desacom);
      }
      List<Guardia__c> listGuardiasTmp = new List<Guardia__c>();
      listGuardiasTmp = [SELECT Id, Name, Nombre_Asesor__c, Nombre_Asesor__r.Name, Roll__c, Fecha_Guardia__c, Contador_temp__c, Desarrollo_Comercial__c
        FROM Guardia__c
        WHERE Desarrollo_Comercial__c
        IN: listDesarrolloComercial
        AND Fecha_Guardia__c = TODAY
        ORDER BY Fecha_Guardia__c, Roll__c ASC];
      Set<Id> guardiasSet = new Set<Id>();
      for(Guardia__c guard : listGuardiasTmp) {
        guardiasSet.add(guard.Nombre_Asesor__c);
      }

      List<Lead> leadList = new List<Lead>([
        SELECT Id, OwnerId, Dia_reasignacion__c FROM Lead
        WHERE Dia_reasignacion__c = TODAY
        AND OwnerId IN: guardiasSet
        ORDER by CreatedDate
      ]);

      Map<String, List<Guardia__c>> mapguardias = new Map<String, List<Guardia__c>>();
      for(Lead prospecto : listLeadsTrigger) {
        Desarrollo_Comercial__c getDesCom = new Desarrollo_Comercial__c();
        getDesCom = mapDesarrolloComIds.get(prospecto.Desarrollo_Comercial__c);
        prospecto.Email_gerente__c = getDesCom.Gerente_Ventas__r.Email;
        prospecto.Dia_reasignacion__c = System.today();
        Id usuarioId = null;
        if(getDesCom.Asig_Automatica__c == true && !guardiasSet.isEmpty()) {
          usuarioId = obtenerUsuarioReasignado(guardiasSet, leadList);
        } else {
          usuarioId = getDesCom.Gerente_Ventas__c;
        }
        if  (String.isNotBlank(prospecto.Asesor_Primario__c)) {
          usuarioId = prospecto.Asesor_Primario__c;
        }
        prospecto.OwnerId = usuarioId;
        prospecto.Reasignado__c = true;
        leadList.add(prospecto);
        leadToUpdate.add(prospecto);
      }
      update leadToUpdate;
    }
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description obtiene el agente a partir de las guardias del dia
  *
  * @Autor       Joel Soto
  * @Date        28/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static String obtenerUsuarioReasignado(Set<Id> guardiasSet, List<Lead> leadList) {
    String usuarioId = null;
    Integer menor = 100;
    /*
    List<Lead> leadList = new List<Lead>([
          SELECT Id, OwnerId, Dia_reasignacion__c FROM Lead
          WHERE Dia_reasignacion__c = TODAY
          AND OwnerId IN: guardiasSet
          ORDER by CreatedDate
        ]);
        */
    Map<Id, Integer> mapeo = new Map<Id, Integer>();
    for(Lead leader : leadList) {
      if(mapeo.containsKey(leader.OwnerId)) {
        mapeo.put(leader.OwnerId, mapeo.get(leader.OwnerId) + 1);
      } else {
        mapeo.put(leader.OwnerId, 1);
      }
    }
    for(String idUsuario : guardiasSet) {
      if(!mapeo.containsKey(idUsuario)) {
        mapeo.put(idUsuario, 0);
      }
    }
    List<Id> userIds = new List<Id>();
    if(!mapeo.isEmpty()) {
      userIds.addAll(mapeo.keySet());
      for(Integer i = 0; i < userIds.size(); i++) {
        if(i == 0) {
          usuarioId = userIds[i];
          menor = mapeo.get(userIds[i]);
        } else {
          if(mapeo.get(userIds[i]) < menor) {
            usuarioId = userIds[i];
            menor = mapeo.get(userIds[i]);
          }
        }
      }
    }
    return usuarioId;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description transferirGuardia
  *
  * @Autor       Joel Soto
  * @Date        07/11/2019
  * @Param
  * @Param
  * @Return
  * @example
  **/
  /*
  public static void transferirGuardia(List<Desarrollo_Comercial__c> desComerciales) {
  Set<String> setIdDesCom = new Set<String>();
  for(Desarrollo_Comercial__c desCom : desComerciales) {
  setIdDesCom.add(desCom.Id);
  }
  Map<String, List<Guardia__c>> mapforIdDescom = new Map<String, List<Guardia__c>>();
  List<Guardia__c> listGuardias = [SELECT Id, Name FROM Guardia__c WHERE Desarrollo_Comercial__c IN :setIdDesCom];
  for(Guardia__c grd : listGuardias) {
  if(mapforIdDescom.containsKey(grd.Id)) {
  List<Guardia__c> lstTmp = mapforIdDescom.get(grd.Id);
  lstTmp.add(grd);
  mapforIdDescom.put(grd.Id, lstTmp);
  } else {
  List<Guardia__c> lstTmp = new List<Guardia__c>();
  lstTmp.add(grd);
  mapforIdDescom.put(grd.Id, lstTmp);
  }
  }
  }
  */
}