@isTest
private class TEST_Oportunidad {
	private static Account cuenta;
	private static Opportunity oportunidad;
	private static Carpeta__c carpeta;
	private static Attachment attachmentCarpeta;
	private static C_Oferta__c cartaOferta;
	private static Attachment attachmentCartaOferta;
	
	private static void init() {
		cuenta = new Account();
        cuenta.Name = 'Cuenta Prueba';
        
        oportunidad = new Opportunity();
		oportunidad.Name = 'Oportunidad Prueba';
		oportunidad.StageName = 'Nueva';
		oportunidad.CloseDate = Date.today().addDays(1);		
		
		carpeta = new Carpeta__c();
		carpeta.Name = 'Carpeta Prueba';
		
		attachmentCarpeta = new Attachment();
		attachmentCarpeta.Name = 'Attachment Carpeta Prueba';
		attachmentCarpeta.body = Blob.valueOf('Attachment Carpeta Prueba');
		
		cartaOferta = new C_Oferta__c();
		
		attachmentCartaOferta = new Attachment();
		attachmentCartaOferta.Name = 'Attachment Carta Oferta Prueba';
		attachmentCartaOferta.body = Blob.valueOf('Attachment Carta Oferta Prueba');
	}

	static testMethod void test1() {
		init();
		insert cuenta;
		oportunidad.AccountId = cuenta.Id;
		insert oportunidad;
		carpeta.Oportunidad__c = oportunidad.Id;
		insert carpeta;
		attachmentCarpeta.ParentId = carpeta.Id;
		insert attachmentCarpeta;
		cartaOferta.Oportunidad__c = oportunidad.Id;
		insert cartaOferta;
		attachmentCartaOferta.ParentId = cartaOferta.Id;
		insert attachmentCartaOferta;
		delete oportunidad;
	}
	
	/* Se quito debido a que la funcionalidad se paso a Inmueble__c */
	/*
	static testMethod void test2() {
		Account cuenta2 = new Account();
        cuenta2.LastName = 'Cuenta Prueba';
        insert cuenta2;
        
        Desarrollo_Comercial__c desarrolloC = new Desarrollo_Comercial__c();
        desarrolloC.Name = 'Prueba DesarrolloC';
        insert desarrolloC;
        
        Desarrollo__c desarrollo = new Desarrollo__c();
        desarrollo.Name = 'Desarrollo Prueba';
        Time myTime = Time.newInstance(1, 2, 3, 4);
        desarrollo.Inicio__c = myTime;
        desarrollo.Desarrollo_Comercial__c = desarrolloC.Id;
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c();
        etapa.Name = 'Etapa Prueba';
        etapa.No_de_Pisos__c = 1;
        etapa.Desarrollo__c = desarrollo.Id;
        etapa.Id_Etapa_Enkontrol__c = '1';
        etapa.Tipo__c = 'Torre';
        insert etapa;        
        
        Inmueble__c departamento = new Inmueble__c();
		departamento.Name = 'Departamento Prueba';
		departamento.Etapa__c = etapa.Id;
		insert departamento;
		
        Opportunity oportunidad2 = new Opportunity();
		oportunidad2.Name = 'Oportunidad Prueba';
		oportunidad2.StageName = 'Nueva';
		oportunidad2.CloseDate = Date.today().addDays(1);
		oportunidad2.AccountId = cuenta2.Id;
		oportunidad2.Departamento__c = departamento.Id;
		insert oportunidad2;
		
		Carpeta__c carpeta = new Carpeta__c();
		carpeta.Name = 'Carpeta Prueba';
		carpeta.Oportunidad__c = oportunidad2.Id;
		carpeta.Documentos_Sincronizados__c = true;
		insert carpeta;
		
		oportunidad2.StageName = 'Vendido';
		update oportunidad2;
	}
	*/
}