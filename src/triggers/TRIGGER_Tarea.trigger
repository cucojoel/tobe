trigger TRIGGER_Tarea on Task (after update) {

	CLASS_Tarea classTarea = new CLASS_Tarea();
	classTarea.cambioEstatusCitaConfirmada(Trigger.oldMap, Trigger.newMap);
	classTarea.cambioEstatusCitaNoConfirmada(Trigger.oldMap, Trigger.newMap);
	classTarea.cambioEstatusCitaNoAsistio(Trigger.oldMap, Trigger.newMap);
}