/*
	Mario Quevedo Diaz
	Metodo de prueba para la clase - EnviarPorCorreoMasivo -
*/

@isTest
public class EnviarPorCorreoMasivo_Test {
	static testMethod void test01() {
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
        
        Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'BGR',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
        
        Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'VENDIDO',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = false
		);
		insert departamento;
		
        Opportunity oportunidad = new Opportunity(
			Name = '15810-8',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			Desarrollo__c = desarrollo.Id,
			Etapa__c = etapa.Id,
			Departamento__c = departamento.Id,
			Compania__c = compania.Id,
			Lote_Id__c = '322',
			Id_Prospecto_Enkontrol__c = '15810',
			ID_Cliente_Enkontrol__c = '5108',
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		Test.startTest();
			EnviarPorCorreoMasivo.SavePdf(oportunidad.Id);
		Test.stopTest();
	}
	
	static testMethod void test02() {
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
        
        Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'PFU',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
        
        Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'VENDIDO',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = false
		);
		insert departamento;
		
        Opportunity oportunidad = new Opportunity(
			Name = 'CoberturaOportunidad',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			Desarrollo__c = desarrollo.Id,
			Etapa__c = etapa.Id,
			Departamento__c = departamento.Id,
			Compania__c = compania.Id,
			Lote_Id__c = '322',
			Id_Prospecto_Enkontrol__c = '15810',
			ID_Cliente_Enkontrol__c = '5108',
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		Test.startTest();
			EnviarPorCorreoMasivo.SavePdf(oportunidad.Id);
		Test.stopTest();
	}
	
	static testMethod void test03() {
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			PersonEmail = 'a@a.a',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
		
		Attachment adjunto = new Attachment(
			Name = 'ECtaBG_123456789012345678',
			Body = Blob.valueOf('CoberturaCuerpoPDF'),
			ParentId = cliente.Id
		);
        insert adjunto;
        
        Opportunity oportunidad = new Opportunity(
			Name = 'CoberturaOportunidad',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		list<SFDC_BG_EdoCta_temp__c> temporalLista = new list<SFDC_BG_EdoCta_temp__c>();
		SFDC_BG_EdoCta_temp__c temporal1 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'BGR',
			InteresMoratorio__c = '0.00',
			Email__c = 'a@a.a'
		);
		temporalLista.add(temporal1);
		
		SFDC_BG_EdoCta_temp__c temporal2 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'BGR',
			InteresMoratorio__c = '10.00',
			Email__c = 'a@a.a'
		);
		temporalLista.add(temporal2);
		insert temporalLista;
        
		Test.startTest();
			EnviarPorCorreoMasivo.enviarMail(cliente.Id);
		Test.stopTest();
	}
	
	static testMethod void test04() {
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			PersonEmail = 'a@a.a',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
		
		Attachment adjunto = new Attachment(
			Name = 'ECtaBG_123456789012345678_x',
			Body = Blob.valueOf('CoberturaCuerpoPDF'),
			ParentId = cliente.Id
		);
        insert adjunto;
        
        Opportunity oportunidad = new Opportunity(
			Name = 'CoberturaOportunidad',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
		
		list<SFDC_BG_EdoCta_temp__c> temporalLista = new list<SFDC_BG_EdoCta_temp__c>();
		SFDC_BG_EdoCta_temp__c temporal1 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'PFU',
			InteresMoratorio__c = '0.00',
			Email__c = 'a@a.a'
		);
		temporalLista.add(temporal1);
		
		SFDC_BG_EdoCta_temp__c temporal2 = new SFDC_BG_EdoCta_temp__c(
			Name = 'CoberturaTemporal',
			Saldo__c = '4159629.00',
			PagoMes__c = '0.00',
			SaldoCorte__c = '0.00',
			Descuento__c = '-187818.00',
			TotalPagado__c = '-1846048.00',
			OperationValue__c = '6193495.00',
			FechaEstadoCuenta__c = '03 de Octubre del 2019',
			ReferenciaDepositos__c = 'BGR00000051086',
			Departamento__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612',
			Desarrollo__c = 'REFORMA',
			Oportunidad__c = oportunidad.Id,
			DesarrolloCompleto__c = 'BE GRAND REFORMA',
			NumeroDepartamento__c = '3612',
			DesapareceDesarrollo__c = false,
			SiglaProyecto__c = 'PFU',
			InteresMoratorio__c = '10.00',
			Email__c = 'a@a.a'
		);
		temporalLista.add(temporal2);
		insert temporalLista;
        
		Test.startTest();
			EnviarPorCorreoMasivo.enviarMail(cliente.Id);
		Test.stopTest();
	}
}