trigger Trigger_Inmueble on Inmueble__c (after insert, after update) {

    if(Trigger.isAfter && Trigger.isInsert){
        Handler_InmuebleTrigger.isAfterUpdate(Trigger.New);
    }
  
}