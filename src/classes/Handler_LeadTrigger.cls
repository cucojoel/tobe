public class Handler_LeadTrigger {
	public static void beforeInsertHandler(List<Lead> triggerNew) {
		Schema.DescribeFieldResult fieldResult_1 = Lead.LeadSource.getDescribe();
		String publicLabel;
		list<Lead> newLeadSf = new List<Lead>();
		list<Schema.PicklistEntry> values = fieldResult_1.getPicklistValues();
		map<String,String> mapValueLabelLeadSource = new Map<String,String>();
		
		for(Schema.PicklistEntry v : values) {
			mapValueLabelLeadSource.put(v.getValue(),v.getLabel());
		}
		
		for(Lead isnewLead : triggerNew ) {
			publicLabel = mapValueLabelLeadSource.get(isnewLead.LeadSource);
			isnewLead.Origen_Prospeccion__c = publicLabel;
			isnewLead.Origen_Medio__c = isnewLead.Medio__c;
			
			/*
			Jesus Castro
			Comentado a peticion de BeGrand
			if(isnewLead.Fecha_Alta_Sistema__c == null) {
				isnewLead.Fecha_Alta_Sistema__c = System.today();
			}
			*/
			
			if(String.isBlank(isnewLead.ID_Prospecto_Enkontrol__c) ) {
				newLeadSf.add(isnewLead);
			}
		}
		
		if (newLeadSf.size() > 0) {
			Service_LeadTrigger.asignarDesarrollo(newLeadSf);
			Service_LeadTrigger.asignarDesarrolloPrimario(newLeadSf);
			Service_LeadTrigger.asignarGuardia(newLeadSf);
		}
	}
	
	public static void afterInsertHandler(List<Lead> triggerNew) {
		list<Lead> leadsInsertToEnk = new List<Lead>();
		list<String> idsDuplicate = new list<String>();
		
		for(Lead lid:triggerNew) {
			list<Lead> listLeadTmp = new List<Lead>();
			listLeadTmp.add(lid);
			try {
				list<Datacloud.FindDuplicatesResult> results = Datacloud.FindDuplicates.findDuplicates(listLeadTmp);
				if(results.size() > 0) {
					for(Datacloud.FindDuplicatesResult findDupeResult : results) {
						for(Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
							for(Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
								for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
									system.debug('Valor duplicado: ' + matchRecord.getRecord());
									for(Datacloud.FieldDiff fielDiff : matchRecord.getFieldDiffs()) {
										if(fielDiff.getDifference() == 'Same') {
											idsDuplicate.add(lid.Id);
											system.debug('Valor duplicado: ' + fielDiff.getName());
											Lead orgLead= [SELECT Id, OwnerId, Email_gerente__c, Desarrollo_Comercial__c, Web_to_lead__c, Owner.Email FROM Lead WHERE Id =: matchRecord.getRecord().Id];
											String destinatario = '';											
											if(orgLead.Desarrollo_Comercial__c != lid.Desarrollo_Comercial__c) {
												list<String> sendTo = new List<String>();
												list<String> bccAddresses = new List<String>();
												destinatario = lid.Email_gerente__c;
												String htmlBody = '<html><body><img src="https://begrand--c.na53.content.force.com/servlet/servlet.ImageServer?id=015f200000245FI&oid=00Df20000019tKD&lastMod=1548702583000" alt="membrete"><p>Estimado Gerente.</p>Haz recibido este correo debido a que un prospecto previamente dado de alta en otro desarrollo, esta solicitando información de tu desarrollo.<br/>Te pedimos asignarlo a un Asesor de Ventar para que generarle una segunda oportunidad de venta.<br/>A continuación los datos del prospecto</p><br/>&nbsp;&nbsp;&nbsp;&nbsp;Nombre: ' + lid.FirstName + '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Apellido paterno: ' + lid.LastName + '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Apellido Materno: ' + lid.Apellido_Materno__c + '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Email: ' + lid.Email + '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Telefono: ' + lid.Phone + '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Desarrollo: ' + lid.Desarrollo_Web__c + '<p>Salesforce.</p><img src="https://begrand--c.na53.content.force.com/servlet/servlet.ImageServer?id=015f200000245it&oid=00Df20000019tKD&lastMod=1548910059000" alt="footer"></img><div style="display: inline-block;bottom: 43px;position: relative;">Soporte: <a href="servicios.sf@begrand.com" "email="" me"="">servicios.sf@begrand.com</a></div></body></html>';
												bccAddresses.add('joel.soto@cloudmobile.com.mx');
												bccAddresses.add('jose.martinez@begrand.mx');
												sendTo.add(destinatario);
												Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
												email.setToAddresses(sendTo);
												email.setBccAddresses(bccAddresses);
												email.setReplyTo('servicios.sf@begrand.mx');
												email.setSenderDisplayName('Be Grand');
												email.setSubject('Prospecto interesado en tu desarrollo');
												email.setHtmlBody(htmlBody);
												Messaging.SendEmailResult [] sendEmailResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
												system.debug('endEmailResult: ' + sendEmailResult);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			catch(Exception ex) {
				system.debug(ex.getMessage());
			}
			
			if(String.isBlank(lid.ID_Prospecto_Enkontrol__c) && !idsDuplicate.contains(lid.id)) {
				leadsInsertToEnk.add(lid);
			}
		}
		
		if(!leadsInsertToEnk.isEmpty()) {
			Service_LeadTrigger.sincronizarProspectos(leadsInsertToEnk, false);
		}
		
		if(idsDuplicate.size() > 0) {
			Database.delete(idsDuplicate);
		}
	}
	
	public static void beforeUpdateHandler(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap) {
		list<Lead> prospectoscambiodesarrollo = new List<Lead>();
		Profile profileAsesor    = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
		Profile profileAsistente = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
		Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
		Profile profileRecepcion = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
		Profile profileAdmin     = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'];
		
		for(Lead lid:triggerNew) {
			Lead lidold = triggerOldMap.get(lid.Id);
			/*
				Mario Quevedo Diaz - 13 mayo 2019
				Comentado a peticion del usuario. Se encuentra en Process builder
			*/
			/*
			if(lidold.Agente__c != lid.Agente__c && UserInfo.getProfileId() == profileAsistente.Id) {
				system.debug('**** El agente ha cambiado profileAsistente');
				lid.OwnerId = lid.Agente__c;
				system.debug('BU agente: ' + lid.Agente__c);
				system.debug('BU owner: ' + lid.OwnerId);
			}
			*/
			
			if(lidold.OwnerId != lid.OwnerId && UserInfo.getProfileId() == profileGerente.Id) {
				system.debug('**** El owner ha cambiado profileGerente');
				lid.Agente__c = lid.OwnerId;
				system.debug('BU agente: '+lid.Agente__c);
				system.debug('BU owner: '+lid.OwnerId);
			}
			
			/*
				Mario Quevedo Diaz - 13 mayo 2019
				Comentado a peticion del usuario. Se encuentra en Process builder
			*/
			/*
			if(lidold.Agente__c != lid.Agente__c && UserInfo.getProfileId() == profileAdmin.Id) {
				system.debug('**** El agente ha cambiado profileAdmin');
				lid.OwnerId = lid.Agente__c;
				system.debug('BU agente: ' + lid.Agente__c);
				system.debug('BU owner: ' + lid.OwnerId);
			}
			*/
			
			if((lid.Desarrollo__c != lidold.Desarrollo__c) || (lid.Desarrollo_Comercial__c != lidold.Desarrollo_Comercial__c) || (lid.Desarrollo_Web__c != lidold.Desarrollo_Web__c)) {
				prospectoscambiodesarrollo.add(lid);
			}
		}
		
		if(prospectoscambiodesarrollo.size() != 0) {
			Service_LeadTrigger.asignarDesarrollo(prospectoscambiodesarrollo);
		}
	}
	
	public static void afterUpdateHandler(map<Id, Lead> triggerNewMap, map<Id, Lead> triggerOldMap) {
		list<Lead> leadsToUpdate = new List<Lead>();
		for(Lead pro : triggerNewMap.values()) {
			if(!Helper_RecursiveTrigger.leadsYaActualizados.contains(pro.Id)) {
				Helper_RecursiveTrigger.leadsYaActualizados.add(pro.Id);
				system.debug('**** Sincronizado_en_Enkontrol__c: ' + pro.Sincronizado_en_Enkontrol__c);
				system.debug('**** ID_Prospecto_Enkontrol__c: ' + pro.ID_Prospecto_Enkontrol__c);
				if(pro.ID_Prospecto_Enkontrol__c != null) {
					leadsToUpdate.add(pro);
				}
			}
			if(!leadsToUpdate.isEmpty()) {
				Service_LeadTrigger.sincronizarProspectos(leadsToUpdate, true);
			}
		}
	}
	
	public void createObjectClass() {
		String nameAttach;
	    String descriptionAttach;
	    String parentIdAttach;
	    String bodyAttach;
	    String publicAttach;
	    String objectId;
	    Boolean isActive;
	    Id idProspect;
	    String prospectAgent;
	    String prospectCompany;
	    String prospectDes;
	    String prospectEmail;
	    String prospectName;
	    String prospectGender;
	    String prospectPhone;
	    Integer prospectIdEnkontrol;
	    String prospectMoral;
	    String prospectLastName;
	    String prospectPuntoProspectacion;
	    Id prospectIdSalesforce;
	    String prospectFirstName;
	    String prospectStreet;
	    String prospectBirthdate;
	    String prospectCity;
	    String prospectCountry;
	    String prospectColony;
	    String prospectEmail2;
	    String prospectNacionality;
	    String prospectPhone2;
	    String prospectPhone3;
	    Integer prospectZipCode;
	    String prospectRFC;
	    String prospectState;    
	    String nameAttachB;
	    String descriptionAttachB;
	    String parentIdAttachB;
	    String bodyAttachB;
	    String publicAttachB;
	    String objectIdB;
	    Boolean isActiveB;
	    Id idProspectB;
	    String prospectAgentB;
	    String prospectCompanyB;
	    String prospectDesB;
	    String prospectEmailB;
	    String prospectNameB;
	    String prospectGenderB;
	    String prospectPhoneB;
	    Integer prospectIdEnkontrolB;
	    String prospectMoralB;
	    String prospectLastNameB;
	    String prospectPuntoProspectacionB;
	    Id prospectIdSalesforceB;
	    String prospectFirstNameB;
	    String prospectStreetB;
	    String prospectBirthdateB;
	    String prospectCityB;
	    String prospectCountryB;
	    String prospectColonyB;
	    String prospectEmail2B;
	    String prospectNacionalityB;
	    String prospectPhone2B;
	    String prospectPhone3B;
	    Integer prospectZipCodeB;
	    String prospectRFCB;
	    String prospectStateB;
	}
}

/*
	Version BeGrand en QA - tiene menos lineas de codigo debido a los comentarios - 13 mayo 2019
*/

/*
public class Handler_LeadTrigger {
  public static void beforeInsertHandler(List<Lead> triggerNew){
    System.debug('* => beforeInsertHandler =>');
    List<Lead> newLeadSf = new List<Lead>();
    Schema.DescribeFieldResult fieldResult_1 = Lead.LeadSource.getDescribe();
    List<Schema.PicklistEntry> values = fieldResult_1.getPicklistValues();
    Map<String,String> mapValueLabelLeadSource = new Map<String,String>();
    String publicLabel;
    for( Schema.PicklistEntry v : values) {
      mapValueLabelLeadSource.put(v.getValue(),v.getLabel());
    }
    for (Lead isnewLead : triggerNew ){
      publicLabel = mapValueLabelLeadSource.get(isnewLead.LeadSource);
      isnewLead.Origen_Prospeccion__c = publicLabel;
      isnewLead.Origen_Medio__c = isnewLead.Medio__c;
      if(isnewLead.Fecha_Alta_Sistema__c == null){
        isnewLead.Fecha_Alta_Sistema__c = System.today();
      }
      if(String.isBlank(isnewLead.ID_Prospecto_Enkontrol__c) ){
        newLeadSf.add(isnewLead);
      }
    }
    if (newLeadSf.size() > 0){
      Service_LeadTrigger.asignarDesarrollo(newLeadSf);
      Service_LeadTrigger.asignarGuardia(newLeadSf);
    }
    System.debug('<= beforeInsertHandler <= *');
  }
 
  public static void afterInsertHandler(List<Lead> triggerNew){
    System.debug(' ** => afterInsertHandler =>');
    List<Lead> leadsInsertToEnk = new List<Lead>();
    System.debug(' ** triggerNew ***' + triggerNew);
    list<String> idsDuplicate = new list<String>();
    for(Lead lid:triggerNew){
      System.debug(lid.Web_to_lead__c);
      System.debug(' *** => busca duplicados =>');
      List<Lead> listLeadTmp = new List<Lead>();
      listLeadTmp.add(lid);
      try{
        List<Datacloud.FindDuplicatesResult> results = Datacloud.FindDuplicates.findDuplicates(listLeadTmp);
        if(results.size() > 0){
          for (Datacloud.FindDuplicatesResult findDupeResult : results) {
            for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
              for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                for (Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                  System.debug('### Duplicate Record: ' + matchRecord.getRecord());
                  for (Datacloud.FieldDiff fielDiff : matchRecord.getFieldDiffs()){
                    if(fielDiff.getDifference() == 'Same'){
                      idsDuplicate.add(lid.Id);
                      System.debug('Duplicate field: ' + fielDiff.getName());
                      Lead orgLead= [SELECT Id, OwnerId, Email_gerente__c, Desarrollo_Comercial__c,Web_to_lead__c, Owner.Email FROM Lead WHERE Id =:matchRecord.getRecord().Id];
                      String destinatario = '';
                      if(orgLead.Desarrollo_Comercial__c != lid.Desarrollo_Comercial__c){
                        destinatario = lid.Email_gerente__c;
                        System.debug('destinatario: '+destinatario);
                        String myUrlLead = URL.getSalesforceBaseUrl().toExternalForm()+'/'+matchRecord.getRecord().Id;
                        String ahref = '';
                        // ahref = '<a href="'+myUrlLead+'">'+matchRecord.getRecord().Id+'</a>';
                        String htmlBody = '<html><body><img src="https://begrand--c.na53.content.force.com/servlet/servlet.ImageServer?id=015f200000245FI&oid=00Df20000019tKD&lastMod=1548702583000" alt="membrete"><p>Estimado Gerente.</p>Haz recibido este correo debido a que un prospecto previamente dado de alta en otro desarrollo, esta solicitando información de tu desarrollo.<br/>Te pedimos asignarlo a un Asesor de Ventar para que generarle una segunda oportunidad de venta.<br/>A continuación los datos del prospecto</p><br/>&nbsp;&nbsp;&nbsp;&nbsp;Nombre: '+lid.FirstName+'<br/>&nbsp;&nbsp;&nbsp;&nbsp;Apellido paterno: '+lid.LastName+'<br/>&nbsp;&nbsp;&nbsp;&nbsp;Apellido Materno: '+lid.Apellido_Materno__c+'<br/>&nbsp;&nbsp;&nbsp;&nbsp;Email: '+lid.Email+'<br/>&nbsp;&nbsp;&nbsp;&nbsp;Telefono: '+lid.Phone+'<br/>&nbsp;&nbsp;&nbsp;&nbsp;Desarrollo: '+lid.Desarrollo_Web__c+'<p>Salesforce.</p><img src="https://begrand--c.na53.content.force.com/servlet/servlet.ImageServer?id=015f200000245it&oid=00Df20000019tKD&lastMod=1548910059000" alt="footer"></img><div style="display: inline-block;bottom: 43px;position: relative;">Soporte: <a href="servicios.sf@begrand.com" "email="" me"="">servicios.sf@begrand.com</a></div></body></html>';
                        List<String> sendTo = new List<String>();
                        List<String> bccAddresses = new List<String>();
                        bccAddresses.add('joel.soto@cloudmobile.com.mx');
                        bccAddresses.add('jose.martinez@begrand.mx');
                        sendTo.add(destinatario);
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        email.setToAddresses(sendTo);
                        email.setBccAddresses(bccAddresses);
                        email.setReplyTo('servicios.sf@begrand.mx');
                        email.setSenderDisplayName('Be Grand');
                        email.setSubject('Prospecto interesado en tu desarrollo');
                        email.setHtmlBody(htmlBody);
                        Messaging.SendEmailResult [] sendEmailResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                        System.debug('### endEmailResult: '+ sendEmailResult);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      catch(Exception ex){
        System.debug('catch duplicados');
        System.debug(ex.getMessage());
      }
      System.debug(' <= busca duplicados <= ***');
      if(String.isBlank(lid.ID_Prospecto_Enkontrol__c) && !idsDuplicate.contains(lid.id)){
        leadsInsertToEnk.add(lid);
      }
    }
    if(!leadsInsertToEnk.isEmpty()){
      System.debug(' ** Leads a insertar ***' + leadsInsertToEnk);
      Service_LeadTrigger.sincronizarProspectos(leadsInsertToEnk, false);
    }
    if(idsDuplicate.size() > 0){
      Database.delete(idsDuplicate);
    }
    System.debug(' <= afterInsertHandler <= **');
  }
  
  public static void beforeUpdateHandler(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap){
    System.debug(' *** => beforeUpdateHandler =>');
    System.debug('*****Usuario beforeUpdateHandler: ' + UserInfo.getUserId());
    List<Lead> prospectoscambiodesarrollo = new List<Lead>();
    Profile profileAsesor = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
    System.debug('*****profileAsesor: ' + profileAsesor);
    Profile profileAsistente = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
    System.debug('*****profileAsistente: ' + profileAsistente);
    Profile profileGerente = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    System.debug('*****profileGerente: ' + profileGerente);
    Profile profileRecepcion = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
    System.debug('*****profileRecepcion: ' + profileRecepcion);
    Profile profileAdmin = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'];
    System.debug('*****profileAsesor: ' + profileAsesor);
    for(Lead lid:triggerNew){
      Lead lidold = triggerOldMap.get(lid.Id);
      if (lidold.Agente__c != lid.Agente__c &&   UserInfo.getProfileId() == profileAsistente.Id){
        System.debug(' **** El agente ha cambiado profileAsistente');
        lid.OwnerId = lid.Agente__c;
        System.debug('BU agente: '+lid.Agente__c);
        System.debug('BU owner: '+lid.OwnerId);
      }
      if (lidold.OwnerId != lid.OwnerId && UserInfo.getProfileId() == profileGerente.Id){
        System.debug(' **** El owner ha cambiado profileGerente');
        lid.Agente__c = lid.OwnerId;
        System.debug('BU agente: '+lid.Agente__c);
        System.debug('BU owner: '+lid.OwnerId);
      }
      if(lidold.Agente__c != lid.Agente__c && UserInfo.getProfileId() == profileAdmin.Id){
        System.debug(' **** El agente ha cambiado profileAdmin');
        lid.OwnerId = lid.Agente__c;
        System.debug('BU agente: '+lid.Agente__c);
        System.debug('BU owner: '+lid.OwnerId);
      }
      if ((lid.Desarrollo__c != lidold.Desarrollo__c) || (lid.Desarrollo_Comercial__c != lidold.Desarrollo_Comercial__c) || (lid.Desarrollo_Web__c != lidold.Desarrollo_Web__c) ){
        prospectoscambiodesarrollo.add(lid);
      }
    }
    if(prospectoscambiodesarrollo.size() != 0){
      Service_LeadTrigger.asignarDesarrollo(prospectoscambiodesarrollo);
    }
    System.debug(' <= beforeUpdateHandler <= ***');
  }
  
  public static void afterUpdateHandler(Map<Id, Lead> triggerNewMap, Map<Id, Lead> triggerOldMap){
    System.debug(' **** => afterUpdateHandler =>');
    System.debug(' **** triggerNewMap ****' + triggerNewMap);
    List<Lead> leadsToUpdate = new List<Lead>();
    for(Lead pro : triggerNewMap.values()){
      if(!Helper_RecursiveTrigger.leadsYaActualizados.contains(pro.Id)){
        Helper_RecursiveTrigger.leadsYaActualizados.add(pro.Id);
        System.debug(' **** Sincronizado_en_Enkontrol__c: '+pro.Sincronizado_en_Enkontrol__c);
        System.debug(' **** ID_Prospecto_Enkontrol__c: ' +pro.ID_Prospecto_Enkontrol__c);
        if(pro.ID_Prospecto_Enkontrol__c != null) {
          leadsToUpdate.add(pro);
          system.debug(pro.OwnerId);
        }
      }
      if(!leadsToUpdate.isEmpty()){
        System.debug(' **** Leads a actualizar ****' + leadsToUpdate);
        Service_LeadTrigger.sincronizarProspectos(leadsToUpdate, true);
      }
    }
    System.debug(' <= afterUpdateHandler <= *****');
  }
  public void createObjectClass(){
    String nameAttach;
    String descriptionAttach;
    String parentIdAttach;
    String bodyAttach;
    String publicAttach;
    String objectId;
    Boolean isActive;
    Id idProspect;
    String prospectAgent;
    String prospectCompany;
    String prospectDes;
    String prospectEmail;
    String prospectName;
    String prospectGender;
    String prospectPhone;
    Integer prospectIdEnkontrol;
    String prospectMoral;
    String prospectLastName;
    String prospectPuntoProspectacion;
    Id prospectIdSalesforce;
    String prospectFirstName;
    String prospectStreet;
    String prospectBirthdate;
    String prospectCity;
    String prospectCountry;
    String prospectColony;
    String prospectEmail2;
    String prospectNacionality;
    String prospectPhone2;
    String prospectPhone3;
    Integer prospectZipCode;
    String prospectRFC;
    String prospectState;
    String nameAttachB;
    String descriptionAttachB;
    String parentIdAttachB;
    String bodyAttachB;
    String publicAttachB;
    String objectIdB;
    Boolean isActiveB;
    Id idProspectB;
    String prospectAgentB;
    String prospectCompanyB;
    String prospectDesB;
    String prospectEmailB;
    String prospectNameB;
    String prospectGenderB;
    String prospectPhoneB;
    Integer prospectIdEnkontrolB;
    String prospectMoralB;
    String prospectLastNameB;
    String prospectPuntoProspectacionB;
    Id prospectIdSalesforceB;
    String prospectFirstNameB;
    String prospectStreetB;
    String prospectBirthdateB;
    String prospectCityB;
    String prospectCountryB;
    String prospectColonyB;
    String prospectEmail2B;
    String prospectNacionalityB;
    String prospectPhone2B;
    String prospectPhone3B;
    Integer prospectZipCodeB;
    String prospectRFCB;
    String prospectStateB;
  }
}
*/