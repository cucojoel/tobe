global class QUEUEABLE_ProspectoNoContactado implements Queueable{

	List<Lead> leadsNoContactar;
    public QUEUEABLE_ProspectoNoContactado(List<Lead> leadsNoContactarOrigen) {
        this.leadsNoContactar = leadsNoContactarOrigen;
    }
    public void execute(QueueableContext context) {
         
        System.debug('*****leadsNoContactar 1:' + leadsNoContactar);
		for(Lead prospecto : leadsNoContactar){
			
			prospecto.Prospecto_No_Contactado__c = true;
			prospecto.Status = 'No contactado';
		}
		
		System.debug('*****leadsNoContactar 2:' + leadsNoContactar);
		update leadsNoContactar;
    }
}