public class LC_DetalleDeficienciaController {

    @AuraEnabled
    public static Detalle_del_Caso__c obtenerMatrizRelacionada (Id deficienciaId) {

        List<Detalle_del_Caso__c> matrizDeficiencias = [
                SELECT ID, Matriz_de_Deficiencias__c, Matriz_de_Deficiencias__r.Zona__c, Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c,
                        Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c, Matriz_de_Deficiencias__r.Especialidad__c,
                        Matriz_de_Deficiencias__r.Id_Subespecialidad_Enkontrol__c, Matriz_de_Deficiencias__r.Subespecialidad__c
                FROM Detalle_del_Caso__c
                WHERE Id = :deficienciaId
                LIMIT 1
        ];

        system.debug(':::detalle obtenido: ' + matrizDeficiencias.get(0));
        return matrizDeficiencias.get(0);
    }

    @AuraEnabled
    public static Map<Decimal, String> obtenerZonas(Id deficienciaId) {
        System.debug('entrando a obtener zonas');

        Detalle_del_Caso__c deficiencia = obtenerDeficienciaPorId(deficienciaId);
        Id etapaId = deficiencia.Casos__r.Entrega__r.Inmueble__r.Etapa__c;

        System.debug('Etapa ID ==>' + etapaId);

        List<Matriz_Deficiencias__c> matrizDeficiencias = [
                SELECT ID, Id_Zona_Enkontrol__c, Zona__c
                FROM Matriz_Deficiencias__c
                WHERE Etapa__c = :etapaId
        ];

        Map<Decimal, String> mapaZonas = new Map<Decimal, String>();

        for (Matriz_Deficiencias__c matriz : matrizDeficiencias) {
            if (!mapaZonas.containsKey(matriz.Id_Zona_Enkontrol__c)) {
                mapaZonas.put(matriz.Id_Zona_Enkontrol__c, matriz.Zona__c);
            }
        }

        System.debug('Valor de mapaZonas ==>' + mapaZonas);

        return mapaZonas;
    }

    @AuraEnabled
    public static Map<Decimal, String> obtenerEspecialidadesRelacionadas(Decimal zonaExtId, Id deficienciaId) {

        Detalle_del_Caso__c deficiencia = obtenerDeficienciaPorId(deficienciaId);
        Id etapaId = deficiencia.Casos__r.Entrega__r.Inmueble__r.Etapa__c;

        List<Matriz_Deficiencias__c> matrizDeficiencias = [
                SELECT ID, Id_Zona_Enkontrol__c, Etapa__c,
                        Especialidad__c, Id_Especialidad_Enkontrol__c
                FROM Matriz_Deficiencias__c
                WHERE Id_Zona_Enkontrol__c = :zonaExtId
                AND Etapa__c =:etapaId
                LIMIT 100
        ];
        System.debug('id zona externa ==>' + zonaExtId);

        Map<Decimal, String> mapaEspecialidades = new Map<Decimal, String>();

        for (Matriz_Deficiencias__c matriz : matrizDeficiencias) {
            if (!mapaEspecialidades.containsKey(matriz.Id_Especialidad_Enkontrol__c)) {
                mapaEspecialidades.put(matriz.Id_Especialidad_Enkontrol__c, matriz.Especialidad__c);
            }
        }

        System.debug('Valor de especialidades ==>' + mapaEspecialidades);

        return mapaEspecialidades;
    }

    @AuraEnabled
    public static Map<Decimal, String> obtenerSubEspecialidadesRelacionadas(Decimal especialidadExtId, Id deficienciaId) {

        Detalle_del_Caso__c deficiencia = obtenerDeficienciaPorId(deficienciaId);
        Id etapaId = deficiencia.Casos__r.Entrega__r.Inmueble__r.Etapa__c;

        System.debug('Etapa ID en subespecialidades ==>' + etapaId);

        List<Matriz_Deficiencias__c> matrizDeficiencias = [
                SELECT ID, Id_Especialidad_Enkontrol__c,
                        Subespecialidad__c, Id_Subespecialidad_Enkontrol__c, Etapa__c
                FROM Matriz_Deficiencias__c
                WHERE Id_Especialidad_Enkontrol__c = :especialidadExtId
                AND Etapa__c =:etapaId
                LIMIT 100
        ];

        Map<Decimal, String> mapaSubespecialidades = new Map<Decimal, String>();

        for (Matriz_Deficiencias__c matriz : matrizDeficiencias) {
            if (!mapaSubespecialidades.containsKey(matriz.Id_Subespecialidad_Enkontrol__c)) {
                mapaSubespecialidades.put(matriz.Id_Subespecialidad_Enkontrol__c, matriz.Subespecialidad__c);
            }
        }

        System.debug('Valor de subespecialidades ==>' + mapaSubespecialidades);

        return mapaSubespecialidades;
    }

    @AuraEnabled
    public static void asignarMatriz(Id deficienciaId, Decimal idZona, Decimal idEspecialidad, Decimal idSubespecialidad) {

        Detalle_del_Caso__c deficiencia = obtenerDeficienciaPorId(deficienciaId);
        Id etapaId = deficiencia.Casos__r.Entrega__r.Inmueble__r.Etapa__c;

        System.debug('Etapa Id =>' + etapaId);

        List<Matriz_Deficiencias__c> deficiencias = [
                SELECT Id
                FROM Matriz_Deficiencias__c
                WHERE Id_Zona_Enkontrol__c = :idZona
                AND Id_Especialidad_Enkontrol__c = :idEspecialidad
                AND Id_Subespecialidad_Enkontrol__c = :idSubespecialidad
                AND Etapa__c = :etapaId
        ];

        if(deficiencias != null && deficiencias.size() > 0){
            Matriz_Deficiencias__c matrizElegida = deficiencias.get(0);

            System.debug('La matriz =>' + matrizElegida);

            deficiencia.Matriz_de_Deficiencias__c = matrizElegida.Id;

            update deficiencia;
        } else {

        }
    }


    public static Detalle_del_Caso__c obtenerDeficienciaPorId(Id deficienciaId){
        List<Detalle_del_Caso__c> detalleDeficiencia = [
                SELECT Id, Casos__r.Entrega__r.Inmueble__r.Etapa__c
                FROM Detalle_del_Caso__c
                WHERE Id = :deficienciaId
        ];

        Detalle_del_Caso__c deficiencia = detalleDeficiencia.get(0);

        return deficiencia;
    }
}