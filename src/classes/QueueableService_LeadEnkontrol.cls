public class QueueableService_LeadEnkontrol implements Queueable, Database.AllowsCallouts {
    /*
    *
    */
    public List<Lead> leadsAMandar;
    public Boolean esActualizacion;
    @TestVisible
    public boolean ejecutarCallout = true;
    /*
    *
    */
    public QueueableService_LeadEnkontrol(List<Lead> leads, Boolean esActualizacion) {
      this.leadsAMandar = leads;
      this.esActualizacion = esActualizacion;
    }
    /*
    *
    */
    public void execute(QueueableContext context) {
     if(ejecutarCallout){
      ejecutarCalloutAEnkontrol();
    }
  }
    /*
    *
    */
    public void ejecutarCalloutAEnkontrol(){
      System.debug('*** => ejecutarCalloutAEnkontrol ***');
      if (esActualizacion){
        System.debug('### Es actualizacion ###');
      }
      else{
        System.debug('### Es insert ###');
      }
      Set<Id> leads = new Set<Id>();
      Set<Id> userIds = new Set<Id>();
      List<Lead> leadsParaActualizar = new List<Lead>();
      Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();
      Utils_WS_EnkontrolRequestBuilder.SecurityWrapper securityData = new Utils_WS_EnkontrolRequestBuilder.SecurityWrapper();
      securityData.password = configuracionEnkontrol.Password__c;
      securityData.username = configuracionEnkontrol.Username__c;
      Boolean esFormatoCompleto = false;
      if (esActualizacion) {
        securityData.soapOperation = Utils_WS_EnkontrolRequestBuilder.OPERATION_UPDATE;
      }
      else {
        securityData.soapOperation = Utils_WS_EnkontrolRequestBuilder.OPERATION_CREATE;
      }
      for (Lead l : leadsAMandar) {
        leads.add(l.Id);
        /*if (l.Agente__c != null) {
          userIds.add(l.Agente__c);
        }
        else*/ if (l.OwnerId != null) {
          userIds.add(l.OwnerId);
        }
        else {
          if(l.LastModifiedById != null && l.CreatedById == l.LastModifiedById){
            userIds.add(l.LastModifiedById);
          }
          else {
            userIds.add(l.CreatedById);
          }
        }
        userIds.add(l.CreatedById);
        if(l.LastModifiedById != null){
          userIds.add(l.LastModifiedById);
        }
      }
      List<Lead> leadsPorSincronizar = [
      SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
      Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
      Telefono2__c, Telefono_3__c,Email, Email2__c, Street, City, Country, Nacionalidad__c, Pais_Nacimiento__c,ID_Prospecto_Enkontrol__c,Status,
      PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
      CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c, Error_Sincronizacion__c
      FROM Lead
      WHERE Id IN :leads
      LIMIT 49
      ];
      System.debug('leadsPorSincronizar: '+leadsPorSincronizar);
      Map<Id, User> ownerUsers = new Map<Id, User>([
        SELECT Id, Name, Id_Enkontrol__c, ProfileId
        FROM User
        WHERE Id IN :userIds
        ]);
        System.debug('###* ownerUsers ###* ' + ownerUsers);
      Set<Id> profilesIds = new Set<Id>();
      for (User u : ownerUsers.values()) {
        profilesIds.add(u.ProfileId);
      }
      Map<Id, Profile> profilesFromIds = new Map<Id, Profile>([
        SELECT Id, Name
        FROM Profile
        WHERE Id IN :profilesIds
        ]);
      System.debug('###* Leads a mandar a enkontrol ###*');
      Map<String,String> equivalenciaEdos = new Map<String,String>();
      List<EquivalenciaEstados__c> listEdos =[SELECT apiEnk__c,Name FROM EquivalenciaEstados__c];
      for (EquivalenciaEstados__c edo : listEdos){
        equivalenciaEdos.put(edo.Name, edo.apiEnk__c);
      }
      for (Lead unLead : leadsPorSincronizar) {
        System.debug('###* Datos de lead ya en queueable ###*');
        System.debug('###* Desarrollo ###*' + unLead.Desarrollo__c);
        System.debug('###* Desarrollo Comercial ###*' + unLead.Desarrollo_Comercial__c);
        System.debug('###* Compania ###*' + unLead.Compania__c);
          system.debug('###* Owner Id ###* ' + unLead.OwnerId);
        try{

          Http http = new Http();
          HttpRequest req = new HttpRequest();
          req.setEndpoint(configuracionEnkontrol.Service_Endpoint__c);
          req.setMethod('POST');
          req.setHeader('Content-Type', 'text/xml');
          String requestBody = '';
          WS_EnkontrolIntegration.Prospectos prospecto = new WS_EnkontrolIntegration.Prospectos();
          String userIdEnkontrol = [SELECT Id_Enkontrol__c FROM User WHERE Id =: UserInfo.getUserId()].Id_Enkontrol__c;
          prospecto.apellidoMaterno    = unLead.Apellido_Materno__c;
          prospecto.apellidoPaterno    = unLead.LastName;
          prospecto.calleYNumero       = unLead.Street;
          prospecto.ciudad             = unLead.City;
          prospecto.colonia            = unLead.Colonia__c;
          prospecto.compania           = unLead.Compania__r.Id_Compania_Enkontrol__c;
          prospecto.cp                 = unLead.PostalCode;
          prospecto.desarrollo         = unLead.Desarrollo__r.Id_Desarrollo_Enkontrol__c;
          prospecto.email1             = unLead.Email;
          prospecto.email2             = unLead.Email2__c;
          prospecto.empresa            = unLead.Company;
          prospecto.fechaDeNacimiento  = (unLead.Fecha_Nacimiento__c != null) ? String.valueOf(unLead.Fecha_Nacimiento__c) : null;
          prospecto.idSalesforce       = unLead.Id;
          prospecto.nacionalidad       = unLead.Pais_Nacimiento__c;
          prospecto.nombre             = unLead.FirstName;
          prospecto.pais               = unLead.Country;
          prospecto.personaMoral       = unLead.Persona_Moral__c;
          prospecto.puntoProspectacion = (unLead.LeadSource != null) ? Integer.valueOf(unLead.LeadSource) : null;
          prospecto.rfc                = unLead.RFC__c;
          prospecto.sexo               = unLead.Sexo__c;
          prospecto.telefono1          = unLead.Phone;
          prospecto.telefono2          = unLead.Telefono2__c;
          prospecto.telefono3          = unLead.Telefono_3__c;
          prospecto.userId             = userIdEnkontrol;
          if(equivalenciaEdos.containsKey(unLead.State)){
            prospecto.estado = equivalenciaEdos.get(unLead.State);
          }
          prospecto.idEnkontrol = (unLead.ID_Prospecto_Enkontrol__c != null) ? Integer.valueOf(unLead.ID_Prospecto_Enkontrol__c) : null;
          /*/if(unLead.Agente__c != null){
            //prospecto.agente= Integer.valueOf(ownerUsers.get(unLead.Agente__c).Id_Enkontrol__c);
            prospecto.agente= Integer.valueOf(ownerUsers.get(unLead.OwnerId).Id_Enkontrol__c);
              
          }*/
          //else {
            prospecto.agente = Integer.valueOf(ownerUsers.get(unLead.OwnerId).Id_Enkontrol__c);
          //}

          System.debug('###* id agente ###*' + prospecto.agente);
          Profile profileRecepcion = [SELECT Id FROM Profile WHERE Name = 'Recepción'];

          if(profileRecepcion.Id != UserInfo.getProfileId()){
            esFormatoCompleto = true;
          }
          System.debug('###* es formato completo ###*' + esFormatoCompleto);
          if (esFormatoCompleto) {
            req.setHeader('SOAPAction', Utils_WS_EnkontrolRequestBuilder.SOAPActionProspectExecute);
            requestBody = Utils_WS_EnkontrolRequestBuilder.construirProspectosRequestCompleto(prospecto, securityData);
          }
          else {
            req.setHeader('SOAPAction', Utils_WS_EnkontrolRequestBuilder.SOAPActionBasicProspectExecute);
            requestBody = Utils_WS_EnkontrolRequestBuilder.construirProspectosRequestBasico(prospecto, securityData);
          }
          System.debug('###* REQUEST ###*' + requestBody);
          req.setBody(requestBody);
          HttpResponse res = http.send(req);
          System.debug('###* RESPONSE ###*' + res.getBody());
          /** Actualizar prospecto con el id **/
          if (res.getBody() != null) {
            Utils_WS_EnkontrolRequestBuilder.ProspectResponseWrapper responseWrapper = new Utils_WS_EnkontrolRequestBuilder.ProspectResponseWrapper();
            if (esFormatoCompleto) {
              responseWrapper = Utils_WS_EnkontrolRequestBuilder.parseProspectosResponse(res);
            }
            else {
              responseWrapper = Utils_WS_EnkontrolRequestBuilder.parseProspectosBasicResponse(res);
            }
            System.debug('###* PARSED RESPONSE ###*' + responseWrapper);
            try {
              if (esActualizacion) {
                if (responseWrapper.success) {
                  if(unLead.Sincronizado_en_Enkontrol__c == false){
                    unLead.Sincronizado_en_Enkontrol__c = true;
                    unLead.Error_Sincronizacion__c = null;
                    if(unLead.Status == 'Bloqueado'){
                      unLead.Status = 'Asignado';
                    }
                    leadsParaActualizar.add(unLead);
                    Helper_RecursiveTrigger.leadsYaActualizados.add(unLead.Id);
                  }
                }
                else {
                  if(unLead.Sincronizado_en_Enkontrol__c == true && unLead.Error_Sincronizacion__c == null){
                    unLead.Sincronizado_en_Enkontrol__c = false;
                    unLead.Error_Sincronizacion__c = responseWrapper.errorMessage;
                    if(unLead.Status == 'Asignado'){
                      unLead.Status = 'Bloqueado';
                    }
                    leadsParaActualizar.add(unLead);
                    Helper_RecursiveTrigger.leadsYaActualizados.add(unLead.Id);
                  }
                }
              }
              else {
                if (responseWrapper.success) {
                  unLead.Sincronizado_en_Enkontrol__c = true;
                  unLead.ID_Prospecto_Enkontrol__c = String.valueOf(responseWrapper.enkontrolId);
                  unLead.Error_Sincronizacion__c = null;
                  if(unLead.Status == 'Bloqueado'){
                    unLead.Status = 'Asignado';
                  }
                  leadsParaActualizar.add(unLead);
                  Helper_RecursiveTrigger.leadsYaActualizados.add(unLead.Id);
                }
                else {
                  unLead.Sincronizado_en_Enkontrol__c = false;
                  unLead.Error_Sincronizacion__c = responseWrapper.errorMessage;
                  unLead.Status = 'Bloqueado';
                  leadsParaActualizar.add(unLead);
                  Helper_RecursiveTrigger.leadsYaActualizados.add(unLead.Id);
                }
              }
            }
            catch (Exception e) {
              System.debug('###** Error encontrado al interpretar respuesta del servicio. ###**');
              if(unLead.Sincronizado_en_Enkontrol__c == true && unLead.Error_Sincronizacion__c == null){
                unLead.Sincronizado_en_Enkontrol__c = false;
                unLead.Error_Sincronizacion__c = 'Error interno del servidor.';
                leadsParaActualizar.add(unLead);
                Helper_RecursiveTrigger.leadsYaActualizados.add(unLead.Id);
              }
            }
          }
        }
        catch(Exception e){
          System.debug('###** Error encontrado en un punto de la ejecución interna ###**' + e.getMessage() + ' en: '+ e.getLineNumber());
          if(unLead.Sincronizado_en_Enkontrol__c == true && unLead.Error_Sincronizacion__c == null){
            unLead.Sincronizado_en_Enkontrol__c = false;
            unLead.Error_Sincronizacion__c = 'Error interno de Salesforce. Por favor contacte un administrador. Los detalles son: ' + e.getMessage() + ' en la línea: ' + e.getLineNumber();
            leadsParaActualizar.add(unLead);
            Helper_RecursiveTrigger.leadsYaActualizados.add(unLead.Id);
          }
        }
      }
      if (!leadsParaActualizar.isEmpty()) {
        Helper_RecursiveTrigger.setApagaBandera();
        update leadsParaActualizar;
      }
      System.debug('*** ejecutarCalloutAEnkontrol <=***');
    }
  /*
  *
  */
  public void createObjectClass() {
    String name;
    String objectId;
    Boolean isActive;
    Integer amount;
    String language;
    String request;
    Boolean requestValid;
    String idRequest;
    Integer requestAttempts;
    String requestResponse;
    String requestBody;
    String requestParameter;
    Id idUserRequest;
    String messageSuccess;
    String messageFailure;
    Integer enkontrolId;
    String stage;
    String operation;
    Id idProspect;
    String prospectAgent;
    String prospectCompany;
    String prospectDes;
    String prospectEmail;
    String prospectName;
    String prospectGender;
    String prospectPhone;
    Integer prospectIdEnkontrol;
    String prospectMoral;
    String prospectLastName;
    String prospectPuntoProspectacion;
    Id prospectIdSalesforce;
    String prospectFirstName;
    String prospectStreet;
    String prospectBirthdate;
    String prospectCity;
    String prospectCountry;
    String prospectColony;
    String prospectEmail2;
    String prospectNacionality;
    String prospectPhone2;
    String prospectPhone3;
    Integer prospectZipCode;
    String prospectRFC;
    String prospectState;
    String origin;
    Integer errorCode;
    String errorMessage;
    Integer successCode;
    String successMessage;
    String opportunity;
    Id opportunityId;
    Date createdDate;
    String description;
    Boolean delivery;
    String source;
    String appartment;
    Id idAppartment;
    String assignatedToName;
    Id assignatedToId;
    String ownerName;
    Id owner;
    Id externalId;
    String reference;
    Date requestDate;
    Id parentId;
    Decimal actualCost;
    String typeRequest;
    String idDropbox;
    String specialTerms;
    String contractTerm;
    Id accountId;
    Date deadLine;
    Integer access;
    String zone;
    Integer maxTime;
    Integer minTime;
    Integer averageTime;
    Integer duration;
    String contractNumber;
    String title;
    Id contractId;
    String development;
    Boolean completed;
    String nextStep;
    Integer probability;
    String subStatus;
    String bill;
    Date activatedDate;
    Integer orderReferenceNumber;
    Id originalOrderId;
    Integer discount;
    Date expirationDate;
    String quoteToName;
    Boolean isSync;
    String alias;
    String divisionName;
    String signature;
    Id managerId;
    Id userProfileId;
    Id managerProfileId;
    String name0;
    name = (String.isBlank(name) == true ? null : name);
    String objectId0;
    objectId = (String.isBlank(objectId) == true ? null : objectId);
    Boolean isActive0;
    isActive = (String.isBlank(String.valueOf(isActive)) == true ? null : isActive);
    Integer amount0;
    amount = (String.isBlank(String.valueOf(amount)) == true ? null : amount);
    String language0;
    language = (String.isBlank(language) == true ? null : language);
    String request0;
    request = (String.isBlank(request) == true ? null : request);
    Boolean requestValid0;
    requestValid = (String.isBlank(String.valueOf(requestValid)) == true ? null : requestValid);
    String idRequest0;
    idRequest = (String.isBlank(idRequest) == true ? null : idRequest);
    Integer requestAttempts0;
    requestAttempts = (String.isBlank(String.valueOf(requestAttempts)) == true ? null : requestAttempts);
    String requestResponse0;
    requestResponse = (String.isBlank(requestResponse) == true ? null : requestResponse);
    String requestBody0;
    requestBody = (String.isBlank(requestBody) == true ? null : requestBody);
    String requestParameter0;
    requestParameter = (String.isBlank(requestParameter) == true ? null : requestParameter);
    Id idUserRequest0;
    idUserRequest = (String.isBlank(String.valueOf(idUserRequest)) == true ? null : idUserRequest);
    String messageSuccess0;
    messageSuccess = (String.isBlank(messageSuccess) == true ? null : messageSuccess);
    String messageFailure0;
    messageFailure = (String.isBlank(messageFailure) == true ? null : messageFailure);
    Integer enkontrolId0;
    enkontrolId = (String.isBlank(String.valueOf(enkontrolId)) == true ? null : enkontrolId);
    String stage0;
    stage = (String.isBlank(stage) == true ? null : stage);
    String operation0;
    operation = (String.isBlank(operation) == true ? null : operation);
    Id idProspect0;
    idProspect = (String.isBlank(String.valueOf(idProspect)) == true ? null : idProspect);
    String prospectAgent0;
    prospectAgent = (String.isBlank(prospectAgent) == true ? null : prospectAgent);
    String prospectCompany0;
    prospectCompany = (String.isBlank(prospectCompany) == true ? null : prospectCompany);
    String prospectDes0;
    prospectDes = (String.isBlank(prospectDes) == true ? null : prospectDes);
    String prospectEmail0;
    prospectEmail = (String.isBlank(prospectEmail) == true ? null : prospectEmail);
    String prospectName0;
    prospectName = (String.isBlank(prospectName) == true ? null : prospectName);
    String prospectGender0;
    prospectGender = (String.isBlank(prospectGender) == true ? null : prospectGender);
    String prospectPhone0;
    prospectPhone = (String.isBlank(prospectPhone) == true ? null : prospectPhone);
    Integer prospectIdEnkontrol0;
    prospectIdEnkontrol = (String.isBlank(String.valueOf(prospectIdEnkontrol)) == true ? null : prospectIdEnkontrol);
    String prospectMoral0;
    prospectMoral = (String.isBlank(prospectMoral) == true ? null : prospectMoral);
    String prospectLastName0;
    prospectLastName = (String.isBlank(prospectLastName) == true ? null : prospectLastName);
    String prospectPuntoProspectacion0;
    prospectPuntoProspectacion = (String.isBlank(prospectPuntoProspectacion) == true ? null : prospectPuntoProspectacion);
    Id prospectIdSalesforce0;
    prospectIdSalesforce = (String.isBlank(String.valueOf(prospectIdSalesforce)) == true ? null : prospectIdSalesforce);
    String prospectFirstName0;
    prospectFirstName = (String.isBlank(prospectFirstName) == true ? null : prospectFirstName);
    String prospectStreet0;
    prospectStreet = (String.isBlank(prospectStreet) == true ? null : prospectStreet);
    String prospectBirthdate0;
    prospectBirthdate = (String.isBlank(prospectBirthdate) == true ? null : prospectBirthdate);
    String prospectCity0;
    prospectCity = (String.isBlank(prospectCity) == true ? null : prospectCity);
    String prospectCountry0;
    prospectCountry = (String.isBlank(prospectCountry) == true ? null : prospectCountry);
    String prospectColony0;
    prospectColony = (String.isBlank(prospectColony) == true ? null : prospectColony);
    String prospectEmail20;
    prospectEmail2 = (String.isBlank(prospectEmail2) == true ? null : prospectEmail2);
    String prospectNacionality0;
    prospectNacionality = (String.isBlank(prospectNacionality) == true ? null : prospectNacionality);
    String prospectPhone20;
    prospectPhone2 = (String.isBlank(prospectPhone2) == true ? null : prospectPhone2);
    String prospectPhone30;
    prospectPhone3 = (String.isBlank(prospectPhone3) == true ? null : prospectPhone3);
    Integer prospectZipCode0;
    prospectZipCode = (String.isBlank(String.valueOf(prospectZipCode)) == true ? null : prospectZipCode);
    String prospectRFC0;
    prospectRFC = (String.isBlank(prospectRFC) == true ? null : prospectRFC);
    String prospectState0;
    prospectState = (String.isBlank(prospectState) == true ? null : prospectState);
    String origin0;
    origin = (String.isBlank(origin) == true ? null : origin);
    Integer errorCode0;
    errorCode = (String.isBlank(String.valueOf(errorCode)) == true ? null : errorCode);
    String errorMessage0;
    errorMessage = (String.isBlank(errorMessage) == true ? null : errorMessage);
    Integer successCode0;
    successCode = (String.isBlank(String.valueOf(successCode)) == true ? null : successCode);
    String successMessage0;
    successMessage = (String.isBlank(successMessage) == true ? null : successMessage);
    String opportunity0;
    opportunity = (String.isBlank(opportunity) == true ? null : opportunity);
    Id opportunityId0;
    opportunityId = (String.isBlank(String.valueOf(opportunityId)) == true ? null : opportunityId);
    Date createdDate0;
    createdDate = (String.isBlank(String.valueOf(createdDate)) == true ? null : createdDate);
    String description0;
    description = (String.isBlank(description) == true ? null : description);
    Boolean delivery0;
    delivery = (String.isBlank(String.valueOf(delivery)) == true ? null : delivery);
    String source0;
    source = (String.isBlank(source) == true ? null : source);
    String appartment0;
    appartment = (String.isBlank(appartment) == true ? null : appartment);
    Id idAppartment0;
    idAppartment = (String.isBlank(String.valueOf(idAppartment)) == true ? null : idAppartment);
    String assignatedToName0;
    assignatedToName = (String.isBlank(assignatedToName) == true ? null : assignatedToName);
    Id assignatedToId0;
    assignatedToId = (String.isBlank(String.valueOf(assignatedToId)) == true ? null : assignatedToId);
    String ownerName0;
    ownerName = (String.isBlank(ownerName) == true ? null : ownerName);
    Id owner0;
    owner = (String.isBlank(String.valueOf(owner)) == true ? null : owner);
    Id externalId0;
    externalId = (String.isBlank(String.valueOf(externalId)) == true ? null : externalId);
    String reference0;
    reference = (String.isBlank(reference) == true ? null : reference);
    Date requestDate0;
    requestDate = (String.isBlank(String.valueOf(requestDate)) == true ? null : requestDate);
    Id parentId0;
    parentId = (String.isBlank(String.valueOf(parentId)) == true ? null : parentId);
    Decimal actualCost0;
    actualCost = (String.isBlank(String.valueOf(actualCost)) == true ? null : actualCost);
    String typeRequest0;
    typeRequest = (String.isBlank(typeRequest) == true ? null : typeRequest);
    String idDropbox0;
    idDropbox = (String.isBlank(idDropbox) == true ? null : idDropbox);
    String specialTerms0;
    specialTerms = (String.isBlank(specialTerms) == true ? null : specialTerms);
    String contractTerm0;
    contractTerm = (String.isBlank(contractTerm) == true ? null : contractTerm);
    Id accountId0;
    accountId = (String.isBlank(String.valueOf(accountId)) == true ? null : accountId);
    Date deadLine0;
    deadLine = (String.isBlank(String.valueOf(deadLine)) == true ? null : deadLine);
    Integer access0;
    access = (String.isBlank(String.valueOf(access)) == true ? null : access);
    String zone0;
    zone = (String.isBlank(zone) == true ? null : zone);
    Integer maxTime0;
    maxTime = (String.isBlank(String.valueOf(maxTime)) == true ? null : maxTime);
    Integer minTime0;
    minTime = (String.isBlank(String.valueOf(minTime)) == true ? null : minTime);
    Integer averageTime0;
    averageTime = (String.isBlank(String.valueOf(averageTime)) == true ? null : averageTime);
    Integer duration0;
    duration = (String.isBlank(String.valueOf(duration)) == true ? null : duration);
    String contractNumber0;
    contractNumber = (String.isBlank(contractNumber) == true ? null : contractNumber);
    String title0;
    title = (String.isBlank(title) == true ? null : zone);
    Id contractId0;
    contractId = (String.isBlank(String.valueOf(contractId)) == true ? null : contractId);
    String development0;
    development = (String.isBlank(development) == true ? null : development);
    Boolean completed0;
    completed = (String.isBlank(String.valueOf(completed)) == true ? null : completed);
    String nextStep0;
    nextStep = (String.isBlank(nextStep) == true ? null : nextStep);
    Integer probability0;
    probability = (String.isBlank(String.valueOf(probability)) == true ? null : probability);
    String subStatus0;
    subStatus = (String.isBlank(subStatus) == true ? null : subStatus);
    String bill0;
    bill = (String.isBlank(bill) == true ? null : bill);
    Date activatedDate0;
    activatedDate = (String.isBlank(String.valueOf(activatedDate)) == true ? null : activatedDate);
    Integer orderReferenceNumber0;
    orderReferenceNumber = (String.isBlank(String.valueOf(orderReferenceNumber)) == true ? null : orderReferenceNumber);
    Id originalOrderId0;
    originalOrderId = (String.isBlank(String.valueOf(originalOrderId)) == true ? null : originalOrderId);
    Integer discount0;
    discount = (String.isBlank(String.valueOf(discount)) == true ? null : discount);
    Date expirationDate0;
    expirationDate = (String.isBlank(String.valueOf(expirationDate)) == true ? null : expirationDate);
    String quoteToName0;
    quoteToName = (String.isBlank(quoteToName) == true ? null : quoteToName);
    Boolean isSync0;
    isSync = (String.isBlank(String.valueOf(isSync)) == true ? null : isSync);
    String alias0;
    alias = (String.isBlank(alias) == true ? null : alias);
    String divisionName0;
    divisionName = (String.isBlank(divisionName) == true ? null : divisionName);
    String signature0;
    signature = (String.isBlank(signature) == true ? null : signature);
    Id managerId0;
    managerId = (String.isBlank(String.valueOf(managerId)) == true ? null : managerId);
    Id userProfileId0;
    userProfileId = (String.isBlank(String.valueOf(userProfileId)) == true ? null : userProfileId);
    Id managerProfileId0;
    managerProfileId = (String.isBlank(String.valueOf(managerProfileId)) == true ? null : managerProfileId);
  }
}