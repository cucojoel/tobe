public with sharing class CONTROL_CrearCitaCuenta {

	public Account cliente {get; set;}
	public Set<Id> idsAsesores;
	public Map<String,Task> mapaAsesorHorasTareas;
	public Map<String,Event> mapaAsesorHorasEventos;
	public List<SelectOption> horariosDisponibles {get; set;}
	public String horarioSeleccionado {get; set;}
	public String nombreDesarrolloComercialSeleccionado;

	public CONTROL_CrearCitaCuenta() {

		cliente = [SELECT Id, Name, F_CM__c, DC_CM__c, Comentarios_Cita__c, OwnerId, Owner.Desarrollo__c, Grado_de_interes__c FROM Account WHERE Id =: ApexPages.currentPage().getParameters().get('id')];
		cliente.F_CM__c = null;
		cliente.DC_CM__c = null;
		cliente.Comentarios_Cita__c = null;

		idsAsesores = new Set<Id>();
		mapaAsesorHorasTareas = new Map<String,Task>();
		mapaAsesorHorasEventos = new Map<String,Event>();
		horariosDisponibles = new List<SelectOption>();
		horariosDisponibles.add(new SelectOption('','--Ninguna--'));
		horarioSeleccionado = null;
	}

	//Metodo para obtener los estatus cuando un cliente ha contestado pero no desea agendar cita
	public List<SelectOption> getGradosInteres(){

		List<SelectOption> optionList = new List<SelectOption>();
		optionList.add(new SelectOption('','--Ninguno--'));

		Schema.DescribeFieldResult fieldResult = Account.Grado_de_interes__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		for(Schema.PicklistEntry f : ple){
			optionList.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return optionList;
	}

	//Metodo para buscar los horarios disponibles de los asesores en base a la fecha seleccionada y las guardias existentes
	public void buscarHorariosDisponibles(){

		idsAsesores = new Set<Id>();
		mapaAsesorHorasTareas = new Map<String,Task>();
		mapaAsesorHorasEventos = new Map<String,Event>();
		horariosDisponibles = new List<SelectOption>();
		horariosDisponibles.add(new SelectOption('','--Ninguna--'));
		horarioSeleccionado = null;

		System.debug('*****cliente.F_CM__c: ' + cliente.F_CM__c);
		System.debug('*****cliente.DC_CM__c: ' + cliente.DC_CM__c);

		if(cliente.DC_CM__c != null){

			//Se obtienen las guardias disponibles para el dia seleccionado
			/*List<Guardia__c> guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: cliente.DC_CM__c AND Fecha_Guardia__c =: cliente.F_CM__c];*/

			//Se busca el nombre del desarrollo comercial elegido
			Desarrollo_Comercial__c desarrolloComercialSeleccionado = [SELECT Id, Name FROM Desarrollo_Comercial__c WHERE Id =: cliente.DC_CM__c];
			nombreDesarrolloComercialSeleccionado = desarrolloComercialSeleccionado.Name.toLowerCase();

			//Si el desarrollo comercial elegido no es el mismo al que pertenece el propietario de la cuenta se buscaran las guardias del
			//desarrollo comercial elegido
			List<Guardia__c> guardias = new List<Guardia__c>();
			if(nombreDesarrolloComercialSeleccionado != cliente.Owner.Desarrollo__c.toLowerCase()){
				System.debug('*****guardias 1');
				guardias = [SELECT Nombre_Asesor__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: cliente.DC_CM__c AND Fecha_Guardia__c =: cliente.F_CM__c];
			}
			System.debug('*****guardias: ' + guardias);

			//Se obtienen los asesores asignados en las guardias disponibles para el dia seleccionado
			//Si el desarrollo comercial elegido es el mismo al que pertenece el propietario de la cuenta se agregan a la lista de asesores
			//el propietario del prospecto
			if(nombreDesarrolloComercialSeleccionado == cliente.Owner.Desarrollo__c.toLowerCase()){
				idsAsesores.add(cliente.Owner.Id);
			}
			//Si el desarrollo comercial elegido no es el mismo al que pertenece el propietario de la cuenta se agregan a la lista de asesores
			//los usuarios de las guardias obtenidas
			else{
				for(Guardia__c g : guardias){
					idsAsesores.add(g.Nombre_Asesor__c);
				}
			}
			System.debug('*****idsAsesores: ' + idsAsesores);

			//Se obtienen todas las tareas (citas) de los asesores obtenidos previamente
			List<Task> tareasAsesores = [SELECT Id, OwnerId, Fecha_Cita__c, ActivityDate FROM Task WHERE OwnerId IN : idsAsesores AND DAY_ONLY(Fecha_Cita__c) =: cliente.F_CM__c];
			System.debug('*****tareasAsesores: ' + tareasAsesores);

			//En base a las tareas obtenidas se crea un mapa con el asesor y la hora de la tarea
			for(Task ta : tareasAsesores){
				mapaAsesorHorasTareas.put(ta.OwnerId + '-' + ta.Fecha_Cita__c.hour(),ta);
			}
			System.debug('*****mapaAsesorHorasTareas: ' + mapaAsesorHorasTareas);

			//Se obtienen todos los eventos de los asesores obtenidos previamente
			List<Event> eventosAsesores = [SELECT Id, OwnerId, StartDateTime, EndDateTime, ActivityDate, DurationInMinutes FROM Event WHERE OwnerId IN : idsAsesores AND ActivityDate =: cliente.F_CM__c];
			System.debug('*****eventosAsesores: ' + eventosAsesores);

			//En base a los eventos obtenidos se crea un mapa con el asesor y la hora del evento
			Integer horasDuracionEvento;
			Integer j;
			Integer sumaHora;
			for(Event ea : eventosAsesores){

				//Se obtienen cuantas horas dura el evento
				horasDuracionEvento = Integer.valueOf(Math.ceil(Decimal.valueOf(ea.DurationInMinutes) / 60.0));

				//En base al numero de horas que dura el evento se crean elementos en el mapa comenzando con la hora de inicio del evento
				j = 1;
				sumaHora = ea.StartDateTime.hour();
				while(horasDuracionEvento >= j){
					mapaAsesorHorasEventos.put(ea.OwnerId + '-' + sumaHora,ea);
					j++;
					sumaHora++;
				}
			}
			System.debug('*****mapaAsesorHorasEventos: ' + mapaAsesorHorasEventos);

			Desarrollo_Comercial__c desarrolloComercialElegido = [SELECT Id, Hora_Inicio__c, Hora_Fin__c FROM Desarrollo_Comercial__c WHERE Id =: cliente.DC_CM__c];
			System.debug('*****desarrolloComercialElegido.Hora_Inicio__c: ' + desarrolloComercialElegido.Hora_Inicio__c);
			System.debug('*****desarrolloComercialElegido.Hora_Inicio__c.hour(): ' + desarrolloComercialElegido.Hora_Inicio__c.hour());
			System.debug('*****desarrolloComercialElegido.Hora_Fin__c.hour(): ' + desarrolloComercialElegido.Hora_Fin__c.hour());
			Integer horaInicio = desarrolloComercialElegido.Hora_Inicio__c.hour();
			Integer horaFin = desarrolloComercialElegido.Hora_Fin__c.hour() - 1;
			Boolean horarioDisponible = false;
			String llaveAsesorHora = '';
			/*List<Integer> horariosDisponiblesInt = new List<Integer>();*/
			//Se hace un ciclo desde la hora de inicio hasta la hora de fin de actividades del desarrollo comercial
			for(Integer i = horaInicio; i <= horaFin; i++){

				System.debug('*****i:' + i);
				//Se hace un ciclo por cada uno de los asesores
				horarioDisponible = false;
				for(Id ida : idsAsesores){

					//Si ni el mapa de tareas ni el mapa de eventos contienen el asesor y la hora es una hora disponible a elegir
					llaveAsesorHora = ida + '-' + i;
					System.debug('*****llaveAsesorHora: ' + llaveAsesorHora);
					if(mapaAsesorHorasTareas.containsKey(llaveAsesorHora) == false && mapaAsesorHorasEventos.containsKey(llaveAsesorHora) == false){
						horarioDisponible = true;
					}

					System.debug('*****horarioDisponible: ' + horarioDisponible);
				}

				if(horarioDisponible == true){
					/*horariosDisponiblesInt.add(i);*/
					horariosDisponibles.add(new SelectOption(String.valueOf(i),(String.valueOf(i).length() == 1 ? '0'+i+':00' : i+':00')));
				}
			}
			/*System.debug('*****horariosDisponiblesInt:' + horariosDisponiblesInt);*/
		}
	}

	//Metodo para guardar una cita en fecha y horario especificado
	public PageReference guardar(){

		//Validaciones
		Boolean errorFecha = false;
		Boolean errorHorario = false;
		if(String.isBlank(String.valueOf(cliente.F_CM__c)) == true){
			errorFecha = true;
		}
		if(String.isBlank(horarioSeleccionado) == true){
			errorHorario = true;
		}

		//Si hubo error(es) en las validaciones se muestra mensaje de error
		if(errorFecha == true || errorHorario == true){

			if(errorFecha == true){
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar una fecha.');
	    		ApexPages.addMessage(mensaje);
			}
			if(errorHorario == true){
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.WARNING,'Debe seleccionar un horario.');
	    		ApexPages.addMessage(mensaje);
			}

			return null;
		}
		//Si no hubo error(es) en las validaciones se procede a guardar la cita
		else{

			String[] arregloHorarioElegido = horarioSeleccionado.split('\\:');
			Integer horaElegida = Integer.valueOf(arregloHorarioElegido[0]);
			System.debug('*****horaElegida:' + horaElegida);

			//Se recorren los asesores para verificar cuales estan disponibles en el horario seleccionado
			String llaveAsesorHora = '';
			Set<Id> idsAsesoresDisponibles = new Set<Id>();
			System.debug('*****idsAsesores Guardar:' + idsAsesores);
			for(Id ida : idsAsesores){

				//Si ni el mapa de tareas ni el mapa de eventos contienen el asesor y la hora es un asesor al que se le puede asignar la cita
				llaveAsesorHora = ida + '-' + horaElegida;
				System.debug('*****llaveAsesorHora: ' + llaveAsesorHora);
				if(mapaAsesorHorasTareas.containsKey(llaveAsesorHora) == false && mapaAsesorHorasEventos.containsKey(llaveAsesorHora) == false){
					idsAsesoresDisponibles.add(ida);
				}
			}
			System.debug('*****idsAsesoresDisponibles: ' + idsAsesoresDisponibles);

			for(User testAsesor : [SELECT Id, Name FROM User WHERE Id IN : idsAsesoresDisponibles]){
				System.debug('*****Asesor Disponible: ' + testAsesor.Name);
			}

			//Se buscan las guardias de los asesores disponibles para la comparacion de roll y contador de citas para determinar a cual asesor se
			//le asigna la cita
			List<Guardia__c> guardiasAsesoresDisponibles = [SELECT Nombre_Asesor__c, Roll__c, Contador_Citas__c FROM Guardia__c WHERE Desarrollo_Comercial__c =: cliente.DC_CM__c AND Fecha_Guardia__c =: cliente.F_CM__c AND Nombre_Asesor__c IN : idsAsesoresDisponibles ORDER BY Roll__c];
			System.debug('*****guardiasAsesoresDisponibles: ' + guardiasAsesoresDisponibles);

			Guardia__c guardiaActual = new Guardia__c();
			Guardia__c guardiaSiguiente = new Guardia__c();
			Integer contadorCitasGuardiaActual = 0;
			Integer contadorCitasGuardiaSiguiente = 0;
			for(Integer i = 0; i < guardiasAsesoresDisponibles.size(); i++){

				if(i==0){
					guardiaActual = guardiasAsesoresDisponibles.get(i);
				}

				if(i < (guardiasAsesoresDisponibles.size() - 1)){

					guardiaSiguiente = guardiasAsesoresDisponibles.get(i + 1);

					System.debug('*****guardiaActual:' + guardiaActual);
					System.debug('*****guardiaSiguiente:' + guardiaSiguiente);

					contadorCitasGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c));
					contadorCitasGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Citas__c));
					System.debug('*****contadorCitasGuardiaActual:' + contadorCitasGuardiaActual);
					System.debug('*****contadorCitasGuardiaSiguiente:' + contadorCitasGuardiaSiguiente);

					if(contadorCitasGuardiaActual <= contadorCitasGuardiaSiguiente){
						guardiaActual = guardiaActual;
						System.debug('****entro a if');
					}
					else{
						guardiaActual = guardiaSiguiente;
						System.debug('****entro a else');
					}
				}
				else{
					break;
				}
			}

			System.debug('*****guardiaActual: ' + guardiaActual);

			Task cita = new Task();
			cita.Subject = 'Cita';
			//cita.OwnerId = guardiaActual.Nombre_Asesor__c;
			cita.WhatId = cliente.Id;
			cita.Desarrollo_Comercial__c = cliente.DC_CM__c;
			cita.Fecha_Cita__c = DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0);
			cita.ActivityDate = Date.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day());
			cita.Estatus_de_Visita__c = 'Pendiente';
			cita.Cita_para_cuenta__c = true;

			//Si el desarrollo comercial elegido es el mismo al que pertenece el propietario de la cuenta se asigna como propietario de la tarea
			//el propietario de la cuenta
			if(nombreDesarrolloComercialSeleccionado == cliente.Owner.Desarrollo__c.toLowerCase()){
				cita.OwnerId = cliente.OwnerId;
			}
			//Si el desarrollo comercial elegido no es el mismo al que pertenece el propietario de la cuenta se asigna como propietario de la tarea
			//el usuario de la guardia correspondiente
			else{
				cita.OwnerId = guardiaActual.Nombre_Asesor__c;
			}
			System.debug('*****cita:' + cita);

			try{
				insert cita;
			}
			catch(Exception e){
				System.debug('*****Error al crear la cita:' + e.getMessage());
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
	    		ApexPages.addMessage(mensaje);
			}
			System.debug('*****Cita guardada (Tarea):' + cita);

			Event evento = new Event();
			//evento.OwnerId = cliente.OwnerId;
			evento.Desarrollo_Comercial__c = cliente.DC_CM__c;
			evento.WhatId = cliente.Id;
			evento.StartDateTime =  DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0);
			evento.EndDateTime =  DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida +  1,0,0);
			evento.ActivityDateTime = DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0);
			evento.IsReminderSet = true;
			evento.ReminderDateTime = DateTime.newInstance(cliente.F_CM__c.year(),cliente.F_CM__c.month(),cliente.F_CM__c.day(),horaElegida,0,0).addDays(-1);
			evento.Subject = 'Cita';

			//Si el desarrollo comercial elegido es el mismo al que pertenece el propietario de la cuenta se asigna como propietario del evento
			//el propietario de la cuenta
			if(nombreDesarrolloComercialSeleccionado == cliente.Owner.Desarrollo__c.toLowerCase()){
				evento.OwnerId = cliente.OwnerId;
			}
			//Si el desarrollo comercial elegido no es el mismo al que pertenece el propietario de la cuenta se asigna como propietario del evento
			//el usuario de la guardia correspondiente
			else{
				evento.OwnerId = guardiaActual.Nombre_Asesor__c;
			}
			System.debug('*****evento:' + evento);

			try{
				insert evento;
			}
			catch(Exception e){
				System.debug('*****Error al crear el evento:' + e.getMessage());
				ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
				ApexPages.addMessage(mensaje);
			}
			System.debug('*****Cita guardada (Evento):' + evento);

			//Si el desarrollo comercial elegido no es el mismo al que pertenece el propietario de la cuenta se actualiza el contador de citas
			//de la bitacora
			if(nombreDesarrolloComercialSeleccionado != cliente.Owner.Desarrollo__c.toLowerCase()){
				guardiaActual.Contador_Citas__c = (String.isBlank(String.valueOf(guardiaActual.Contador_Citas__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Citas__c)) + 1;
				try{
					update guardiaActual;
				}
				catch(Exception e){
					System.debug('*****Error al actualizar la guardia:' + e.getMessage());
					ApexPages.Message mensaje = new ApexPages.Message(ApexPages.Severity.FATAL,'Error: ' + e.getMessage());
		    		ApexPages.addMessage(mensaje);
				}
			}

			PageReference pageRef = new PageReference('/' + cliente.Id);
			return pageRef;
		}
	}

	//Metodo para guardar una cita en fecha y horario especificado
	public PageReference cancelar(){

		PageReference pageRef = new PageReference('/' + cliente.Id);
		return pageRef;
	}
}