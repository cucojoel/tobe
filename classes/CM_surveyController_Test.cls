@isTest(SeeAllData=true)
private class CM_surveyController_Test {
  static testMethod void unitTest() {
      /*     *     */
    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    User gerenteUser = new User();
    gerenteUser.Alias = 'gereUser';
    gerenteUser.Desarrollo__c = 'Coapa';
    gerenteUser.Email = 'gerenteUser@begrand.com';
    gerenteUser.EmailEncodingKey = 'UTF-8';
    gerenteUser.Id_Enkontrol__c ='00001';
    gerenteUser.LanguageLocaleKey = 'en_US';
    gerenteUser.LastName = 'gerenteUser';
    gerenteUser.LocaleSidKey = 'en_US';
    gerenteUser.ProfileId = profileGerente.Id;
    gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
    gerenteUser.Username = 'gerenteUser@begrand.com';
    insert gerenteUser;
      /*     *     */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaName';
    insert compania;
      /*     *     */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = gerenteUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = gerenteUser.Id;
    insert desarrolloComercial;
      /*     *     */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;
      /*     *     */
    Lead lead_1 = new Lead();
    lead_1.FirstName = 'lead_1';
    lead_1.LastName = 'lead_1';
    lead_1.Email = 'lead_1@begrand.com';
    lead_1.Phone = '0000000001';
      /*lead_1.Desarrollo_Comercial__c = desarrolloComercial.Id;*/
    lead_1.LeadSource = '1';
    lead_1.Agente__c=gerenteUser.Id;
    lead_1.Desarrollo__c=desarrollo.Id;
    lead_1.Web_to_lead__c=false;
    lead_1.Medio__c='Google';
    insert lead_1;
      /*     *     */
    Opportunity oportunidad = new Opportunity();
    oportunidad.Name = 'Oportunidad Prueba';
    oportunidad.StageName = 'Nueva';
      //oportunidad.Medio__c = 'Internet';
    oportunidad.CloseDate = Date.today().addDays(1);
    insert oportunidad;
      /*     *     */
    Survey getSurvey = [SELECT ActiveVersionId,Description,DeveloperName,Id,Name FROM Survey limit 1];
    SurveyInvitation surveyInvitation = [SELECT Id,InviteExpiryDateTime,SurveyId FROM SurveyInvitation WHERE SurveyId =: getSurvey.Id AND InviteExpiryDateTime = null limit 1];
    Realizadas_cm_survey__c realizadas_cm_survey= new Realizadas_cm_survey__c();
    List<RespuestasCmSurvey__c> listrcms = new List<RespuestasCmSurvey__c>();
    RespuestasCmSurvey__c rcms = new RespuestasCmSurvey__c();
    listrcms.add(rcms);
    Test.startTest();

    List<CM_surveyController.PregRes> getPregRes = new List<CM_surveyController.PregRes>();
    CM_surveyController.PregRes pregRes = new CM_surveyController.PregRes();
    getPregRes.add(pregRes);

    List<CM_surveyController.PagPregRes> getPagPregRes = new List<CM_surveyController.PagPregRes>();
    CM_surveyController.PagPregRes pagPregRes = new CM_surveyController.PagPregRes();
    getPagPregRes.add(pagPregRes);
      /*     *     */
    ApexPages.currentPage().getParameters().put('id', oportunidad.Id);
    ApexPages.currentPage().getParameters().put('name', 'O');
    ApexPages.StandardController sco = new ApexPages.StandardController( oportunidad );
    CM_surveyController ctrl = new CM_surveyController();
    CM_surveyController.saveRes(realizadas_cm_survey,listrcms);
    Test.stopTest();
  }
  static testMethod void unitTest2() {
      /*     *     */
    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    User gerenteUser = new User();
    gerenteUser.Alias = 'gereUser';
    gerenteUser.Desarrollo__c = 'Coapa';
    gerenteUser.Email = 'gerenteUser@begrand.com';
    gerenteUser.EmailEncodingKey = 'UTF-8';
    gerenteUser.Id_Enkontrol__c ='00001';
    gerenteUser.LanguageLocaleKey = 'en_US';
    gerenteUser.LastName = 'gerenteUser';
    gerenteUser.LocaleSidKey = 'en_US';
    gerenteUser.ProfileId = profileGerente.Id;
    gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
    gerenteUser.Username = 'gerenteUser@begrand.com';
    insert gerenteUser;
      /*     *     */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaName';
    insert compania;
      /*     *     */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = gerenteUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = gerenteUser.Id;
    insert desarrolloComercial;
      /*     *     */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;
      /*     *     */
    Lead lead_1 = new Lead();
    lead_1.FirstName = 'lead_1';
    lead_1.LastName = 'lead_1';
    lead_1.Email = 'lead_1@begrand.com';
    lead_1.Phone = '0000000001';
      /*lead_1.Desarrollo_Comercial__c = desarrolloComercial.Id;*/
    lead_1.LeadSource = '1';
    lead_1.Agente__c=gerenteUser.Id;
    lead_1.Desarrollo__c=desarrollo.Id;
    lead_1.Web_to_lead__c=false;
    lead_1.Medio__c='Google';
    insert lead_1;
      /*     *     */
    Opportunity oportunidad = new Opportunity();
    oportunidad.Name = 'Oportunidad Prueba';
    oportunidad.StageName = 'Nueva';
      //oportunidad.Medio__c = 'Internet';
    oportunidad.CloseDate = Date.today().addDays(1);
    insert oportunidad;
      /*     *     */
    Survey getSurvey = [SELECT ActiveVersionId,Description,DeveloperName,Id,Name FROM Survey limit 1];
    SurveyInvitation surveyInvitation = [SELECT Id,InviteExpiryDateTime,SurveyId FROM SurveyInvitation WHERE SurveyId =: getSurvey.Id AND InviteExpiryDateTime = null limit 1];
    Test.startTest();
    List<CM_surveyController.PagPregRes> getPagPregRes = new List<CM_surveyController.PagPregRes>();
    CM_surveyController.PagPregRes pagPregRes = new CM_surveyController.PagPregRes();
    getPagPregRes.add(pagPregRes);
    ApexPages.currentPage().getParameters().put('id', lead_1.Id);
    ApexPages.currentPage().getParameters().put('name', 'P');
    ApexPages.StandardController sc = new ApexPages.StandardController( lead_1 );
    Test.stopTest();
  }
}