trigger TRIGGER_Tarea on Task (before update, after update) {

	if(Trigger.isBefore){
		CLASS_Tarea classTarea = new CLASS_Tarea();
		classTarea.cerrarTareaCita(Trigger.oldMap, Trigger.newMap);
	}
	else if(Trigger.isAfter){
		CLASS_Tarea classTarea = new CLASS_Tarea();
		classTarea.cambioEstatusCitaConfirmada(Trigger.oldMap, Trigger.newMap);
		classTarea.cambioEstatusCitaNoConfirmada(Trigger.oldMap, Trigger.newMap);
		classTarea.cambioEstatusCitaNoAsistio(Trigger.oldMap, Trigger.newMap);
		classTarea.cambioEstatusCitaVisito(Trigger.oldMap, Trigger.newMap);
	}
}