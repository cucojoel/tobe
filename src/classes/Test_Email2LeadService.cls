@isTest
public class Test_Email2LeadService {
  @isTest static void handleInboundEmailLaMudiTestA(){
    Profile profileAsesor      = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
    Profile profileAsistente   = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
    Profile profileGerente     = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    Profile profileRecepcion   = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
    Profile profileAdmin   = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'];
    /*
    *
    */
    User adminUser = new User();
    adminUser.Alias = 'admiUser';
    adminUser.Desarrollo__c = 'Coapa';
    adminUser.Email = 'adminUser@begrand.com';
    adminUser.EmailEncodingKey = 'UTF-8';
    adminUser.Id_Enkontrol__c ='1';
    adminUser.LanguageLocaleKey = 'en_US';
    adminUser.LastName = 'adminUser';
    adminUser.LocaleSidKey = 'en_US';
    adminUser.ProfileId = profileAdmin.Id;
    adminUser.TimeZoneSidKey = 'America/Los_Angeles';
    adminUser.Username = 'adminUser@begrand.com';
    insert adminUser;
    /*
    *
    */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaItem';
    insert compania;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = adminUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = adminUser.Id ;
    insert desarrolloComercial;
    /*
    *
    */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;

    List<Desarrollo_Comercial__c> listDesCom = new List<Desarrollo_Comercial__c>();
    listDesCom.add(desarrolloComercial);
    String msg = '';
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    Messaging.InboundEmailResult salida = new Messaging.InboundEmailResult();
    Email2LeadService etls = new Email2LeadService();
    msg = 'estimado agustin acabas de recibir un mensaje respecto a la siguiente propiedad anunciada en lamudi. a continuación los detalles detalles de la propiedad be grand coapa méxico huixquilucan listing id 1593 detalles de contacto nombre begrand corp correo electrónico begrand@begrand.mx teléfono 5512345678 detalles del mensaje qué tal encontré tus propiedades en lamudi. ¿podrías enviarme más información por favor? gracias el equipo lamudi ¿necesitasayuda? ¡contactanos! encuentra tu propiedad buscar ahora socializa copyright © 2018 medios de clasificados.el material no puede ser publicado o reproducido de ninguna forma sin el permiso previo y por escrito. lamudi.com.mx this email was sent by lamudi internet holding sarlzossener str. 56 berlin berlin 10961 de';
    email.plainTextBody = msg;
    Test.startTest();
    salida = etls.handleInboundEmail(email, env);
    System.AssertEquals(salida, null);
    Test.stopTest();
  }
  @isTest static void handleInboundEmailICasasTestA(){
    Profile profileAsesor      = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
    Profile profileAsistente   = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
    Profile profileGerente     = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    Profile profileRecepcion   = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
    Profile profileAdmin   = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'];
    /*
    *
    */
    User adminUser = new User();
    adminUser.Alias = 'admiUser';
    adminUser.Desarrollo__c = 'Coapa';
    adminUser.Email = 'adminUser@begrand.com';
    adminUser.EmailEncodingKey = 'UTF-8';
    adminUser.Id_Enkontrol__c ='1';
    adminUser.LanguageLocaleKey = 'en_US';
    adminUser.LastName = 'adminUser';
    adminUser.LocaleSidKey = 'en_US';
    adminUser.ProfileId = profileAdmin.Id;
    adminUser.TimeZoneSidKey = 'America/Los_Angeles';
    adminUser.Username = 'adminUser@begrand.com';
    insert adminUser;
    /*
    *
    */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaItem';
    insert compania;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = adminUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = adminUser.Id ;
    insert desarrolloComercial;
    /*
    *
    */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;



    List<Desarrollo_Comercial__c> listDesCom = new List<Desarrollo_Comercial__c>();
    listDesCom.add(desarrolloComercial);
    String msg = '';
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    Messaging.InboundEmailResult salida = new Messaging.InboundEmailResult();
    Email2LeadService etls = new Email2LeadService();
    msg = 'solicitud de información 1 de febrero de 2019 solicitud de información ¡hola! hemos recibido una solicitud de información sobre be grand coapa hacienda de las palmas huixquilucan de degollado méxico (número de referencia 1603340). aquí tienen los datos del interesado para que puedan brindarle información de su oferta nombre y apellidos begrand corp teléfono contacto 5523456789 email begrand@begrand.mx observaciones hola estoy interesado en esta propiedad con referencia 1603340 que he visto en el portal www.icasas.mx. quisiera que me contacten para recibir más información.horario preferido de contacto por la tarderespondan este correo electrónico para contestar la solicitud de antonio hauad y así satisfacer su petición. adelante. seguro que pueden alcanzar un acuerdo.gracias por confiar en icasas. icasas.mx 2019';
    email.plainTextBody = msg;
    salida = etls.handleInboundEmail(email, env);
    Test.startTest();
    System.AssertEquals(salida, null);
    Test.stopTest();
  }
  @isTest static void handleInboundEmailMercadoLibreTestA(){
    Profile profileAsesor      = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
    Profile profileAsistente   = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
    Profile profileGerente     = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    Profile profileRecepcion   = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
    Profile profileAdmin   = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'];
    /*
    *
    */
    User adminUser = new User();
    adminUser.Alias = 'admiUser';
    adminUser.Desarrollo__c = 'Coapa';
    adminUser.Email = 'adminUser@begrand.com';
    adminUser.EmailEncodingKey = 'UTF-8';
    adminUser.Id_Enkontrol__c ='1';
    adminUser.LanguageLocaleKey = 'en_US';
    adminUser.LastName = 'adminUser';
    adminUser.LocaleSidKey = 'en_US';
    adminUser.ProfileId = profileAdmin.Id;
    adminUser.TimeZoneSidKey = 'America/Los_Angeles';
    adminUser.Username = 'adminUser@begrand.com';
    insert adminUser;
    /*
    *
    */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaItem';
    insert compania;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = adminUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = adminUser.Id ;
    insert desarrolloComercial;
    /*
    *
    */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;



    List<Desarrollo_Comercial__c> listDesCom = new List<Desarrollo_Comercial__c>();
    listDesCom.add(desarrolloComercial);
    String msg = '';
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    Messaging.InboundEmailResult salida = new Messaging.InboundEmailResult();
    Email2LeadService etls = new Email2LeadService();
    msg = ' begrand corp cotizó la unidad departamento tipo 02 del desarrollo be grand coapa. precio $ 3 730 155 m² construidos 58.9 recámaras 1 baños 1 m² totales piso estacionamientos 1 sus datos de contacto son begrand@begrand.mx y 5534567890. ir a la cotización ¡descarga gratis la aplicación!';
    email.plainTextBody = msg;
    Test.startTest();
    salida = etls.handleInboundEmail(email, env);
    System.AssertEquals(salida, null);
    Test.stopTest();
  }
  @isTest static void handleInboundEmailInmuebles24TestA(){
    Profile profileAsesor      = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
    Profile profileAsistente   = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
    Profile profileGerente     = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    Profile profileRecepcion   = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
    Profile profileAdmin   = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'];
    /*
    *
    */
    User adminUser = new User();
    adminUser.Alias = 'admiUser';
    adminUser.Desarrollo__c = 'Coapa';
    adminUser.Email = 'adminUser@begrand.com';
    adminUser.EmailEncodingKey = 'UTF-8';
    adminUser.Id_Enkontrol__c ='1';
    adminUser.LanguageLocaleKey = 'en_US';
    adminUser.LastName = 'adminUser';
    adminUser.LocaleSidKey = 'en_US';
    adminUser.ProfileId = profileAdmin.Id;
    adminUser.TimeZoneSidKey = 'America/Los_Angeles';
    adminUser.Username = 'adminUser@begrand.com';
    insert adminUser;
    /*
    *
    */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaItem';
    insert compania;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = adminUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = adminUser.Id ;
    insert desarrolloComercial;
    /*
    *
    */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;


    List<Desarrollo_Comercial__c> listDesCom = new List<Desarrollo_Comercial__c>();
    listDesCom.add(desarrolloComercial);
    String msg = '';
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    Messaging.InboundEmailResult salida = new Messaging.InboundEmailResult();
    Email2LeadService etls = new Email2LeadService();
    msg = 'be grand recibiste una consulta de roberto ramirez en tu aviso be grand coapa preventa de departamentos fase iv (cód 228m9l) calzada del hueso 859 col. ex hacienda coapa c.p. 14300 del. tlalpan méxico d.f. mn 3 730 053 cod. del anunciante 228m9l cód. de aviso 50456091 datos del interesado begrand corp tel. 5545678901 e-mail begrand@begrand.mx el mensaje que envió fue hola me interesa este inmueble que vi en inmuebles24 y quiero que me contacten. gracias. si respondes este e-mail la respuesta le llegará al interesado. roberto ramirez el equipo de inmuebles 24 - este es un mensaje de correo electrónico automatizado no responda directamente a ésta casilla. usted ha recibido éste mensaje dado a que al registrarse en el sitio web www.inmuebles24.com nos autorizó a enviarle este tipo de información. en inmuebles 24 tomamos seriamente la privacidad de nuestros usuarios es por eso que esta comunicación cumplimenta la legislación vigente en méxico. la ley federal de protección de datos personales en posesión de los particulares establece que "cualquier titular o en su caso su representante legal podrá ejercer los derechos de acceso rectificación cancelación y oposición previstos en la presente ley. el ejercicio de cualquiera de ellos no es requisito previo ni impide el ejercicio de otro. los datos personales deben ser resguardados de tal manera que permitan el ejercicio sin dilación de estos derechos. los titulares tienen derecho a acceder a sus datos personales que obren en poder del responsable así como conocer el aviso de privacidad al que está sujeto el tratamiento. el titular de los datos tendrá derecho a rectificarlos cuando sean inexactos o incompletos. el titular tendrá en todo momento el derecho a cancelar sus datos personales." (arts. 22 23 24 y 25). para conocer más sobre nuestras políticas de privacidad y protección de la información consulte los términos y condiciones del sitio o a legales@navent.com si no quiere recibir comunicaciones similares en el futuro puede des-suscribirse haciendo click aquí.';
    email.plainTextBody = msg;
    Test.startTest();
    salida = etls.handleInboundEmail(email, env);
    System.AssertEquals(salida, null);
    Test.stopTest();
  }
}