@isTest
private class TEST_MapaContactacion {

	private static Desarrollo_Comercial__c desarrolloComercial;
	private static Desarrollo_Comercial__c desarrolloComercialBDC;
	private static Desarrollo__c desarrollo;
	private static User usuario;
	private static Guardia__c guardia1DesarrolloComercialBDC;
	private static Guardia__c guardia2DesarrolloComercialBDC;
	private static Guardia__c guardia3DesarrolloComercialBDC;
	private static Guardia__c guardia4DesarrolloComercialBDC;
	private static Lead prospecto;
	private static Bitacora__c bitacoraLlamada;
	private static Bitacora__c bitacoraCorreo;
	private static Bitacora__c bitacoraWhatsApp;

	private static void init(){

		desarrolloComercial = new Desarrollo_Comercial__c();
		desarrolloComercial.Name = 'test';
		desarrolloComercial.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercial.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrolloComercialBDC = new Desarrollo_Comercial__c();
		desarrolloComercialBDC.Name = 'BDC';
		desarrolloComercialBDC.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercialBDC.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrollo = new Desarrollo__c();
		desarrollo.Name = 'test';
		desarrollo.Inicio__c = Time.newInstance(7,0,0,0);
		desarrollo.Fin__c = Time.newInstance(22,0,0,0);

		usuario = new User();
		usuario.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id;
		usuario.Alias = 'usreje';
		usuario.Country = 'México';
		usuario.Email = 'ejecutivo@email.com';
		usuario.EmailEncodingKey = 'UTF-8';
		usuario.LastName = 'ejecutivo';
		usuario.LanguageLocaleKey = 'es_MX';
		usuario.LocaleSidKey = 'es_MX';
		usuario.TimeZoneSidKey = 'America/Mexico_City';
		usuario.UserName='ejecutivo@email.com';
		usuario.Country = 'Mexico';

		guardia1DesarrolloComercialBDC = new Guardia__c();
		guardia1DesarrolloComercialBDC.Fecha_Guardia__c = Date.today();
		guardia1DesarrolloComercialBDC.Roll__c = 1;

		guardia2DesarrolloComercialBDC = new Guardia__c();
		guardia2DesarrolloComercialBDC.Fecha_Guardia__c = Date.today();
		guardia2DesarrolloComercialBDC.Roll__c = 2;

		guardia3DesarrolloComercialBDC = new Guardia__c();
		guardia3DesarrolloComercialBDC.Fecha_Guardia__c = Date.today().addDays(1);
		guardia3DesarrolloComercialBDC.Roll__c = 1;

		guardia4DesarrolloComercialBDC = new Guardia__c();
		guardia4DesarrolloComercialBDC.Fecha_Guardia__c = Date.today().addDays(1);
		guardia4DesarrolloComercialBDC.Roll__c = 2;

		prospecto = new Lead();
		prospecto.FirstName = 'prospecto';
		prospecto.LastName = 'prospecto';
		prospecto.Email = 'prospecto@test.com';
		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Cantidad_No_Shows__c = 0;
		prospecto.LeadSource = '6';
		prospecto.RFC__c = 'CASJ821112UT6';
		prospecto.Phone = '2222334455';

		bitacoraLlamada = new Bitacora__c();
		bitacoraLlamada.Forma_de_Contacto__c = 'Llamada';

		bitacoraCorreo = new Bitacora__c();
		bitacoraCorreo.Forma_de_Contacto__c = 'Correo';

		bitacoraWhatsApp = new Bitacora__c();
		bitacoraWhatsApp.Forma_de_Contacto__c = 'WhatsApp';
	}

	static testMethod void test1(){

		init();

		insert desarrolloComercial;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Status = 'Asignado';
		insert prospecto;
		DateTime hace9Dias = Datetime.now().addDays(-9);
		Test.setCreatedDate(prospecto.Id, hace9Dias);

		bitacoraLlamada.Lead__c = prospecto.Id;
		insert bitacoraLlamada;

		bitacoraCorreo.Lead__c = prospecto.Id;
		insert bitacoraCorreo;
		DateTime hace7Dias = Datetime.now().addDays(-7);
		Test.setCreatedDate(bitacoraCorreo.Id, hace7Dias);

		bitacoraWhatsApp.Lead__c = prospecto.Id;
		insert bitacoraWhatsApp;
		Test.setCreatedDate(bitacoraWhatsApp.Id, hace9Dias);

		ApexPages.StandardController sc = new ApexPages.StandardController(prospecto);
        CONTROL_MapaContactacion controller = new CONTROL_MapaContactacion(sc);
	}

	static testMethod void test2(){

		init();

		insert desarrolloComercial;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
		insert desarrollo;

		insert usuario;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;


		insert prospecto;

		prospecto.Status = 'Contactado';
		update prospecto;

		bitacoraLlamada.Lead__c = prospecto.Id;
		insert bitacoraLlamada;

		ApexPages.StandardController sc = new ApexPages.StandardController(prospecto);
        CONTROL_MapaContactacion controller = new CONTROL_MapaContactacion(sc);
	}
}