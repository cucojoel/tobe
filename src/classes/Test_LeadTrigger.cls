@isTest
public with sharing class Test_LeadTrigger {
  public static testMethod void testTriggerCapturaCompletaSuccess() {
    List<Lead> leadList = new List<Lead>();
    Map<String, String> mockResponses = new Map<String, String>();
    /*
    *
    */
    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    User gerenteUser = new User();
    gerenteUser.Alias = 'gereUser';
    gerenteUser.Desarrollo__c = 'Coapa';
    gerenteUser.Email = 'gereUser@begrand.com';
    gerenteUser.EmailEncodingKey = 'UTF-8';
    gerenteUser.Id_Enkontrol__c ='00001';
    gerenteUser.LanguageLocaleKey = 'en_US';
    gerenteUser.LastName = 'gereUser';
    gerenteUser.LocaleSidKey = 'en_US';
    gerenteUser.ProfileId = profileGerente.Id;
    gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
    gerenteUser.Username = 'gereUser@begrand.com';
    insert gerenteUser;
    /*
    *
    */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '1';
    compania.Name = 'companiaName';
    insert compania;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = gerenteUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = gerenteUser.Id ;
    insert desarrolloComercial;
    
    Desarrollo_Comercial__c desarrolloComercial1 = new Desarrollo_Comercial__c();
    desarrolloComercial1.Name = 'Contadero';
    desarrolloComercial1.Compania__c = compania.Id;
    desarrolloComercial1.Gerente_Ventas__c = gerenteUser.Id;
    desarrolloComercial1.Gerente_Administrativo__c = gerenteUser.Id ;
    insert desarrolloComercial1;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercialBDC = new Desarrollo_Comercial__c();
    desarrolloComercialBDC.Name = 'BDC';
    desarrolloComercialBDC.Compania__c = compania.Id;
    desarrolloComercialBDC.Gerente_Ventas__c = gerenteUser.Id;
    desarrolloComercialBDC.Gerente_Administrativo__c = gerenteUser.Id ;
    insert desarrolloComercialBDC;

    Guardia__c guardia1 = new Guardia__c();
    guardia1.Nombre_Asesor__c = gerenteUser.Id;
	guardia1.Fecha_Guardia__c = Date.today();
	guardia1.Roll__c = 2;
	guardia1.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
	guardia1.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
	guardia1.Desarrollo_Comercial_Secundario__c = desarrolloComercial.Id;
	insert guardia1;
	
    Guardia__c guardia2 = new Guardia__c();
	guardia2.Nombre_Asesor__c = gerenteUser.Id;
	guardia2.Fecha_Guardia__c = Date.today().addDays(1);
	guardia2.Roll__c = 2;
	guardia2.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
	guardia2.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
	guardia2.Desarrollo_Comercial_Secundario__c = desarrolloComercial.Id;
	insert guardia2;
	
	Guardia__c guardia3 = new Guardia__c();
    guardia3.Nombre_Asesor__c = gerenteUser.Id;
	guardia3.Fecha_Guardia__c = Date.today();
	guardia3.Roll__c = 2;
	guardia3.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
	guardia3.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
	guardia3.Desarrollo_Comercial_Secundario__c = desarrolloComercial1.Id;
	insert guardia3;
	
    Guardia__c guardia4 = new Guardia__c();
	guardia4.Nombre_Asesor__c = gerenteUser.Id;
	guardia4.Fecha_Guardia__c = Date.today().addDays(1);
	guardia4.Roll__c = 2;
	guardia4.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
	guardia4.Desarrollo_Comercial_Primario__c = desarrolloComercial1.Id;
	guardia4.Desarrollo_Comercial_Secundario__c = desarrolloComercial1.Id;
	insert guardia4;
    /*
    *
    */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;
    
    Desarrollo__c desarrollo1 = new Desarrollo__c();
    desarrollo1.Name = 'Contadero';
    desarrollo1.Id_Desarrollo_Enkontrol__c = 'CO5';
    desarrollo1.Desarrollo_Comercial__c = desarrolloComercial1.Id;
    desarrollo1.Desarrollo_Activo__c = true;
    desarrollo1.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo1;
    /*
    *
    */
    Guardia__c guardia = new Guardia__c();
    guardia.Nombre_Asesor__c = gerenteUser.Id;
    guardia.Desarrollo_Comercial__c = desarrolloComercial.Id;
    guardia.Fecha_Guardia__c = Date.today().addDays(1);
    guardia.Roll__c = 1;
    insert guardia;
    /*
    *
    */
    Lead lead_1 = new Lead();
    lead_1.LastName = 'lead_1';
    lead_1.Email = 'lead_test00@test.com';
    lead_1.Phone = '1234567890';
    //lead_1.Desarrollo_Comercial__c = desarrolloComercial.Id;
    lead_1.LeadSource = '1';
    lead_1.Agente__c=gerenteUser.Id;
    lead_1.Desarrollo__c=desarrollo.Id;
    lead_1.Web_to_lead__c=false;
    insert lead_1;
    leadList.add(lead_1);
    update lead_1;
    /*
    *
    */
    Helper_RecursiveTrigger.setIsExecuting(true);
    Boolean isExe = Helper_RecursiveTrigger.getIsExecuting();
    Helper_RecursiveTrigger.setPrendeBandera();
    /*
    *
    */
    Lead lead_2 = new Lead();
    lead_2.LastName = 'lead_2';
    lead_2.Email = 'lead_test01@test.com';
    lead_2.Phone = '2345678901';
    //lead_2.Desarrollo_Comercial__c = desarrolloComercial.Id;
    lead_2.LeadSource = '1';
    lead_2.Agente__c=gerenteUser.Id;
    lead_2.Desarrollo__c=null;
    lead_2.Web_to_lead__c=true;
    insert lead_2;
    /*
    *
    */
    Lead lead_3 = new Lead();
    lead_3.LastName = 'lead_3';
    lead_3.Email = 'lead_test02@test.com';
    lead_3.Phone = '3456789012';
    //lead_2.Desarrollo_Comercial__c = desarrolloComercial.Id;
    lead_3.LeadSource = '6';
    lead_3.Agente__c=gerenteUser.Id;
    lead_3.Desarrollo_web__c='Contadero';
    lead_3.Web_to_lead__c=true;
    insert lead_3;
    leadList.add(lead_3);
    /*
    *
    */
    System.runAs(gerenteUser) {
      Test.startTest();
      Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
      mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
      mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_FAIL);
      mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_BASICO_SUCCESS);
      Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
      Test.setMock(HttpCalloutMock.class, multipleMock);
      Utils_WS_EnkontrolRequestBuilder controller = new Utils_WS_EnkontrolRequestBuilder();
      controller.createObjectClassPrimary();
      controller.createObjectClass();
      controller.statusResponse();
      Service_LeadTrigger controller_2 = new Service_LeadTrigger();
      controller_2.createObjectClass();
      Handler_LeadTrigger controller_3 = new Handler_LeadTrigger();
      controller_3.createObjectClass();
      Test.stopTest();
    }
    Helper_RecursiveTrigger.setApagaBandera();
  }
  /*
  *
  */
  public static testMethod void testQueueableServiceBasicoSuccess(){
    List<Lead> leadList = new List<Lead>();
    Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [
    SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
    Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
    Telefono2__c, Telefono_3__c,Email, Email2__c, Street, City, Country, Nacionalidad__c,
    ID_Prospecto_Enkontrol__c,PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c,
    Compania__r.Id_Compania_Enkontrol__c, CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c,
    LeadSource, LastModifiedById, Desarrollo_Comercial__c
    FROM Lead
    LIMIT 1
    ];
    System.debug(leadList);
    /*
    *
    */
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCallout = false;
    ID jobID = System.enqueueJob(caseJob);
    caseJob.createObjectClass();
    Test.stopTest();
  }
  /*
  *
  */
  public static testMethod void testQueueableServiceCompletoCallout(){
    List<Lead> leadList = new List<Lead>();
    Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [
    SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
    Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
    Telefono2__c, Telefono_3__c,Email, Email2__c, Street, City, Country, Nacionalidad__c, ID_Prospecto_Enkontrol__c,
    PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
    CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c
    FROM Lead
    LIMIT 1
    ];
    System.debug(leadList);
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    Test.stopTest();
  }
  /*
  *
  */
  public static testMethod void testTriggerCapturaCompletaUpdateSuccess() {
    List<Lead> leadList = new List<Lead>();
    Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [
    SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
    Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
    Telefono2__c, Telefono_3__c,Email, Email2__c, Street, City, Country, Nacionalidad__c, ID_Prospecto_Enkontrol__c,
    PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
    CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c
    FROM Lead
    LIMIT 1
    ];
    System.debug(leadList);
    /*
    *
    */
    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    User gerenteUser = new User();
    gerenteUser.Alias = 'gereUser';
    gerenteUser.Desarrollo__c = 'Coapa';
    gerenteUser.Email = 'gerenteUser@begrand.com';
    gerenteUser.EmailEncodingKey = 'UTF-8';
    gerenteUser.Id_Enkontrol__c ='00001';
    gerenteUser.LanguageLocaleKey = 'en_US';
    gerenteUser.LastName = 'gerenteUser';
    gerenteUser.LocaleSidKey = 'en_US';
    gerenteUser.ProfileId = profileGerente.Id;
    gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
    gerenteUser.Username = 'gerenteUser@begrand.com';
    insert gerenteUser;
    /*
    *
    */
    Compania__c compania = new Compania__c();
    compania.Id_Compania_Enkontrol__c = '00001';
    compania.Name = 'GRAND COAPATEST S.A. DE C.V.';
    insert compania;
    /*
    *
    */
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    desarrolloComercial.Compania__c = compania.Id;
    desarrolloComercial.Gerente_Ventas__c = gerenteUser.Id;
    desarrolloComercial.Gerente_Administrativo__c = gerenteUser.Id ;
    insert desarrolloComercial;
    /*
    *
    */
    Desarrollo__c desarrollo = new Desarrollo__c();
    desarrollo.Name = 'Coapa';
    desarrollo.Id_Desarrollo_Enkontrol__c = 'BO4';
    desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
    desarrollo.Desarrollo_Activo__c = true;
    desarrollo.Inicio__c = Time.newInstance(1, 2, 3, 4);
    insert desarrollo;
    /*
    *
    */
    for(Lead l : leadList){
      l.Sincronizado_en_Enkontrol__c = true;
      l.ID_Prospecto_Enkontrol__c = '90092';
      l.Desarrollo_Comercial__c = desarrolloComercial.Id;
    }
    update leadList;
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    Test.stopTest();
  }
  /*
  *
  */
  public static testMethod void testTriggerMediosDigitalesCompletaSuccess() {
    List<Lead> leadList = new List<Lead>();
    Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);
    leadList = [
    SELECT Id, FirstName, LastName, Apellido_Materno__c, OwnerId,
    Persona_Moral__c, Phone, Colonia__c, Desarrollo__c, Desarrollo__r.Id_Desarrollo_Enkontrol__c,
    Telefono2__c, Telefono_3__c,Email, Email2__c, Street, City, Country, Nacionalidad__c, ID_Prospecto_Enkontrol__c,
    PostalCode, State, Sexo__c, Fecha_Nacimiento__c, RFC__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c,
    CreatedById, Company, Sincronizado_en_Enkontrol__c, Agente__c, LeadSource, LastModifiedById, Desarrollo_Comercial__c
    FROM Lead
    LIMIT 1
    ];
    for(Lead l : leadList){
      l.LeadSource = '10';
    }
    update leadList;
    System.debug(leadList);
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    Test.stopTest();
  }
  /*
  * Administrador del sistema
  */
  public static testMethod void testTriggerCapturaBasicaFail() {
    List<Lead> leadList = new List<Lead>();
    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
    User gerenteUser = new User();
    gerenteUser.Alias = 'gereUser';
    gerenteUser.Desarrollo__c = 'Coapa';
    gerenteUser.Email = 'gerenteUser@begrand.com';
    gerenteUser.EmailEncodingKey = 'UTF-8';
    gerenteUser.Id_Enkontrol__c ='00001';
    gerenteUser.LanguageLocaleKey = 'en_US';
    gerenteUser.LastName = 'gerenteUser';
    gerenteUser.LocaleSidKey = 'en_US';
    gerenteUser.ProfileId = profileGerente.Id;
    gerenteUser.TimeZoneSidKey = 'America/Los_Angeles';
    gerenteUser.Username = 'gerenteUser@begrand.com';
    insert gerenteUser;
    Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c();
    desarrolloComercial.Name = 'Coapa';
    insert desarrolloComercial;
    Desarrollo_Comercial__c desarrolloComercialBDC = new Desarrollo_Comercial__c();
    desarrolloComercialBDC.Name = 'BDC';
    insert desarrolloComercialBDC;
    Guardia__c guardia1 = new Guardia__c();
    guardia1.Nombre_Asesor__c = gerenteUser.Id;
	guardia1.Fecha_Guardia__c = Date.today();
	guardia1.Roll__c = 2;
	guardia1.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
	guardia1.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
	guardia1.Desarrollo_Comercial_Secundario__c = desarrolloComercial.Id;
	insert guardia1;
    Guardia__c guardia2 = new Guardia__c();
	guardia2.Nombre_Asesor__c = gerenteUser.Id;
	guardia2.Fecha_Guardia__c = Date.today().addDays(1);
	guardia2.Roll__c = 2;
	guardia2.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
	guardia2.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
	guardia2.Desarrollo_Comercial_Secundario__c = desarrolloComercial.Id;
	insert guardia2;
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_PROSPECTOS_BASICO_FAIL);
    Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
    Test.setMock(HttpCalloutMock.class, multipleMock);    
    List<Desarrollo_Comercial__c> listDesarrolloComercial = new List<Desarrollo_Comercial__c>();
    listDesarrolloComercial.add(desarrolloComercial);
    Lead lead_1 = new Lead();
    lead_1.LastName = 'lead_1';
    lead_1.Email = 'lead_test04@test.com';
    lead_1.Phone = '4567890123';
    lead_1.Desarrollo_Comercial__c = desarrolloComercial.Id;
    lead_1.LeadSource = '6';
    lead_1.Agente__c=gerenteUser.Id;
    lead_1.Desarrollo__c=null;
    lead_1.Web_to_lead__c=false;
    insert lead_1;
    leadList.add(lead_1);
    Test.startTest();
    QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(leadList, false);
    caseJob.ejecutarCalloutAEnkontrol();
    Test.stopTest();
  }
}