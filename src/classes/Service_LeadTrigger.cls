global class Service_LeadTrigger {
  /*
  *
  */
  public static void sincronizarProspectos(List<Lead> triggerLeads, Boolean esActualizacion) {
    Configuracion_Enkontrol__c configuracionEnkontrol = Configuracion_Enkontrol__c.getOrgDefaults();
    if(!Test.isRunningTest() && configuracionEnkontrol.Activar__c == true){
      QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(triggerLeads, esActualizacion);
      Id jobID = System.enqueueJob(caseJob);
      System.debug(' ### sincronizarProspectos jobID: '+jobID);
    }
  }
  /*
  *
  */
  public static void asignarDesarrollo(List<Lead> listLeadsTrigger) {
    System.debug('* => asignarDesarrollo =>');
    /*
    * vars
    */
    Map<String,Desarrollo_Comercial__c> mapDesarrollosComName = new Map<String, Desarrollo_Comercial__c>();
    Map<String,Desarrollo_Comercial__c> mapDesarrollosComId = new Map<String, Desarrollo_Comercial__c>();
    DateTime alertaHoras;
    Time antesde10 = Time.newInstance(10,0,0,0);
    Time despde5 = Time.newInstance(17,1,0,0);
    if(Datetime.now().time() > antesde10 && Datetime.now().time() < despde5){
      alertaHoras = Datetime.now().addHours(2);
    }
    else {
      Date myDate = System.today().addDays(1);
      Time myTime = Time.newInstance(11, 0, 0, 0);
      alertaHoras= DateTime.newInstance(myDate, myTime);
    }
    /*
    * busca desarrollo comercial a partir de desarrollo web
    */
    Map<Id,Desarrollo__c> mapDesarrollos = new Map<Id, Desarrollo__c>([SELECT Id, Name,Desarrollo_Comercial__c,Desarrollo_Comercial__r.Name,Desarrollo_Comercial__r.Compania__c FROM Desarrollo__c]);
    System.debug('# mapDesarrollos: '+mapDesarrollos);
    List<Desarrollo_Comercial__c> listdesarrolloComercial = [SELECT Id, Name,Compania__c FROM Desarrollo_Comercial__c];
    for(Desarrollo_Comercial__c desarrolloComercial :listdesarrolloComercial){
      String desarrolloComerciallower = desarrolloComercial.Name.toLowerCase().replaceAll(' ','').replaceAll('á','a').replaceAll('é','e').replaceAll('í','i').replaceAll('ó','o').replaceAll('ú','u');
      mapDesarrollosComName.put(desarrolloComerciallower, desarrolloComercial);
      mapDesarrollosComId.put(desarrolloComercial.Id, desarrolloComercial);
    }
    for(Lead leadTrigger : listLeadsTrigger){
      leadTrigger.Alerta_4_horas__c = alertaHoras;
      if(String.isBlank(String.valueOf(leadTrigger.Dia_asignacion__c)) == true ){
        leadTrigger.Dia_asignacion__c=Date.today();
      }
      if(leadTrigger.Desarrollo_Web__c != null){
        System.debug('# busca desarrollo comercial por Desarrollo web # ' + leadTrigger.Desarrollo_Web__c);
        String desWeblower = leadTrigger.Desarrollo_Web__c.toLowerCase().replaceAll(' ','').replaceAll('á','a').replaceAll('é','e').replaceAll('í','i').replaceAll('ó','o').replaceAll('ú','u');
        if (desWeblower == 'altopolancoii' || desWeblower == 'altopolancoiii'){
          desWeblower = 'altopolanco';
        }
        leadTrigger.Desarrollo_Comercial__c = mapDesarrollosComName.get(desWeblower).Id;
        leadTrigger.Compania__c = mapDesarrollosComId.get(leadTrigger.Desarrollo_Comercial__c).Compania__c;
      }
      if(leadTrigger.Desarrollo__c != null){
        System.debug('# busca Desarrollo comercial por desarrollo # ' + leadTrigger.Desarrollo__c);
        Desarrollo__c caux = [SELECT Id, Name FROM Desarrollo__c WHERE Id =: leadTrigger.Desarrollo__c];
        System.debug('# busca Desarrollo comercial por desarrollo Name # ' + caux.Name + ' Id: '+ caux.Id);
        leadTrigger.Desarrollo_Comercial__c = mapDesarrollos.get(leadTrigger.Desarrollo__c).Desarrollo_Comercial__c;
        leadTrigger.Compania__c = mapDesarrollos.get(leadTrigger.Desarrollo__c).Desarrollo_Comercial__r.Compania__c;
      }
      System.debug('# El Lead actual # ' + leadTrigger);
      System.debug('# Desarrollo Comercial # ' + leadTrigger.Desarrollo_Comercial__c);
      System.debug('# Desarrollo Financiero (Etapa) # ' + leadTrigger.Desarrollo__c);
      System.debug('# La Compania # ' + leadTrigger.Compania__c);
    }
    System.debug('<= asignarDesarrollo <= *');
  }
  
	public static void asignarDesarrolloPrimario(List<Lead> listLeadsTrigger) {
		
		for(Lead leadTrigger : listLeadsTrigger){
			
			leadTrigger.Desarrollo_Comercial_Primario__c = leadTrigger.Desarrollo_Comercial__c;
		}
	}
  
  
  /*
  *
  */
	public static void asignarGuardia(List<Lead> listLeadsTrigger) {
	
		System.debug('* => asignarGuardia =>');
    	System.debug(' # listLeadsTrigger: '+listLeadsTrigger);
    
    	Profile profileAsesor    = [SELECT Id FROM Profile WHERE Name = 'Asesor de Ventas'];
	    Profile profileAsistente = [SELECT Id FROM Profile WHERE Name = 'Asistente de Ventas'];
	    Profile profileGerente   = [SELECT Id FROM Profile WHERE Name = 'Gerente de ventas'];
	    Profile profileRecepcion = [SELECT Id FROM Profile WHERE Name = 'Recepción'];
    
    	//Se buscan los desarrollos comerciales definidos por Be Grand a los que se le debe asignar un BDC
    	/*Map<Id,Desarrollo_Comercial__c> desarrollosComercialesBDC = new Map<Id,Desarrollo_Comercial__c>([
    	SELECT 
    		Id, 
    		Name 
    	FROM 
    		Desarrollo_Comercial__c 
    	WHERE 
    		Name IN ('Alto Pedregal', 'Alto Polanco', 'Bosques', 'Coapa', 'Contadero', 'Coyoacán', 'Del Valle', 'Lomas', 'Park Bosques', 'Park San Angel', 'Pedregal', 'Reforma', 'Reforma Oficinas', 'San Angel', 'Universidad')]);*/
    	
    	//Se buscan los desarrollos comerciales definidos por Be Grand a los que se le debe asignar un BDC en la primer fase solo se asignaran
    	//algunos desarrollos comerciales, no todos
    	Map<Id,Desarrollo_Comercial__c> desarrollosComercialesBDC = new Map<Id,Desarrollo_Comercial__c>([
    	SELECT 
    		Id, 
    		Name 
    	FROM 
    		Desarrollo_Comercial__c 
    	WHERE 
    		Name IN ('Alto Pedregal', 'Contadero', 'Park Bosques', 'Reforma')]);
    	
    	Set<Id> setDesarrolloComIds = new Set<Id>();
    	Set<Lead> prospectosAsignarBDC = new Set<Lead>();
    	Set<Id> desarrollosComercialesAsignarBDC = new Set<Id>();
    	for(Lead leadTrigger : listLeadsTrigger){
			
			if(String.isNotBlank(leadTrigger.Desarrollo_Comercial__c) && (leadTrigger.LeadSource == '6' || leadTrigger.LeadSource == '10' || leadTrigger.LeadSource == '12')){
				
				//Si el desarrollo comercial del prospecto corresponde con algun desarrollo comercial definido por Be Grand
				//que se les debe asignar un agente BDC, se agrega a una lista para su posterior procesamiento
				if(desarrollosComercialesBDC.containsKey(leadTrigger.Desarrollo_Comercial__c) == true){
					prospectosAsignarBDC.add(leadTrigger);
					desarrollosComercialesAsignarBDC.add(leadTrigger.Desarrollo_Comercial__c);
				}
				//Si el desarrollo comercial del prospecto no corresponde con algun desarrollo comercial definido por Be Grand
				//que se les debe asignar un agente BDC, se agrega a una lista para su procesamiento anterior a BDC
				else{
					setDesarrolloComIds.add(leadTrigger.Desarrollo_Comercial__c);
				}
				System.debug(' # Desarrollo Comercial # ' + leadTrigger.Desarrollo_Comercial__c);
			}
			else {
				System.debug('es webtolead: '+leadTrigger.Web_to_lead__c);

				if ((UserInfo.getProfileId() == profileRecepcion.Id || UserInfo.getProfileId() == profileAsistente.Id) && leadTrigger.Agente__c != null ){
					leadTrigger.OwnerId = leadTrigger.Agente__c;
					System.debug('BI agente: '+leadTrigger.Agente__c);
					System.debug('BI owner: '+leadTrigger.OwnerId);
				}
				else if(leadTrigger.Agente__c != null){
					leadTrigger.OwnerId = leadTrigger.Agente__c;
					System.debug('BI agente: '+leadTrigger.Agente__c);
					System.debug('BI owner: '+leadTrigger.OwnerId);
        		}
				else {
					leadTrigger.Agente__c = UserInfo.getUserId();
					System.debug('BI agente: '+leadTrigger.Agente__c);
					System.debug('BI owner: '+leadTrigger.OwnerId);
				}
				
				if ((UserInfo.getProfileId() == profileAsesor.Id || UserInfo.getProfileId() == profileAsistente.Id || UserInfo.getProfileId() == profileGerente.Id || UserInfo.getProfileId() == profileRecepcion.Id || UserInfo.getProfileId() == profileGerente.Id) && leadTrigger.Status != 'Asignado') {
					leadTrigger.Status.addError('El estatus de un prospecto nuevo no puede ser diferente de asignado.');
				}
			}
		}
		System.debug('*****prospectosAsignarBDC: ' + prospectosAsignarBDC);
		System.debug('*****desarrollosComercialesAsignarBDC: ' + desarrollosComercialesAsignarBDC);		
		System.debug(' # setDesarrolloComIds: '+setDesarrolloComIds);
    
    	//Si hay prospectos a los que se les debe asignar un agente BDC se invoca al metodo que realizara dicha asignacion
    	//se envia desde la invocacion del metodo la hora actual por cuestiones de la clase de prueba
    	if(prospectosAsignarBDC.size() > 0){
    		
    		Integer horaActual = DateTime.now().hour();
    		asignarAgenteBDC(prospectosAsignarBDC, desarrollosComercialesAsignarBDC, horaActual);
    	}
    	//Si hay prospectos a los que no se les debe asignar un agente BDC se procede como anterior a BDC se hacia
		if(setDesarrolloComIds.size() > 0){
			
			Map<String, Desarrollo_Comercial__c> mapDesarrolloComIds = new Map<String, Desarrollo_Comercial__c>();
			Integer hour = DateTime.now().hour();
			Integer minute = DateTime.now().minute();
			Time timeNow = Time.newInstance(hour, minute, 0, 0);
			Time timeToCompare = Time.newInstance(19, 0, 0, 0);
			Date diaAsignacion = Date.today();
			
			if(timeNow > timeToCompare){
				diaAsignacion = Date.today().addDays(1);
			}
			System.debug(' # diaAsignacion '+diaAsignacion);

			List<Desarrollo_Comercial__c> listDesarrolloComercial = new List<Desarrollo_Comercial__c>([
				SELECT Id, Gerente_Ventas__r.Email, Asig_Automatica__c, Gerente_Ventas__c
				FROM Desarrollo_Comercial__c
				WHERE Id IN :setDesarrolloComIds
			]);
			System.debug(' # listDesarrolloComercial '+listDesarrolloComercial);

			for(Desarrollo_Comercial__c desacom :listDesarrolloComercial){
				mapDesarrolloComIds.put(desacom.Id, desacom);
			}

			List<Guardia__c> listGuardias = new List<Guardia__c>([
				SELECT Id, Name, Nombre_Asesor__c, Roll__c, Fecha_Guardia__c,Contador_temp__c
				FROM Guardia__c
				WHERE Desarrollo_Comercial__c
				IN :listDesarrolloComercial
				AND Fecha_Guardia__c =:diaAsignacion
				ORDER BY Fecha_Guardia__c,Roll__c ASC
			]);			
			System.debug(' # listGuardias '+listGuardias);
      
			for(Lead prospecto : listLeadsTrigger){

				Desarrollo_Comercial__c desa = new Desarrollo_Comercial__c();
				desa = mapDesarrolloComIds.get(prospecto.Desarrollo_Comercial__c);
				prospecto.Email_gerente__c = desa.Gerente_Ventas__r.Email;
				System.debug(' # LeadSource: '+prospecto.LeadSource);
        
				Id usuarioId = null;
				usuarioId = obtenerUsuarioAsignado(listGuardias);
				System.debug(' # obtenerUsuarioAsignado: '+usuarioId);
				
				prospecto.Dia_asignacion__c = diaAsignacion;
				prospecto.Fecha_Asignacion__c = Datetime.now();
				
				if(usuarioId != null){
					
					prospecto.Agente__c = null;
					prospecto.OwnerId = null;
					
					if(desa.Asig_Automatica__c == true){
						System.debug(' # obtenerUsuarioAsignado: '+usuarioId);
						prospecto.Agente__c = usuarioId;
						prospecto.OwnerId = usuarioId;
						System.debug(' # Asignacion automatica: '+prospecto.Agente__c);
						System.debug(' # Asignacion automatica: '+prospecto.OwnerId);
					}
					else {
						usuarioId = desa.Gerente_Ventas__c;
						prospecto.Agente__c = usuarioId;
						prospecto.OwnerId = usuarioId;
						System.debug(' # Sin asignacion automatica: '+prospecto.Agente__c);
						System.debug(' # Sin asignacion automatica: '+prospecto.OwnerId);
					}
				}
        
				System.debug(' # obtenerUsuarioAsignado: '+prospecto.Agente__c);
				System.debug(' # obtenerOwnerAsignado :  '+prospecto.OwnerId);
			}
		}
		
		System.debug('<= asignarGuardia <= *');
	}
	
	//Metodo para asignar un agente BDC a el(los) prospecto(s) que ingresaron al trigger
	public static void asignarAgenteBDC(Set<Lead> prospectos, Set<Id> desarrollosComerciales, Integer horaActual){
		
		Id idDesarrolloComercialBDC = [SELECT Id FROM Desarrollo_Comercial__c WHERE Name = 'BDC'].Id;
		List<Guardia__c> guardiasAsesoresDisponibles = new List<Guardia__c>();
		
		//Si el horario es entre las 4 pm y las 7 pm se obtienen solo las guardias del dia actual y del rol 2
		if(horaActual >= 16 && horaActual < 19){
			
			System.debug('*****Entro a query 1 horaActual >= 16 && horaActual < 19');
			guardiasAsesoresDisponibles = [
			SELECT 
				Id,
				Nombre_Asesor__c, 
				Roll__c, 
				Desarrollo_Comercial_Primario__c, 
				Desarrollo_Comercial_Secundario__c, 
				Contador_Prospectos__c 
			FROM 
				Guardia__c 
			WHERE 
				Desarrollo_Comercial__c =: idDesarrolloComercialBDC 
			AND(
				Desarrollo_Comercial_Primario__c IN : desarrollosComerciales 
			OR 
				Desarrollo_Comercial_Secundario__c IN : desarrollosComerciales) 
			AND 
				Fecha_Guardia__c =: Date.today() 
			AND 
				Roll__c = 2];
		}
		//Si el horario es entre las 0 am y las 4 pm se obtienen solo las guardias del dia actual y de ambos roles
		else if(horaActual >= 0 && horaActual < 16){
			
			System.debug('*****Entro a query 2 horaActual >= 0 && horaActual < 16');
			guardiasAsesoresDisponibles = [
			SELECT 
				Id,
				Nombre_Asesor__c, 
				Roll__c, 
				Desarrollo_Comercial_Primario__c, 
				Desarrollo_Comercial_Secundario__c, 
				Contador_Prospectos__c 
			FROM 
				Guardia__c 
			WHERE 
				Desarrollo_Comercial__c =: idDesarrolloComercialBDC 
			AND(
				Desarrollo_Comercial_Primario__c IN : desarrollosComerciales 
			OR 
				Desarrollo_Comercial_Secundario__c IN : desarrollosComerciales) 
			AND 
				Fecha_Guardia__c =: Date.today() 
			ORDER BY Roll__c DESC];
		}
		//Si el horario es entre las 19 pm y las 23 pm se obtienen solo las guardias del dia siguiente y de ambos roles
		//else if(horaActual >= 19 && horaActual < 23){
		/*
			Modificacion mayo 21, 2019 - MQD. Se amplia el rango ya que el codigo solo funcionaba hasta las 23 hrs, marcaba error entre las 23:00:00 y las 23:59:59
			Nota: el codigo mandara un error si el usuario intenta guardar en el segundo entre las 23:59:59 y las 00:00:00
		*/
		else if(DateTime.now().hour() >= 19 && DateTime.now().hour() <= 23 && DateTime.now().minute() <= 59 && DateTime.now().second() <= 59) {
			
			System.debug('*****Entro a query 3 horaActual >= 19 && horaActual < 23');
			guardiasAsesoresDisponibles = [
			SELECT 
				Id,
				Nombre_Asesor__c, 
				Roll__c, 
				Desarrollo_Comercial_Primario__c, 
				Desarrollo_Comercial_Secundario__c, 
				Contador_Prospectos__c 
			FROM 
				Guardia__c 
			WHERE 
				Desarrollo_Comercial__c =: idDesarrolloComercialBDC 
			AND(
				Desarrollo_Comercial_Primario__c IN : desarrollosComerciales 
			OR 
				Desarrollo_Comercial_Secundario__c IN : desarrollosComerciales) 
			AND 
				Fecha_Guardia__c =: Date.today().addDays(1) 
			ORDER BY Roll__c DESC];
		}
		
		//List<Guardia__c> guardiasElegibles = new List<Guardia__c>();
		List<GuardiaWrapper> guardiasElegibles = new List<GuardiaWrapper>();
		Guardia__c guardiaActual = new Guardia__c();
		Guardia__c guardiaSiguiente = new Guardia__c();
		Integer contadorProspectosGuardiaActual = 0;
		Integer contadorProspectosGuardiaSiguiente = 0;
		for(Lead p : prospectos){
			
			//guardiasElegibles = new List<Guardia__c>();
			guardiasElegibles = new List<GuardiaWrapper>();
			for(Guardia__c g : guardiasAsesoresDisponibles){
				
				if(p.Desarrollo_Comercial__c == g.Desarrollo_Comercial_Primario__c || p.Desarrollo_Comercial__c == g.Desarrollo_Comercial_Secundario__c){
					//guardiasElegibles.add(g);
					guardiasElegibles.add(new GuardiaWrapper(g));
				}
			}
			
			System.debug('*****guardiasElegibles: ' + guardiasElegibles);
			guardiasElegibles.sort();
			System.debug('*****guardiasElegibles sort:' + guardiasElegibles);
			
			guardiaActual = new Guardia__c();
			guardiaSiguiente = new Guardia__c();
			contadorProspectosGuardiaActual = 0;
			contadorProspectosGuardiaSiguiente = 0;
			for(Integer i = 0; i < guardiasElegibles.size(); i++){
				
				if(i==0){
					guardiaActual = guardiasElegibles.get(i).oppy;	
				}							
				
				if(i < (guardiasElegibles.size() - 1)){
					
					guardiaSiguiente = guardiasElegibles.get(i + 1).oppy;
					
					System.debug('*****guardiaActual:' + guardiaActual);
					System.debug('*****guardiaSiguiente:' + guardiaSiguiente);
					
					contadorProspectosGuardiaActual = (String.isBlank(String.valueOf(guardiaActual.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Prospectos__c));
					contadorProspectosGuardiaSiguiente = (String.isBlank(String.valueOf(guardiaSiguiente.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaSiguiente.Contador_Prospectos__c));
					System.debug('*****contadorProspectosGuardiaActual:' + contadorProspectosGuardiaActual);
					System.debug('*****contadorProspectosGuardiaSiguiente:' + contadorProspectosGuardiaSiguiente);
					
					if(contadorProspectosGuardiaActual <= contadorProspectosGuardiaSiguiente){
						guardiaActual = guardiaActual;
						System.debug('****entro a if');
					}
					else{
						guardiaActual = guardiaSiguiente;
						System.debug('****entro a else');
					}
				}
				else{
					break;
				}
			}

			p.OwnerId = guardiaActual.Nombre_Asesor__c;
			p.Agente__c = guardiaActual.Nombre_Asesor__c;
			p.Asignado_BDC__c = true;
			p.Status = 'Asignado';
			
			guardiaActual.Contador_Prospectos__c = (String.isBlank(String.valueOf(guardiaActual.Contador_Prospectos__c)) == true ? 0 : Integer.valueOf(guardiaActual.Contador_Prospectos__c)) + 1;
			update guardiaActual;
		}
			
	}
	
  /*
  *
  */
  public static String obtenerUsuarioAsignado(List<Guardia__c> listGuardias){
    System.debug('* => obtenerUsuarioAsignado =>');
    System.debug('  ## listGuardias: '+listGuardias);
    String usuarioId = null;
    Integer menor = 100;
    Set<Id> guardiasSet = new Set<Id>();
    Map<String, Guardia__c> mapGuardiasRoll = new Map<String, Guardia__c>();
    Integer hour = DateTime.now().hour();
    Integer minute = DateTime.now().minute();
    Time timeNow = Time.newInstance(hour, minute, 0, 0);
    Time timeToCompare = Time.newInstance(19, 0, 0, 0);
    Date diaAsignacion = Date.today();
    if(timeNow > timeToCompare){
      diaAsignacion = Date.today().addDays(1);
    }
    System.debug('  ## diaAsignacion '+diaAsignacion);
    for(Guardia__c guard : listGuardias){
      mapGuardiasRoll.put(guard.Nombre_Asesor__c, guard);
      guardiasSet.add(guard.Nombre_Asesor__c);
    }
    List<Lead> leadList = new List<Lead>([
      SELECT Id, OwnerId, Dia_asignacion__c FROM Lead
      WHERE Dia_asignacion__c = :diaAsignacion
      AND OwnerId IN: mapGuardiasRoll.keySet()
      AND LeadSource in ('6','10','12')
      ORDER by CreatedDate
      ]);
    System.debug('  ## Leads que tienen como owner alguien de la guardia # ' + leadList);
    if(leadList != null){
      Map<Id, Integer> mapeo = new Map<Id, Integer>();
      for(Lead leader : leadList){
        if(mapeo.containsKey(leader.OwnerId)){
          mapeo.put(leader.OwnerId, mapeo.get(leader.OwnerId) + 1);
        }
        else {
          mapeo.put(leader.OwnerId, 1);
        }
      }
      for(String idUsuario : guardiasSet){
        if(!mapeo.containsKey(idUsuario)){
          mapeo.put(idUsuario, 0);
        }
      }
      List<Id> userIds = new List<Id>();
      System.debug('  ## El mapa de owners y leads asignados # ' + mapeo);
      if(mapeo != null){
        userIds.addAll(mapeo.keySet());
        System.debug('  ## userIds: '+userIds);
        for(Integer i=0; i < userIds.size(); i++){
          if(i == 0){
            usuarioId = userIds[i];
            menor = mapeo.get(userIds[i]);
          }
          else {
            if(mapeo.get(userIds[i]) < menor){
              usuarioId = userIds[i];
              menor = mapeo.get(userIds[i]);
            }
          }
        }
      }
    }
    else {
      System.debug('  ## listGuardias'+listGuardias);
      for (Guardia__c guardia : listGuardias) {
        if (guardia.Roll__c == 1){
          usuarioId = guardia.Id;
        }
        else {
          usuarioId = listGuardias.get(0).Id;
        }
      }
    }
    System.debug('  ## El id del usuario asignado es # '+usuarioId);
    System.debug('<= obtenerUsuarioAsignado <= *');
    return usuarioId;
  }
  /*
  *
  *
  public static void asignarCampania(List<Lead> leadsEnTrigger) {
    System.debug('# Entró a asignar la campania # ');
    Map<String, Campaign> campanasPorTipo = new Map<String, Campaign>();
    List<CampaignMember> miembrosParaInsertar = new List<CampaignMember>();
    List<Campaign> todasLasCampanasActivas = [
    SELECT Id, Type, IsActive
    FROM Campaign
    WHERE IsActive = true
    ];
    System.debug('# Campanias activas # ' + todasLasCampanasActivas);
    for(Campaign c : todasLasCampanasActivas){
      if(!campanasPorTipo.containsKey(c.Type)){
        campanasPorTipo.put(c.Type, c);
      }
    }
    System.debug('# Campanias mapa de tipo # ' + campanasPorTipo);
    for (Lead prospecto : leadsEnTrigger) {
      CampaignMember nuevoMiembro = new CampaignMember();
      nuevoMiembro.LeadId = prospecto.Id;
      System.debug('# Detalles del prospecto # ' + prospecto);
      System.debug('# Lead source prospecto # ' + prospecto.LeadSource);
      if(prospecto.LeadSource == '10'){
        Campaign campania = campanasPorTipo.get('Facebook_Lead_Ads');
        if(campania != null){
          nuevoMiembro.CampaignId = campania.Id;
          miembrosParaInsertar.add(nuevoMiembro);
        }
      }
      else if(prospecto.LeadSource == '11'){
        Campaign campania = campanasPorTipo.get('Linkedin');
        if(campania != null){
          nuevoMiembro.CampaignId = campania.Id;
          miembrosParaInsertar.add(nuevoMiembro);
        }
      }
      else if(prospecto.LeadSource == '12'){
        Campaign campania = campanasPorTipo.get('Portal_Inmobiliario');
        if(campania != null){
          System.debug('si existe la campania =>' + miembrosParaInsertar);
          nuevoMiembro.CampaignId = campania.Id;
          miembrosParaInsertar.add(nuevoMiembro);
        }
      }
      else if(prospecto.LeadSource == '13'){
        Campaign campania = campanasPorTipo.get('Página_Web');
        if(campania != null){
          nuevoMiembro.CampaignId = campania.Id;
          miembrosParaInsertar.add(nuevoMiembro);
        }
      }
    }
    System.debug('# Miembros para insertar # ' + miembrosParaInsertar);
    if(!miembrosParaInsertar.isEmpty()){
      insert miembrosParaInsertar;
    }
  }
  /*
  *
  */
  public void createObjectClass(){
    String name;
    String objectId;
    Boolean isActive;
    Integer amount;
    String language;
    String request;
    Boolean requestValid;
    String idRequest;
    Integer requestAttempts;
    String requestResponse;
    String requestBody;
    String requestParameter;
    Id idUserRequest;
    String messageSuccess;
    String messageFailure;
    Integer enkontrolId;
    String stage;
    String operation;
    Id idProspect;
    String prospectAgent;
    String prospectCompany;
    String prospectDes;
    String prospectEmail;
    String prospectName;
    String prospectGender;
    String prospectPhone;
    Integer prospectIdEnkontrol;
    String prospectMoral;
    String prospectLastName;
    String prospectPuntoProspectacion;
    Id prospectIdSalesforce;
    String prospectFirstName;
    String prospectStreet;
    String prospectBirthdate;
    String prospectCity;
    String prospectCountry;
    String prospectColony;
    String prospectEmail2;
    String prospectNacionality;
    String prospectPhone2;
    String prospectPhone3;
    Integer prospectZipCode;
    String prospectRFC;
    String prospectState;
    String origin;
    Integer errorCode;
    String errorMessage;
    Integer successCode;
    String successMessage;
    String opportunity;
    Id opportunityId;
    Date createdDate;
    String description;
    Boolean delivery;
    String source;
    String appartment;
    Id idAppartment;
    String assignatedToName;
    Id assignatedToId;
    String ownerName;
    Id owner;
    Id externalId;
    String reference;
    Date requestDate;
    Id parentId;
    Decimal actualCost;
    String typeRequest;
    String idDropbox;
    String specialTerms;
    String contractTerm;
    Id accountId;
    Date deadLine;
    Integer access;
    String zone;
    Integer maxTime;
    Integer minTime;
    Integer averageTime;
    Integer duration;
    String contractNumber;
    String title;
    Id contractId;
    String development;
    Boolean completed;
    String nextStep;
    Integer probability;
    String subStatus;
    String bill;
    Date activatedDate;
    Integer orderReferenceNumber;
    Id originalOrderId;
    Integer discount;
    Date expirationDate;
    String quoteToName;
    Boolean isSync;
    String alias;
    String divisionName;
    String signature;
    Id managerId;
    Id userProfileId;
    Id managerProfileId;
    String name0;
    name = (String.isBlank(name) == true ? null : name);
    String objectId0;
    objectId = (String.isBlank(objectId) == true ? null : objectId);
    Boolean isActive0;
    isActive = (String.isBlank(String.valueOf(isActive)) == true ? null : isActive);
    Integer amount0;
    amount = (String.isBlank(String.valueOf(amount)) == true ? null : amount);
    String language0;
    language = (String.isBlank(language) == true ? null : language);
    String request0;
    request = (String.isBlank(request) == true ? null : request);
    Boolean requestValid0;
    requestValid = (String.isBlank(String.valueOf(requestValid)) == true ? null : requestValid);
    String idRequest0;
    idRequest = (String.isBlank(idRequest) == true ? null : idRequest);
    Integer requestAttempts0;
    requestAttempts = (String.isBlank(String.valueOf(requestAttempts)) == true ? null : requestAttempts);
    String requestResponse0;
    requestResponse = (String.isBlank(requestResponse) == true ? null : requestResponse);
    String requestBody0;
    requestBody = (String.isBlank(requestBody) == true ? null : requestBody);
    String requestParameter0;
    requestParameter = (String.isBlank(requestParameter) == true ? null : requestParameter);
    Id idUserRequest0;
    idUserRequest = (String.isBlank(String.valueOf(idUserRequest)) == true ? null : idUserRequest);
    String messageSuccess0;
    messageSuccess = (String.isBlank(messageSuccess) == true ? null : messageSuccess);
    String messageFailure0;
    messageFailure = (String.isBlank(messageFailure) == true ? null : messageFailure);
    Integer enkontrolId0;
    enkontrolId = (String.isBlank(String.valueOf(enkontrolId)) == true ? null : enkontrolId);
    String stage0;
    stage = (String.isBlank(stage) == true ? null : stage);
    String operation0;
    operation = (String.isBlank(operation) == true ? null : operation);
    Id idProspect0;
    idProspect = (String.isBlank(String.valueOf(idProspect)) == true ? null : idProspect);
    String prospectAgent0;
    prospectAgent = (String.isBlank(prospectAgent) == true ? null : prospectAgent);
    String prospectCompany0;
    prospectCompany = (String.isBlank(prospectCompany) == true ? null : prospectCompany);
    String prospectDes0;
    prospectDes = (String.isBlank(prospectDes) == true ? null : prospectDes);
    String prospectEmail0;
    prospectEmail = (String.isBlank(prospectEmail) == true ? null : prospectEmail);
    String prospectName0;
    prospectName = (String.isBlank(prospectName) == true ? null : prospectName);
    String prospectGender0;
    prospectGender = (String.isBlank(prospectGender) == true ? null : prospectGender);
    String prospectPhone0;
    prospectPhone = (String.isBlank(prospectPhone) == true ? null : prospectPhone);
    Integer prospectIdEnkontrol0;
    prospectIdEnkontrol = (String.isBlank(String.valueOf(prospectIdEnkontrol)) == true ? null : prospectIdEnkontrol);
    String prospectMoral0;
    prospectMoral = (String.isBlank(prospectMoral) == true ? null : prospectMoral);
    String prospectLastName0;
    prospectLastName = (String.isBlank(prospectLastName) == true ? null : prospectLastName);
    String prospectPuntoProspectacion0;
    prospectPuntoProspectacion = (String.isBlank(prospectPuntoProspectacion) == true ? null : prospectPuntoProspectacion);
    Id prospectIdSalesforce0;
    prospectIdSalesforce = (String.isBlank(String.valueOf(prospectIdSalesforce)) == true ? null : prospectIdSalesforce);
    String prospectFirstName0;
    prospectFirstName = (String.isBlank(prospectFirstName) == true ? null : prospectFirstName);
    String prospectStreet0;
    prospectStreet = (String.isBlank(prospectStreet) == true ? null : prospectStreet);
    String prospectBirthdate0;
    prospectBirthdate = (String.isBlank(prospectBirthdate) == true ? null : prospectBirthdate);
    String prospectCity0;
    prospectCity = (String.isBlank(prospectCity) == true ? null : prospectCity);
    String prospectCountry0;
    prospectCountry = (String.isBlank(prospectCountry) == true ? null : prospectCountry);
    String prospectColony0;
    prospectColony = (String.isBlank(prospectColony) == true ? null : prospectColony);
    String prospectEmail20;
    prospectEmail2 = (String.isBlank(prospectEmail2) == true ? null : prospectEmail2);
    String prospectNacionality0;
    prospectNacionality = (String.isBlank(prospectNacionality) == true ? null : prospectNacionality);
    String prospectPhone20;
    prospectPhone2 = (String.isBlank(prospectPhone2) == true ? null : prospectPhone2);
    String prospectPhone30;
    prospectPhone3 = (String.isBlank(prospectPhone3) == true ? null : prospectPhone3);
    Integer prospectZipCode0;
    prospectZipCode = (String.isBlank(String.valueOf(prospectZipCode)) == true ? null : prospectZipCode);
    String prospectRFC0;
    prospectRFC = (String.isBlank(prospectRFC) == true ? null : prospectRFC);
    String prospectState0;
    prospectState = (String.isBlank(prospectState) == true ? null : prospectState);
    String origin0;
    origin = (String.isBlank(origin) == true ? null : origin);
    Integer errorCode0;
    errorCode = (String.isBlank(String.valueOf(errorCode)) == true ? null : errorCode);
    String errorMessage0;
    errorMessage = (String.isBlank(errorMessage) == true ? null : errorMessage);
    Integer successCode0;
    successCode = (String.isBlank(String.valueOf(successCode)) == true ? null : successCode);
    String successMessage0;
    successMessage = (String.isBlank(successMessage) == true ? null : successMessage);
    String opportunity0;
    opportunity = (String.isBlank(opportunity) == true ? null : opportunity);
    Id opportunityId0;
    opportunityId = (String.isBlank(String.valueOf(opportunityId)) == true ? null : opportunityId);
    Date createdDate0;
    createdDate = (String.isBlank(String.valueOf(createdDate)) == true ? null : createdDate);
    String description0;
    description = (String.isBlank(description) == true ? null : description);
    Boolean delivery0;
    delivery = (String.isBlank(String.valueOf(delivery)) == true ? null : delivery);
    String source0;
    source = (String.isBlank(source) == true ? null : source);
    String appartment0;
    appartment = (String.isBlank(appartment) == true ? null : appartment);
    Id idAppartment0;
    idAppartment = (String.isBlank(String.valueOf(idAppartment)) == true ? null : idAppartment);
    String assignatedToName0;
    assignatedToName = (String.isBlank(assignatedToName) == true ? null : assignatedToName);
    Id assignatedToId0;
    assignatedToId = (String.isBlank(String.valueOf(assignatedToId)) == true ? null : assignatedToId);
    String ownerName0;
    ownerName = (String.isBlank(ownerName) == true ? null : ownerName);
    Id owner0;
    owner = (String.isBlank(String.valueOf(owner)) == true ? null : owner);
    Id externalId0;
    externalId = (String.isBlank(String.valueOf(externalId)) == true ? null : externalId);
    String reference0;
    reference = (String.isBlank(reference) == true ? null : reference);
    Date requestDate0;
    requestDate = (String.isBlank(String.valueOf(requestDate)) == true ? null : requestDate);
    Id parentId0;
    parentId = (String.isBlank(String.valueOf(parentId)) == true ? null : parentId);
    Decimal actualCost0;
    actualCost = (String.isBlank(String.valueOf(actualCost)) == true ? null : actualCost);
    String typeRequest0;
    typeRequest = (String.isBlank(typeRequest) == true ? null : typeRequest);
    String idDropbox0;
    idDropbox = (String.isBlank(idDropbox) == true ? null : idDropbox);
    String specialTerms0;
    specialTerms = (String.isBlank(specialTerms) == true ? null : specialTerms);
    String contractTerm0;
    contractTerm = (String.isBlank(contractTerm) == true ? null : contractTerm);
    Id accountId0;
    accountId = (String.isBlank(String.valueOf(accountId)) == true ? null : accountId);
    Date deadLine0;
    deadLine = (String.isBlank(String.valueOf(deadLine)) == true ? null : deadLine);
    Integer access0;
    access = (String.isBlank(String.valueOf(access)) == true ? null : access);
    String zone0;
    zone = (String.isBlank(zone) == true ? null : zone);
    Integer maxTime0;
    maxTime = (String.isBlank(String.valueOf(maxTime)) == true ? null : maxTime);
    Integer minTime0;
    minTime = (String.isBlank(String.valueOf(minTime)) == true ? null : minTime);
    Integer averageTime0;
    averageTime = (String.isBlank(String.valueOf(averageTime)) == true ? null : averageTime);
    Integer duration0;
    duration = (String.isBlank(String.valueOf(duration)) == true ? null : duration);
    String contractNumber0;
    contractNumber = (String.isBlank(contractNumber) == true ? null : contractNumber);
    String title0;
    title = (String.isBlank(title) == true ? null : zone);
    Id contractId0;
    contractId = (String.isBlank(String.valueOf(contractId)) == true ? null : contractId);
    String development0;
    development = (String.isBlank(development) == true ? null : development);
    Boolean completed0;
    completed = (String.isBlank(String.valueOf(completed)) == true ? null : completed);
    String nextStep0;
    nextStep = (String.isBlank(nextStep) == true ? null : nextStep);
    Integer probability0;
    probability = (String.isBlank(String.valueOf(probability)) == true ? null : probability);
    String subStatus0;
    subStatus = (String.isBlank(subStatus) == true ? null : subStatus);
    String bill0;
    bill = (String.isBlank(bill) == true ? null : bill);
    Date activatedDate0;
    activatedDate = (String.isBlank(String.valueOf(activatedDate)) == true ? null : activatedDate);
    Integer orderReferenceNumber0;
    orderReferenceNumber = (String.isBlank(String.valueOf(orderReferenceNumber)) == true ? null : orderReferenceNumber);
    Id originalOrderId0;
    originalOrderId = (String.isBlank(String.valueOf(originalOrderId)) == true ? null : originalOrderId);
    Integer discount0;
    discount = (String.isBlank(String.valueOf(discount)) == true ? null : discount);
    Date expirationDate0;
    expirationDate = (String.isBlank(String.valueOf(expirationDate)) == true ? null : expirationDate);
    String quoteToName0;
    quoteToName = (String.isBlank(quoteToName) == true ? null : quoteToName);
    Boolean isSync0;
    isSync = (String.isBlank(String.valueOf(isSync)) == true ? null : isSync);
    String alias0;
    alias = (String.isBlank(alias) == true ? null : alias);
    String divisionName0;
    divisionName = (String.isBlank(divisionName) == true ? null : divisionName);
    String signature0;
    signature = (String.isBlank(signature) == true ? null : signature);
    Id managerId0;
    managerId = (String.isBlank(String.valueOf(managerId)) == true ? null : managerId);
    Id userProfileId0;
    userProfileId = (String.isBlank(String.valueOf(userProfileId)) == true ? null : userProfileId);
    Id managerProfileId0;
    managerProfileId = (String.isBlank(String.valueOf(managerProfileId)) == true ? null : managerProfileId);
  }
  
  global class GuardiaWrapper implements Comparable {

    public Guardia__c oppy;
    
    // Constructor
    public GuardiaWrapper(Guardia__c op) {
        oppy = op;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        GuardiaWrapper compareToOppy = (GuardiaWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (oppy.Roll__c > compareToOppy.oppy.Roll__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (oppy.Roll__c < compareToOppy.oppy.Roll__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}
}