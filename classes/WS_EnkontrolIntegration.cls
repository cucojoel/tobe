/*---------------------------------------------------------------------------------------------------------
  Autor: Scarlett Castillo - 08-2018 - CloudCo
  Descripción: Clase Web Service que expone los métodos de integración con Enkontrol
  Test Class: Test_WS_EnkontrolIntegration
  ---------------------------------------------------------------------------------------------------------*/
  global with sharing class WS_EnkontrolIntegration {
  /**
  * Metodo de prueba de conexión entre SFDC y Enkontrol
  * Return : OutputResult - Objeto de Resultado
  **/
  webService static OutputResult getUserInfo() {
    OutputResult result = new OutputResult();
    result.success = true;
    result.errorMsg = '';
    result.userInfo = 'Conexión Exitosa:::: Id Usuario : ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ' ; Id ORG : ' + UserInfo.getOrganizationId() + '; Id de Sessión:' + UserInfo.getSessionId();
    return result;
  }
  /**
  * WS de integración de prospectos
  *
  * @param prospectos lista de prospectos provenientes de enKontrol para sincronizar
  *
  * @return lista de mensajes de error y exito
  */
  webService static List<WS_EnkontrolIntegration.SaveResult> actualizacionProspectos(List<Prospectos> listaProspectos) {
    return Handler_WS_EnkontrolIntegration.actualizacionProspectosHandler(listaProspectos);
  }
  /**
  * Método para registrar Oportunidades provenientes de EnKontrol
  *
  * @param la lista de oportunidades
  *
  * @return lista de mensajes de error y exito
  */
  webService static List<WS_EnkontrolIntegration.SaveResult> registroOportunidades(List<Oportunidades> oportunidades) {
    return Handler_WS_EnkontrolIntegration.creacionOportunidadesHandler(oportunidades);
  }
  /**
  * WS de integración de oportunidades
  */
  webService static List<WS_EnkontrolIntegration.SaveResult> actualizacionOportunidades(List<Oportunidades> oportunidades) {
    return Handler_WS_EnkontrolIntegration.actualizacionOportunidadesHandler(oportunidades);
  }
  /**
  * Metodo para registrar proveedores desde EnKontrol
  **/
  webService static List<WS_EnkontrolIntegration.SaveResult> registroProveedores(List<Proveedores> proveedores) {
    return Handler_WS_EnkontrolIntegration.creacionProveedoresHandler(proveedores);
  }
  /**
  * Metodo para actualizar proveedores existentes desde EnKontrol
  **/
  webService static List<WS_EnkontrolIntegration.SaveResult> actualizacionProveedores(List<Proveedores> proveedores) {
    return Handler_WS_EnkontrolIntegration.actualizacionProveedoresHandler(proveedores);
  }
  /**
  * Metodo para recibir las deficiencias desde EnKontrol
  **/
  webService static List<WS_EnkontrolIntegration.SaveResult> bienvenidaCliente(List<Fallas> deficiencias) {
    return Handler_WS_EnkontrolIntegration.creacionDeficienciasHandler(deficiencias);
  }
  /**
  * Metodo para recibir la Carta Oferta desde EnKontrol
  **/
  webService static List<WS_EnkontrolIntegration.SaveResult> registroCartaOferta(List<CartasOferta> cartasOferta) {
    return Handler_WS_EnkontrolIntegration.creacionCartaOfertaHandler(cartasOferta);
  }
  /**
  * Metodo para actualizar el Estatus de un Departamento desde EnKontrol
  **/
  webService static List<WS_EnkontrolIntegration.SaveResult> actualizaEstatusDepto(List<ActualizarEstatusDepto> actualizaEstatusDepto) {
    return Handler_WS_EnkontrolIntegration.actualizarEstatusDeptoHandler(actualizaEstatusDepto);
  }
  /*
  webService static List<WS_EnkontrolIntegration.SaveResult> registroPagos(List<ObjectPagos> listObjectPagos) {
  return Handler_WS_EnkontrolIntegration.registroPagosHandler(listObjectPagos);
  }
  */
  global class SaveResult {
    webService Boolean success       { get; set; }
    webService String  errorMsg      { get; set; }
    webService String  id            { get; set; }
    webService String  idCaso        { get; set; }
    webService String  idDesarrollo  { get; set; }
    webService String  idFalla       { get; set; }
    webService String  idOportunidad { get; set; }
    webService String  idPago        { get; set; }
    webService String  idProspecto   { get; set; }
    webService String  warningMsg    { get; set; }
  }
  global class OutputResult {
    webService Boolean success  { get; set; }
    webService String  errorMsg { get; set; }
    webService String  userInfo { get; set; }
  }
  global class Fallas {
    webService Boolean esCuadrilla            { get; set; }
    webService Boolean esProveedorEmergente   { get; set; }
    webService Integer folio                  { get; set; }
    webService Integer idAplicacionOrigen     { get; set; }
    webService Integer idAsignadoObra         { get; set; }
    webService Integer idCoordinadorSC        { get; set; }
    webService Integer idEmpleado             { get; set; }
    webService Integer idEspecialidad         { get; set; }
    webService Integer idEtapaEntrega         { get; set; }
    webService Integer idProveedor            { get; set; }
    webService Integer idSubespecialidad      { get; set; }
    webService Integer idSupervisor           { get; set; }
    webService Integer idZona                 { get; set; }
    webService Integer numeroSello            { get; set; }
    webService String  correoPersonaEntrega   { get; set; }
    webService String  descripcion            { get; set; }
    webService String  dropboxFolderUrl       { get; set; }
    webService String  especialidad           { get; set; }
    webService String  idDesarrollo           { get; set; }
    webService String  idEtapa                { get; set; }
    webService String  idFalla                { get; set; }
    webService String  idFallaAnterior        { get; set; }
    webService String  idInmueble             { get; set; }
    webService String  idLote                 { get; set; }
    webService String  idPiso                 { get; set; }
    webService String  personaEntrega         { get; set; }
    webService String  proveedor              { get; set; }
    webService String  sellosDropboxFolderUrl { get; set; }
    webService String  actasDropboxFolderUrl  { get; set; }
    webService String  checklistDropboxUrl    { get; set; }
    webService String  subespecialidad        { get; set; }
    webService String  tipo                   { get; set; }
    webService String  zona                   { get; set; }
    public     Boolean existe                 { get; set; }
    public     String  estatus                { get; set; }
  }
  global class Proveedores {
    webService Integer idProveedor     { get; set; }
    webService String  nombreProveedor { get; set; }
    public     Boolean existe          { get; set; }
  }
  global class Prospectos {
    webService Boolean personaMoral       { get; set; }
    webService Integer agente             { get; set; }
    webService Integer idEnkontrol        { get; set; }
    webService Integer puntoProspectacion { get; set; }
    webService String  apellidoMaterno    { get; set; }
    webService String  apellidoPaterno    { get; set; }
    webService String  calleYNumero       { get; set; }
    webService String  ciudad             { get; set; }
    webService String  colonia            { get; set; }
    webService String  compania           { get; set; }
    webService String  cp                 { get; set; }
    webService String  desarrollo         { get; set; }
    webService String  email1             { get; set; }
    webService String  email2             { get; set; }
    webService String  idEstatus          { get; set; }
    webService String  empresa            { get; set; }
    webService String  estado             { get; set; }
    webService String  fechaAlta          { get; set; }
    webService String  fechaDeNacimiento  { get; set; }
    webService String  idSalesforce       { get; set; }
    webService String  nacionalidad       { get; set; }
    webService String  nombre             { get; set; }
    webService String  pais               { get; set; }
    webService String  rfc                { get; set; }
    webService String  sexo               { get; set; }
    webService String  telefono1          { get; set; }
    webService String  telefono2          { get; set; }
    webService String  telefono3          { get; set; }
    /* SFDC - MQD - Inicio - Cambio a Produccion 25 Julio 2019 */
    /* Campo faltante - Lead : Medio__c */
    webService String  medio              { get; set; }
    /* SFDC - MQD - Fin - Cambio a Produccion 25 Julio 2019 */
    public Boolean existe                 { get; set; }
    public String userId                  { get; set; }
  }
  global class Oportunidades {
    webService Integer agente             { get; set; }
    webService Integer gradoDeInteres     { get; set; }
    webService Integer idEnkontrol        { get; set; }
    webService Integer idProspecto        { get; set; }
    webService Integer numeroCliente      { get; set; }
    webService Integer puntoProspectacion { get; set; }
    webService String  compania           { get; set; }
    webService String  desarrollo         { get; set; }
    webService String  etapa              { get; set; }
    webService String  idSalesforce       { get; set; }
    webService String  loteId             { get; set; }
    webService String  observaciones      { get; set; }
    webService String  referencia         { get; set; }
    public     Boolean esValido           { get; set; }
    public     Boolean tieneCliente       { get; set; }
    webService List<Documentos> dropboxFolderUrls { get; set; }
  }
  global class Documentos{
    webService Integer idDocumento      { get; set; }
    webService String  documentoEnviado { get; set; }
    webService String  urlCarpeta       { get; set; }
    public     String  idOpp            { get; set; }
  }
  global class CartasOferta {
    webService Integer idDocumento { get; set; }
    webService Integer idEnkontrol { get; set; }
    webService String  idOppSf     { get; set; }
    webService String  urlDropbox  { get; set; }
  }
  global class ActualizarEstatusDepto {
    webService String desarrollo { get; set; }
    webService String estatus    { get; set; }
    webService String loteId     { get; set; }
  }
  global class ObjectPagos {
    webService String customerId     { get; set; }
    webService String developmentKey { get; set; }
    webService String discount       { get; set; }
    webService String endingBalance  { get; set; }
    webService String id             { get; set; }
    webService String idLoteEnk      { get; set; }
    webService String monthPayment   { get; set; }
    webService String operationValue { get; set; }
    webService String prospectId     { get; set; }
    webService String reference      { get; set; }
    webService String totalBalance   { get; set; }
    webService String totalPaid      { get; set; }
    webService List<ObjectDetallePagos> objectDetallePagos {get; set; }
  }
  global class ObjectDetallePagos {
    webService String balance            { get; set; }
    webService String id                 { get; set; }
    webService String paymentAmount      { get; set; }
    webService String paymentCommitment  { get; set; }
    webService String paymentDate        { get; set; }
    webService String paymentDescription { get; set; }
  }
}