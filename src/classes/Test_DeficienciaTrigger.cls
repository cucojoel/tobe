@isTest
public class Test_DeficienciaTrigger {
    
    @isTest static void pruebaCambiarEstadoConCasoResuelto(){
        
        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
        Test.setMock(HttpCalloutMock.class, multipleMock);
        
        List<Detalle_del_Caso__c> ddcList = [
            SELECT Id, OwnerId, Id_Falla_WS__c, No_Sello__c, Proveedor_Emergente__c, Casos__r.Folio_Enkontrol__c, Name,
            Casos__r.Entrega__r.Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c,
            Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c, Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c,
            Matriz_de_Deficiencias__r.Cuadrilla__c, Matriz_de_Deficiencias__r.Proveedor__r.Id_Proveedor_Enkontrol__c,
            Estatus__c, Tipo_Falla__c, Descripcion__c
            FROM Detalle_del_Caso__c
            LIMIT 1
        ];
        
        List<Case> casoList = [SELECT Id, Type, Status, Fallas_Canceladas__c, FallasPendientes__c, FallasReparadas__c,
                               Total_FallasDetectadas__c, Entrega__c, Folio_Enkontrol__c
                               FROM Case
                               LIMIT 1];

        System.debug(ddcList);
        System.debug(casoList);
        
        Test.startTest();
        try{
            casoList[0].Status = 'Resuelto';
            casoList[0].Cierre_Sincronizado_En_Enkontrol__c = true;
            update casoList[0];
            
            ddcList[0].Estatus__c = 'Reparado';
            update ddcList[0];
            
            ddcList[0].Estatus__c = 'Nuevo';
       		update ddcList[0];
            
        }catch(Exception e){
           System.debug('El cambio lanzo una excepcion =>' + e.getMessage());
        }
       
        Test.stopTest();
        
    }
    
    @isTest static void pruebaCambiarInfoCasoConActualizacion() {
        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
        Test.setMock(HttpCalloutMock.class, multipleMock);

        List<Detalle_del_Caso__c> ddcList = [
                SELECT Id, OwnerId, Id_Falla_WS__c, No_Sello__c, Proveedor_Emergente__c, Casos__r.Folio_Enkontrol__c, Name,
                        Casos__r.Entrega__r.Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c,
                        Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c, Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c,
                        Matriz_de_Deficiencias__r.Cuadrilla__c, Matriz_de_Deficiencias__r.Proveedor__r.Id_Proveedor_Enkontrol__c,
                        Estatus__c, Tipo_Falla__c, Descripcion__c
                FROM Detalle_del_Caso__c
                LIMIT 2
        ];

        List<Case> casoList = [
                SELECT Id, Type, Status, Fallas_Canceladas__c, FallasPendientes__c, FallasReparadas__c,
                        Total_FallasDetectadas__c, Entrega__c, Folio_Enkontrol__c
                FROM Case
                LIMIT 1
        ];

        System.debug(ddcList);
        System.debug(casoList);

        Test.startTest();

        ddcList[0].Casos__c = casoList[0].Id;
        ddcList[0].Estatus__c = 'Nuevo';
        update ddcList[0];

        ddcList[1].Casos__c = casoList[0].Id;
        ddcList[1].Estatus__c = 'Cancelado';
        update ddcList[1];

        Case casoN = [Select Id, FallasPendientes__c, FallasReparadas__c, Fallas_Canceladas__c, Total_FallasDetectadas__c FROM Case WHERE Id = :casoList[0].Id];

        System.assertEquals(1, casoN.FallasPendientes__c);
        System.assertEquals(1, casoN.Fallas_Canceladas__c);
        System.assertEquals(0, casoN.FallasReparadas__c);
        System.assertEquals(2, casoN.Total_FallasDetectadas__c);

        Test.stopTest();
    }
    
    @isTest static void testQueueableService(){

        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        Map<String, String> mockResponses = new Map<String, String>();

        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_DEFICIENCIAS_SUCCESS);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);
		
        List<Detalle_del_Caso__c> ddcList = [
            SELECT Id, OwnerId, Id_Falla_WS__c, No_Sello__c, Proveedor_Emergente__c, Casos__r.Folio_Enkontrol__c, Name,
            Casos__r.Entrega__r.Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c,
            Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c, Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c,
            Matriz_de_Deficiencias__r.Cuadrilla__c, Matriz_de_Deficiencias__r.Proveedor__r.Id_Proveedor_Enkontrol__c,
            Estatus__c, Tipo_Falla__c, Descripcion__c
            FROM Detalle_del_Caso__c
            LIMIT 1
        ];

        System.debug(ddcList);

        Test.startTest();
        QueueableService_DeficienciaEnkontrol caseJob = new QueueableService_DeficienciaEnkontrol(ddcList);
        caseJob.ejecutarCalloutAEnkontrol();
        Test.stopTest();
    }
    
    @isTest static void testActualizar(){
        
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        Map<String, String> mockResponses = new Map<String, String>();

        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL_DEFICIENCIAS_SUCCESS);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);
		
        List<Detalle_del_Caso__c> ddcList = [
            SELECT Id, OwnerId, Id_Falla_WS__c, No_Sello__c, Proveedor_Emergente__c, Casos__r.Folio_Enkontrol__c, Name,
            Casos__r.Entrega__r.Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c,
            Matriz_de_Deficiencias__r.Id_Zona_Enkontrol__c, Matriz_de_Deficiencias__r.Id_Especialidad_Enkontrol__c,
            Matriz_de_Deficiencias__r.Cuadrilla__c, Matriz_de_Deficiencias__r.Proveedor__r.Id_Proveedor_Enkontrol__c,
            Estatus__c, Tipo_Falla__c, Descripcion__c
            FROM Detalle_del_Caso__c
            LIMIT 1
        ];
        List<Detalle_del_Caso__c> newDdcList = new List<Detalle_del_Caso__c>();
        for(Detalle_del_Caso__c ddc : ddcList){
            ddc.Estatus__c = 'Cancelado';
            newDdcList.add(ddc);
        }
        Test.startTest();
        update newDdcList;
        Test.stopTest();
    }
    
    @TestSetup
    public static void crearDatosPrueba() {
        //Creación del caso y el detalle del caso.
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('Veralia', desarrolloComercial.Id, '1', 'DF4');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case caso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c ddc = Test_Utilities.crearDeficiencia(caso.Id);
        Detalle_del_Caso__c dc = new Detalle_del_Caso__c();
        dc.Casos__c = caso.Id;
        dc.Descripcion__c = 'Test1';
        dc.Tipo_Falla__c = 'D';
        dc.Estatus__c = 'Nuevo';
        dc.Dropbox_Folder_URL__c = '/TEST/test1.jpeg';

        insert dc;

    }

    
}