trigger Trigger_Lead on Lead (before insert, before update, after insert, after update) {
  if(Trigger.isInsert && Trigger.isBefore){
    Handler_LeadTrigger.beforeInsertHandler(Trigger.new);
  }
  if(Trigger.isInsert && Trigger.isAfter){
    Handler_LeadTrigger.afterInsertHandler(Trigger.new);
  }
  if(Trigger.isUpdate && Trigger.isBefore){
    Handler_LeadTrigger.beforeUpdateHandler(Trigger.new, Trigger.oldMap);
  }
  if(Trigger.isUpdate && Trigger.isAfter){
    Handler_LeadTrigger.afterUpdateHandler(Trigger.newMap, Trigger.oldMap);
  }
}