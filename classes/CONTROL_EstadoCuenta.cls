/* Servicio que consume los estados de cuenta de enkontrol */
public with sharing class CONTROL_EstadoCuenta {
  public static final String SOAP_ENVELOPE = 'http://schemas.xmlsoap.org/soap/envelope/';
  public static final String TEM_NAMESPACE = 'http://tempuri.org/';
  public static final String BEG_NAMESPACE = 'http://schemas.datacontract.org/2004/07/BeGrand.Common.Services';
  public static final String DTO_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO';
  public static final String ENK_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Domain';
  public Id sfId {get;set;}
  public String typeRender {get;set;}
  public String mS_Item {get;set;}
  public String urlPdf {get;set;}
  public String bSave {get;set;}
  public List<EstadoCuenta> estadosCuenta {get; set;}
  public List<EstadoCuenta> estadosCuenta2 {get; set;}
  public DateTime ahora {get; set;}
  public Opportunity opp {get; set;}
  public Account acc {get; set;}
  public Map<String,Desarrollo__c> mapDesarrollo {get; set;}
  public Configuracion_Enkontrol__c configuracion {get; set;}
  public User usuario {get;set;}
  public String estadoCuentaCorte {get;set;}
  /* Proceso de Edo de Cta separado */
  public String edoCtaSeparado {get; set;}
  public String[] parts {get; set;}
  /* Muestra el campo: SALDO INT. MORA.: */
  public decimal muestraSaldoInteresMoratorio {get; set;}
  
  /* Constructor */
  public CONTROL_EstadoCuenta() {
  	muestraSaldoInteresMoratorio = 0.0;
    edoCtaSeparado = null;
    sfId = ApexPages.currentPage().getParameters().get('id');
    typeRender = 'HTML';
    bSave = 'false';
    
    if(ApexPages.currentPage().getParameters().get('render') != null) {
      typeRender = ApexPages.currentPage().getParameters().get('render');
    }
    
    if(ApexPages.currentPage().getParameters().get('save') != null) {
      bSave = ApexPages.currentPage().getParameters().get('save');
    }
    
    /* Proceso de Edo de Cta separado */
    if(ApexPages.currentPage().getParameters().get('edocta') != null) {
      edoCtaSeparado = ApexPages.currentPage().getParameters().get('edocta');
      parts = edoCtaSeparado.split('_');
    }
    
    ahora = DateTime.now();
    TimeZone tz = UserInfo.getTimeZone();
    DateTime dt = Datetime.now();
    ahora = dt.addSeconds((tz.getOffset(dt)/1000));
    configuracion = [SELECT Username__c, Password__c, Service_Endpoint__c FROM Configuracion_Enkontrol__c];
    usuario = [SELECT Id_Enkontrol__c FROM User WHERE Id =: UserInfo.getUserId()];
    list<Desarrollo__c> listDesa = [SELECT Id, Name, Id_Desarrollo_Enkontrol__c From Desarrollo__c];
    mapDesarrollo = new Map<String,Desarrollo__c>();
    
    for(Desarrollo__c desa:listDesa) {
      mapDesarrollo.put(desa.Id_Desarrollo_Enkontrol__c, desa);
    }
    
    String dtdt = String.valueOf(dt);
    String dtdtdt = dtdt.replaceAll('\\ ', 'T');
    estadoCuentaCorte = formatearFechaMesNombre(dtdtdt);
    
    try { acc = [SELECT Id,Name From Account WHERE Id =: sfId]; } catch(Exception ex) { }
    try { opp = [SELECT Id,Name From Opportunity WHERE Id =: sfId]; } catch(Exception ex) { }
  }
  
  /* Obtiene el estado de cuenta a partir de la oportunidad */
  public void obtenerEstadoCuenta() {
    list<EstadoCuenta> estadosCuentatmp = new list<EstadoCuenta>();
    try {
      opp = [SELECT Id, Name, Desarrollo__c,Desarrollo__r.Name, Desarrollo__r.Id_Desarrollo_Enkontrol__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c, ID_Cliente_Enkontrol__c,Id_Prospecto_Enkontrol__c, Id_Edo_Cta__c From Opportunity WHERE Id =: sfId];
      String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:enk="http://schemas.datacontract.org/2004/07/EnKontrol.Request">'
      +'<soapenv:Header>'
      +'<Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
      +'<UsernameToken>'
      +'<Username>' + configuracion.Username__c + '</Username>'
      +'<Password>' + configuracion.Password__c + '</Password>'
      +'</UsernameToken>'
      +'</Security>'
      +'</soapenv:Header>'
      +'<soapenv:Body>'
      +'<tem:AccountStatementExecute>'
      +'<tem:request>'
      +'<enk:AccountStatementType>ByProspect</enk:AccountStatementType>'
      +'<enk:AgentId>' + system.label.SFDC_AgentId + '</enk:AgentId>'
      +'<enk:ProspectId>' + opp.Id_Prospecto_Enkontrol__c+ '</enk:ProspectId>'
      +'<enk:ValidateAgent>false</enk:ValidateAgent>'
      +'</tem:request>'
      +'</tem:AccountStatementExecute>'
      +'</soapenv:Body>'
      +'</soapenv:Envelope>';
      /* Modificacion para agregar la Cobranza Activa */
      /* El valor opp.ID_Cliente_Enkontrol__c ya no se usara el valor dentro del metodo, se usara ::: CustomerNumber ::: */
      list<EstadoCuenta> estadoCuentassss = consumeServicioWeb(body, opp.ID_Cliente_Enkontrol__c, opp.Id);
      estadosCuenta = estadoCuentassss;
      SaveOpp(estadosCuenta);
    }
    catch (Exception ex) { 
      system.debug('catch: ' + ex.getMessage()); 
    }
  }
  
  /* Contruye los estados de cuenta a partir de la cuenta - Estado de Cuenta multiple */
  /*
  public void obtenerEstadosCuentabyAcc() {
    list<EstadoCuenta> estadosCuentatmp = new list<EstadoCuenta>();
    try {
      list<Opportunity> listOpps = [SELECT Id, Name, Desarrollo__c, Desarrollo__r.Name, Desarrollo__r.Id_Desarrollo_Enkontrol__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c, ID_Cliente_Enkontrol__c,Id_Prospecto_Enkontrol__c, Account.Name From Opportunity WHERE AccountId =: sfId];
      for (Opportunity opp_2 : listOpps) {
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:enk="http://schemas.datacontract.org/2004/07/EnKontrol.Request">'
        +'<soapenv:Header>'
        +'<Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
        +'<UsernameToken>'
        +'<Username>' + configuracion.Username__c + '</Username>'
        +'<Password>' + configuracion.Password__c + '</Password>'
        +'</UsernameToken>'
        +'</Security>'
        +'</soapenv:Header>'
        +'<soapenv:Body>'
        +'<tem:AccountStatementExecute>'
        +'<tem:request>'
        +'<enk:AccountStatementType>ByProspect</enk:AccountStatementType>'
        +'<enk:AgentId>' + usuario.Id_Enkontrol__c + '</enk:AgentId>'
        +'<enk:ProspectId>' + opp_2.Id_Prospecto_Enkontrol__c+ '</enk:ProspectId>'
        +'<enk:ValidateAgent>false</enk:ValidateAgent>'
        +'</tem:request>'
        +'</tem:AccountStatementExecute>'
        +'</soapenv:Body>'
        +'</soapenv:Envelope>';          
        list<EstadoCuenta> estadoCuentass = consumeServicioWeb(body, opp_2.ID_Cliente_Enkontrol__c, opp_2.Id);
        if(estadoCuentass.size() > 0) {
          estadosCuentatmp.add(estadoCuentass[0]);
        }
      }
      estadosCuenta= estadosCuentatmp;
    }
    catch(Exception ex) { 
      system.debug('catch: ' + ex.getMessage()); 
    }
  }
  */
  
  /* Contruye los estados de cuenta a partir de la cuenta - Estado de Cuenta web  */
  /*
  public void obtenerEstadosCuenta() {
    list<EstadoCuenta> estadosCuentatmp = new list<EstadoCuenta>();
    try {
      acc = [SELECT Id,ID_Prospecto__pc, ID_Cliente__pc, Name From Account WHERE Id =: sfId];
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:enk="http://schemas.datacontract.org/2004/07/EnKontrol.Request">'
        +'<soapenv:Header>'
        +'<Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
        +'<UsernameToken>'
        +'<Username>' + configuracion.Username__c + '</Username>'
        +'<Password>' + configuracion.Password__c + '</Password>'
        +'</UsernameToken>'
        +'</Security>'
        +'</soapenv:Header>'
        +'<soapenv:Body>'
        +'<tem:AccountStatementExecute>'
        +'<tem:request>'
        +'<enk:AccountStatementType>ByProspect</enk:AccountStatementType>'
        +'<enk:AgentId>' + usuario.Id_Enkontrol__c + '</enk:AgentId>'
        +'<enk:ProspectId>' + acc.ID_Prospecto__pc + '</enk:ProspectId>'
        // El valor "true" determina si se puede mostrar o no la informacion al usuario solicitante. Tip: "falso" se usa para enviar por correo
        +'<enk:ValidateAgent>true</enk:ValidateAgent>'
        +'</tem:request>'
        +'</tem:AccountStatementExecute>'
        +'</soapenv:Body>'
        +'</soapenv:Envelope>';
        list<EstadoCuenta> estadoCuentass = consumeServicioWeb(body, acc.ID_Prospecto__pc, acc.Id);
        estadosCuenta = estadoCuentass;
      } 
      catch (Exception ex) { 
        system.debug('catch: ' + ex); 
      }
    }
    */
    
    /* Metodo que procesa el body y genera los estados de cuenta */
    public List<EstadoCuenta> consumeServicioWeb(String body, String prosEnk, string idopp) {
      // Imagenes de los desarrollos
      integer apareceImagen = -1;
    map<String, Integer> desarrollos = new map<String, Integer>();
    desarrollos.put('ALTO PEDREGAL',0);
    desarrollos.put('ALTO POLANCO TORRE I',1);
    desarrollos.put('ALTO POLANCO TORRE II',2);
    desarrollos.put('ALTO POLANCO TORRE III',3);
    desarrollos.put('ALTO POLANCO TORRE IV',4);
    desarrollos.put('BOSQUES TORRE I',5);
    desarrollos.put('BOSQUES TORRE II Y III',6);
    desarrollos.put('COAPA FASE I',7);              /* Solo existe en Sandbox */
    desarrollos.put('COAPA FASE II',7);
    desarrollos.put('COAPA FASE III',8);
    desarrollos.put('COAPA FASE IV',9);
    desarrollos.put('CONTADERO TORRE I',10);
    desarrollos.put('CONTADERO TORRE II',11);
    desarrollos.put('COYOACAN TORRE VII',12);
    desarrollos.put('COYOACAN TORRE VIII',13);
    desarrollos.put('COYOACAN TORRE I, II, III Y IV',10);    /* Solo existe en Sandbox */
    desarrollos.put('COYOACAN TORRE V',10);            /* Solo existe en Sandbox */
    desarrollos.put('COYOACAN TORRE VI',10);          /* Solo existe en Sandbox */
    desarrollos.put('DEL VALLE',14);
    desarrollos.put('LOMAS TORRE I',15);
    desarrollos.put('LOMAS TORRE II',16);
    desarrollos.put('LOMAS TORRE III',17);
    desarrollos.put('LOMAS TORRE IV',18);
    desarrollos.put('PARK BOSQUES TORRE I, II Y III',19);
    desarrollos.put('PARK BOSQUES TORRE I',19);         /* Solo existe en Sandbox */
    desarrollos.put('PARK BOSQUES TORRE IV Y V',20);
    desarrollos.put('PARK BOSQUES TORRE VI Y VII',21);
    desarrollos.put('PARK SAN ANGEL',22);
    desarrollos.put('PEDREGAL',23);
    desarrollos.put('REFORMA',24);
    desarrollos.put('REFORMA OFICINAS',25);
    desarrollos.put('SAN ANGEL TORRE I',26);
    desarrollos.put('SAN ANGEL TORRE II',27);
    desarrollos.put('UNIVERSIDAD',28);
      // ***************************
      
      Integer flag = 0;
      list<SFDC_BG_EdoCta_temp__c> sfdc_bg_edocta_temp = new list<SFDC_BG_EdoCta_temp__c>();
      list<EstadoCuenta> edoscta = new list<EstadoCuenta>();
      list<EstadoCuenta> edoscta2 = new list<EstadoCuenta>();
      
      Http http = new Http();
      HttpRequest req = new HttpRequest();
      req.setEndpoint(configuracion.Service_Endpoint__c);
      req.setMethod('GET');
      req.setHeader('Content-Type', 'text/xml');
      req.setHeader('SOAPAction', 'http://tempuri.org/IEnKontrolService/AccountStatementExecute');
      // Agregado el 28 de Octubre para ampliar el timeout
      req.setTimeout(120000);
      req.setBody(body);
      
      HTTPResponse res = http.send(req);
      String error = BuscaError(res.getBody());
      
      Dom.Document doc = res.getBodyDocument();
      Dom.XMLNode  nodeRoot = doc.getRootElement();
      
      if(res.getStatusCode() == 200) {
        Dom.XmlNode nodeBody = nodeRoot.getChildElement('Body', SOAP_ENVELOPE) ;
        Dom.XmlNode nodeStatementExeRespon = nodeBody.getChildElement('AccountStatementExecuteResponse', TEM_NAMESPACE);
        Dom.XmlNode nodeStatementExeResult = nodeStatementExeRespon.getChildElement('AccountStatementExecuteResult', TEM_NAMESPACE);
        Dom.XmlNode errorList = nodeStatementExeResult.getChildElement('ErrorList', BEG_NAMESPACE);
        Dom.XmlNode nodeList = nodeStatementExeResult.getChildElement('List', BEG_NAMESPACE);
        Dom.XmlNode isSuccess = nodeStatementExeResult.getChildElement('Success', BEG_NAMESPACE);
        for(Dom.XmlNode nodeStatementDto: nodeList.getChildren()) {
          String agentId = nodeStatementDto.getChildElement('AgentId',DTO_NAMESPACE).getText();
          String apartmentLocation = nodeStatementDto.getChildElement('ApartmentLocation',DTO_NAMESPACE).getText();
          /* Se agrego solo para recibir el numero del Departamento */
          String ApartmentNumber = nodeStatementDto.getChildElement('ApartmentNumber',DTO_NAMESPACE).getText();
          String companyId = nodeStatementDto.getChildElement('CompanyId',DTO_NAMESPACE).getText();
          /* Se agrego para recibir el nombre completo del Desarrollo */
          String CompanyName = nodeStatementDto.getChildElement('CompanyName',DTO_NAMESPACE).getText();
          String customerName = nodeStatementDto.getChildElement('CustomerName',DTO_NAMESPACE).getText();
          /* Se agrego cuando se paso de Oportunidad a Departamento */
          String customerNumber = nodeStatementDto.getChildElement('CustomerNumber',DTO_NAMESPACE).getText();
          String dateT = nodeStatementDto.getChildElement('Date',DTO_NAMESPACE).getText();
          String developmentKey = nodeStatementDto.getChildElement('DevelopmentKey',DTO_NAMESPACE).getText();
          String discount = nodeStatementDto.getChildElement('Discount',DTO_NAMESPACE).getText();
          String email = nodeStatementDto.getChildElement('Email',DTO_NAMESPACE).getText();
          String endingBalance = nodeStatementDto.getChildElement('EndingBalance',DTO_NAMESPACE).getText();
          String footParagraph = nodeStatementDto.getChildElement('FootParagraph',DTO_NAMESPACE).getText();
          String id = nodeStatementDto.getChildElement('Id',DTO_NAMESPACE).getText();
          String monthPayment = nodeStatementDto.getChildElement('MonthPayment',DTO_NAMESPACE).getText();
          String operationValue = nodeStatementDto.getChildElement('OperationValue',DTO_NAMESPACE).getText();
          String reference = nodeStatementDto.getChildElement('Reference',DTO_NAMESPACE).getText();
          String totalBalance = nodeStatementDto.getChildElement('TotalBalance',DTO_NAMESPACE).getText();
          String totalPaid = nodeStatementDto.getChildElement('TotalPaid',DTO_NAMESPACE).getText();
          String lateChargeBalance = nodeStatementDto.getChildElement('LateChargeBalance',DTO_NAMESPACE).getText();
          String lateChargeInterest = nodeStatementDto.getChildElement('LateChargeInterest',DTO_NAMESPACE).getText();
          String lateChargePaid = nodeStatementDto.getChildElement('LateChargePaid',DTO_NAMESPACE).getText();
          /* Se agrego para el proceso de separar los Edos de Cta */
          String LoteId = nodeStatementDto.getChildElement('LoteId',DTO_NAMESPACE).getText();
          /* Se agrego apara agrupar los Estados de Cuenta */
          String IdGroup = nodeStatementDto.getChildElement('IdGroup',DTO_NAMESPACE).getText();
          String IsGroupedAccountStatement = nodeStatementDto.getChildElement('IsGroupedAccountStatement',DTO_NAMESPACE).getText();
          
          Dom.XmlNode nodeInfoBancaria = nodeStatementDto.getChildElement('DepositInfo', DTO_NAMESPACE);
          String accountNumber = nodeInfoBancaria .getChildElement('AccountNumber',DTO_NAMESPACE).getText();
          String agreement = nodeInfoBancaria .getChildElement('Agreement',DTO_NAMESPACE).getText();
          String bankName = nodeInfoBancaria .getChildElement('BankName',DTO_NAMESPACE).getText();
          String beneficiary = nodeInfoBancaria .getChildElement('Beneficiary',DTO_NAMESPACE).getText();
          String stdBankCode = nodeInfoBancaria .getChildElement('StandardizedBankCode',DTO_NAMESPACE).getText();
          
          InfoBancaria infoBancaria = new InfoBancaria();
          infoBancaria.accountNumber = accountNumber;
                infoBancaria.agreement = agreement;
                infoBancaria.bankName = bankName;
                infoBancaria.beneficiary = beneficiary;
                infoBancaria.standardizedBankCode = stdBankCode;
                
                EstadoCuenta estadoCuenta = new EstadoCuenta();
                /* Se usara la respuesta del WS no la del valor prosEnk */
                //estadoCuenta.cliente = prosEnk + ' ' + customerName;
                estadoCuenta.cliente = customerNumber + ' ' + customerName;
                estadoCuenta.clienteString = customerName;
                estadoCuenta.departamento =apartmentLocation;
                if(mapDesarrollo.containsKey(developmentKey)) { estadoCuenta.desarrollo =mapDesarrollo.get(developmentKey).Name; }
                estadoCuenta.fechaEstadoCuenta = formatearFechaMesNombre(dateT);
                estadoCuenta.referenciaDepositos = reference;
                estadoCuenta.saldoCorte = Decimal.valueOf(endingBalance).setScale(2);
                estadoCuenta.totalPagado =Decimal.valueOf(totalPaid).setScale(2);
                estadoCuenta.descuento = Decimal.valueOf(discount).setScale(2);
                estadoCuenta.saldo = Decimal.valueOf(totalBalance).setScale(2);
                estadoCuenta.operationValue = Decimal.valueOf(operationValue).setScale(2);
                estadoCuenta.oppId=idopp;
                estadoCuenta.parrafoPie = footParagraph;
                estadoCuenta.infoBancaria = infoBancaria;
                estadoCuenta.pagoMes =Decimal.valueOf(monthPayment).setScale(2);
                estadoCuenta.moratorio =Decimal.valueOf(lateChargeBalance).setScale(2);
                estadoCuenta.intmoratorio =Decimal.valueOf(lateChargeInterest).setScale(2);
                estadoCuenta.pagadmoratorio =Decimal.valueOf(lateChargePaid).setScale(2);
                /* Se agrego para el proceso de separar los Edos de Cta */
                estadoCuenta.LoteId = LoteId;
                /* Se agrego para agrupar los Estados de Cuenta */
                estadoCuenta.IdGroup = IdGroup;
                estadoCuenta.IsGroupedAccountStatement = IsGroupedAccountStatement;
                /* Se va evaluar si hay moratorios. LateChargeInterest y LateChargePaid */
                muestraSaldoInteresMoratorio = muestraSaldoInteresMoratorio + Math.abs(Decimal.valueOf(LateChargePaid).setScale(2));
                muestraSaldoInteresMoratorio = muestraSaldoInteresMoratorio + Math.abs(Decimal.valueOf(LateChargeInterest).setScale(2));
        
            Dom.XmlNode paymentDetails = nodeStatementDto.getChildElement('PaymentDetails', DTO_NAMESPACE);
            for(Dom.XmlNode paymentDetail :paymentDetails.getChildren()) {
              String balance = paymentDetail.getChildElement('Balance',ENK_NAMESPACE).getText();
              String idm = paymentDetail.getChildElement('Id',ENK_NAMESPACE).getText();
              String paymentAmount = paymentDetail.getChildElement('PaymentAmount',ENK_NAMESPACE).getText();
              String paymentCommitment = paymentDetail.getChildElement('PaymentCommitment',ENK_NAMESPACE).getText();
              String paymentDate = paymentDetail.getChildElement('PaymentDate',ENK_NAMESPACE).getText();
              String paymentDescription = paymentDetail.getChildElement('PaymentDescription',ENK_NAMESPACE).getText();
              
              Movimiento m = new Movimiento();
              m.balance = Decimal.valueOf(balance).setScale(2);
              m.id = idm;
              m.paymentAmount = Decimal.valueOf(paymentAmount).setScale(2);
              m.paymentCommitment = Decimal.valueOf(paymentCommitment).setScale(2);
              m.paymentDescription = paymentDescription;
              m.paymentDate = formatearFechaMesNumero(paymentDate);
              m.balance = (m.balance == 0 ? 0 : m.balance);
              m.paymentAmount = (m.paymentAmount == 0 ? null : m.paymentAmount);
              m.paymentCommitment = (m.paymentCommitment == 0 ? null : m.paymentCommitment);
              estadoCuenta.movimientos.add(m);
            }
            
            /* Evaluacion para los Estados de Cuenta separados */
            /* parts[0] Lleva el LoteId --- parts[1] Lleve el CompanyId */
            flag = 0;
            if(edoCtaSeparado != null && estadoCuenta.LoteId == parts[0] && companyId == parts[1] && flag == 0) {
              estadoCuenta.imagenH = String.valueOf(desarrollos.get([select Id, Desarrollo__r.Name from Opportunity where Id =: estadoCuenta.oppId].Desarrollo__r.Name));
              edoscta2.add(estadoCuenta);
              
              SFDC_BG_EdoCta_temp__c tmp = new SFDC_BG_EdoCta_temp__c (
                Departamento__c = String.valueOf(estadoCuenta.departamento),
                Desarrollo__c = String.valueOf(estadoCuenta.desarrollo),
                Descuento__c = String.valueOf(estadoCuenta.descuento),
                OperationValue__c = String.valueOf(estadoCuenta.operationValue),
                Oportunidad__c = String.valueOf(estadoCuenta.oppId),
                PagoMes__c = String.valueOf(estadoCuenta.pagoMes),
                FechaEstadoCuenta__c = String.valueOf(estadoCuenta.fechaEstadoCuenta),
                ReferenciaDepositos__c = String.valueOf(estadoCuenta.referenciaDepositos),
                Saldo__c = String.valueOf(estadoCuenta.saldo),
                SaldoCorte__c = String.valueOf(estadoCuenta.saldoCorte),
                TotalPagado__c = String.valueOf(estadoCuenta.totalPagado),
                DesarrolloCompleto__c = String.valueOf(CompanyName),
                NumeroDepartamento__c = String.valueOf(ApartmentNumber),
                SiglaProyecto__c = String.valueOf(developmentKey),
                InteresMoratorio__c = String.valueOf(estadoCuenta.moratorio),
                Email__c = String.valueOf(email),
                Cliente__c = String.valueOf(estadoCuenta.clienteString),
                IdGroup__c = String.valueOf(estadoCuenta.IdGroup),
                IsGroupedAccountStatement__c = String.valueOf(estadoCuenta.IsGroupedAccountStatement)
              );
              sfdc_bg_edocta_temp.add(tmp);
              flag++;
            }
            //else {
              //edoscta.add(estadoCuenta);
            //}
          }
          
          if(!sfdc_bg_edocta_temp.isEmpty()) { insert sfdc_bg_edocta_temp; }
        }
          
          //if(flag > 0) { 
            //edoscta = edoscta2; 
          //}
        return edoscta2;
  }
  
  /* HISTORICO Y NO MODIFICADO */
  public void SaveOpp(List<EstadoCuenta> listEstadoCuenta) {
    for(EstadoCuenta edoCuenta :listEstadoCuenta) {
      Opportunity updateOpp = new Opportunity();
      updateOpp.Id = sfId;
          updateOpp.Banco__c = edoCuenta.infoBancaria.bankName;
      updateOpp.Beneficiario__c = edoCuenta.infoBancaria.beneficiary;
      updateOpp.Int_Moratorios__c = null;
      updateOpp.Int_mora_pagados__c = null;
      updateOpp.Pago_del_mes__c = edoCuenta.pagoMes;
      updateOpp.Saldo__c = edoCuenta.saldo;
      updateOpp.Saldo_al_corte__c = edoCuenta.saldoCorte;
      updateOpp.Saldo_int_mora__c = null;
      updateOpp.Total_pagado__c = edoCuenta.totalPagado;
      updateOpp.Valor_operacion__c = edoCuenta.operationValue;
      updateOpp.Amount = edoCuenta.operationValue;
      updateOpp.Descuento__c = edoCuenta.descuento;
      updateOpp.ApartmentLocation__c= edoCuenta.departamento;
      update updateOpp;
      }
    }
    
    /* HISTORICO Y NO MODIFICADO */
    public PageReference getLinkPdf() {
      PageReference pdfPage = new PageReference('/apex/test1?id='+mS_Item);
      pdfPage.setRedirect(true);
      bSave = 'false';
      return pdfPage;
    }
    
    /* HISTORICO Y NO MODIFICADO */
    /* Metodo que formatea la fecha que manda enkontrol */
    public static String formatearFechaMesNumero(String fechaHoraString) {
      String[] fechaHoraArreglo = fechaHoraString.split('T');
      String[] fechaArreglo = fechaHoraArreglo[0].split('-');
      String fecha = fechaArreglo[2] + '/' + fechaArreglo[1] + '/' + fechaArreglo[0];
      return fecha;
    }
    
    /* HISTORICO Y NO MODIFICADO */
    /* Metodo que formatea la fecha a dia - mes letras - año */
    public static String formatearFechaMesNombre(String fechaHoraString) {
      Map<Integer, String> mesesEspaniol = new Map<Integer, String>();
      mesesEspaniol.put(1, 'Enero');
      mesesEspaniol.put(2, 'Febrero');
      mesesEspaniol.put(3, 'Marzo');
      mesesEspaniol.put(4, 'Abril');
      mesesEspaniol.put(5, 'Mayo');
      mesesEspaniol.put(6, 'Junio');
      mesesEspaniol.put(7, 'Julio');
      mesesEspaniol.put(8, 'Agosto');
      mesesEspaniol.put(9, 'Septiembre');
      mesesEspaniol.put(10, 'Octubre');
      mesesEspaniol.put(11, 'Noviembre');
      mesesEspaniol.put(12, 'Diciembre');
      String[] fechaHoraArreglo = fechaHoraString.split('T');
      String[] fechaArreglo = fechaHoraArreglo[0].split('-');
      String fecha = fechaArreglo[2] + ' de ' + mesesEspaniol.get(Integer.valueOf(fechaArreglo[1])) + ' del ' + fechaArreglo[0];
      return fecha;
    }
    
    /* HISTORICO Y NO MODIFICADO */
    public String BuscaError(String body) {
      String error = '';
      XmlStreamReader reader = new XmlStreamReader(body);
      Boolean isSafeToGetNextXmlElement = true;
      
      while(isSafeToGetNextXmlElement) {
        /* Si el error es de tipo ErrorDTO (provocado por el atributo AgentId) */
        if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'ErrorDTO') {
          error = CONTROL_EstadoCuenta.parseError(reader);
          isSafeToGetNextXmlElement = false;
          break;
        }
        
        /* Si el error es de tipo ErrorDTO (provocado por el atributo CompanyId o DevelopmentKey) */
        if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'Fault') {
          error = CONTROL_EstadoCuenta.parseError(reader);
          isSafeToGetNextXmlElement = false;
          break;
        }
        
        /* Si aun hay elementos en el cuerpo de la respuesta se sigue leyendo */
        if(reader.hasNext()) {
          reader.next();
        }
        /* Si ya no hay elementos en el cuerpo de la respuesta se rompe el ciclo */
        else {
          isSafeToGetNextXmlElement = false;
          break;
        }
      }
      return error;
    }
    
    /* HISTORICO Y NO MODIFICADO */
    public static String parseError(XmlStreamReader reader) {
      String error = '';
      Boolean isSafeToGetNextXmlElement = true;
      
      while(isSafeToGetNextXmlElement) {
        if(reader.getEventType() == XmlTag.END_ELEMENT && ('ErrorDTO' == reader.getLocalName() || 'Fault' == reader.getLocalName())) {
          isSafeToGetNextXmlElement = false;
          break;
        }
        
        if(reader.getEventType() == XmlTag.START_ELEMENT) {
          /* Si el error es de tipo ErrorDTO (provocado por el atributo AgentId) */
          if('Message' == reader.getLocalName()) {
            error = parseString(reader);
            isSafeToGetNextXmlElement = false;
            break;
          }
          /* Si el error es de tipo ErrorDTO (provocado por el atributo CompanyId o DevelopmentKey) */
          else if('faultstring' == reader.getLocalName()) {
            error = parseString(reader);
            isSafeToGetNextXmlElement = false;
            break;
          }
        }
        
        /* Si aun hay elementos en el cuerpo de la respuesta se sigue leyendo */
        if(reader.hasNext()) {
          reader.next();
        }
        /* Si ya no hay elementos en el cuerpo de la respuesta se rompe el ciclo */
        else {
          isSafeToGetNextXmlElement = false;
          break;
        }
      }
      return error;
  }
  
    /* HISTORICO Y NO MODIFICADO */
    public static String parseString(XmlStreamReader reader) {
      String ret = '';
      boolean isSafeToGetNextXmlElement = true;
      
      while(isSafeToGetNextXmlElement) {
        if(reader.getEventType() == XmlTag.END_ELEMENT) {
          break;
        }
        else if (reader.getEventType() == XmlTag.CHARACTERS) {
          ret = ret + reader.getText();
        }
        else if (reader.getEventType() == XmlTag.CDATA) {
          ret = reader.getText();
        }
        
        /* Si aun hay elementos en el cuerpo de la respuesta se sigue leyendo */
        if (reader.hasNext()) {
          reader.next();
        }
        /* Si ya no hay elementos en el cuerpo de la respuesta se rompe el ciclo */
        else {
          isSafeToGetNextXmlElement = false;
           break;
        }
      }
      return ret.trim();
    }
    
  /* Objeto custom estado de cuenta */
    public class EstadoCuenta {
      public Boolean errorExiste {get; set;}
      public String errorMensaje {get; set;}
      public Decimal descuento {get; set;}
      public Decimal operationValue {get; set;}
      public Decimal pagoMes {get; set;}
      public Decimal moratorio {get; set;}
      public Decimal intmoratorio {get; set;}
      public Decimal pagadmoratorio {get; set;}
      public Decimal saldo {get; set;}
      public Decimal saldoCorte {get; set;}
      public Decimal totalPagado {get; set;}
      public Decimal valorOperacion {get; set;}
      /* Se agrego para el proceso de separar los Edos de Cta */
      public String LoteId {get; set;}
      /* Se agrego para insertar la imagen solicitada */
      public String imagenH {get; set;}
      public String cliente {get; set;}
      public String clienteString {get; set;}
      public String departamento {get; set;}
      public String desarrollo {get; set;}
      public String oppId {get; set;}
      public String fechaEstadoCuenta {get; set;}
      public String parrafoPie {get; set;}
      public String referenciaDepositos {get; set;}
      public List<Movimiento> movimientos {get; set;}
      public InfoBancaria infoBancaria {get; set;}
      /* Se agrego para agrupar los correos */
      public String IdGroup {get; set;}
      public String IsGroupedAccountStatement {get; set;}
      public EstadoCuenta() {
        valorOperacion = null;
      cliente = '';
      clienteString = '';
      departamento = '';
      desarrollo = '';
      oppId = '';
      fechaEstadoCuenta = '';
      referenciaDepositos = '';
      saldoCorte = null;
      moratorio = null;
      intmoratorio = null;
      pagadmoratorio = null;
      /* Se agrego para el proceso de separar los Edos de Cta */
      LoteId = null;
      /* Se agrego para insertar la imagen solicitada */
      imagenH = null;
      totalPagado = null;
      descuento = null;
      saldo = null;
      pagoMes = null;
      parrafoPie = '';
      movimientos = new List<Movimiento>();
      operationValue = null;
      IdGroup = null;
      IsGroupedAccountStatement = null;
      }
    }
    
    /* HISTORICO Y NO MODIFICADO */
    /* Objeto custom de movimientos */
    public class Movimiento {
      public Decimal balance {get; set;}
      public String id {get; set;}
      public Decimal paymentAmount {get; set;}
      public Decimal paymentCommitment {get; set;}
      public String paymentDate {get; set;}
      public String paymentDescription {get; set;}
      public Movimiento() {
        balance = null;
        id = '';
        paymentAmount = null;
        paymentCommitment = null;
        paymentDate = '';
        paymentDescription = '';
      }
  }
  
  /* HISTORICO Y NO MODIFICADO */
  /* Objeto custom de movimientos */
  public class InfoBancaria {
    public String accountNumber {get; set;}
    public String agreement {get; set;}
    public String bankName {get; set;}
    public String beneficiary {get; set;}
    public String standardizedBankCode {get; set;}
    public InfoBancaria() {
      accountNumber = '';
      agreement = '';
      bankName = '';
      beneficiary = '';
      standardizedBankCode = '';
    }
  } 
}