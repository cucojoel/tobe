@isTest
public class Test_EntregaTrigger {

    static TestMethod void myUnitTest(){

        List<Account> cuentas = Test_Service.CrearCuenta(1);
        insert cuentas;

        system.assertNotEquals(0, cuentas.size());

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        List<Desarrollo__c> desarrollos = Test_Service.CrearDesarrollo(1);
        Time myTime = Time.newInstance(1, 2, 3, 4);

        for(Desarrollo__c des : desarrollos){
            des.Desarrollo_Comercial__c = desarrolloComercial.Id;
            des.Inicio__c = myTime;
        }

        insert desarrollos;

        system.assertNotEquals(0, desarrollos.size());

        List<Etapa__c> etapas = Test_Service.CrearEtapa(1, desarrollos[0].Id);
        insert etapas;

        system.assertNotEquals(0, etapas.size());

        List<Inmueble__c> inmuebles = Test_Service.CrearInmueble(1, etapas[0].Id);
        insert inmuebles;

        system.assertNotEquals(0, inmuebles.size());

        List<Carta_Oferta__c> entregas = Test_Service.CrearEntrega(1, inmuebles[0].Id);
        insert entregas;

        system.assertNotEquals(0, entregas.size());

        Test.startTest();

        entregas[0].Se_entrega_a_Servicio_al_Cliente__c = 'NO';
        entregas[0].Subestatus__c = 'Reparacion Constructora';
        entregas[0].Estatus__c = 'Entrega';

        update entregas;

        Case unCaso = Test_Utilities.crearCaso(entregas[0].Id, cuentas.get(0).Id); //-> LINEA AGREGADA POR JCS CLOUDMOBILE 211118

        Case casoCreado = [SELECT Id, Origin FROM Case WHERE Entrega__c =: entregas[0].Id AND Origin = 'Deficiencias'];

        system.assertNotEquals(null, casoCreado);

        Test.stopTest();

    }

}