public with sharing class test1 {
	public String urlPdf {get;set;}
	public String parentid {get;set;}
    public Transient Blob strCSV {get;set;}
	public Attachment myLastAttach {get;set;}
    public Configuracion_Enkontrol__c configuracion {get; set;}
    public Opportunity opp {get; set;}
    public User usuario {get;set;}
    public static final String SOAP_ENVELOPE = 'http://schemas.xmlsoap.org/soap/envelope/';
  	public static final String TEM_NAMESPACE = 'http://tempuri.org/';
  	public static final String BEG_NAMESPACE = 'http://schemas.datacontract.org/2004/07/BeGrand.Common.Services';
  	public static final String DTO_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Domain.DTO';
  	public static final String ENK_NAMESPACE = 'http://schemas.datacontract.org/2004/07/EnKontrol.Domain';
    public Map<String,Desarrollo__c> mapDesarrollo {get; set;}
    public List<EstadoCuenta> listaEstados {get; set;}
	
	public test1(ApexPages.StandardController stdController) {
		parentid = ApexPages.currentPage().getParameters().get('id');
		System.debug('parentid: ' + parentid);
		urlPdf = '/apex/EstadoCuenta?Id=' + parentid + '&render=PDF&save=true';
        configuracion = [SELECT Username__c, Password__c, Service_Endpoint__c FROM Configuracion_Enkontrol__c];
        usuario = [SELECT Id_Enkontrol__c FROM User WHERE Id =: UserInfo.getUserId()];
        List<Desarrollo__c> listDesa = [SELECT Id, Name, Id_Desarrollo_Enkontrol__c From Desarrollo__c];
    	mapDesarrollo = new Map<String,Desarrollo__c>();
    
    	for(Desarrollo__c desa:listDesa) {
    		mapDesarrollo.put(desa.Id_Desarrollo_Enkontrol__c, desa);
    	}
        
        listaEstados = new List<EstadoCuenta>();
        listaEstados = obtenerEstadoCuenta(); 
    }
	
	public Pagereference regresarACotizacion() {
		PageReference cotizacionPage = new Pagereference('/' + parentid);
		cotizacionPage.setRedirect(true);
		return cotizacionPage;
	}
	
	public static void SavePdf(String oppId) {
		Date deit = System.today();
		list<Attachment> listAttach = new list<Attachment>();		
		list<Opportunity> listOpps = [SELECT Id, Name, Desarrollo__c, Desarrollo__r.Name, Desarrollo__r.Id_Desarrollo_Enkontrol__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c, ID_Cliente_Enkontrol__c, Id_Prospecto_Enkontrol__c, AccountId,ApartmentLocation__c From Opportunity WHERE Id =: oppId];

		for(Opportunity opp_3 : listOpps) {
			PageReference pdf = Page.EstadoCuenta;
			pdf.getParameters().put('id',opp_3.Id);
			pdf.getParameters().put('render','PDF');
			
			Attachment attach;
			try { attach = [SELECT Id,ParentId From Attachment WHERE Name =:deit.format() + ' ' +opp_3.ApartmentLocation__c + '.pdf' AND ParentId =:opp_3.Id ]; }
			catch (Exception ex) { attach = new Attachment(); }
			
			Blob body;
			try { body = pdf.getContentAsPDF(); }
			catch (VisualforceException e) { body = Blob.valueOf('Error de carga de datos'); }
				
			attach.Body = body;
			attach.Name = deit.format() + ' ' + opp_3.ApartmentLocation__c + '.pdf';
			attach.IsPrivate = false;
			attach.Description = 'false';
			if(attach.ParentId == null) {
				attach.ParentId = opp_3.Id;
			}
			attach.ContentType = 'application/pdf';
			listAttach.add(attach);
		}
		
		try { upsert listAttach; }
		catch(Exception ex) { system.debug(ex.getMessage()); }
	}
  
  	public Pagereference enviarMail() {
	  	SavePdf(parentid);
  		System.debug('$$$$$ ' + listaEstados);
  		if(parentid != NULL) {
	  		Opportunity oppRecord = [SELECT Id, Name, Account.Name,Account.PersonEmail, Desarrollo__c,Desarrollo__r.Name, Desarrollo__r.Id_Desarrollo_Enkontrol__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c, ID_Cliente_Enkontrol__c,Id_Prospecto_Enkontrol__c, AccountId,ApartmentLocation__c FROM Opportunity WHERE Id = :parentid];
  			Pagereference pageRef = null;
  			myLastAttach = [SELECT Id, Body FROM Attachment WHERE ParentId = :parentid ORDER BY CreatedDate DESC LIMIT 1];
  			Date deit = System.today();
  		
  			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
  			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
  			efa.setFileName('Estado de cuenta - '+deit.format() + ' ' +oppRecord.ApartmentLocation__c+'.pdf');
  			efa.setBody(myLastAttach.Body);
  		
  			//string[] toAddresses = New String[] {oppRecord.Account.PersonEmail};
  			string[] toAddresses = New String[] {'marco.alavez@cloudmobile.com.mx','mario.quevedo@cloudmobile.com.mx','eduardo.solares@cloudmobile.com.mx'};
  		
  			email.setSubject( 'Estado de cuenta ' + deit.format());
  			email.setToAddresses( toAddresses );
  			email.setPlainTextBody( 'Estimado: ' + oppRecord.Account.Name + ' hemos generado un nuevo estado de cuenta para usted la fecha cual se generó el estado de cuenta es ' + deit.format() + '.');
  			
            if(!listaEstados.isEmpty()){
            
            String bodyHtml = 	'<img src="https://begrand--qa--c.cs34.visual.force.com/resource/1557239935000/headerpdfmult" style="width: 100%;"/>'+
                				'<br/><br/>'+
                				'Estimado: ' + oppRecord.Account.Name + ' hemos generado un nuevo estado de cuenta para usted la fecha cual se generó el estado de cuenta es ' + deit.format() + '.' +
                				'<br/><br/>'+
                				'<table style="width:100%; border 1px solid black;">'+
                					'<thead>'+
                						'<tr style="background: rgb(0, 112, 210); color:#FFF;">'+
                							'<th style="width:12%; max-width: 12%;">'+
                								'Desarrollo'+
                							'</th>'+
                							'<th style="width:12%; max-width: 12%;">'+
                								'Departamento'+
                							'</th>'+
                							'<th style="width:12%; max-width: 12%;">'+
                								'Referencia'+
                							'</th>'+
                							'<th style="width:12%; max-width: 12%;">'+
                								'Fecha de vencimiento del pago'+
                							'</th>'+
                							'<th style="width:8%; max-width: 8%;">'+
                								'Valor de la operacion'+
                							'</th>'+
                							'<th style="width:10%; max-width: 10%;">'+
                								'Total pagado'+
                							'</th>'+
                							'<th style="width:8%; max-width: 8%;">'+
                								'Descuento '+
                							'</th>'+
                							'<th style="width:8%; max-width: 8%;">'+
                								'Saldo al corte'+
                							'</th>'+
                							'<th style="width:8%; max-width: 8%;">'+
                								'A pagar en el mes'+ 
                							'</th>'+
                							'<th style="width:8%; max-width: 8%;">'+
                								'Saldo'+ 
                							'</th>'+
                						'</tr>'+
                					'</thead>'+
                					'<tbody>';
            for(EstadoCuenta edoCuenta : listaEstados){		
            		bodyHtml +=	'<tr>'+
                        			'<td style="width:10%; max-width: 12%;">'+
                        				edoCuenta.desarrollo +
                					'</td>'+
                					'<td style="width:10%; max-width: 12%;">'+
                        				edoCuenta.departamento +
                					'</td>'+
                					'<td style="width:10%; max-width: 12%;">'+
                        				edoCuenta.referenciaDepositos +
                					'</td>'+
                					'<td style="width:10%; max-width: 12%;">'+
                        				edoCuenta.fechaEstadoCuenta +
                					'</td>'+
                					'<td style="width:8%; max-width: 8%; text-align: right;">'+
                        				edoCuenta.valorOperacion +
                					'</td>'+
                					'<td style="width:8%; max-width: 8%; text-align: right;">'+
                        				edoCuenta.totalPagado +
                					'</td>'+
                					'<td style="width:8%; max-width: 8%; text-align: right;">'+
                        				edoCuenta.descuento +
                					'</td>'+
                					'<td style="width:8%; max-width: 8%; text-align: right;">'+
                        				edoCuenta.saldoCorte +
                					'</td>'+
                					'<td style="width:8%; max-width: 8%; text-align: right;">'+
                        				edoCuenta.pagoMes +
                					'</td>'+
                					'<td style="width:8%; max-width: 8%; text-align: right;">'+
                        				edoCuenta.saldo +
                					'</td>'+	
                				'</tr>';
                        }
               bodyHtml += '</tbody>' + '</table>';
               email.setHtmlBody(bodyHtml);
            }
            
            
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
  		
	  		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
  			for(Messaging.SendEmailResult rr : r) {
	  			if(rr.isSuccess() == true) {
  					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Mensaje enviado'));
  				}
  			}
  			return pageRef;
  		}
  		return null;
  	}
    
    public class EstadoCuenta{
    public Boolean errorExiste {get; set;}
    public String errorMensaje {get; set;}
    public Decimal descuento {get; set;}
    public Decimal operationValue {get; set;}
    public Decimal pagoMes {get; set;}
    public Decimal moratorio {get; set;}
    public Decimal intmoratorio {get; set;}
    public Decimal pagadmoratorio {get; set;}
    public Decimal saldo {get; set;}
    public Decimal saldoCorte {get; set;}
    public Decimal totalPagado {get; set;}
    public Decimal valorOperacion {get; set;}
    public String cliente {get; set;}
    public String departamento {get; set;}
    public String desarrollo {get; set;}
    public String oppId {get; set;}
    public String fechaEstadoCuenta {get; set;}
    public String parrafoPie {get; set;}
    public String referenciaDepositos {get; set;}
    public List<Movimiento> movimientos {get; set;}
    public InfoBancaria infoBancaria {get; set;}
    public EstadoCuenta(){
      valorOperacion = null;
      cliente = '';
      departamento = '';
      desarrollo = '';
      oppId = '';
      fechaEstadoCuenta = '';
      referenciaDepositos = '';
      saldoCorte = null;
      moratorio = null;
      intmoratorio = null;
      pagadmoratorio = null;
      totalPagado = null;
      descuento = null;
      saldo = null;
      pagoMes = null;
      parrafoPie = '';
      movimientos = new List<Movimiento>();
      operationValue = null;
    }
  }
    
    public class Movimiento{
    public Decimal balance {get; set;}
    public String id {get; set;}
    public Decimal paymentAmount {get; set;}
    public Decimal paymentCommitment {get; set;}
    public String paymentDate {get; set;}
    public String paymentDescription {get; set;}
    public Movimiento(){
      balance = null;
      id = '';
      paymentAmount = null;
      paymentCommitment = null;
      paymentDate = '';
      paymentDescription = '';
    }
  }
    
  public class InfoBancaria{
    public String accountNumber {get; set;}
    public String agreement {get; set;}
    public String bankName {get; set;}
    public String beneficiary {get; set;}
    public String standardizedBankCode {get; set;}
    public InfoBancaria(){
      accountNumber = '';
      agreement = '';
      bankName = '';
      beneficiary = '';
      standardizedBankCode = '';
    }
  }
   
  
  public list<EstadoCuenta> obtenerEstadoCuenta() {
  	list<EstadoCuenta> estadosCuentatmp = new list<EstadoCuenta>();
    list<EstadoCuenta> estadoCuentassss = new list<EstadoCuenta>();
  	try {
  		opp = [SELECT Id, Name, Desarrollo__c,Desarrollo__r.Name, Desarrollo__r.Id_Desarrollo_Enkontrol__c, Compania__c, Compania__r.Id_Compania_Enkontrol__c, ID_Cliente_Enkontrol__c,Id_Prospecto_Enkontrol__c From Opportunity WHERE Id =: parentid];
  		
  		String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:enk="http://schemas.datacontract.org/2004/07/EnKontrol.Request">'
			+'<soapenv:Header>'
			+'<Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
			+'<UsernameToken>'
			+'<Username>' + configuracion.Username__c + '</Username>'
			+'<Password>' + configuracion.Password__c + '</Password>'
			+'</UsernameToken>'
			+'</Security>'
			+'</soapenv:Header>'
			+'<soapenv:Body>'
			+'<tem:AccountStatementExecute>'
			+'<tem:request>'
			+'<enk:AccountStatementType>ByProspect</enk:AccountStatementType>'
			+'<enk:AgentId>' + usuario.Id_Enkontrol__c + '</enk:AgentId>'
			+'<enk:ProspectId>' + opp.Id_Prospecto_Enkontrol__c+ '</enk:ProspectId>'
			+'<enk:ValidateAgent>true</enk:ValidateAgent>'
			+'</tem:request>'
			+'</tem:AccountStatementExecute>'
			+'</soapenv:Body>'
			+'</soapenv:Envelope>';
			
      estadoCuentassss = consumeServicioWeb(body, opp.ID_Cliente_Enkontrol__c, opp.Id);
      
    } 
    catch (Exception ex) { system.debug('catch: '+ex.getMessage()); }
      
      return estadoCuentassss;
  }  
    
  public List<EstadoCuenta> consumeServicioWeb(String body, String prosEnk, string idopp){
    Http http = new Http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint(configuracion.Service_Endpoint__c);
    req.setMethod('GET');
    req.setHeader('Content-Type', 'text/xml');
    req.setHeader('SOAPAction', 'http://tempuri.org/IEnKontrolService/AccountStatementExecute');
    req.setBody(body);
    HTTPResponse res = http.send(req);
    String error = BuscaError(res.getBody());

    Dom.Document doc = res.getBodyDocument();
    Dom.XMLNode  nodeRoot = doc.getRootElement();
    List<EstadoCuenta> edoscta = new List<EstadoCuenta>();
    if( res.getStatusCode() == 200 ){
      Dom.XmlNode nodeBody = nodeRoot.getChildElement('Body', SOAP_ENVELOPE) ;
      Dom.XmlNode nodeStatementExeRespon = nodeBody.getChildElement('AccountStatementExecuteResponse', TEM_NAMESPACE) ;
      Dom.XmlNode nodeStatementExeResult = nodeStatementExeRespon.getChildElement('AccountStatementExecuteResult', TEM_NAMESPACE) ;
      Dom.XmlNode errorList = nodeStatementExeResult.getChildElement('ErrorList', BEG_NAMESPACE) ;
      Dom.XmlNode nodeList = nodeStatementExeResult.getChildElement('List', BEG_NAMESPACE) ;
      Dom.XmlNode isSuccess = nodeStatementExeResult.getChildElement('Success', BEG_NAMESPACE) ;
      for(Dom.XmlNode nodeStatementDto: nodeList.getChildren()){
        String agentId = nodeStatementDto.getChildElement('AgentId',DTO_NAMESPACE).getText();
        system.debug('agentId: '+agentId);
        String apartmentLocation = nodeStatementDto.getChildElement('ApartmentLocation',DTO_NAMESPACE).getText();
        system.debug('apartmentLocation: '+apartmentLocation);
        String companyId = nodeStatementDto.getChildElement('CompanyId',DTO_NAMESPACE).getText();
        system.debug('companyId: '+companyId);
        String customerName = nodeStatementDto.getChildElement('CustomerName',DTO_NAMESPACE).getText();
        system.debug('customerName: '+customerName);
        String dateT = nodeStatementDto.getChildElement('Date',DTO_NAMESPACE).getText();
        system.debug('dateT: '+dateT);
        String developmentKey = nodeStatementDto.getChildElement('DevelopmentKey',DTO_NAMESPACE).getText();
        system.debug('developmentKey: '+developmentKey);
        String discount = nodeStatementDto.getChildElement('Discount',DTO_NAMESPACE).getText();
        system.debug('discount: '+discount);
        String endingBalance = nodeStatementDto.getChildElement('EndingBalance',DTO_NAMESPACE).getText();
        system.debug('endingBalance: '+endingBalance);
        String footParagraph = nodeStatementDto.getChildElement('FootParagraph',DTO_NAMESPACE).getText();
        system.debug('footParagraph: '+footParagraph);
        String id = nodeStatementDto.getChildElement('Id',DTO_NAMESPACE).getText();
        system.debug('id: '+id);
        String monthPayment = nodeStatementDto.getChildElement('MonthPayment',DTO_NAMESPACE).getText();
        system.debug('monthPayment: '+monthPayment);
        String operationValue = nodeStatementDto.getChildElement('OperationValue',DTO_NAMESPACE).getText();
        system.debug('operationValue: '+operationValue);
        String reference = nodeStatementDto.getChildElement('Reference',DTO_NAMESPACE).getText();
        system.debug('reference: '+reference);
        String totalBalance = nodeStatementDto.getChildElement('TotalBalance',DTO_NAMESPACE).getText();
        system.debug('totalBalance: '+totalBalance);
        String totalPaid = nodeStatementDto.getChildElement('TotalPaid',DTO_NAMESPACE).getText();
        system.debug('totalPaid: '+totalPaid);
        String lateChargeBalance = nodeStatementDto .getChildElement('LateChargeBalance',DTO_NAMESPACE).getText();
        system.debug('lateChargeBalance: '+lateChargeBalance);
        String lateChargeInterest = nodeStatementDto .getChildElement('LateChargeInterest',DTO_NAMESPACE).getText();
        system.debug('lateChargeInterest: '+lateChargeInterest);
        String lateChargePaid = nodeStatementDto .getChildElement('LateChargePaid',DTO_NAMESPACE).getText();
        system.debug('lateChargePaid: '+lateChargePaid);
        Dom.XmlNode nodeInfoBancaria = nodeStatementDto.getChildElement('DepositInfo', DTO_NAMESPACE) ;
        String accountNumber = nodeInfoBancaria .getChildElement('AccountNumber',DTO_NAMESPACE).getText();
        system.debug('accountNumber: '+accountNumber);
        String agreement = nodeInfoBancaria .getChildElement('Agreement',DTO_NAMESPACE).getText();
        system.debug('agreement: '+agreement);
        String bankName = nodeInfoBancaria .getChildElement('BankName',DTO_NAMESPACE).getText();
        system.debug('bankName: '+bankName);
        String beneficiary = nodeInfoBancaria .getChildElement('Beneficiary',DTO_NAMESPACE).getText();
        system.debug('beneficiary: '+beneficiary);
        String stdBankCode = nodeInfoBancaria .getChildElement('StandardizedBankCode',DTO_NAMESPACE).getText();
        system.debug('standardizedBankCode: '+stdBankCode);
        system.debug(mapDesarrollo);
        InfoBancaria infoBancaria = new InfoBancaria();
        infoBancaria.accountNumber = accountNumber;
        infoBancaria.agreement = agreement;
        infoBancaria.bankName = bankName;
        infoBancaria.beneficiary = beneficiary;
        infoBancaria.standardizedBankCode = stdBankCode;
        EstadoCuenta estadoCuenta = new EstadoCuenta();
        estadoCuenta.cliente = prosEnk + ' ' + customerName;
        estadoCuenta.departamento =apartmentLocation;
        if(mapDesarrollo.containsKey(developmentKey)) { estadoCuenta.desarrollo =mapDesarrollo.get(developmentKey).Name; }
        estadoCuenta.fechaEstadoCuenta = formatearFechaMesNombre(dateT);
        estadoCuenta.referenciaDepositos = reference;
        estadoCuenta.saldoCorte = Decimal.valueOf(endingBalance).setScale(2);
        estadoCuenta.totalPagado =Decimal.valueOf(totalPaid).setScale(2);
        estadoCuenta.descuento = Decimal.valueOf(discount).setScale(2);
        estadoCuenta.saldo = Decimal.valueOf(totalBalance).setScale(2);
        estadoCuenta.operationValue = Decimal.valueOf(operationValue).setScale(2);
        estadoCuenta.oppId=idopp;
        estadoCuenta.parrafoPie = footParagraph;
        estadoCuenta.infoBancaria = infoBancaria;
        estadoCuenta.pagoMes =Decimal.valueOf(monthPayment).setScale(2);
        estadoCuenta.moratorio =Decimal.valueOf(lateChargeBalance).setScale(2);
        estadoCuenta.intmoratorio =Decimal.valueOf(lateChargeInterest).setScale(2);
        estadoCuenta.pagadmoratorio =Decimal.valueOf(lateChargePaid).setScale(2);
        Dom.XmlNode paymentDetails = nodeStatementDto.getChildElement('PaymentDetails', DTO_NAMESPACE) ;
        for(Dom.XmlNode paymentDetail :paymentDetails.getChildren()) {
          String balance = paymentDetail.getChildElement('Balance',ENK_NAMESPACE).getText();
          system.debug('balance: '+balance);
          String idm = paymentDetail.getChildElement('Id',ENK_NAMESPACE).getText();
          system.debug('idm: '+idm);
          String paymentAmount = paymentDetail.getChildElement('PaymentAmount',ENK_NAMESPACE).getText();
          system.debug('paymentAmount: '+paymentAmount);
          String paymentCommitment = paymentDetail.getChildElement('PaymentCommitment',ENK_NAMESPACE).getText();
          system.debug('paymentCommitment: '+paymentCommitment);
          String paymentDate = paymentDetail.getChildElement('PaymentDate',ENK_NAMESPACE).getText();
          system.debug('paymentDate: '+paymentDate);
          String paymentDescription = paymentDetail.getChildElement('PaymentDescription',ENK_NAMESPACE).getText();
          system.debug('paymentDescription: '+paymentDescription);
          Movimiento m = new Movimiento();
          m.balance = Decimal.valueOf(balance).setScale(2);
          m.id = idm;
          m.paymentAmount = Decimal.valueOf(paymentAmount).setScale(2);
          m.paymentCommitment = Decimal.valueOf(paymentCommitment).setScale(2);
          m.paymentDescription = paymentDescription;
          m.paymentDate = formatearFechaMesNumero(paymentDate);
          m.balance = (m.balance == 0 ? null : m.balance);
          m.paymentAmount = (m.paymentAmount == 0 ? null : m.paymentAmount);
          m.paymentCommitment = (m.paymentCommitment == 0 ? null : m.paymentCommitment);
          estadoCuenta.movimientos.add(m);
        }
        edoscta.add(estadoCuenta);
      }
    }
    return edoscta;
  }
  
    
  public String BuscaError(String body) {
  	String error = '';
    XmlStreamReader reader = new XmlStreamReader(body);
    Boolean isSafeToGetNextXmlElement = true;
    while(isSafeToGetNextXmlElement) {
        //Si el error es de tipo ErrorDTO (provocado por el atributo AgentId)
        if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'ErrorDTO') {
          error = CONTROL_EstadoCuenta.parseError(reader);
          isSafeToGetNextXmlElement = false;
          break;
        }
        //Si el error es de tipo ErrorDTO (provocado por el atributo CompanyId o DevelopmentKey)
        if(reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'Fault') {
          error = CONTROL_EstadoCuenta.parseError(reader);
          isSafeToGetNextXmlElement = false;
          break;
        }
        //Si aun hay elementos en el cuerpo de la respuesta se sigue leyendo
        if(reader.hasNext()) {
          reader.next();
        }
        //Si ya no hay elementos en el cuerpo de la respuesta se rompe el ciclo
        else{
          isSafeToGetNextXmlElement = false;
          break;
        }
      }
      return error;
    }  
    
    
    public static String formatearFechaMesNumero(String fechaHoraString){
    String[] fechaHoraArreglo = fechaHoraString.split('T');
    String[] fechaArreglo = fechaHoraArreglo[0].split('-');
    String fecha = fechaArreglo[2] + '/' + fechaArreglo[1] + '/' + fechaArreglo[0];
    return fecha;
  	}
    
    public static String formatearFechaMesNombre(String fechaHoraString){
    Map<Integer, String> mesesEspaniol = new Map<Integer, String>();
    mesesEspaniol.put(1, 'Enero');
    mesesEspaniol.put(2, 'Febrero');
    mesesEspaniol.put(3, 'Marzo');
    mesesEspaniol.put(4, 'Abril');
    mesesEspaniol.put(5, 'Mayo');
    mesesEspaniol.put(6, 'Junio');
    mesesEspaniol.put(7, 'Julio');
    mesesEspaniol.put(8, 'Agosto');
    mesesEspaniol.put(9, 'Septiembre');
    mesesEspaniol.put(10, 'Octubre');
    mesesEspaniol.put(11, 'Noviembre');
    mesesEspaniol.put(12, 'Diciembre');
    String[] fechaHoraArreglo = fechaHoraString.split('T');
    String[] fechaArreglo = fechaHoraArreglo[0].split('-');
    String fecha = fechaArreglo[2] + ' de ' + mesesEspaniol.get(Integer.valueOf(fechaArreglo[1])) + ' del ' + fechaArreglo[0];
    return fecha;
  }
    
}