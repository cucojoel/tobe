/**
 * Created by scarlettcastillo on 9/24/18.
 */

public class Helper_RecursiveTrigger {
    public static Set<Id> leadsYaActualizados = new Set<Id>();
   
    private static Boolean isExecuting = true;
    
    public static void setIsExecuting(Boolean bol){
        isExecuting = bol;
    } 
    
    public static Boolean getIsExecuting(){
        return isExecuting;
    }
    
    public static void setPrendeBandera(){
        isExecuting = true;
    }
    
    public static void setApagaBandera(){
        isExecuting = false;
    }
}