/*---------------------------------------------------------------------------------------------------------
Autor: Scarlett Castillo - 08-2018 - CloudCo
Descripción: Clase Handler que llama a los service de enkontrol
Test Class: Test_WS_EnkontrolIntegration
---------------------------------------------------------------------------------------------------------*/
public class Handler_WS_EnkontrolIntegration {
  /**
  * Método de actualización de proveedores para WS
  *
  * @param lista proveedores a crear
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> creacionProveedoresHandler(List<WS_EnkontrolIntegration.Proveedores> proveedores){
    return Service_WS_EnkontrolIntegration.creacionProveedoresService(proveedores);
  }
  /**
  * Método de actualización de prospectos para WS
  *
  * @param  lista de proveedores a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> actualizacionProveedoresHandler(List<WS_EnkontrolIntegration.Proveedores> proveedores){
    return Service_WS_EnkontrolIntegration.actualizacionProveedoresService(proveedores);
  }
  /**
  * Crea las deficiencias recibidas a través del WS
  *
  * @param listaDeficiencias lista de deficiencias
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> creacionDeficienciasHandler(List<WS_EnkontrolIntegration.Fallas> listaDeficiencias){
    return Service_WS_EnkontrolIntegration_Bis.creacionDeficienciasService(listaDeficiencias);
  } 
  /**
  * Método de actualización de prospectos para WS
  *
  * @param listaProspectos la lista de prospectos a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> actualizacionProspectosHandler(List<WS_EnkontrolIntegration.Prospectos> listaProspectos){
    return Service_WS_EnkontrolIntegration.actualizacionProspectosService(listaProspectos);
  }
  /**
  * Método de actualización de oportunidades para WS
  *
  * @param listaOpps la lista de oportunidades a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> creacionOportunidadesHandler(List<WS_EnkontrolIntegration.Oportunidades> listaOpps){
    return Service_WS_EnkontrolIntegration.creacionOportunidadesService(listaOpps);
  }
  /**
  * Método de actualización de oportunidades para WS
  *
  * @param listaOpps la lista de oportunidades a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> actualizacionOportunidadesHandler(List<WS_EnkontrolIntegration.Oportunidades> listaOpps){
    return Service_WS_EnkontrolIntegration.actualizacionOportunidadesService(listaOpps);
  }
  /**
  * Método de registro de Carta Oferta para WS
  *
  * @param listaCartasOferta la lista de prospectos a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> creacionCartaOfertaHandler(List<WS_EnkontrolIntegration.CartasOferta> listaCartasOferta){
    return Service_WS_EnkontrolIntegration.creacionCartaOfertaService(listaCartasOferta);
  }
  /**
  * Método de actualización de Departamento para WS
  *
  * @param listaActualizarEstatusDepto la lista de estatus a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> actualizarEstatusDeptoHandler(List<WS_EnkontrolIntegration.ActualizarEstatusDepto> listaActualizarEstatus){
    return Service_WS_EnkontrolIntegration.actualizarEstatusDeptoService(listaActualizarEstatus);
  }
  /**
  * Método de actualización de Departamento para WS
  *
  * @param listaActualizarEstatusDepto la lista de estatus a actualizar
  *
  * @return lista de mensajes de error y exito
  */
  public static List<WS_EnkontrolIntegration.SaveResult> registroPagosHandler(List<WS_EnkontrolIntegration.ObjectPagos> listObjectPagos){
    return Service_WS_EnkontrolIntegration.registroPagosService(listObjectPagos);
  }
}