@isTest
private class TEST_Oportunidad {

	private static Account cuenta;
	private static Opportunity oportunidad;
	private static Carpeta__c carpeta;
	private static Attachment attachmentCarpeta;
	private static C_Oferta__c cartaOferta;
	private static Attachment attachmentCartaOferta;
	
	private static void init(){
		
		cuenta = new Account();
        cuenta.Name = 'Cuenta Prueba';
        
        oportunidad = new Opportunity();
		oportunidad.Name = 'Oportunidad Prueba';
		oportunidad.StageName = 'Nueva';
		oportunidad.CloseDate = Date.today().addDays(1);		
		
		carpeta = new Carpeta__c();
		carpeta.Name = 'Carpeta Prueba';
		
		attachmentCarpeta = new Attachment();
		attachmentCarpeta.Name = 'Attachment Carpeta Prueba';
		attachmentCarpeta.body = Blob.valueOf('Attachment Carpeta Prueba');
		
		cartaOferta = new C_Oferta__c();
		
		attachmentCartaOferta = new Attachment();
		attachmentCartaOferta.Name = 'Attachment Carta Oferta Prueba';
		attachmentCartaOferta.body = Blob.valueOf('Attachment Carta Oferta Prueba');
	}

	static testMethod void test1(){
		
		init();
		
		insert cuenta;
		
		oportunidad.AccountId = cuenta.Id;
		insert oportunidad;
		
		carpeta.Oportunidad__c = oportunidad.Id;
		insert carpeta;
		
		attachmentCarpeta.ParentId = carpeta.Id;
		insert attachmentCarpeta;
		
		cartaOferta.Oportunidad__c = oportunidad.Id;
		insert cartaOferta;
		
		attachmentCartaOferta.ParentId = cartaOferta.Id;
		insert attachmentCartaOferta;
		
		delete oportunidad;
	}
}