/**
 * @File Name          : RESTGarantias_REPO_PROV_26-11-2019.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 26/11/2019 12:26:39
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    26/11/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@RestResource(urlMapping='/garantias/*')
global class RESTGarantia {
    @HttpPost()
    global static CreateServiceResponsePayload Create() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        //// Creación de objeto de respuesta del servicio inicializando success = true y creación de lista vacía de errores en upserts
        CreateServiceResponsePayload response = new CreateServiceResponsePayload();
        response.success = true;

        //// Se crea la lista de objetos
        List<CreateIncomingPayload> incomingData = new List<CreateIncomingPayload>();

        //// Se obtiene el JSON origen y se verifica que tenga el formato correcto ( Una lista de objetos [{}] )
        String json_requestData = req.requestBody.toString().trim();
        try{
            incomingData = (List<CreateIncomingPayload>)JSON.deserialize(json_requestData, List<CreateIncomingPayload>.class);
        }catch(Exception e) {
            response.success = false;
            response.errorCode = 400;
            response.errorMessage = 'Error de formato de Payload, revise el formato del JSON enviado, debe ser una lista de objetos: [{}]';
            response.technicalErrorMessage = e.getMessage();
            return response;
        }
        system.debug('**Se recibe peticion: ' + incomingData);


        //// Listas que se llenan con el JSON origen
        List<String> deptosCompanias = new List<String>();
        List<String> deptosLotes = new List<String>();
        List<String> clientes = new List<String>();
        List<String> deptos = new List<String>();
        List<Integer> casos = new List<Integer>();

        //// Mapas que se llenan con SOQL con las listas anteriores para empatar idEnKontrol con IdSalesForce
        Map<String, Id> idsDeptos = new Map<String, Id>();
        Map<String, Id> idsClientes = new Map<String, Id>();
        Map<String, Id> idsCasos = new Map<String, Id>();
        Map<String, Id> idsEntregas = new Map<String, Id>();

        /// Listas para transacciones DML
        List<String> casosToDelete = new List<String>();
        List<Case> CaseListToUpsert = new List<Case>();
        List<Orden_de_Trabajo__c> OrdenListToUpsert = new List<Orden_de_Trabajo__c>();
        List<Trabajo_Programado__c> fallasListToUpsert =  new List<Trabajo_Programado__c>();
        List<Seguimiento_Garantia__c> bitacorasListToUpsert =  new List<Seguimiento_Garantia__c>();
        List<task> citasListToUpsert =  new List<task>();
        List<Recursos_Garantias__c> recursosListToUpsert =  new List<Recursos_Garantias__c>();
        List<Proveedor__c> proveedorListToUpsert = new List<Proveedor__c>();
        List<Attachment> recursoOrdenListToUpsert = new List<Attachment>();

        //// Mapas auxiliares
        Map<String, Id> mapCasesUpserted = new Map<String, Id>();
        Map<String, Id> mapOrdenesUpserted = new Map<String, Id>();
        Map<String, Id> mapTrabajoProgramadoUpserted = new Map<String, Id>();

        //Lista de tickets ingresados con exito
        List<idsTickets> ticketsins = new List<idsTickets>();
        idsTickets ticketresp = new idsTickets();
        ticketresp.success = Boolean.valueOf(true);
        idsTickets ticketresperr = new idsTickets();
        ticketresperr.success = Boolean.valueOf(false);
        Map<Id, String> tickets = new Map<Id, String>();
        Map<String, String> ticketsErr = new Map<String, String>();


        //// Llenado de listas con JSON origen
        try{
            for(CreateIncomingPayload g : incomingData){
                deptosCompanias.add(g.Departamento.Compania);
                deptosLotes.add(g.Departamento.IdLote);
                clientes.add(g.Departamento.IdProspecto);
                casos.add(Integer.valueOf(g.id));
            }
        }catch(Exception e) {
            response.errorMessage = 'Se produjo un error al procesar los datos del payload, revise el formato del JSON enviado y que los valores sean correctos.';
            return response;
        }


        //// LLenado de mapas con SOQL
        for(CreateIncomingPayload g : incomingData){
            try {
                List<Inmueble__c> inmuebles = [SELECT Etapa__r.Desarrollo__r.Desarrollo_Comercial__r.Compania__r.Id_Compania_Enkontrol__c, Lote_Id__c, Id, Id_Prospecto_Enkontrol__c, Cliente__c FROM Inmueble__c WHERE Etapa__r.Desarrollo__r.Desarrollo_Comercial__r.Compania__r.Id_Compania_Enkontrol__c IN :deptosCompanias AND Lote_Id__c IN :deptosLotes];
                for(Inmueble__c i : inmuebles){
                    idsDeptos.put(i.Etapa__r.Desarrollo__r.Desarrollo_Comercial__r.Compania__r.Id_Compania_Enkontrol__c + '-' + i.Lote_Id__c, i.Id);
                    deptos.add(i.id);
                }
            }
            catch(Exception e) {
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Ocurrió un error al obtener los datos del inmueble, revise que los Ids de los clientes sean correctos y que existan en el sistema.');
            }

        try {
            List<Account> clienteslst = [SELECT ID_Prospecto__pc, id FROM Account WHERE ID_Prospecto__pc IN :clientes];
            for(Account cllst : clienteslst){
                idsClientes.put(cllst.ID_Prospecto__pc , cllst.id);
                }
            }
            catch(Exception e) {
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Ocurrió un error al obtener los datos de los clientes, revise que los Ids de los clientes sean correctos y que existan en el sistema.');
            }

            try{
                List<Case> caseslst = [SELECT Folio_EnKontrol__C, id FROM Case WHERE Folio_EnKontrol__C IN :casos];
                for(Case c : caseslst){
                    idsCasos.put(String.valueOf(c.Folio_EnKontrol__C), c.id);
                }
            }catch(Exception e){
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + 'ocurrió un error al obtener los datos de los casos.');
            }
        }
        System.debug('**IDSDEpTOS: ' + idsDeptos );
        System.debug('**IDSCLientes: ' + idsClientes );
        System.debug('**IDSCasos: ' + idsCasos );
        //// Iteración de todos los objetos padre (case) para llenado de lista CaseListToUpsert para upsert DML
        for(CreateIncomingPayload g : incomingData){
            Case newCase;
            String idcase = '';
            try{
                idcase = String.valueOf(g.Id);
                newCase = new Case(
                    // CAMPOS GENERALES
                    Folio_EnKontrol__c = g.Id,
                    //OwnerId = String.valueOf.([SELECT Id FROM User WHERE Id_Enkontrol__c =: g.IdEmpleado]).replace(/Id),
                    RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Garantía').getRecordTypeId(),
                    Descripcion_Corta__c = g.DescripcionCorta,
                    Subject = g.ClasificacionCliente,
                    Description = g.DescripcionLarga,
                    Descripcion_Larga__c = g.DescripcionLarga,
                    Justificacion_No_Procede__c = g.JustificacionNoProcede,
                    Clasificacion_del_Cliente__c = g.ClasificacionCliente,
                    Status = g.Estatus,
                    Fecha__c = convertDate(g.Fecha),
                    Priority = g.Prioridad,
                    Asignado_a__c = g.NombreEmpleado,
                    //Motivo_Descripcion__c = g.Estatus,
                    //CAMPOS TIPO DE GARANTÍA
                    Tipo_de_Garantia__c = g.TipoGarantia.Descripcion,
                    Vigencia__c = g.TipoGarantia.VigenciaDescripcion,
                    Vigencia_Meses__c = g.TipoGarantia.VigenciaMeses,
                    Motivo_de_Rechazo__c = g.MotivoRechazo,
                    //DEPARTAMENTO
                    Departamento_busqueda__c = idsDeptos.get(g.Departamento.Compania+'-'+g.Departamento.IdLote) != null ? idsDeptos.get(g.Departamento.Compania+'-'+g.Departamento.IdLote) : '0000000000F0aaa', // Se concatena compania con idLote ya que se obtiene la relación al departamento con estos 2 campos
                    //Entrega__c = idsEntregas.get(idsDeptos.get(g.Departamento.Compania+'-'+g.Departamento.IdLote)) != null ? idsEntregas.get(idsDeptos.get(g.Departamento.Compania+'-'+g.Departamento.IdLote)) : '0000000000F0aaa',
                    //CLIENTE
                    AccountId = idsClientes.get(g.Departamento.IdProspecto) != null ? idsClientes.get(g.Departamento.IdProspecto) : '0000000000F0aaa');
              //  if(newCase.Status == 'Cerrado'){
              //      newCase.Status = 'Resuelto';
              //  }
                CaseListToUpsert.add(newcase);
                System.debug('**El ticket tiene Id en saleforce de: ' + ticketresp.IdTicketSalesforce);
            }
            catch(Exception e) {
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de CASO');
                System.debug('**Regresa el error');
            }
        }


        //// Upsert de objeto padre (Case)
        try{
            Schema.SObjectField externalKey = Case.Fields.Folio_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(CaseListToUpsert, externalKey, false);
            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){
                    upsertError ue = new upsertError();
                    for(Database.Error err : SR[i].getErrors()) {
                        ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),'Error en el campo '+ err.getFields() + '. ' + err.getMessage() + '. No se realizó UPSERT en CASO');
                        System.debug('**Regresa el error');
                    }
                }else{
                    System.debug('**Se inserto el caso con exito');
                    tickets.put(SR[i].Id, String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                }
            }
        }catch(Exception e) {
            upsertError ue = new upsertError();
        }

        //// Llenado de mapa mapCasesUpserted para asignar relación a objetos hijos
        for(Case cu : CaseListToUpsert){
            mapCasesUpserted.put(String.ValueOf(cu.Folio_Enkontrol__c), cu.Id);
        }

        System.debug('++MapaCaso: ' + mapCasesUpserted);

        //// Iteración para llenar lista OrdenListToUpsert para upsert de todos los objetos hijos (Orden_de_Trabajo__c)
        for(CreateIncomingPayload g : incomingData){
            System.debug('**Primer visualización del mapa de Casos: ' + mapCasesUpserted.get(String.valueOf(g.Id)));
            if(g.Orden.IdTicket != null){ //// Solo se llena si la garantía tiene Orden de trabajo, si no se omite para evitar errores.
                String ido = '';
                try{
                    ido = g.Orden.Id;
                    Orden_de_Trabajo__c ot = new Orden_de_Trabajo__c();
                    ot.Ticket__c = mapCasesUpserted.get(String.valueOf(g.Id));
                    ot.Id_Enkontrol__c = g.Orden.IdTicket;
                    ot.Name = g.Orden.Id;
                    ot.Fecha_del_Reporte__c = convertDate(g.Orden.Fecha);
                    ot.URL_Pdf__c = g.Orden.urlPdf;
                    if(g.Orden.Encuesta.FechaAplicacion != null){
                        System.debug('**Entral al if de encuesta');
                        ot.Fecha_Cuestionario__c = convertDate(g.Orden.Encuesta.FechaAplicacion);
                    }else{
                        System.debug('Sin fecha de aplicación');
                    }
                    if(g.Orden.Encuesta.Id != null){
                        ot.Pregunta_1__c = g.Orden.Encuesta.Preguntas[0].Descripcion;
                        ot.Pregunta_2__c = g.Orden.Encuesta.Preguntas[1].Descripcion;
                        ot.Pregunta_3__c = g.Orden.Encuesta.Preguntas[2].Descripcion;
                        ot.R1__c =  decimal.valueOf(g.Orden.Encuesta.Preguntas[0].Valor);
                        ot.R2__c =  decimal.valueOf(g.Orden.Encuesta.Preguntas[1].Valor);
                        ot.R3__c =  decimal.valueOf(g.Orden.Encuesta.Preguntas[2].Valor);
                    }
                    OrdenListToUpsert.add(ot);
                    System.debug('**Lista de orden de trabajo: ' + OrdenListToUpsert);
                    System.debug('**Fin de llenado de orden');
                }
                catch(Exception e) {
                    ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de ORDEN DE TRABAJO');
                    tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
                    System.debug('**Regresa el error');
                }
            }else{
                System.debug('**No se envió fecha de aplicación...');
                System.debug('**Hasta aqui nop hay error4');
            }
        }


        //// Upsert de objeto hijo (Orden_de_Trabajo__c)
        try{
            Schema.SObjectField externalKey = Orden_de_Trabajo__c.Fields.Id_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(ordenListToUpsert, externalKey, false);

            for(Integer i=0;i<SR.size();i++){
                if(ordenListToUpsert.get(i).Ticket__c != null){
                    if(!SR[i].isSuccess()){
                        for(Database.Error err : SR[i].getErrors()) {
                            ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),err.getMessage() + '. Error en el porcesod de UPSERT ORDEN DE TRABAJO');
                            tickets.remove(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                        }
                    }
                }
                System.debug('**Se inserto la orden correctamente');
            }
        }catch(Exception e) {
        }

        //// Llenado de mapa mapOrdenesUpserted para asignar relación a objetos hijos del objeto hijo (Orden_de_Trabajo__c)
        for(Case cu : CaseListToUpsert){
            mapCasesUpserted.put(String.ValueOf(cu.Folio_Enkontrol__c), cu.Id);
            System.debug('**Mapa de casos: ' + mapCasesUpserted);
        }

        //// Llenado de mapa mapOrdenesUpserted para asignar relación a objetos hijos del objeto hijo (Orden_de_Trabajo__c)
        for(Orden_de_Trabajo__c ot : OrdenListToUpsert){
            mapOrdenesUpserted.put(String.ValueOf(ot.Id_Enkontrol__c), ot.Id);
            System.debug('**Mapa de Ordenes de trabajo: ' + mapOrdenesUpserted);
        }

        //// Iteración para llenar lista recursoOrdenListToUpsert para upsert de todos los objetos hijos (Recurso_Orden__c) hijo de (Orden_de_Trabajo__c)
        for(CreateIncomingPayload g : incomingData){
            if(g.Orden.Id != null){
                try{
                    for(RecursoOrden b : g.Orden.Recursos){
                        String filename = b.Nombre;
                        String fileurl = b.Ruta;
                        String Orden = g.Orden.Id;
                        String referencia = mapOrdenesUpserted.get(String.valueOf(b.IdTicket));
                        SincronizacionRecursoOrdenDrop.insertarAttachment(filename, fileurl, referencia, Orden);
                    }
                }catch(Exception e) {
                    ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de RECURSO DE ORDEN DE TRABAJO');
                    tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
                    System.debug('**Regresa el error');
                }
            }else{
                System.debug('**No se envió recurso');
            }
        }


        //// Iteración para llenar lista fallasListToUpsert para upsert de todos los objetos hijos (Trabajo_Programado__c) hijo de (Orden_de_Trabajo__c)
        for(CreateIncomingPayload g : incomingData){
            try{
                for(Falla f : g.Fallas){
                    if(Test.isRunningTest()){
                        Trabajo_Programado__c tp = new Trabajo_Programado__c();

                        mapCasesUpserted.put('123', Id.valueOf('5002f000002edc9AAA'));

                        String concluido;
                        tp.Concluido__c='SI';
                        tp.Id_Enkontrol__c = '123';
                        tp.Partida__c = 'ACCESORIOS';
                        tp.Subpartida__c = 'CESPOL MAL INSTALADO';
                        tp.Folios_de_Inventario__c = '123';
                        tp.Descripcion__c = 'Ejemplo';
                        tp.Zona__c = 'BAÑO 1';
                        tp.Tipo_de_Trabajo__c = '1';
                        tp.Ticket__c = mapCasesUpserted.get('123');
                        tp.Falla__c = '123';
                        tp.Atiende__c = 'Angeles Verdes';
                    }
                    if(mapCasesUpserted.get(String.valueOf(g.Id)) != null){
                        Trabajo_Programado__c tp = new Trabajo_Programado__c();
                        String concluido;
                        if(f.EstaConcluido != null){
                            concluido = f.EstaConcluido.equals('true') ? 'Si' : f.EstaConcluido.equals('false') ? 'No' : 'N/A';
                        }else{
                            concluido = 'N/A';
                        }
                        tp.Id_Enkontrol__c = f.Id;
                        tp.Partida__c = f.PartidaDescripcion;
                        tp.Subpartida__c = f.SubpartidaDescripcion;
                        tp.Folios_de_Inventario__c = f.FolioInventario;
                        tp.Descripcion__c = f.FallaDescripcion;
                        tp.Concluido__c = concluido;
                        tp.Zona__c = f.ZonaDescripcion;
                        tp.Tipo_de_Trabajo__c = f.TipoTrabajo;
                        tp.Ticket__c = mapCasesUpserted.get(String.valueOf(g.Id));
                        tp.Falla__c = f.Id;
                        tp.Atiende__c = f.EquipoAtencion;
                        if(f.EquipoAtencion != 'Proveedor'){
                            tp.Proveedor__c = f.EquipoAtencion;
                        }else{
                            tp.Proveedor__c = f.Proveedor.Nombre;
                        }
                        fallasListToUpsert.add(tp);
                        System.debug('**Lista de fallas: ' + fallasListToUpsert);
                    }else{
                        System.debug('**No se envió Falla');
                    }
                }
            }catch(Exception e) {
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de FALLAS');
                tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
                System.debug('**Regresa el error');
            }
        }

        //Upsert FALLAS (Trabajo_Programado__c)
        try{
            Schema.SObjectField externalKey = Trabajo_Programado__c.Fields.Id_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(fallasListToUpsert, externalKey, false);

            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){
                    for(Database.Error err : SR[i].getErrors()) {
                        ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),err.getMessage() + '. Error en el proceso de UPSERT FALLAS');
                        tickets.remove(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                        System.debug('**Regresa el error');
                    }
                }
                system.debug('**Se inserto la falla correctamente');
            }
        }catch(Exception e) {
            System.debug('**Regresa el error');
        }

        // Llenado de mapa mapOrdenesUpserted para asignar relación a objetos hijos del objeto hijo (Orden_de_Trabajo__c)
        for(Trabajo_Programado__c tp : fallasListToUpsert){
            mapTrabajoProgramadoUpserted.put(String.ValueOf(tp.Id_Enkontrol__c), tp.Id);
        }
        System.debug('**Mapa de mapTrabajoProgramadoUpserted: ' + mapTrabajoProgramadoUpserted);
        System.debug('**Lista de fallas: ' + fallasListToUpsert);


        //Llenado de lista ProveedorListToUpsert
        for(CreateIncomingPayload g : incomingData){
            try{
                for(Falla f : g.Fallas){
                    if(f.EquipoAtencion == 'Proveedor'){
                        if(f.Proveedor.IdFalla != null){
                            Proveedor__c pr = new Proveedor__c();
                            pr.Trabajo_Programado__c = mapTrabajoProgramadoUpserted.get(f.Proveedor.IdFalla);
                            pr.Id_clave_fracc__c = f.Proveedor.Desarrollo;
                            pr.Proveedor__c = f.Proveedor.Nombre;
                            pr.Descripcion__c = f.Proveedor.Especialidad;
                            pr.Id_Enkontrol__c = f.Proveedor.IdFalla;
                            proveedorListToUpsert.add(pr);
                        }else{
                            System.debug('**No se tiene proveedor asignado a la falla');
                        }
                    }else{
                        System.debug('No se asigno a priveedor');
                    }
                }
                System.debug('----No está entrando al ciclo for----');
            }catch(Exception e) {
                idsTickets ue = new idsTickets();
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de PROVEEDOR');
                tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
            }
        }

        //UPSERT de Proveedor__c
        try{
            Schema.SObjectField externalKey = Proveedor__c.Fields.Id_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(proveedorListToUpsert, externalKey, false);

            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){
                    if(proveedorListToUpsert.get(i).Id_Enkontrol__c != null){
                        for(Database.Error err : SR[i].getErrors()) {
                            ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),err.getMessage() + '. En el proceso de UPSERT PROVEEDOR');
                            tickets.remove(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                            System.debug('**Regresa el error');
                        }
                    }
                }
                System.debug('**Se inserto correctamente el proveedor');
            }
        }catch(Exception e) {
            upsertError ue = new upsertError();
            System.debug('**Regresa el error');
        }

        //// Llenado de lista bitacorasListToUpsert para upsert de todos los objetos hijos (Seguimiento_Garantia__c) || No se necesita otra ireación ya que este objeto no tiene objetos hijos
        for(CreateIncomingPayload g : incomingData){
            String idb = '';
            try{
                for(bitacora b : g.Bitacora){
                    idb = b.Id;
                    Seguimiento_Garantia__c sg = new Seguimiento_Garantia__c();
                    sg.Caso__c = mapCasesupserted.get(String.valueOf(g.Id));
                    sg.Descripcion__c = b.Descripcion;
                    sg.Estatus__c = b.Estatus;
                    sg.Estatus_Detalle__c = b.EstatusDetalle;
                    sg.Id_Enkontrol__c = b.Id;
                    sg.Fecha__c = convertDate(b.Fecha);
                    sg.Reasignacion__c = b.NombreEmpleadoReasignado;
                    bitacorasListToUpsert.add(sg);
                    System.debug('**Bitacoras a insertar: ' + bitacorasListToUpsert);
                }
            }
            catch(Exception e) {
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de BITACORA');
                tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
                System.debug('**Regresa el error');
            }


            //// Llenado de lista citasListToUpsert para upsert de todos los objetos hijos (task) || No se necesita otra ireación ya que este objeto no tiene objetos hijos
            String idc = '';
            try{
                for(Cita b : g.Citas){
                    idc = b.Id;
                    task c = new task();
                    c.WhatId = mapCasesupserted.get(String.valueOf(g.Id));
                    c.Id_Enkontrol__c = b.Id;
                    c.Fecha_Cita__c = convertDate(b.Fecha);
                    c.Subject = b.Comentario;
                    c.Description = b.Comentario;
                    citasListToUpsert.add(c);
                    System.debug('**Regresa el error');
                }

            }catch(Exception e) {
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de BITACORA');
                tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
                System.debug('**Regresa el error');
            }

            //// Llenado de lista recursosListToUpsert para upsert de todos los objetos hijos (Recursos_Garantias__c) || No se necesita otra ireación ya que este objeto no tiene objetos hijos
            try{
                for(Recurso b : g.Recursos){
                    Recursos_Garantias__c r = new Recursos_Garantias__c();
                    r.Garantia__c = mapCasesupserted.get(String.valueOf(g.Id));
                    r.Descripcion__c = b.Descripcion;
                    r.Fecha__c = convertDate(b.Fecha);
                    r.Id_Enkontrol__c = b.Id;
                    r.Name = b.Nombre;
                    r.URL__c = b.Ruta;
                    String ext = b.Nombre;
                    r.Extension__c = ext.substringAfter('.');
                    recursosListToUpsert.add(r);
                    System.debug('**Lista de Recursos a insertar' + recursosListToUpsert);}
            }catch(Exception e) {
                System.debug('**Tickets: ' + tickets);
                tickets.remove(mapCasesUpserted.get(String.valueOf(g.Id)));
                ticketsErr.put(String.valueOf(g.Id),e.getMessage() + '. Error en el llenado de datos a nivel de RECURSO DE CASO');
                System.debug('**Regresa el error');
            }
        }


        ////// INICIAN TRANSACCIONES DML DE OBJETOS HIJOS SIN HIJOS //////////////
        // Upsert Seguimiento_Garantia__c / bitacoras
        try{
            Schema.SObjectField externalKey = Seguimiento_Garantia__c.Fields.Id_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(bitacorasListToUpsert, externalKey, false);

            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){
                    if(bitacorasListToUpsert.get(i).Caso__c != null){
                        for(Database.Error err : SR[i].getErrors()) {
                            ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),err.getMessage() + '. En el proceso de UPSERT BITACORA');
                            tickets.remove(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                            System.debug('**Regresa el error');
                        }
                    }
                }
            }
        }catch(Exception e) {

        }


        // Upsert Task / Citas
        try{
            Schema.SObjectField externalKey = Task.Fields.Id_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(citasListToUpsert, externalKey, false);

            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){
                    for(Database.Error err : SR[i].getErrors()) {
                        //ue.entity = 'Task';
                        ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),err.getMessage() + '. En el proceso de UPSERT CITA');
                        tickets.remove(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                        System.debug('**Regresa el error');
                    }
                }
            }
        }catch(Exception e) {
            System.debug('**Regresa el error');
        }


        /// Upsert Recursos_Garantias__c
        try{
            Schema.SObjectField externalKey = Recursos_Garantias__c.Fields.Id_Enkontrol__c;
            List<Database.UpsertResult> SR = Database.upsert(recursosListToUpsert, externalKey, false);

            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){
                    if(recursosListToUpsert.get(i).Garantia__c != null){
                        for(Database.Error err : SR[i].getErrors()) {
                            ticketsErr.put(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c),err.getMessage() + '. En el proceso de UPSERT RECURSO DE CASO');
                            tickets.remove(String.valueOf(CaseListToUpsert.get(i).Folio_Enkontrol__c));
                            System.debug('**Regresa el error');
                        }
                    }
                }
            }
        }catch(Exception e) {
            upsertError ue = new upsertError();
        }


        //Llenado de mapa con los tickets correctos
        for (Id idsf : tickets.keySet()){
            idsTickets ti = new idsTickets();
            ti.IdTicket = tickets.get(idsf);
            ti.IdTicketSalesForce = idsf;
            ti.success = true;
            ticketsins.add(ti);
        }

        //Llenado de mapa con los tickets incorrectos
        for (String idsf : ticketsErr.keySet()){
            idsTickets ti = new idsTickets();
            ti.IdTicket = idsf;
            ti.errorMessage = ticketsErr.get(idsf);
            ti.success = false;
            ticketsins.add(ti);
        }

        response.idsInsertados = ticketsins;

        //// Se eliminan todas las garantías que eran nuevas y que tuvieron errores para no guardar registros incompletos
        try{
            system.debug('**Casos a eliminar list' + casosToDelete);
            List<Case> ctd = [SELECT Id FROM Case WHERE Id IN :casosToDelete];
            system.debug('**Casos a eliminar obj' + ctd);
            delete ctd;
        }catch(Exception e) {
        }

        //ticketsins.add(ticketresp);
        //ticketsins.add(ticketresperr);

        return response;
    }

    public Static Date convertDate(String d){
        String substringDate = d.substringAfter('(').substringBefore(')');
        List<String> substringDateSplited = substringDate.split('-');
        substringDate = substringDateSplited.size() > 2 ? '-'+substringDateSplited[1] : substringDateSplited[0];
        DateTime dt = datetime.newinstance(long.valueOf(substringDate));
        return date.newinstance(dt.year(), dt.month(), dt.day());
    }

    global class CreateServiceResponsePayload {
        public Boolean success {get;set;}
        public Integer errorCode {get;set;}
        public String errorMessage {get;set;}
        public String technicalErrorMessage {get;set;}
        public List<idsTickets> idsInsertados {get;set;}
        //public Map<Id, String> idsInsertados {get;set;}
        //public List<upsertError> upsertErrors {get;set;}
    }

    global class idsTickets{
        public String IdTicket {get;set;}
        public String IdTicketSalesForce {get;set;}
        public String errorFields {get;set;}
        public String errorMessage {get;set;}
        public boolean success {get; set;}
    }

    global class upsertError{
    }

    /*global class CreateServiceResponsePayloadDelete{
public Boolean success {get;set;}
public Integer errorCode {get;set;}
public String errorMessage {get;set;}
public String technicalErrorMessage {get;set;}
public List<Elemento> ElementosEliminados {get;set;}
}*/

    GLobal class CreateIncomingPayload {
        public Decimal Id {get;set;}
        public String DescripcionCorta {get;set;}
        public String DescripcionLarga {get;set;}
        public String JustificacionNoProcede {get;set;}
        //public String PartidaDescripcion {get;set;}
        //public String Descripcion {get;set;}
        public String ClasificacionCliente {get;set;}
        public String Estatus {get;set;}
        public String Fecha {get;set;}
        //public String EstatusDetalle {get;set;}
        //public String IdUsuario {get;set;}
        //public String IdEmpleado {get;set;}
        public String Prioridad {get;set;}
        public String MotivoRechazo {get;set;}
        public String NombreEmpleado {get;set;}

        public Depto Departamento {get;set;}
        public tipoGarantia TipoGarantia {get;set;}
        public orden Orden {get;set;}
        //public falla Falla {get;set;}

        public List<Falla> Fallas {get;set;}
        public List<bitacora> Bitacora {get;set;}
        public List<Cita> Citas {get;set;}
        public List<Recurso> Recursos {get;set;}
        //public String VigenciaClasificacionCliente {get;set;}
    }

    public class orden{
        public String Id {get;set;}
        //public String Folio {get;set;}
        public String Fecha {get;set;}
        public String IdTicket {get;set;}
        public String urlPdf {get;set;}
        //public String Atiende {get;set;}
        public encuesta Encuesta {get;set;}
        public List<RecursoOrden> Recursos {get;set;}
    }

    public class encuesta{
        public String Id {get;set;}
        //public String IdCuestionario {get;set;}
        //public String IdEmpleado {get;set;}
        //public String NombreCuestionario {get;set;}
        public String FechaAplicacion {get;set;}
        public List<Pregunta> Preguntas {get;set;}
    }

    public class Pregunta{
        //public String IdElemento {get;set;}
        public String Descripcion {get;set;}
        public String Valor {get;set;}
    }

    public class Cita{
        public String Id {get;set;}
        public String Fecha {get;set;}
        public String Comentario {get;set;}
        //public String IdEmpleado {get;set;}
    }

    public class bitacora{
        public String Id {get;set;}
        public String Estatus {get;set;}
        public String EstatusDetalle {get;set;}
        public String Descripcion {get;set;}
        public String Fecha {get;set;}
        public String NombreEmpleadoReasignado {get;set;}
        //public String PlantillaRechazo {get;set;}
    }

    public class Depto{
        //public String Id {get;set;}
        public String IdLote {get;set;}
        public String Compania {get;set;}
        //public String Desarrollo {get;set;}
        public String IdCliente {get;set;}
        public String IdProspecto {get;set;}
        //public String Observaciones {get;set;}
        //public String EstaConcluido {get;set;}
    }

    public class Falla{
        public String Id {get;set;}
        public String SubpartidaDescripcion {get;set;}
        public String PartidaDescripcion {get;set;}
        public String ZonaDescripcion {get;set;}
        public String FallaDescripcion {get;set;}
        //public String Observaciones {get;set;}
        public String EstaConcluido {get;set;}
        public String IdTicket {get;set;}
        public String TipoTrabajo {get; set;}
        public String FolioInventario {get;set;}
        public proveedor Proveedor {get;set;}
        PUBLIC String EquipoAtencion {get;set;}
    }

    public class proveedor{
        public String Nombre {get;set;}
        public String Desarrollo {get;set;}
        //public String Descripcion {get;set;}
        //public String Id {get;set;}
        public String Especialidad {get;set;}
        public String IdFalla {get; set;}

    }

    public class tipoGarantia{
        //public String Id {get;set;}
        public String Descripcion {get;set;}
        public String DescripcionLarga {get;set;}
        public String VigenciaDescripcion {get;set;}
        public String VigenciaMeses {get;set;}
        //public String Vigencia {get;set;}
    }

    public class CreateIncomingPayloadDelete{
        //public List<Elemento> Elementos {get;set;}
    }

    public class Elemento{
        //public String Objeto {get;set;}
        //public String Id {get;set;}
    }

    public class Recurso{
        public String Fecha {get;set;}
        public String Id {get;set;}
        public String Nombre {get;set;}
        public String Descripcion {get;set;}
        public String Ruta {get;set;}
        public String IdTicket {get;set;}
        //public String Extension {get;set;}
    }

    public class RecursoOrden {
        public String Ruta {get;set;}
        public String Nombre {get;set;}
        public String idTicket {get;set;}
    }
}