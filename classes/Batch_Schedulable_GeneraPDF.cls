/* Este codigo se debe de correr en un minuto impar */
global class Batch_Schedulable_GeneraPDF implements Schedulable {
	global void execute(SchedulableContext SC) {
		PreparaEstadosCuenta__c tiempoGenerarPDF = [select Id, Name, Inicio_hora__c, Fin_hora__c from PreparaEstadosCuenta__c where Name = 'HoraInicioBatchGeneraPDF'];
		String horaLocal = String.valueOf(system.now());
		String [] diaPartes = horaLocal.split(' ');
		String [] horaPartes = diaPartes[1].split(':');
		if(Decimal.valueOf(horaPartes[0]) < tiempoGenerarPDF.Fin_hora__c) {
			// Ejecuta el procedimiento para mandar llamar el servicio del PDF
			list<Inmueble__c> departamentos = Database.query(system.label.Batch_Schedulable_GeneraPDF);
			for(Inmueble__c forData : departamentos) { forData.Edo_Cta_Ejecutado__c = true; }
			Database.update(departamentos, false);
			
			// Mata el proceso BatchNuevo
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'GeneraPDFBatchable'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Genera proceso BatchNuevo
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Batchable_GeneraPDF bsPDF = new Batch_Batchable_GeneraPDF();
			String job = System.schedule('GeneraPDFBatchable', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
		else {
			// Mata el proceso BatchNuevo
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'GeneraPDFBatchable'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Activa el proceso que evaluara PDF es blanco
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_ReportaPDFBlanco bsPDFBlanco = new Batch_Schedulable_ReportaPDFBlanco();
			String job = System.schedule('BorrandoPDFBlanco', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDFBlanco);
		}
	}
}