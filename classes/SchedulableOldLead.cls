/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        SchedulableOldLead
* @Autor       A. Martinez
* @Date        29/10/2019
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global class SchedulableOldLead implements Schedulable {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       A. Martinez
  * @Date        29/10/2019
  * @Param       SchedulableContext
  **/
  global void execute(SchedulableContext sc) {
    Id batchId = Database.executeBatch(new BatchOldLead(), 2);
  }
}