@isTest
public with sharing class Batch_Schedulable_ReportaPDFBlanco_Test {
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	static TestMethod void test_01() {
		// Para el Departamento y la Oportunidad
		Account cliente = new Account(
			LastName = 'CoberturaCuenta',
			ID_Prospecto__pc = '15810',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Persona Física').getRecordTypeId()
		);
		insert cliente;
		
		Compania__c compania = new Compania__c(
        	Name = 'CoberturaCompania',
        	Activa__c = true,
        	Id_Compania_Enkontrol__c = '32'
        );
        insert compania;
        
        Desarrollo_Comercial__c desarrolloComercial = new Desarrollo_Comercial__c(
        	Name = 'CoberturaDesarrolloComercial'
        );
        insert desarrolloComercial;
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        Desarrollo__c desarrollo = new Desarrollo__c(
        	Name = 'CoberturaDesarrollo',
        	Inicio__c = myTime,
        	Desarrollo_Comercial__c = desarrolloComercial.Id,
        	Id_Desarrollo_Enkontrol__c = 'BGR',
        	Compania__c = compania.Id
        );
        insert desarrollo;
        
        Etapa__c etapa = new Etapa__c(
        	Name = 'CoberturaEtapa',
        	No_de_Pisos__c = 1,
        	Desarrollo__c = desarrollo.Id,
        	Id_Etapa_Enkontrol__c = '1',
        	Tipo__c = 'Torre'
        );
        insert etapa;
        
		Inmueble__c departamento = new Inmueble__c(
			Name = 'CoberturaDepartamento',
			Etapa__c = etapa.Id,
			Estatus__c = 'VENDIDO',
			Lote_Id__c = '322',
			Edo_Cta_Ejecutado__c = true,
			Id_Cliente_Enkontrol__c = '5108',
			Id_Prospecto_Enkontrol__c = '15810',
			OportunidadTexto__c = '0062f000001ffTuAAI'
		);
		insert departamento;
		
		Opportunity oportunidad = new Opportunity(
			Name = '15810-8',
			StageName = 'Nueva',
			CloseDate = Date.today().addDays(1),
			AccountId = cliente.Id,
			Desarrollo__c = desarrollo.Id,
			Etapa__c = etapa.Id,
			Departamento__c = departamento.Id,
			Compania__c = compania.Id,
			Lote_Id__c = '322',
			Id_Prospecto_Enkontrol__c = '15810',
			ID_Cliente_Enkontrol__c = '5108',
			ApartmentLocation__c = 'DEPARTAMENTO Torre: 1 Piso: 36 Depto: 3612'
		);
		insert oportunidad;
        
        // Para el adjunto
    	Attachment attach = new Attachment();       
        attach.Name = 'ECtaBG_' + oportunidad.Id;
        Blob bodyBlob = Blob.valueOf('Cuerpo del PDF');
        attach.body = bodyBlob;
        attach.parentId = departamento.Id;
        insert attach;
        
        Test.startTest();
			String jobId = System.schedule('Batch_Schedulable_ReportaPDFBlanco', CRON_EXP, new Batch_Schedulable_ReportaPDFBlanco());
		Test.stopTest();
	}
}