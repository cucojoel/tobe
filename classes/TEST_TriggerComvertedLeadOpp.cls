@isTest(SeeAllData=true)
public class TEST_TriggerComvertedLeadOpp {

    private static Desarrollo_Comercial__c desarrolloComercial1;
    private static Desarrollo__c desarrollo;
    private static User usuario;
    private static Lead prospecto;
    private static Bitacora__c bitacoraDummy;
    private static Guardia__c guardia1DesarrolloComercial1;
    private static Guardia__c guardia2DesarrolloComercial1;
    private static Guardia__c guardia1DesarrolloComercial2;
    private static Guardia__c guardia2DesarrolloComercial2;
    private static Guardia__c guardia1DesarrolloComercialBDC;
    private static Guardia__c guardia2DesarrolloComercialBDC;
    private static Guardia__c guardia3DesarrolloComercialBDC;
    private static Guardia__c guardia4DesarrolloComercialBDC;
    private static Task tarea;
    private static Event evento;
    
    @isTest
    public static void init(){
        desarrolloComercial1 = new Desarrollo_Comercial__c();
        desarrolloComercial1.Name = 'Reforma';
        desarrolloComercial1.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
        desarrolloComercial1.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);
        
        usuario = new User();
        usuario.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id;
        usuario.Alias = 'usreje';
        usuario.Country = 'México';
        usuario.Email = 'ejecutivo@email.com';
        usuario.EmailEncodingKey = 'UTF-8';
        usuario.LastName = 'ejecutivo';
        usuario.LanguageLocaleKey = 'es_MX';
        usuario.LocaleSidKey = 'es_MX';
        usuario.TimeZoneSidKey = 'America/Mexico_City';
        usuario.UserName='ejecutivo@email.com';
        usuario.Country = 'Mexico';
        
        prospecto = new Lead();
        prospecto.FirstName = 'prospecto';
        prospecto.LastName = 'prospecto';
        prospecto.Email = 'prospecto@test.com';
        prospecto.Desarrollo_Web__c = 'Reforma';
        prospecto.Cantidad_No_Shows__c = 0;
        prospecto.LeadSource = '6';
        prospecto.RFC__c = 'CASJ821112UT6';
        prospecto.Phone = '2222334455';
        
        bitacoraDummy = new Bitacora__c();
        insert desarrolloComercial1;
        
        desarrollo = new Desarrollo__c();
        desarrollo.Desarrollo_Comercial__c = desarrolloComercial1.Id;
        desarrollo.Inicio__c = time.newInstance(9, 0, 0, 0);
        desarrollo.FIn__c = time.newInstance(19, 0, 0, 0);
        insert desarrollo;
        
        insert usuario;
        
        Account cliente = new Account();
        cliente.FirstName = 'cliente';
        cliente.LastName = 'cliente';
        cliente.F_CM__c = Date.today().addDays(1);
        insert cliente;
        
        prospecto.Desarrollo__c = desarrollo.Id;
        prospecto.Desarrollo_Comercial__c = desarrolloComercial1.Id;
        prospecto.F_CM__c = Date.today().addDays(1);
        prospecto.Agente__c = usuario.Id;
        prospecto.ID_Prospecto_Enkontrol__c = '1234';
        prospecto.ConvertedAccountId = cliente.Id;
        prospecto.IsConverted = true;
        insert prospecto;
        
        bitacoraDummy.Forma_de_Contacto__c = 'Correo';
        bitacoraDummy.Forma_de_Contacto__c = 'Correo';
        bitacoraDummy.Contesta__c = 'No';
        bitacoraDummy.Resultado__c = null;
        bitacoraDummy.Lead__c = prospecto.Id;
        bitacoraDummy.Fecha_futura_de_contacto__c = DateTime.now().addDays(1);
        insert bitacoraDummy;
        
        Opportunity oportunidad = new Opportunity();
        oportunidad.Name = 'Oportunidad Prueba';
        oportunidad.StageName = 'Nueva';
        oportunidad.CloseDate = Date.today().addDays(1);
        oportunidad.AccountId = cliente.Id;
        oportunidad.ID_Cliente_Enkontrol__c = '1234';
        insert oportunidad;
    }
    
}