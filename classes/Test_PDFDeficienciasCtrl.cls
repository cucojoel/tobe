/**
 * Created by scarlettcastillo on Oct/8/18.
 */

@IsTest
public with sharing class Test_PDFDeficienciasCtrl {

    public static testMethod void Test_PDFDeficienciasCtrl(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);
		Test.setCurrentPageReference(new PageReference('Page.PDFDeficienciasPag')); 
		System.currentPageReference().getParameters().put('id', unCaso.Id);
        Test.startTest();
		PDFDeficienciasCtrl oDefCtrl = new PDFDeficienciasCtrl();
        Test.stopTest();
    }
}