global class Batch_Schedulable_EnviaPDF_idClienteMix1 implements Schedulable {
	global void execute(SchedulableContext SC) {
		set<Id> cuentas = new set<Id>();
		list<Account> accValidas = new list<Account>();
		list<Account> accValidas2 = new list<Account>();
		
		for(Opportunity forData : Database.query(system.label.Batch_Schedulable_EnviaPDF_idClienteMix)) { // Se quito el - limit 10 -
			cuentas.add(forData.AccountId);
		}
		
		if(!cuentas.isEmpty()) {
			for(Account forData : [select Id, Edo_Cta_Ejecutado__c, (select Id from Attachments where Name like 'ECtaBG_%') from Account where Id IN : cuentas]) {
				if(forData.Attachments.size() > 0) {
					accValidas.add(forData);
				}
			}
		}
		
		if(!accValidas.isEmpty()) {
			if(accValidas.size() <  10) { for(Account forData : accValidas) { forData.Edo_Cta_Ejecutado__c = true; Database.update(accValidas, false); } }
			if(accValidas.size() >= 10) { for(integer i=0; i<10; i++) { accValidas2.add(accValidas[i]); } for(Account forData : accValidas2) { forData.Edo_Cta_Ejecutado__c = true; } Database.update(accValidas2, false); }
				
			// Mata los envios individuales
			list<CronTrigger> ctI1 = new list<CronTrigger>();
			ctI1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCte'];
			if(!ctI1.isEmpty()) { System.abortJob(ctI1[0].Id); }
			
			// Mata los envios individuales
			list<CronTrigger> ctI2 = new list<CronTrigger>();
			ctI2 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCte1'];
			if(!ctI2.isEmpty()) { System.abortJob(ctI2[0].Id); }
			
			// Mata el proceso de envio
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCteX'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Genera proceso BatchNuevo
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_EnviaPDF_idClienteMix bsPDF = new Batch_Schedulable_EnviaPDF_idClienteMix();
			String job = System.schedule('EnviaPDFCteX', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);
		}
		else {
			// Mata los envios individuales
			list<CronTrigger> ctI1 = new list<CronTrigger>();
			ctI1 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCte'];
			if(!ctI1.isEmpty()) { System.abortJob(ctI1[0].Id); }
			
			// Mata los envios individuales
			list<CronTrigger> ctI2 = new list<CronTrigger>();
			ctI2 = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCte1'];
			if(!ctI2.isEmpty()) { System.abortJob(ctI2[0].Id); }
			
			// Mata el proceso de envio
			list<CronTrigger> ct = new list<CronTrigger>();
			ct = [select Id from CronTrigger where CronJobDetail.Name = 'EnviaPDFCteX'];
			if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
			
			// Genera proceso para el ultimo envio de PDF
			DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
			Batch_Schedulable_EnviaPDF bsPDF = new Batch_Schedulable_EnviaPDF();
			String job = System.schedule('EnviaPDF', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsPDF);				
		}
	}    
}