/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Enkontrol_Hlp
* @Autor       Joel Soto
* @Date        30/12/2019
*
* @Group       BeGrand
* @Description Clase auxiliar para generar mapas de objetos
* @Changes
*/
public with sharing class Enkontrol_Hlp {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapDesarrollosEnk(setIds)
  **/
  public static Map<String, Desarrollo__c> getMapDesarrollosEnk(Set<String> setString) {
    Map<String, Desarrollo__c> mapDesarollosEnk = new Map<String, Desarrollo__c>();
    List<Desarrollo__c> listDesarrollosSf = getQuery('Desarrollo__c', 'Id_Desarrollo_Enkontrol__c', 'Desarrollo_Comercial__r.Gerente_Ventas__r.Email, ', setString);
    for (Desarrollo__c desarrollo : listDesarrollosSf) {
      mapDesarollosEnk.put(desarrollo.Id_Desarrollo_Enkontrol__c, desarrollo);
    }
    return mapDesarollosEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapCompaniasEnk(setIds)
  **/
  public static Map<String, Compania__c> getMapCompaniasEnk(Set<String> setString) {
    Map<String, Compania__c> mapCompaniasEnk = new Map<String, Compania__c>();
    List<Compania__c> listCompaniasSf = getQuery('Compania__c', 'Id_Compania_Enkontrol__c', '', setString);
    for (Compania__c compania : listCompaniasSf) {
      mapCompaniasEnk.put(compania.Id_Compania_Enkontrol__c, compania);
    }
    return mapCompaniasEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapEstados(setIds)
  **/
  public static Map<String, String> getMapEstados() {
    Map<String, String> mapEstados = new Map<String, String>();
    final String SOQLQUERY = 'SELECT ' + String.escapeSingleQuotes(Bg_Utils_Cls.getStrCampos('EquivalenciaEstados__c')) + ' FROM EquivalenciaEstados__c';
    List<EquivalenciaEstados__c> listEstadosSf = (List<EquivalenciaEstados__c>)Database.query(SOQLQUERY);
    for (EquivalenciaEstados__c edo : listEstadosSf) {
      mapEstados.put(edo.apiEnk__c, edo.Name);
    }
    return mapEstados;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapLeadsEnk(setIds)
  **/
  public static Map<String, Lead> getMapLeadsEnk(Set<String> setString) {
    Map<String, Lead> mapLeadsEnk = new Map<String, Lead>();
    final String SOQLQUERY = 'SELECT ' + String.escapeSingleQuotes(Bg_Utils_Cls.getStrCampos('Lead')) + ' FROM Lead WHERE (Bg_Id_Ws__c IN :setString OR Id IN :setString) AND IsConverted = false';
    List<Lead> listLeadsSf = (List<Lead>)Database.query(SOQLQUERY);
    for (Lead tLead : listLeadsSf) {
      mapLeadsEnk.put(tLead.Bg_Id_Ws__c, tLead);
      mapLeadsEnk.put(tLead.Id, tLead);
    }
    return mapLeadsEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapDeptosEnk(setIds)
  **/
  public static Map<String, Inmueble__c> getMapDeptosEnk(Set<String> setString) {
    Map<String, Inmueble__c> maoDeptosEnk = new Map<String, Inmueble__c>();
    List<Inmueble__c> listDeptosSf = getQuery('Inmueble__c', 'Bg_Id_Ws__c', 'Etapa__r.Desarrollo__c, ', setString);
    for (Inmueble__c deptoSf : listDeptosSf) {
      maoDeptosEnk.put(deptoSf.Bg_Id_Ws__c, deptoSf);
    }
    return maoDeptosEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapCasesEnk(setIds)
  **/
  public static Map<String, Case> getMapCasesEnk(Set<String> setString) {
    Map<String, Case> mapCasesEnk = new Map<String, Case>();
    final String SOQLQUERY = 'SELECT Entrega__r.Estatus__c, ' + String.escapeSingleQuotes(Bg_Utils_Cls.getStrCampos('Case')) + ' FROM Case WHERE Bg_Id_Ws__c IN :setString AND Status != \'Resuelto\' ORDER BY CreatedDate DESC';
    List<Case> listCasesSf = (List<Case>)Database.query(SOQLQUERY);
    for (Case caso : listCasesSf) {
      mapCasesEnk.put(caso.Bg_Id_Ws__c, caso);
    }
    return mapCasesEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapEntregasEnk(setIds)
  **/
  public static Map<String, Carta_Oferta__c> getMapEntregasEnk(Set<String> setString) {
    Map<String, Carta_Oferta__c> mapEntregasEnk = new Map<String, Carta_Oferta__c>();
    List<Carta_Oferta__c> listEntregasSf = getQuery('Carta_Oferta__c', 'Bg_Id_Ws__c', 'Inmueble__r.Name, Inmueble__r.Etapa__r.Id_Etapa_Enkontrol__c, Inmueble__r.Lote_Id__c, Inmueble__r.Nivel__c, Inmueble__r.Etapa__r.Desarrollo__r.Id_Desarrollo_Enkontrol__c, ', setString);
    for (Carta_Oferta__c entrega : listEntregasSf) {
      mapEntregasEnk.put(entrega.Bg_Id_Ws__c, entrega);
    }
    return mapEntregasEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapUsersEnk(setIds)
  **/
  public static Map<String, User> getMapUsersEnk(Set<String> setString) {
    Map<String, User> mapUsersEnk = new Map<String, User>();
    List<User> listUsersSf = getQuery('User', 'Id_Enkontrol__c', '', setString);
    for (User recUser :listUsersSf) {
      mapUsersEnk.put(recUser.Id_Enkontrol__c, recUser);
    }
    return mapUsersEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapUsersEnk(setIds)
  **/
  public static Map<Decimal, Account> getMapProvEnk(Set<Integer> setInteger) {
    Map<Decimal, Account> mapProvEnk = new Map<Decimal, Account>();
    final String SOQLQUERY = 'SELECT ' + String.escapeSingleQuotes(Bg_Utils_Cls.getStrCampos('Account')) + ' FROM Account WHERE Id_Proveedor_Enkontrol__c IN :setInteger';
    List<Account> listAcssSf = (List<Account>)Database.query(SOQLQUERY);
    for(Account acc : listAcssSf) {
      mapProvEnk.put(acc.Id_Proveedor_Enkontrol__c, acc);
    }
    return mapProvEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapEtapasEnk(setIds)
  **/
  public static Map<String, Etapa__c> getMapEtapasEnk(Set<String> setString) {
    Map<String, Etapa__c> mapEtapasEnk = new Map<String, Etapa__c>();
    final String SOQLQUERY = 'SELECT ' + String.escapeSingleQuotes(Bg_Utils_Cls.getStrCampos('Etapa__c')) + ' FROM Etapa__c WHERE Bg_Id_Ws__c IN :setString';
    List<Etapa__c> listEtapasSf = getQuery('Etapa__c', 'Bg_Id_Ws__c', '', setString);
    for(Etapa__c etapa : listEtapasSf) {
      mapEtapasEnk.put(etapa.Bg_Id_Ws__c, etapa);
    }
    return mapEtapasEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapEtapasEnk(setIds)
  **/
  public static Map<String, Opportunity> getMapOppsEnk(Set<String> setString) {
    Map<String, Opportunity> mapOppsEnk = new Map<String, Opportunity>();
    List<Opportunity> listOppsSf = getQuery('Opportunity', 'Bg_Id_Ws__c', '', setString);
    for(Opportunity opp : listOppsSf) {
      mapOppsEnk.put(opp.Bg_Id_Ws__c, opp);
    }
    return mapOppsEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapEtapasEnk(setIds)
  **/
  public static Map<String, Account> getMapAccsEnk(Set<String> setString) {
    Map<String, Account> mapAccsEnk = new Map<String, Account>();
    List<Account> listAcssSf = getQuery('Account', 'Bg_Id_Ws__c', '', setString);
    for(Account acc : listAcssSf) {
      mapAccsEnk.put(acc.Bg_Id_Ws__c, acc);
    }
    return mapAccsEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Retorna mapa del objeto con id externo como key
  *
  * @Autor       Joel Soto
  * @Date        30/12/2019
  * @Param       Set<String> setIds
  * @Param       Set<String> setIds
  * @Return      Map<String, sobject>
  * @example     getMapListCarpeta(setIds)
  **/
  public static Map<String, List<Carpeta__c>> getMapListCarpeta(Set<String> setString) {
    Map<String, List<Carpeta__c>> mapRegCar = new Map<String, List<Carpeta__c>>();
    List<Carpeta__c> regCarpetasc = getQuery('Carpeta__c', 'Bg_Id_Ws__c', '', setString);
    for (Carpeta__c carp : regCarpetasc) {
      if(mapRegCar.containsKey(carp.Bg_Id_Ws__c)) {
        List<Carpeta__c> lctemp = mapRegCar.get(carp.Bg_Id_Ws__c);
        lctemp.add(carp);
        mapRegCar.put(carp.Bg_Id_Ws__c, lctemp);
      } else {
        List<Carpeta__c> lctemp = new List<Carpeta__c>();
        lctemp.add(carp);
        mapRegCar.put(carp.Bg_Id_Ws__c, lctemp);
      }
    }
    return mapRegCar;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        09/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static Map<String, Matriz_deficiencias__c> getMapMatrizEnk(Set<String> setString) {
    Map<String, Matriz_deficiencias__c> mapMatrizEnk = new Map<String, Matriz_deficiencias__c>();
    List<Matriz_deficiencias__c> listMatrizSf = getQuery('Matriz_deficiencias__c', 'Bg_Id_Ws__c', '', setString);
    for(Matriz_deficiencias__c matriz : listMatrizSf) {
      mapMatrizEnk.put(matriz.Bg_Id_Ws__c, matriz);
    }
    return mapMatrizEnk;
  }
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Description
  *
  * @Autor       Joel Soto
  * @Date        10/01/2020
  * @Param
  * @Param
  * @Return
  * @example
  **/
  public static List<SObject> getQuery(String objectName, String keySearch, String fieldAdds, Set<String> keyValues) {
    final String SOQLQUERY = 'SELECT ' + fieldAdds + '' + String.escapeSingleQuotes(Bg_Utils_Cls.getStrCampos(objectName)) + ' FROM ' + objectName + ' WHERE ' + keySearch + ' IN :keyValues';
    List<SObject> listQuery = (List<SObject>)Database.query(SOQLQUERY);
    return listQuery;
  }
}