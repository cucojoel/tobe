/*---------------------------------------------------------------------------------------------------------
Autor: Scarlett Castillo - 08-2018 - CloudCo
Descripción: Clase Batch que llama a los servicios de Dropbox para sincronizar drop
Test Class:
---------------------------------------------------------------------------------------------------------*/
global class Batch_SincronizacionMapaSellosDropbox implements Database.Batchable<SObject>, Database.AllowsCallouts {
  public static final String query = 'SELECT Id, Mapa_Sellos__c, Mapa_Sellos_Sync__c FROM Case WHERE Mapa_Sellos__c != null AND Mapa_Sellos_Sync__c = false ORDER BY LastModifiedDate DESC limit 999';
  @TestVisible
  public Boolean makeCallout = true;
  global Database.QueryLocator start(Database.BatchableContext bc)
  {
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext bc, List<Case> scope)
  {
    Set<String> recordIds = new Set<String>();
    for(Case tmpCase : scope) {
      recordIds.add(tmpCase.Id);
    }
    if (makeCallout) {
      Map<String, List<ContentDocumentLink>> mapListCDLink = new Map<String, List<ContentDocumentLink>>();
      Map<String, Case> mapCase = new Map<String, Case>();
      Map<String, Case> mapCaseName = new Map<String, Case>();
      Map<String, ContentVersion> mapCVersionName = new Map<String, ContentVersion>();
      Set<String> setCaseUpdate = new Set<String>();
      Set<String> setCVersionIds = new Set<String>();
      List<ContentDocumentLink> listCDLink = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink where LinkedEntityId =: recordIds];
      for(ContentDocumentLink cDLink :listCDLink ) {
        setCVersionIds.add(cDLink.ContentDocumentId);
      }
      List<ContentVersion> listCVersion = [SELECT Title FROM ContentVersion WHERE ContentDocumentId in: setCVersionIds];
      Set<String> setFileNames = new Set<String>();
      for (ContentVersion cVersion : listCVersion) {
        setFileNames.add(cVersion.Title);
      }
      System.debug('The scope =>' + scope);
      for (Case scopeCase : scope) {
        if (scopeCase.Mapa_Sellos__c != null) {
          mapCase.put(scopeCase.Id, scopeCase);
          Wrapper_DropboxIntegration.ElementList elements =
            Service_DropboxIntegration.obtenerArchivosDeDropbox(scopeCase.Mapa_Sellos__c);
          if (elements.entries != null) {
            for (Wrapper_DropboxIntegration.Metadata metadata : elements.entries) {
              if (metadata.x_tag.toUpperCase() == Wrapper_DropboxIntegration.Tag.FILE.name() && !setFileNames.contains(metadata.name)) {
                Blob file = Service_DropboxIntegration.descargarArchivosDropbox(metadata.name, metadata.path_lower);
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S';
                conVer.PathOnClient = metadata.name;
                conVer.Title = metadata.name;
                conVer.VersionData = file;
                mapCaseName.put(metadata.name, scopeCase);
                mapCVersionName.put(metadata.name, conVer);
              }
            }
          }
        }
      }
      System.debug('*****mapCVersionName:' + mapCVersionName);
      System.debug('Entro a finish =>');
      List<Database.SaveResult> resultadosConsentResults = Database.insert(mapCVersionName.values(), false);
      Set<Id> successfulContentVersionIds = new Set<Id>();
      Set<Id> ucSuccsfulCVersionIds = new Set<Id>();
      for (Database.SaveResult sr : resultadosConsentResults) {
        if (sr.isSuccess()) {
          successfulContentVersionIds.add(sr.Id);
        } else {
          System.debug('*****sr.getErrors()[0]:' + sr.getErrors()[0]);
        }
      }
      List<ContentVersion> conDocs = [
                                      SELECT Id, Title, ContentDocumentId
                                      FROM ContentVersion
                                      WHERE Id IN :successfulContentVersionIds
      ];
      for (ContentVersion conDoc : conDocs) {
        Case unCaso = mapCaseName.get(conDoc.Title);
        System.debug('Detalle actual =>' + unCaso);
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = conDoc.ContentDocumentId;
        cDe.ShareType = 'I';
        cDe.Visibility = 'AllUsers';
        cDe.LinkedEntityId = unCaso.Id;
        if (mapListCDLink.containsKey(unCaso.Id)) {
          mapListCDLink.get(unCaso.Id).add(cDe);
        } else {
          List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>();
          docLinks.add(cDe);
          mapListCDLink.put(unCaso.Id, docLinks);
        }
      }
      if (!mapListCDLink.isEmpty()) {
        List<ContentDocumentLink> contents = new List<ContentDocumentLink>();
        List<Case> setCaseToUpdate = new List<Case>();
        List<List<ContentDocumentLink>> docs = mapListCDLink.values();
        for (Integer i = 0; i < docs.size(); i++) {
          List<ContentDocumentLink> listaDocs = docs[i];
          for (ContentDocumentLink unDoc : listaDocs) {
            contents.add(unDoc);
          }
        }
        System.debug('The contents to insert =>' + contents);
        List<Database.SaveResult> resultadoUpdate = Database.insert(contents, false);
        Set<Id> successfulDocumentLinks = new Set<Id>();
        for (Database.SaveResult sr : resultadoUpdate) {
          if (sr.isSuccess()) {
            successfulDocumentLinks.add(sr.Id);
          }
        }
        Set<String> linkedEntityIds = new Set<String>();
        linkedEntityIds.addAll(mapCase.keySet());
        List<ContentDocumentLink> allDocumentLinks = [
                                                      SELECT Id, LinkedEntityId
                                                      FROM ContentDocumentLink
                                                      WHERE Id IN :successfulDocumentLinks
                                                      AND LinkedEntityId IN :linkedEntityIds
        ];
        for (ContentDocumentLink elDoc : allDocumentLinks) {
          if (mapCase.containsKey(elDoc.LinkedEntityId)) {
            if (!setCaseUpdate.contains(elDoc.LinkedEntityId)) {
              Case unCaso = mapCase.get(elDoc.LinkedEntityId);
              unCaso.Mapa_Sellos_Sync__c = true;
              setCaseUpdate.add(unCaso.Id);
              setCaseToUpdate.add(unCaso);
              System.debug('Caso a actualizar =>' + unCaso);
            }
          }
        }
        if (!setCaseToUpdate.isEmpty()) {
          update setCaseToUpdate;
        }
      }
    }
  }
  global void finish(Database.BatchableContext bc)
  {
  }
  public void createObjectClass()
  {
    String name;
    String objectId;
    Boolean isActive;
    Integer amount;
    String language;
    String request;
    Boolean requestValid;
    String idRequest;
    Integer requestAttempts;
    String requestResponse;
    String requestBody;
    String requestParameter;
    Id idUserRequest;
    String messageSuccess;
    String messageFailure;
    Integer enkontrolId;
    String stage;
    String operation;
    Id idProspect;
    String prospectAgent;
    String prospectCompany;
    String prospectDes;
    String prospectEmail;
    String prospectName;
    String prospectGender;
    String prospectPhone;
    Integer prospectIdEnkontrol;
    String prospectMoral;
    String prospectLastName;
    String prospectPuntoProspectacion;
    Id prospectIdSalesforce;
    String prospectFirstName;
    String prospectStreet;
    String prospectBirthdate;
    String prospectCity;
    String prospectCountry;
    String prospectColony;
    String prospectEmail2;
    String prospectNacionality;
    String prospectPhone2;
    String prospectPhone3;
    Integer prospectZipCode;
    String prospectRFC;
    String prospectState;
    String origin;
    Integer errorCode;
    String errorMessage;
    Integer successCode;
    String successMessage;
    String opportunity;
    Id opportunityId;
    Date createdDate;
    String description;
    Boolean delivery;
    String source;
    String appartment;
    Id idAppartment;
    String assignatedToName;
    Id assignatedToId;
    String ownerName;
    Id owner;
    Id externalId;
    String reference;
    Date requestDate;
    Id parentId;
    Decimal actualCost;
    String typeRequest;
    String idDropbox;
    String specialTerms;
    String contractTerm;
    Id accountId;
    Date deadLine;
    Integer access;
    String zone;
    Integer maxTime;
    Integer minTime;
    Integer averageTime;
    Integer duration;
    String contractNumber;
    String title;
    Id contractId;
    String development;
    Boolean completed;
    String nextStep;
    Integer probability;
    String subStatus;
    String bill;
    Date activatedDate;
    Integer orderReferenceNumber;
    Id originalOrderId;
    Integer discount;
    Date expirationDate;
    String quoteToName;
    Boolean isSync;
    String alias;
    String divisionName;
    String signature;
    Id managerId;
    Id userProfileId;
    Id managerProfileId;
    String name0;
    name = (String.isBlank(name) == true ? null : name);
    String objectId0;
    objectId = (String.isBlank(objectId) == true ? null : objectId);
    Boolean isActive0;
    isActive = (String.isBlank(String.valueOf(isActive)) == true ? null : isActive);
    Integer amount0;
    amount = (String.isBlank(String.valueOf(amount)) == true ? null : amount);
    String language0;
    language = (String.isBlank(language) == true ? null : language);
    String request0;
    request = (String.isBlank(request) == true ? null : request);
    Boolean requestValid0;
    requestValid = (String.isBlank(String.valueOf(requestValid)) == true ? null : requestValid);
    String idRequest0;
    idRequest = (String.isBlank(idRequest) == true ? null : idRequest);
    Integer requestAttempts0;
    requestAttempts = (String.isBlank(String.valueOf(requestAttempts)) == true ? null : requestAttempts);
    String requestResponse0;
    requestResponse = (String.isBlank(requestResponse) == true ? null : requestResponse);
    String requestBody0;
    requestBody = (String.isBlank(requestBody) == true ? null : requestBody);
    String requestParameter0;
    requestParameter = (String.isBlank(requestParameter) == true ? null : requestParameter);
    Id idUserRequest0;
    idUserRequest = (String.isBlank(String.valueOf(idUserRequest)) == true ? null : idUserRequest);
    String messageSuccess0;
    messageSuccess = (String.isBlank(messageSuccess) == true ? null : messageSuccess);
    String messageFailure0;
    messageFailure = (String.isBlank(messageFailure) == true ? null : messageFailure);
    Integer enkontrolId0;
    enkontrolId = (String.isBlank(String.valueOf(enkontrolId)) == true ? null : enkontrolId);
    String stage0;
    stage = (String.isBlank(stage) == true ? null : stage);
    String operation0;
    operation = (String.isBlank(operation) == true ? null : operation);
    Id idProspect0;
    idProspect = (String.isBlank(String.valueOf(idProspect)) == true ? null : idProspect);
    String prospectAgent0;
    prospectAgent = (String.isBlank(prospectAgent) == true ? null : prospectAgent);
    String prospectCompany0;
    prospectCompany = (String.isBlank(prospectCompany) == true ? null : prospectCompany);
    String prospectDes0;
    prospectDes = (String.isBlank(prospectDes) == true ? null : prospectDes);
    String prospectEmail0;
    prospectEmail = (String.isBlank(prospectEmail) == true ? null : prospectEmail);
    String prospectName0;
    prospectName = (String.isBlank(prospectName) == true ? null : prospectName);
    String prospectGender0;
    prospectGender = (String.isBlank(prospectGender) == true ? null : prospectGender);
    String prospectPhone0;
    prospectPhone = (String.isBlank(prospectPhone) == true ? null : prospectPhone);
    Integer prospectIdEnkontrol0;
    prospectIdEnkontrol = (String.isBlank(String.valueOf(prospectIdEnkontrol)) == true ? null : prospectIdEnkontrol);
    String prospectMoral0;
    prospectMoral = (String.isBlank(prospectMoral) == true ? null : prospectMoral);
    String prospectLastName0;
    prospectLastName = (String.isBlank(prospectLastName) == true ? null : prospectLastName);
    String prospectPuntoProspectacion0;
    prospectPuntoProspectacion = (String.isBlank(prospectPuntoProspectacion) == true ? null : prospectPuntoProspectacion);
    Id prospectIdSalesforce0;
    prospectIdSalesforce = (String.isBlank(String.valueOf(prospectIdSalesforce)) == true ? null : prospectIdSalesforce);
    String prospectFirstName0;
    prospectFirstName = (String.isBlank(prospectFirstName) == true ? null : prospectFirstName);
    String prospectStreet0;
    prospectStreet = (String.isBlank(prospectStreet) == true ? null : prospectStreet);
    String prospectBirthdate0;
    prospectBirthdate = (String.isBlank(prospectBirthdate) == true ? null : prospectBirthdate);
    String prospectCity0;
    prospectCity = (String.isBlank(prospectCity) == true ? null : prospectCity);
    String prospectCountry0;
    prospectCountry = (String.isBlank(prospectCountry) == true ? null : prospectCountry);
    String prospectColony0;
    prospectColony = (String.isBlank(prospectColony) == true ? null : prospectColony);
    String prospectEmail20;
    prospectEmail2 = (String.isBlank(prospectEmail2) == true ? null : prospectEmail2);
    String prospectNacionality0;
    prospectNacionality = (String.isBlank(prospectNacionality) == true ? null : prospectNacionality);
    String prospectPhone20;
    prospectPhone2 = (String.isBlank(prospectPhone2) == true ? null : prospectPhone2);
    String prospectPhone30;
    prospectPhone3 = (String.isBlank(prospectPhone3) == true ? null : prospectPhone3);
    Integer prospectZipCode0;
    prospectZipCode = (String.isBlank(String.valueOf(prospectZipCode)) == true ? null : prospectZipCode);
    String prospectRFC0;
    prospectRFC = (String.isBlank(prospectRFC) == true ? null : prospectRFC);
    String prospectState0;
    prospectState = (String.isBlank(prospectState) == true ? null : prospectState);
    String origin0;
    origin = (String.isBlank(origin) == true ? null : origin);
    Integer errorCode0;
    errorCode = (String.isBlank(String.valueOf(errorCode)) == true ? null : errorCode);
    String errorMessage0;
    errorMessage = (String.isBlank(errorMessage) == true ? null : errorMessage);
    Integer successCode0;
    successCode = (String.isBlank(String.valueOf(successCode)) == true ? null : successCode);
    String successMessage0;
    successMessage = (String.isBlank(successMessage) == true ? null : successMessage);
    String opportunity0;
    opportunity = (String.isBlank(opportunity) == true ? null : opportunity);
    Id opportunityId0;
    opportunityId = (String.isBlank(String.valueOf(opportunityId)) == true ? null : opportunityId);
    Date createdDate0;
    createdDate = (String.isBlank(String.valueOf(createdDate)) == true ? null : createdDate);
    String description0;
    description = (String.isBlank(description) == true ? null : description);
    Boolean delivery0;
    delivery = (String.isBlank(String.valueOf(delivery)) == true ? null : delivery);
    String source0;
    source = (String.isBlank(source) == true ? null : source);
    String appartment0;
    appartment = (String.isBlank(appartment) == true ? null : appartment);
    Id idAppartment0;
    idAppartment = (String.isBlank(String.valueOf(idAppartment)) == true ? null : idAppartment);
    String assignatedToName0;
    assignatedToName = (String.isBlank(assignatedToName) == true ? null : assignatedToName);
    Id assignatedToId0;
    assignatedToId = (String.isBlank(String.valueOf(assignatedToId)) == true ? null : assignatedToId);
    String ownerName0;
    ownerName = (String.isBlank(ownerName) == true ? null : ownerName);
    Id owner0;
    owner = (String.isBlank(String.valueOf(owner)) == true ? null : owner);
    Id externalId0;
    externalId = (String.isBlank(String.valueOf(externalId)) == true ? null : externalId);
    String reference0;
    reference = (String.isBlank(reference) == true ? null : reference);
    Date requestDate0;
    requestDate = (String.isBlank(String.valueOf(requestDate)) == true ? null : requestDate);
    Id parentId0;
    parentId = (String.isBlank(String.valueOf(parentId)) == true ? null : parentId);
    Decimal actualCost0;
    actualCost = (String.isBlank(String.valueOf(actualCost)) == true ? null : actualCost);
    String typeRequest0;
    typeRequest = (String.isBlank(typeRequest) == true ? null : typeRequest);
    String idDropbox0;
    idDropbox = (String.isBlank(idDropbox) == true ? null : idDropbox);
    String specialTerms0;
    specialTerms = (String.isBlank(specialTerms) == true ? null : specialTerms);
    String contractTerm0;
    contractTerm = (String.isBlank(contractTerm) == true ? null : contractTerm);
    Id accountId0;
    accountId = (String.isBlank(String.valueOf(accountId)) == true ? null : accountId);
    Date deadLine0;
    deadLine = (String.isBlank(String.valueOf(deadLine)) == true ? null : deadLine);
    Integer access0;
    access = (String.isBlank(String.valueOf(access)) == true ? null : access);
    String zone0;
    zone = (String.isBlank(zone) == true ? null : zone);
    Integer maxTime0;
    maxTime = (String.isBlank(String.valueOf(maxTime)) == true ? null : maxTime);
    Integer minTime0;
    minTime = (String.isBlank(String.valueOf(minTime)) == true ? null : minTime);
    Integer averageTime0;
    averageTime = (String.isBlank(String.valueOf(averageTime)) == true ? null : averageTime);
    Integer duration0;
    duration = (String.isBlank(String.valueOf(duration)) == true ? null : duration);
    String contractNumber0;
    contractNumber = (String.isBlank(contractNumber) == true ? null : contractNumber);
    String title0;
    title = (String.isBlank(title) == true ? null : zone);
    Id contractId0;
    contractId = (String.isBlank(String.valueOf(contractId)) == true ? null : contractId);
    String development0;
    development = (String.isBlank(development) == true ? null : development);
    Boolean completed0;
    completed = (String.isBlank(String.valueOf(completed)) == true ? null : completed);
    String nextStep0;
    nextStep = (String.isBlank(nextStep) == true ? null : nextStep);
    Integer probability0;
    probability = (String.isBlank(String.valueOf(probability)) == true ? null : probability);
    String subStatus0;
    subStatus = (String.isBlank(subStatus) == true ? null : subStatus);
    String bill0;
    bill = (String.isBlank(bill) == true ? null : bill);
    Date activatedDate0;
    activatedDate = (String.isBlank(String.valueOf(activatedDate)) == true ? null : activatedDate);
    Integer orderReferenceNumber0;
    orderReferenceNumber = (String.isBlank(String.valueOf(orderReferenceNumber)) == true ? null : orderReferenceNumber);
    Id originalOrderId0;
    originalOrderId = (String.isBlank(String.valueOf(originalOrderId)) == true ? null : originalOrderId);
    Integer discount0;
    discount = (String.isBlank(String.valueOf(discount)) == true ? null : discount);
    Date expirationDate0;
    expirationDate = (String.isBlank(String.valueOf(expirationDate)) == true ? null : expirationDate);
    String quoteToName0;
    quoteToName = (String.isBlank(quoteToName) == true ? null : quoteToName);
    Boolean isSync0;
    isSync = (String.isBlank(String.valueOf(isSync)) == true ? null : isSync);
    String alias0;
    alias = (String.isBlank(alias) == true ? null : alias);
    String divisionName0;
    divisionName = (String.isBlank(divisionName) == true ? null : divisionName);
    String signature0;
    signature = (String.isBlank(signature) == true ? null : signature);
    Id managerId0;
    managerId = (String.isBlank(String.valueOf(managerId)) == true ? null : managerId);
    Id userProfileId0;
    userProfileId = (String.isBlank(String.valueOf(userProfileId)) == true ? null : userProfileId);
    Id managerProfileId0;
    managerProfileId = (String.isBlank(String.valueOf(managerProfileId)) == true ? null : managerProfileId);
  }
}