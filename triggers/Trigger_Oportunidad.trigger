/*
	Metodo de prueba:	TEST_Oportunidad
*/
trigger Trigger_Oportunidad on Opportunity (after update, before delete, before update) {

  /*
    if(trigger.isBefore) {
        if(trigger.isDelete) {
            CLASS_Oportunidad claseOportunidad = new CLASS_Oportunidad();
            claseOportunidad.eliminaAttachments(Trigger.oldMap);
        }
    }
    */
    /*
        MQD - 23-Agosto-2019
        Cuando una Oportunidad pase a Cerrada Ganada se debe de asociar en la Cuenta, Oportunidad y Departamento, esto es, que los tres registros tengan su campo de busqueda hacia los demas
        IMPORTANTE: Ya se mando a Produccion pero en Sandbox se quitara para hacer modificacion a Departamento
        IMPORTANTE: Esta funcionalidad quedo obsoleta ya que se cambio a Departamento
    */
    /*
    if(trigger.isBefore) {
        if(trigger.isUpdate) {
            list<Inmueble__c> departamentosAconsultar = null;
            map<Id, Id> oportunidad_cuenta = new map<Id, Id>();
            map<Id, Id> departamento_oportunidad = new map<Id, Id>();

            if(system.label.AsociaCuentaOportunidadDepartamento == 'Si') {
                for(Opportunity forData : trigger.new) {
                    if(trigger.oldMap.get(forData.Id).StageName != null) {
                    	system.debug('Etapa: ' + forData.StageName);
                    	system.debug('Cuenta: ' + forData.AccountId);
                    	system.debug('Departamento: ' + forData.Departamento__c);
                    	system.debug('Sincronizado: ' + forData.Sincronizados__c);
                        if(forData.StageName != trigger.oldMap.get(forData.Id).StageName && forData.StageName == 'Vendido' && forData.AccountId != null && forData.Departamento__c != null && forData.Sincronizados__c > 0) {
                            if(!departamento_oportunidad.containsKey(forData.Departamento__c)) {
                                departamento_oportunidad.put(forData.Departamento__c, forData.Id);
                            }
                            if(!oportunidad_cuenta.containsKey(forData.Id)) {
                                oportunidad_cuenta.put(forData.Id, forData.AccountId);
                            }
                        }
                        // else { forData.addError('Trigger Opportunity. El registro no puede pasar a Vendido si la Oportunidad no tiene Cliente, Departamento asociado o Carpetas sincronizadas'); }
                    }
                }

                // Se consultara la informacion del Departamento para asociar los valores
                departamentosAconsultar = [select Id, Cliente__c from Inmueble__c where Id IN : departamento_oportunidad.keySet()];
                for(Inmueble__c forData : departamentosAconsultar) {
                    forData.Cliente__c = oportunidad_cuenta.get(departamento_oportunidad.get(forData.Id));
                }
                update departamentosAconsultar;
            }
        }
    }
    */

    /*
       SFDC - BG - Generacion asincrona de los Estados de Cuenta
       IMPORTANTE: Obsoleto y se cambio a Departamento
    */
    /*
    if(trigger.isAfter) {
        if(trigger.isUpdate) {
            for(Opportunity forData : trigger.new) {
                if(system.label.AutorizaWSEdoCtaMasivo == 'Si') {
                    if(trigger.oldMap.get(forData.Id).Edo_Cta_Ejecutado__c == false && forData.Edo_Cta_Ejecutado__c == true) {
                        // Esto no se puede masivo
                        EnviarPorCorreoMasivo.SavePdf(forData.Id);
                    }
                }
            }
        }
    }
    */
}