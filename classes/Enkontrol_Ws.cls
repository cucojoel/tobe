/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Enkontrol_Ws
* @Autor       Joel Soto
* @Date        30/12/2019
*
* @Group       BeGrand
* @Description Descripton
* @Changes
*/
global with sharing class Enkontrol_Ws {
  webService static List<Enkontrol_Ws.SaveResult> actualizacionProspectos(List<Enkontrol_Ws.Prospecto> prospecto) {
    return Enkontrol_Hdr.actualizacionProspectosHandler(prospecto);
  }
  webService static List<Enkontrol_Ws.SaveResult> actualizacionOportunidades(List<Enkontrol_Ws.Oportunidad> oportunidad) {
    return Enkontrol_Hdr.actualizacionOportunidadesHandler(oportunidad);
  }
  webService static List<Enkontrol_Ws.SaveResult> actualizacionProveedores(List<Enkontrol_Ws.Proveedor> proveedor) {
    return Enkontrol_Hdr.actualizacionProveedoresHandler(proveedor);
  }
  webService static List<Enkontrol_Ws.SaveResultEstatus> actualizaEstatusDepto(List<Enkontrol_Ws.ActualizarEstatusDepto> departamento) {
    return Enkontrol_Hdr.actualizarEstatusDeptoHandler(departamento);
  }
  webService static List<Enkontrol_Ws.SaveResult> bienvenidaCliente(List<Enkontrol_Ws.Caso> caso) {
    return Enkontrol_Hdr.creacionDeficienciasHandler(caso);
  }
  webService static List<Enkontrol_Ws.SaveResult> registroOportunidades(List<Enkontrol_Ws.Oportunidad> oportunidad) {
    return Enkontrol_Hdr.creacionOportunidadesHandler(oportunidad);
  }
  webService static List<Enkontrol_Ws.SaveResult> registroProveedores(List<Enkontrol_Ws.Proveedor> proveedor) {
    return Enkontrol_Hdr.creacionProveedoresHandler(proveedor);
  }
  global class SaveResult {
    webService List<SaveResult> childs {get; set;}
    webService String errorMsg {get; set;}
    webService String sfId {get; set;}
    webService Boolean success {get; set;}
    webService String wsId {get; set;}
    global SaveResult() {
      childs = new List<SaveResult>();
      errorMsg = '';
      sfId = '';
      success = false;
      wsId = '';
    }
  }
  global class SaveResultEstatus {
    webService String desarrolloId {get; set;}
    webService String errorMsg {get; set;}
    webService String loteId {get; set;}
    webService String sfId {get; set;}
    webService Boolean success {get; set;}
    global SaveResultEstatus() {
      desarrolloId = '';
      errorMsg = '';
      loteId = '';
      sfId = '';
      success = false;
    }
  }
  global class Caso {
    webService String actasDbxUrl {get; set;}
    webService String checklistDbxUrl {get; set;}
    webService List<Falla> fallas {get; set;}
    webService Integer folio {get; set;}// idEnkontrol unico unico por compañia
    webService Integer idAppOrigen {get; set;}//Beobra-BeGrid
    webService Integer idAsignadoObra {get; set;}//id usuario
    webService Integer idCoordinadorSC {get; set;}//id usuario
    webService String idDesarrollo {get; set;}
    webService Integer idEmpleado {get; set;}//id usuario
    webService String idEtapa {get; set;}//torre
    webService Integer idEtapaEntrega {get; set;}//1 2 3
    webService String idInmueble {get; set;}// num depto
    webService String idLote {get; set;}// lote depto id enk
    webService String idPiso {get; set;}//
    webService Integer idSupervisor {get; set;}//id usuario
    webService String sellosDbxUrl {get; set;}
    webService String sfId {get; set;}
    public String sfIdAccount {get; set;}
    public String sfIdInmueble {get; set;}
    public String sfIdEntrega {get; set;}
    global Caso() {
      actasDbxUrl = '';
      checklistDbxUrl = '';
      fallas = new List<Falla>();
      folio = 0;
      idAppOrigen = 0;
      idAsignadoObra = 0;
      idCoordinadorSC = 0;
      idDesarrollo = '';
      idEmpleado = 0;
      idEtapa = '';
      idEtapaEntrega = 0;
      idInmueble = '';
      idLote = '';
      idPiso = '';
      idSupervisor = 0;
      sellosDbxUrl = '';
      sfId = '';
      sfIdAccount = '';
      sfIdInmueble = '';
      sfIdEntrega = '';
    }
  }
  global class Falla {
    webService String correoPersonaEntrega {get; set;}
    webService String dbxFolderUrl {get; set;}
    webService String descripcion {get; set;}
    webService Boolean esCuadrilla {get; set;}
    webService Boolean esEmergente {get; set;}
    webService String especialidad {get; set;}
    webService Integer idEspecialidad {get; set;}
    webService String idFallaAnterior {get; set;}// entero de partida anterior
    webService Integer idProveedor {get; set;}
    webService Integer idSubespecialidad {get; set;}
    webService Integer idZona {get; set;}
    webService Integer numeroSello {get; set;}
    webService String personaEntrega {get; set;}
    webService String proveedor {get; set;}
    webService String sfId {get; set;}//sf id
    webService String sfParentId {get; set;}
    webService String subespecialidad {get; set;}
    webService String tipo {get; set;}
    webService String zona {get; set;}
    public String idEtapa {get; set;}//torre
    global Falla() {
      correoPersonaEntrega = '';
      dbxFolderUrl = '';
      descripcion = '';
      esCuadrilla = false;
      esEmergente = false;
      especialidad = '';
      idEspecialidad = 0;
      idFallaAnterior = '';
      idProveedor = 0;
      idSubespecialidad = 0;
      idZona = 0;
      numeroSello = 0;
      personaEntrega = '';
      proveedor = '';
      sfId = '';
      sfParentId = '';
      subespecialidad = '';
      tipo = '';
      zona = '';
      idEtapa = '';
    }
  }
  global class Proveedor {
    webService Integer idProveedor {get; set;}
    webService String nombreProveedor {get; set;}
    global Proveedor() {
      idProveedor = 0;
      nombreProveedor = '';
    }
  }
  global class Prospecto {
    webService Integer agente {get; set;}
    webService String apellidoMaterno {get; set;}
    webService String apellidoPaterno {get; set;}
    webService String calleYNumero {get; set;}
    webService String ciudad {get; set;}
    webService String colonia {get; set;}
    webService String compania {get; set;}
    webService String cp {get; set;}
    webService String desarrollo {get; set;}
    webService String email1 {get; set;}
    webService String email2 {get; set;}
    webService String idEstatus {get; set;}
    webService String empresa {get; set;}
    webService String estado {get; set;}
    webService String fechaAlta {get; set;}
    webService String fechaDeNacimiento {get; set;}
    webService Integer idEnkontrol {get; set;}
    webService String idGroup {get; set;}
    webService String idSalesforce {get; set;}
    webService String medio {get; set;}
    webService String nacionalidad {get; set;}
    webService String nombre {get; set;}
    webService String pais {get; set;}
    webService Boolean personaMoral {get; set;}
    webService Integer puntoProspectacion {get; set;}
    webService String rfc {get; set;}
    webService String sexo {get; set;}
    webService String telefono1 {get; set;}
    webService String telefono2 {get; set;}
    webService String telefono3 {get; set;}
    global Prospecto() {
      agente = 0;
      apellidoMaterno = '';
      apellidoPaterno = '';
      calleYNumero = '';
      ciudad = '';
      colonia = '';
      compania = '';
      cp = '';
      desarrollo = '';
      email1 = '';
      email2 = '';
      empresa = '';
      estado = '';
      fechaAlta = '';
      fechaDeNacimiento = '';
      idEnkontrol = 0;
      idGroup = '';
      idSalesforce = '';
      medio = '';
      nacionalidad = '';
      nombre = '';
      pais = '';
      personaMoral = false;
      puntoProspectacion = 0;
      rfc = '';
      sexo = '';
      telefono1 = '';
      telefono2 = '';
      telefono3 = '';
    }
  }
  global class Oportunidad {
    webService Integer agente {get; set;}
    webService String compania {get; set;}
    webService List<Documento> dbxUrls {get; set;}
    webService String desarrollo {get; set;}
    webService Boolean esRenta {get; set;}
    webService String estatus {get; set;}
    webService String etapa {get; set;}
    webService Integer gradoDeInteres {get; set;}
    webService Integer idEnkontrol {get; set;}
    webService String idGroup {get; set;}
    webService Integer idProspecto {get; set;}
    webService String idSalesforce {get; set;}
    webService String loteId {get; set;}
    webService Integer metodoPago {get; set;}
    webService String montoApartado {get; set;}
    webService String montoEnganche {get; set;}
    webService String montoTransaccion {get; set;}
    webService String motivoCancelacion {get; set;}
    webService Integer numeroCliente {get; set;}
    webService String observaciones {get; set;}
    webService String precioFianza {get; set;}
    webService String precioRenta {get; set;}
    webService Integer puntoProspectacion {get; set;}
    webService String referencia {get; set;}
    webService String totalRenta {get; set;}
    global Oportunidad() {
      agente = 0;
      compania = '';
      dbxUrls = new List<Documento>();
      desarrollo = '';
      esRenta = false;
      estatus = '';
      etapa = '';
      gradoDeInteres = 0;
      idEnkontrol = 0;
      idGroup = '';
      idProspecto = 0;
      idSalesforce = '';
      loteId = '';
      metodoPago = 0;
      montoApartado = '';
      montoEnganche = '';
      montoTransaccion = '';
      motivoCancelacion = '';
      numeroCliente = 0;
      observaciones = '';
      precioFianza = '';
      precioRenta = '';
      puntoProspectacion = 0;
      referencia = '';
      totalRenta = '';
    }
  }
  global class Documento {
    webService Integer docType {get; set;}
    webService String enviadoPor {get; set;}
    public String sfParentId {get; set;}
    webService String urlCarpeta {get; set;}
    global Documento() {
      docType = 0;
      enviadoPor = '';
      urlCarpeta = '';
      sfParentId = '';
    }
  }
  global class CartaOferta {
    webService Integer idDocumento {get; set;}
    webService Integer idEnkontrol {get; set;}
    webService String idOppSf {get; set;}
    webService String urlDropbox {get; set;}
    global CartaOferta() {
      idDocumento = 0;
      idEnkontrol = 0;
      idOppSf = '';
      urlDropbox = '';
    }
  }
  global class ActualizarEstatusDepto {
    webService String desarrollo {get; set;}
    webService String estatus {get; set;}
    webService String loteId {get; set;}
    global ActualizarEstatusDepto() {
      desarrollo = '';
      estatus = '';
      loteId = '';
    }
  }
  global class Matriz {
    webService String appOrigen {get; set;}
    webService String asignaObra {get; set;}
    webService String cuadrilla {get; set;}
    webService String especialidad {get; set;}
    webService String etapa {get; set;}
    webService String idEtapaEnkontrol {get; set;}
    webService String idDesarEnkontrol {get; set;}
    webService String etapaEntrega {get; set;}
    webService String idEspecEnkontrol {get; set;}
    webService String idSubesEnkontrol {get; set;}
    webService String idZonaEnkontrol {get; set;}
    webService String proveedorEmergente {get; set;}
    webService String proveedor {get; set;}
    webService String responsableCuadrilla {get; set;}
    webService String idProveEnkontrol {get; set;}
    webService String subespecialidad {get; set;}
    webService String zona {get; set;}
    global Matriz() {
      appOrigen = '';
      asignaObra = '';
      cuadrilla = '';
      especialidad = '';
      etapa = '';
      etapaEntrega = '';
      idDesarEnkontrol = '';
      idEspecEnkontrol = '';
      idEtapaEnkontrol = '';
      idProveEnkontrol = '';
      idSubesEnkontrol = '';
      idZonaEnkontrol = '';
      proveedor = '';
      proveedorEmergente = '';
      responsableCuadrilla = '';
      subespecialidad = '';
      zona = '';
    }
  }
}