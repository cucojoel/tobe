@IsTest
public class Service_WS_EnkontrolIntegration_Bis_Test {
    
    static testMethod void wsService0() {
        List<WS_EnkontrolIntegration.Fallas> listFallas = new List<WS_EnkontrolIntegration.Fallas>();
        WS_EnkontrolIntegration.Fallas falla1 = new WS_EnkontrolIntegration.Fallas();
        falla1.idFalla                = null;
        falla1.zona                   = null;
        falla1.idZona                 = null;
        falla1.especialidad           = null;
        falla1.idEspecialidad         = null;
        falla1.subespecialidad        = null;
        falla1.idSubespecialidad      = null;
        falla1.idInmueble             = null;
        falla1.idEtapa                = null;
        falla1.idDesarrollo           = null;
        falla1.idLote                 = null;
        falla1.idPiso                 = null;
        falla1.numeroSello            = null;
        falla1.estatus                = null;
        falla1.existe                 = null;
        falla1.proveedor              = null;
        falla1.idProveedor            = null;
        falla1.esCuadrilla            = null;
        falla1.esProveedorEmergente   = null;
        falla1.idFallaAnterior        = null;
        falla1.tipo                   = null;
        falla1.descripcion            = null;
        falla1.dropboxFolderUrl       = null;
        falla1.checklistDropboxUrl    = null;
        falla1.folio                  = null;
        falla1.idEmpleado             = null;
        falla1.idAplicacionOrigen     = null;
        falla1.idEtapaEntrega         = null;
        falla1.sellosDropboxFolderUrl = null;
        falla1.idSupervisor           = null;
        falla1.PersonaEntrega         = null;
        falla1.correoPersonaEntrega   = null;
        listFallas.add(falla1);
        Test.startTest();
            List<WS_EnkontrolIntegration.SaveResult> rsltsDeficiencias = WS_EnkontrolIntegration.bienvenidaCliente(listFallas);
            Service_WS_EnkontrolIntegration_Bis.getMyAccounts();
            Service_WS_EnkontrolIntegration_Bis controller_0  = new Service_WS_EnkontrolIntegration_Bis();
            controller_0.createObjectClassFifth();
        Test.stopTest();            
    }
    
    
    
    static testMethod void wsService1() {
        
        Account cuenta = new Account();
        cuenta.LastName = 'Melquides';
        cuenta.FirstName = 'Marco';
        cuenta.PersonEmail = 'test@gmail.com';
        cuenta.RFC__pc = 'CASJ811111RD9';
        insert cuenta;
        
        Account cliente = Test_Utilities.crearClientePrueba();
        
        Contact contacto = [SELECT Id FROM Contact WHERE AccountId =: cuenta.Id LIMIT 1];
        
        Desarrollo_Comercial__c unDesarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        
        Time myTime = Time.newInstance(1, 2, 3, 4);
        
        Desarrollo__c unDesarrollo = new Desarrollo__c();
        unDesarrollo.Name = 'Alto Polanco';
        unDesarrollo.Id_Desarrollo_Enkontrol__c = 'AP12';
        unDesarrollo.Desarrollo_Comercial__c = unDesarrolloComercial.Id;
        unDesarrollo.Inicio__c = myTime;
        insert unDesarrollo;
        
        Etapa__c unaEtapa = new Etapa__c();
        unaEtapa.Name = 'Torre 2';
        unaEtapa.Id_Etapa_Enkontrol__c = '203';
        unaEtapa.No_de_Pisos__c = 23;
        unaEtapa.Desarrollo__c = unDesarrollo.Id;
        insert unaEtapa;
        
        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba('102', unDesarrolloComercial.Id, '1', 'AP12');
        unInmueble.Nivel__c = '11';
        update unInmueble;
        
        Carta_Oferta__c cEntrega = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);
        cEntrega.Estatus__c = 'Inventario';
        update cEntrega;

        Case nCase = Test_Utilities.crearCaso(cEntrega.Id, cliente.Id);
        
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(nCase.Id);
        detalle.Id_Falla_WS__c = '12345AP12';
        update detalle;
        
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();
        
        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);
        
        Test.setMock(HttpCalloutMock.class, multipleMock);
        
        test.startTest();
        
        test.stopTest();
    }
    
    
    
    static testMethod void wsService2() {
        List<WS_EnkontrolIntegration.Fallas> listFallas = new List<WS_EnkontrolIntegration.Fallas>();
        Map<String, Object> mapaMocks = new Map<String, Object>();
        
        Desarrollo_Comercial__c unDesarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        Inmueble__c unInmueble = Test_Utilities.crearInmueblePrueba('102', unDesarrolloComercial.Id, '1', 'AP12');
        unInmueble.Nivel__c = '11';
        update unInmueble;
        
        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        
        Account cliente = Test_Utilities.crearClientePrueba();
        
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);
        entrega.Estatus__c = 'Inventario';
        update entrega;

        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);
        detalle.Id_Falla_WS__c = '12345';
        update detalle;
        
        Test_Utilities.crearEntregaPrueba(unInmueble.Id, cliente.Id);
        Test_Utilities.testCrearConfiguracionPersonalizada();
        /** Crear datos de prueba */
        List<User> supervision = [
            SELECT Id, Name, Username, Id_Enkontrol__c
            FROM User
            WHERE Username = 'gerentesupervision@begrand.test'
            LIMIT 1
        ];
        List<User> servicioCliente = [
            SELECT Id, Name, Username, Id_Enkontrol__c
            FROM User
            WHERE Username = 'coordinadoruser@begrand.test'
            LIMIT 1
        ];
        //FALLA 1
        WS_EnkontrolIntegration.Fallas falla1 = new WS_EnkontrolIntegration.Fallas();
        falla1.idFalla = 'FALLA-001';
        falla1.zona = 'Cocina';
        falla1.idZona = 1223;
        falla1.especialidad = 'GRANITO';
        falla1.idEspecialidad = 3314;
        falla1.subespecialidad = 'CUBIERTA';
        falla1.idSubespecialidad = 9902;
        falla1.idInmueble = '1408';
        falla1.idEtapa = '1';
        falla1.idDesarrollo = 'AP12';
        falla1.idLote = '102';
        falla1.idPiso = '11';
        falla1.proveedor = proveedor.Name;
        falla1.idProveedor = Integer.valueOf(proveedor.Id_Proveedor_Enkontrol__c);
        falla1.esCuadrilla = false;
        falla1.esProveedorEmergente = false;
        falla1.idFallaAnterior = null;
        falla1.tipo = 'D';
        falla1.descripcion = 'Description test';
        falla1.dropboxFolderUrl = '/TEST/001';
        falla1.checklistDropboxUrl = '/TEST/001';
        falla1.folio = 122;
        falla1.idEmpleado = 111;
        falla1.idAplicacionOrigen = 1;
        falla1.idEtapaEntrega = 1;
        falla1.sellosDropboxFolderUrl = '/TEST/001';
        falla1.PersonaEntrega = 'Prueba Prueba';
        falla1.correoPersonaEntrega = 'prueba@prueba.com';
        listFallas.add(falla1);
        //FALLA 2
        WS_EnkontrolIntegration.Fallas falla2 = new WS_EnkontrolIntegration.Fallas();
        falla2.idFalla = 'FALLA-002';
        falla2.zona = 'Cocina';
        falla2.idZona = 1223;
        falla2.especialidad = 'GAS';
        falla2.idEspecialidad = 9912;
        falla2.subespecialidad = 'TUBERIAS';
        falla2.idSubespecialidad = 9921;
        falla2.idInmueble = '1408';
        falla2.idEtapa = '1';
        falla2.idDesarrollo = 'AP12';
        falla2.idLote = '102';
        falla2.idPiso = '11';
        falla2.proveedor = 'test proveedor';
        falla2.idProveedor = 0;
        falla2.esCuadrilla = true;
        falla2.esProveedorEmergente = false;
        falla2.idFallaAnterior = null;
        falla2.tipo = 'D';
        falla2.descripcion = 'Description test';
        falla2.folio = 122;
        falla2.idAplicacionOrigen = 1;
        falla2.idEtapaEntrega = 2;
        falla2.sellosDropboxFolderUrl = '/TEST/001';
        falla2.checklistDropboxUrl = '/TEST/001';
        falla2.PersonaEntrega = 'Prueba Prueba';
        falla2.correoPersonaEntrega = 'prueba@prueba.com';
        listFallas.add(falla2);
        //FALLA 3
        WS_EnkontrolIntegration.Fallas falla3 = new WS_EnkontrolIntegration.Fallas();
        falla3.idFalla = 'FALLA-002';
        falla3.zona = 'Cocina';
        falla3.idZona = 1223;
        falla3.especialidad = 'GAS';
        falla3.idEspecialidad = 9912;
        falla3.subespecialidad = 'TUBERIAS';
        falla3.idSubespecialidad = 9921;
        falla3.idInmueble = '1408';
        falla3.idEtapa = '1';
        falla3.idDesarrollo = 'AP12';
        falla3.idLote = '102';
        falla3.idPiso = '11';
        falla3.proveedor = 'test proveedor';
        falla3.idProveedor = 0;
        falla3.esCuadrilla = true;
        falla3.esProveedorEmergente = false;
        falla3.idFallaAnterior = null;
        falla3.tipo = 'D';
        falla3.descripcion = 'Description test';
        falla3.folio = 122;
        falla3.idAplicacionOrigen = 1;
        falla3.idEtapaEntrega = 3;
        falla3.sellosDropboxFolderUrl = '/TEST/001';
        falla3.checklistDropboxUrl = '/TEST/001';
        falla3.personaEntrega = 'Prueba Prueba';
        falla3.correoPersonaEntrega = 'prueba@prueba.com';
        listFallas.add(falla3);
        
        
        Test.startTest();
        List<WS_EnkontrolIntegration.SaveResult> results = WS_EnkontrolIntegration.bienvenidaCliente(listFallas);
        falla2.idInmueble = '1222';
        falla2.idLote = '102';
        results = WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas>{falla2});
        falla1.idEmpleado = 554;
        results = WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas>{falla1});
        Test.stopTest();
    }
    
    static testMethod void wsService3() {
        List<WS_EnkontrolIntegration.Fallas> listFallas = new List<WS_EnkontrolIntegration.Fallas>();
        Map<String, Object> mapaMocks = new Map<String, Object>();
        
        Desarrollo_Comercial__c unDesarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());
        Time myTime = Time.newInstance(1, 2, 3, 4);
        
        Desarrollo__c unDesarrollo = new Desarrollo__c();
        unDesarrollo.Name = 'ALTO PEDREGAL';
        unDesarrollo.Id_Desarrollo_Enkontrol__c = 'AP12';
        unDesarrollo.Desarrollo_Comercial__c = unDesarrolloComercial.Id;
        unDesarrollo.Inicio__c = myTime;
        insert unDesarrollo;
        Etapa__c unaEtapa = new Etapa__c();
        unaEtapa.Name = 'Torre 2';
        unaEtapa.Id_Etapa_Enkontrol__c = '1';
        unaEtapa.No_de_Pisos__c = 23;
        unaEtapa.Desarrollo__c = unDesarrollo.Id;
        insert unaEtapa;
        Inmueble__c unInmueble = new Inmueble__c();
        unInmueble.Name = '102';
        unInmueble.Lote_Id__c = '102';
        unInmueble.Area_depto__c = 72;
        unInmueble.Area_Total__c = 99;
        unInmueble.Estacionamientos__c = 2;
        unInmueble.Etapa__c = unaEtapa.Id;
        unInmueble.Nivel__c = '1';
        insert unInmueble;
        Account proveedor = Test_Utilities.crearProveedorPrueba(1222, 'KVAN SA DE CV');
        Account cliente = Test_Utilities.crearClientePrueba();
        Carta_Oferta__c cartaOferta = new Carta_Oferta__c();
        cartaOferta.Inmueble__c = unInmueble.Id;
        cartaOferta.Cliente__c = cliente.Id;
        cartaOferta.Estatus__c = 'Entrega';
        insert cartaOferta;
        Inmueble__c unInmuebleDos = new Inmueble__c();
        unInmuebleDos.Name = '103';
        unInmuebleDos.Lote_Id__c = '103';
        unInmuebleDos.Area_depto__c = 72;
        unInmuebleDos.Area_Total__c = 99;
        unInmuebleDos.Estacionamientos__c = 2;
        unInmuebleDos.Etapa__c = unaEtapa.Id;
        unInmuebleDos.Nivel__c = '1';
        insert unInmuebleDos;
        Carta_Oferta__c cartaOfertaDos = new Carta_Oferta__c();
        cartaOfertaDos.Inmueble__c = unInmuebleDos.Id;
        cartaOfertaDos.Cliente__c = cliente.Id;
        cartaOfertaDos.Estatus__c = 'Inventario';
        insert cartaOfertaDos;
        Test_Utilities.testCrearConfiguracionPersonalizada();
        /** Crear datos de prueba */
        List<User> supervision = [
            SELECT Id, Name, Username, Id_Enkontrol__c
            FROM User
            WHERE Username = 'gerentesupervision@begrand.test'
            LIMIT 1
        ];
        List<User> servicioCliente = [
            SELECT Id, Name, Username, Id_Enkontrol__c
            FROM User
            WHERE Username = 'coordinadoruser@begrand.test'
            LIMIT 1
        ];
        List<User> asignadoObra = [
            SELECT Id, Name, Username, Id_Enkontrol__c
            FROM User
            WHERE Username = 'communityUser@begrand.test'
            LIMIT 1
        ];
        Matriz_Deficiencias__c md = Test_Utilities.crearMatrizPrueba(1223, 'Cocina', 3314, 'GRANITO', 9902, 'CUBIERTA', unInmueble.Etapa__c, proveedor.Id, false);
        md.Etapa_Entrega__c = '1';
        update md;
        Matriz_Deficiencias__c md1 = Test_Utilities.crearMatrizPrueba(1223, 'Cocina', 3315, 'GRANITO', 9902, 'CUBIERTA', unInmueble.Etapa__c, proveedor.Id, false);
        md1.Etapa_Entrega__c = '2';
        update md1;
        //FALLA 1
        WS_EnkontrolIntegration.Fallas falla1 = new WS_EnkontrolIntegration.Fallas();
        falla1.idFalla = 'FALLA-001';
        falla1.zona = 'Cocina';
        falla1.idZona = 1223;
        falla1.idEtapaEntrega = 1;
        falla1.especialidad = 'GRANITO';
        falla1.idEspecialidad = 3314;
        falla1.subespecialidad = 'CUBIERTA';
        falla1.idSubespecialidad = 9902;
        falla1.idInmueble = '102';
        falla1.idEtapa = '1';
        falla1.proveedor = proveedor.Name;
        falla1.idDesarrollo = 'AP12';
        falla1.idLote = '102';
        falla1.idPiso = unInmueble.Nivel__c;
        falla1.idProveedor = Integer.valueOf(proveedor.Id_Proveedor_Enkontrol__c);
        falla1.esCuadrilla = false;
        falla1.esProveedorEmergente = false;
        falla1.idFallaAnterior = null;
        falla1.tipo = 'O';
        falla1.descripcion = 'Description test';
        falla1.dropboxFolderUrl = '/TEST/001';
        falla1.PersonaEntrega = 'Prueba Prueba';
        falla1.correoPersonaEntrega = 'prueba@prueba.com';
        
        if(supervision != null && !supervision.isEmpty()){
            falla1.idSupervisor = Integer.valueOf(supervision.get(0).Id_Enkontrol__c);
        }
        if(asignadoObra != null && !asignadoObra.isEmpty()){
            falla1.idAsignadoObra = Integer.valueOf(asignadoObra.get(0).Id_Enkontrol__c);
        }
        listFallas.add(falla1);
        //FALLA 2
        WS_EnkontrolIntegration.Fallas falla2 = new WS_EnkontrolIntegration.Fallas();
        falla2.idFalla = 'FALLA-002';
        falla2.zona = 'Cocina';
        falla2.idZona = 1223;
        falla2.idEtapaEntrega = 2;
        falla2.especialidad = 'GAS';
        falla2.idEspecialidad = 3315;
        falla2.subespecialidad = 'TUBERIAS';
        falla2.idSubespecialidad = 9902;
        falla2.idInmueble = '102';
        falla2.idEtapa = '1';
        falla2.idDesarrollo = 'AP12';
        falla2.idLote = '102';
        falla2.proveedor = 'test proveedor';
        falla2.idProveedor = 9902;
        falla2.esCuadrilla = true;
        falla2.esProveedorEmergente = false;
        falla2.idFallaAnterior = null;
        falla2.tipo = 'O';
        falla2.idPiso = unInmuebleDos.Nivel__c;
        falla2.descripcion = 'Description test';
             falla2.PersonaEntrega = 'Prueba Prueba';
        falla2.correoPersonaEntrega = 'prueba@prueba.com';
        //FALLA 3
        WS_EnkontrolIntegration.Fallas falla3 = new WS_EnkontrolIntegration.Fallas();
        falla3.idFalla = 'FALLA-003';
        falla3.zona = 'Cocina';
        falla3.idZona = 1223;
        falla3.especialidad = 'GAS';
        falla3.idEspecialidad = 9912;
        falla3.subespecialidad = 'TUBERIAS';
        falla3.idSubespecialidad = 9921;
        falla3.idInmueble = '1408';
        falla3.idEtapa = '1';
        falla3.idDesarrollo = 'AP12';
        falla3.idLote = '102';
        falla3.idPiso = '11';
        falla3.proveedor = 'test proveedor';
        falla3.idProveedor = 0;
        falla3.esCuadrilla = true;
        falla3.esProveedorEmergente = false;
        falla3.idFallaAnterior = null;
        falla3.tipo = 'O';
        falla3.descripcion = 'Description test';
        falla3.folio = 122;
        falla3.idAplicacionOrigen = 1;
        falla3.sellosDropboxFolderUrl = '/TEST/001';
        falla3.checklistDropboxUrl = '/TEST/001';
             falla3.PersonaEntrega = 'Prueba Prueba';
        falla3.correoPersonaEntrega = 'prueba@prueba.com';
        listFallas.add(falla3);
        
        
        if(supervision != null && !supervision.isEmpty()){
            falla2.idSupervisor = Integer.valueOf(supervision.get(0).Id_Enkontrol__c);
        }
        if(servicioCliente != null && !servicioCliente.isEmpty()){
            falla2.idCoordinadorSC = Integer.valueOf(servicioCliente.get(0).Id_Enkontrol__c);
        }
        listFallas.add(falla2);
        Test.startTest();
        List<WS_EnkontrolIntegration.SaveResult> results = WS_EnkontrolIntegration.bienvenidaCliente(listFallas);

        falla1.idEtapaEntrega = 1;
        results = WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas>{falla1});

        falla2.idEtapaEntrega = 2;
        results = WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas>{falla2});

        falla3.idEtapaEntrega = 2;
        results = WS_EnkontrolIntegration.bienvenidaCliente(new List<WS_EnkontrolIntegration.Fallas>{falla3});

        Test.stopTest();
    }
    
    @TestSetup
    static void crearDatosPrueba(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        Account unCuenta = new Account(Name ='TestCommunityUser') ;
        insert unCuenta;
        Contact unContacto = new Contact(LastName ='TestContact',AccountId = unCuenta.Id);
        insert unContacto;
        System.runAs(thisUser) {
            Profile perfilCoordinador = [SELECT Id FROM Profile WHERE Name = 'Coordinador Servicio a Cliente'];
            User unCoordinador = new User(Alias = 'standt', Email = 'coordinadoruser@begrand.test',
                                   EmailEncodingKey = 'UTF-8', LastName = 'Testing2', LanguageLocaleKey = 'en_US',
                                   LocaleSidKey = 'en_US', ProfileId = perfilCoordinador.Id,
                                   TimeZoneSidKey = 'America/Los_Angeles', Username = 'coordinadoruser@begrand.test',
                                   Id_Enkontrol__c ='99009',
                                   Desarrollo__c = 'ALTO PEDREGAL');
            insert unCoordinador;
            Profile supervisionObra = [SELECT Id FROM Profile WHERE Name = 'Gerente supervisión de obra'];
            User unGerenteSo = new User(Alias = 'standt', Email = 'gerentesupervision@begrand.test',
                                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = supervisionObra.Id,
                                TimeZoneSidKey = 'America/Los_Angeles', Username = 'gerentesupervision@begrand.test',
                                Id_Enkontrol__c ='99011',  Desarrollo__c = 'ALTO PEDREGAL');
            insert unGerenteSo;
            Profile communityProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Community User'];
            User unCustomerCu = new User(Alias = 'test123', Email='communityUser@begrand.test',
                                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                     LocaleSidKey='en_US', ProfileId = communityProfile.Id,
                                     IsActive =true, ContactId = unContacto.Id, TimeZoneSidKey='America/Los_Angeles',
                                     Username='communityUser@begrand.test', Id_Enkontrol__c ='99012',  Desarrollo__c = 'ALTO PEDREGAL');
            insert unCustomerCu;
            Profile standardUserProf = [SELECT Id FROM Profile WHERE Name = 'Jefe de Cuadrilla'];
            User unStandarU = new User(Alias = 'test123', Email='unStandarU@begrand.test',
                                         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                         LocaleSidKey='en_US', ProfileId = standardUserProf.Id,
                                         IsActive =true, TimeZoneSidKey='America/Los_Angeles',
                                         Username='unStandarU@begrand.test', Id_Enkontrol__c ='111',  Desarrollo__c = 'ALTO PEDREGAL');
            insert unStandarU;
        }
    }
}