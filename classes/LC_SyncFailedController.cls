public with sharing class LC_SyncFailedController {
  @AuraEnabled
  public static Lead getLead(Id identificador) {
    return [SELECT Id, Sincronizado_en_Enkontrol__c, Error_Sincronizacion__c
    FROM Lead
    WHERE Id =: identificador];
  }
  @AuraEnabled
  public static void sincronizarProspectos(Id identificador){
    List<Lead> triggerLeads = [SELECT Id, CreatedById, Agente__c, OwnerId, LastModifiedById
    FROM Lead
    WHERE Id =: identificador];
    if(!Test.isRunningTest()){
      QueueableService_LeadEnkontrol caseJob = new QueueableService_LeadEnkontrol(triggerLeads, false);
      ID jobID = System.enqueueJob(caseJob);
    }
  }
  @AuraEnabled
  public static Profile getProfile(){
    Profile pro = [SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId()];
    return pro;
  }
}