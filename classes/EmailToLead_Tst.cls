@isTest
public class EmailToLead_Tst {
  @testSetup
  static void setup() {
    Compania__c compania = Bg_Utils_Cls.genCompania('companiaName', '1');
    insert compania;
    User gerenteUser = Bg_Utils_Cls.genUser('gereUser', 'Gerente de ventas');
    insert gerenteUser;
    Desarrollo_Comercial__c desarrolloComercial = Bg_Utils_Cls.genDesComercial('test', compania.Id, gerenteUser.Id );
    insert desarrolloComercial;
    Desarrollo_Comercial__c desarrolloComercialBDC = Bg_Utils_Cls.genDesComercial('BDC', compania.Id, gerenteUser.Id );
    insert desarrolloComercialBDC;
    Desarrollo__c desarrollo = Bg_Utils_Cls.genDesarrollo('test', 'BO4', desarrolloComercial.Id);
    insert desarrollo;
    Guardia__c guardiaUno = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercial.Id, gerenteUser.Id, 1);
    insert guardiaUno;
    Guardia__c guardiaDos = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercialBDC.Id, gerenteUser.Id, 2);
    insert guardiaDos;
    Guardia__c guardiaTres = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercialBDC.Id, gerenteUser.Id, 2);
    insert guardiaTres;
    Guardia__c guardiaCuatro = Bg_Utils_Cls.genGuardia(desarrolloComercialBDC.Id, desarrolloComercialBDC.Id, gerenteUser.Id, 2);
    insert guardiaCuatro;
    Guardia__c guardiaCero = Bg_Utils_Cls.genGuardia(desarrolloComercial.Id, desarrolloComercialBDC.Id, gerenteUser.Id, 1);
    guardiaCero.Fecha_Guardia__c = Date.today().addDays(1);
    insert guardiaCero;
  }
  static testMethod void testOne() {
    List<String> messagesEmail = new List<String>();
    String msgLamudi = 'estimado agustin acabas de recibir un mensaje respecto a la siguiente propiedad anunciada en lamudi. a continuación los detalles detalles de la propiedad be grand test méxico huixquilucan listing id 1593 detalles de contacto nombre begrand corp correo electrónico lamudi@begrand.mx teléfono 5512345678 detalles del mensaje qué tal encontré tus propiedades en lamudi. ¿podrías enviarme más información por favor? gracias el equipo lamudi ¿necesitasayuda? ¡contactanos! encuentra tu propiedad buscar ahora socializa copyright © 2018 medios de clasificados.el material no puede ser publicado o reproducido de ninguna forma sin el permiso previo y por escrito. lamudi.com.mx this email was sent by lamudi internet holding sarlzossener str. 56 berlin berlin 10961 de';
    messagesEmail.add(msgLamudi);
    String msgIcasas = 'solicitud de información 1 de febrero de 2019 solicitud de información ¡hola! hemos recibido una solicitud de información sobre be grand test hacienda de las palmas huixquilucan de degollado méxico (número de referencia 1603340). aquí tienen los datos del interesado para que puedan brindarle información de su oferta nombre y apellidos begrand corp teléfono contacto 5523456789 email icasas@begrand.mx observaciones hola estoy interesado en esta propiedad con referencia 1603340 que he visto en el portal www.icasas.mx. quisiera que me contacten para recibir más información.horario preferido de contacto por la tarderespondan este correo electrónico para contestar la solicitud de antonio hauad y así satisfacer su petición. adelante. seguro que pueden alcanzar un acuerdo.gracias por confiar en icasas. icasas.mx 2019';
    messagesEmail.add(msgIcasas);
    String msgMlibre = 'mira la respuesta y haz más consultas hola be grand 2 te hicieron una pregunta en desarrollo be grand test hola begrandaltopedregal estoy interesado en desarrollo be grand test por favor comunícate conmigo. ¡gracias! estos son los datos del interesado nombre angelica bravo e-mail angiea817@gmail.com teléfono -52072734 responder ¡éxitos! el equipo de mercadolibre no respondas este e-mail. ayuda.';
    messagesEmail.add(msgMlibre);
    String msgInmuebles = 'be grand recibiste una consulta de abraham aguado en tu aviso be grand test preventa de departamentos (cód 258cp4) carr. méxico toluca 5804 test cuajimalpa de morelos cod. del anunciante 258cp4cód. de aviso 52877554 mn 4 034 162 venta | desarrollo horizontal/vertical cod. del anunciante 258cp4 cód. de aviso 52877554 el mensaje que envió fue hola estoy interesado en esta propiedad que he visto en inmuebles24 y quisiera que me contacten para recibir más información. datos del interesado abraham aguado tel. 5547321520 e-mail abrahamaws@gmail.com si respondes este email la respuesta le llegará a abraham aguado el equipo de visita las políticas de privacidad y los términos y condiciones de uso del sitio. si no quiere recibir comunicaciones similares en el futuro puede desuscribirse haciendo click aquí.';
    messagesEmail.add(msgInmuebles);
    String msgSmano = 'de jelsy garcia saul date mar. 3 sept. 2019 a las 11 45 subject fwd 📥tienes un nuevo mensaje lead no calificado tienes un nuevo mensaje en segundamano. [image logo] hola tienes un nuevo mensaje. test $4 710 000 ciudad de méxico nombre lizandro daniel marin email segundam@begrand.mx teléfono 525514713067 mensaje me interesa tu anuncio. ¿sigue disponible? recibiste este correo porque tienes una cuenta profesional en segundamano. si ya no quieres recibir estos correos puedes cancelar tu suscripción en cualquier momento. segundamano © asm clasificados de méxico sa. de cv. -- [image imagen relacionada] jelsy garcia saul kam - inmuebles | segundamano 55.33.99.51.08 jelsy.garcia@adevinta.com jelsy.garcia@segundamano.mx cultura.segundama';
    messagesEmail.add(msgSmano);
    String msgProgramatics = 'nuevo registro de contacto nombre rosalba correo tapiarosy@hotmail.com teléfono 5519542688 ¿cómo te enteraste de nosotros? descubre be grand propiedad test fecha / hora 2019/03/28 - 11 00 be grand derechos reservados 2018 ';
    messagesEmail.add(msgProgramatics);
    for (String msg : messagesEmail) {
      Messaging.InboundEmail email = new Messaging.InboundEmail();
      Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
      EmailToLead_Cls emailtolead = new EmailToLead_Cls();
      email.plainTextBody = msg;
      emailtolead.handleInboundEmail(email, env);
    }
    Test.startTest();
    System.AssertNotEquals(null, messagesEmail);
    Test.stopTest();
  }
}