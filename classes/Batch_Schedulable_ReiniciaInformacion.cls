global class Batch_Schedulable_ReiniciaInformacion implements Schedulable{
	global void execute(SchedulableContext SC) {
		// Se termina el Batch del mes pasado KillAll
		list<CronTrigger> ct = new list<CronTrigger>();
		ct = [select Id from CronTrigger where CronJobDetail.Name = 'KillAll'];
		if(!ct.isEmpty()) { System.abortJob(ct[0].Id); }
		
		// Borra los registros que hacen los resumenes en el cuerpo del correo electronico
		list<SFDC_BG_EdoCta_temp__c> calculoTemporal = [select Id from SFDC_BG_EdoCta_temp__c];
		if(!calculoTemporal.isEmpty()) { Database.delete(calculoTemporal, false); }

		// Regresar el campo "Edo_Cta_Ejecutado__c" de "Inmueble__c" a "false"
		list<Inmueble__c> departamentoMarcar = [select Id, Edo_Cta_Ejecutado__c from Inmueble__c where Edo_Cta_Ejecutado__c = true];
		if(!departamentoMarcar.isEmpty()) {
			for(Inmueble__c forData : departamentoMarcar) { forData.Edo_Cta_Ejecutado__c = false; }
			Database.SaveResult[] srList = Database.update(departamentoMarcar, false);				// "false" indica que si falla un registro los restantes continuan actualizandose
			for(Database.SaveResult sr : srList) {
				if(!sr.isSuccess() || Test.isRunningTest()) {
					String recibeError = '';
					String elBody = '';
					try { for(Database.Error err : sr.getErrors()) { recibeError = 'CAMPOS: ' + err.getFields() + ' TIPO DE ERROR: ' + err.getStatusCode() + ' DESCRIPCION: ' + err.getMessage() + ' *** '; } } catch(Exception e) { }
					elBody = '<!DOCTYPE html><html><body><hr>' + '<h3>Se identificaron fallas en la actualización de información de Departamento</h3><br/>' + 'Detalles: ' + recibeError + '</body></html>';
					Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
					String[] toAddresses = new String[] {system.label.SFDC_Correo_de_Soporte};
					email.setToAddresses(toAddresses);
					email.setSubject('Error al actualizar el campo Edo_Cta_Ejecutado__c en el Departamento');
					email.setPlainTextBody('');
					email.setHtmlBody(elBody);
					Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
				}
			}
		}
		

		// Se regresan los campos "Edo_Cta_Ejecutado__c" y "Edo_Cta_EjecutadoU__c" de "Account" de "true" a "false"
		list<Account> cuentaMarcar = [select Id, Edo_Cta_Ejecutado__c, Edo_Cta_EjecutadoU__c from Account where (Edo_Cta_Ejecutado__c = true or Edo_Cta_EjecutadoU__c = true)];
		if(!cuentaMarcar.isEmpty()) {
			for(Account forData : cuentaMarcar) { forData.Edo_Cta_Ejecutado__c = false; forData.Edo_Cta_EjecutadoU__c = false;}
			Database.SaveResult[] srList = Database.update(cuentaMarcar, false);					// "false" indica que si falla un registro los restantes continuan actualizandose
			for(Database.SaveResult sr : srList) {
				if(!sr.isSuccess() || Test.isRunningTest()) {
					String recibeError = '';
					String elBody = '';
					try { for(Database.Error err : sr.getErrors()) { recibeError = 'CAMPOS: ' + err.getFields() + ' TIPO DE ERROR: ' + err.getStatusCode() + ' DESCRIPCION: ' + err.getMessage() + ' *** '; } } catch(Exception e) {}
					elBody = '<!DOCTYPE html><html><body><hr>' + '<h3>Se identificaron fallas en la actualización de información de Cliente</h3><br/>' + 'Detalles: ' + recibeError + '</body></html>';
					Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
					String[] toAddresses = new String[] {system.label.SFDC_Correo_de_Soporte};
					email.setToAddresses(toAddresses);
					email.setSubject('Falla en los campos Edo_Cta_Ejecutado__c y Edo_Cta_EjecutadoU__c de Account');
					email.setPlainTextBody('');
					email.setHtmlBody(elBody);
					Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
				}
			}
		}
	}    
}