public class PDFDeficienciasCtrl{
	public string IdCaso{get;set;}
	public Case oCaso{get;set;}
	public list<Detalle_del_Caso__c> lDef{get;set;}
	public map<string,list<ContentDocumentLink>> mImgs{get;set;}
	public list<ContentDocumentLink> lImgsC{get;set;}

   public PDFDeficienciasCtrl(){
   	lDef = new list<Detalle_del_Caso__c>();
   	mImgs =new map<string,list<ContentDocumentLink>>();
   	lImgsC = new list<ContentDocumentLink>();
   	IdCaso = ApexPages.currentPage().getParameters().get('id');
   	oCaso = [SELECT CaseNumber,Owner.Name,Departamento__c,Status,Type,CreatedDate,(SELECT Id,ContentDocument.LatestPublishedVersionId FROM ContentDocumentLinks limit 1) FROM Case WHERE Id=:IdCaso];
   	lImgsC.addAll(oCaso.ContentDocumentLinks);
   	for(Detalle_del_Caso__c oDef:[SELECT Id,Comentarios__c,Name,Departamento__c,Fecha_Asignacion__c
   								  ,Descripcion__c,Especialidad__c,Subespecialidad__c,Zona__c,Estatus__c
   								  ,Tiempo_Min__c,Tiempo_Max__c,Owner.Name,Casos__r.CaseNumber
   								  ,Casos__r.Entrega__r.Inmueble__r.Nivel__c
   								  ,(SELECT Id,ContentDocument.LatestPublishedVersionId FROM ContentDocumentLinks limit 3) 
   								  FROM Detalle_del_Caso__c
   								  WHERE Casos__c =:IdCaso]){
		lDef.add(oDef);
		list<ContentDocumentLink> lImgstemp = new list<ContentDocumentLink>();
		lImgstemp.addall(oDef.ContentDocumentLinks);
		mImgs.put(oDef.Id,oDef.ContentDocumentLinks);
	}
   }

}