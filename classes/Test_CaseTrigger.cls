/**
 * Created by scarlettcastillo on 9/3/18.
 */

@IsTest
public with sharing class Test_CaseTrigger {

    public static testMethod void testCaseTrigger(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);

        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);

        Test.startTest();

        unCaso.Folio_Enkontrol__c = 1001;
        unCaso.Status = 'Resuelto';
        unCaso.FallasPendientes__c = 0; //-> LINEA AGREGADA POR JCS CLOUDMOBILE 211118

        update unCaso;

        Test.stopTest();
    }

    /*
    * Este metodo pureba la cracion de un prospecto apartir de un caso¨
    * Author: Jloyola
    */

    public static testMethod void testcrearProspectoWeb(){

    	List<Case> lstCase = new List<Case>();

    	Case caso1 = new Case ();
    	caso1.Status = 'Nuevo';
    	caso1.ApellidoLiveAgent__c = 'Loyola';
    	caso1.NombreLiveAgent__c = 'Pedro';
    	caso1.TelefonoLiveAgent__c = '5543456789';
    	caso1.EmailLiveAgent__c = 'test@test.com';
    	caso1.DesarrolloLiveAgent__c = 'Alto Polanco';
    	caso1.Subject = 'Cotizacion de inmueble';
    	caso1.Origin = 'Web';
    	lstCase.add(caso1);

    	Case caso2 = new Case ();
    	caso2.Status = 'Nuevo';
    	caso2.ApellidoLiveAgent__c = 'Loyola';
    	caso2.NombreLiveAgent__c = 'Javier';
    	caso2.TelefonoLiveAgent__c = '5546456789';
    	caso2.EmailLiveAgent__c = 'test7@test.com';
    	caso2.DesarrolloLiveAgent__c = 'Alto Pedregal';
    	caso2.Subject = 'Cotizacion de inmueble';
    	caso2.Origin = 'Web';
    	lstCase.add(caso2);


    	Case caso3 = new Case ();
    	caso3.Status = 'Nuevo';
    	caso3.ApellidoLiveAgent__c = 'LLinas';
    	caso3.NombreLiveAgent__c = 'Arturo';
    	caso3.TelefonoLiveAgent__c = '5543476789';
    	caso3.EmailLiveAgent__c = 'test5@test.com';
    	caso3.DesarrolloLiveAgent__c = 'Contadero';
    	caso3.Subject = 'Cotizacion de inmueble';
    	caso3.Origin = 'Web';
    	lstCase.add(caso3);


    	Case caso4 = new Case ();
    	caso4.Status = 'Nuevo';
    	caso4.ApellidoLiveAgent__c = 'Loyola';
    	caso4.NombreLiveAgent__c = 'Santiago';
    	caso4.TelefonoLiveAgent__c = '5558456789';
    	caso4.EmailLiveAgent__c = 'test3@test.com';
    	caso4.DesarrolloLiveAgent__c = 'Park Bosques';
    	caso4.Subject = 'Cotizacion de inmueble';
    	caso4.Origin = 'Web';
    	lstCase.add(caso4);


    	Case caso5 = new Case ();
    	caso5.Status = 'Nuevo';
    	caso5.ApellidoLiveAgent__c = 'Loyola';
    	caso5.NombreLiveAgent__c = 'salma';
    	caso5.TelefonoLiveAgent__c = '5513456789';
    	caso5.EmailLiveAgent__c = 'test1@test.com';
    	caso5.DesarrolloLiveAgent__c = 'Reforma';
    	caso5.Subject = 'Cotizacion de inmueble';
    	caso5.Origin = 'Web';
    	lstCase.add(caso5);


    	Case caso6 = new Case ();
    	caso6.Status = 'Nuevo';
    	caso6.ApellidoLiveAgent__c = 'Loyola';
    	caso6.NombreLiveAgent__c = 'Abram';
    	caso6.TelefonoLiveAgent__c = '5533456789';
    	caso6.EmailLiveAgent__c = 'test2@test.com';
    	caso6.DesarrolloLiveAgent__c = 'Coapa';
    	caso6.Subject = 'Cotizacion de inmueble';
    	caso6.Origin = 'Web';
    	lstCase.add(caso6);

    	//Handler_CaseTrigger.crearProspectoWeb(lstCase) ;
    }

    public static testMethod void testCaseTriggerQueuable(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);

        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);

        Test.startTest();

        QueueableService_CaseEnkontrol queueableCase = new QueueableService_CaseEnkontrol(new List<Case>{unCaso});
        queueableCase.hacerCallout = false;
        ID jobID = System.enqueueJob(queueableCase);

        Test.stopTest();
    }

    public static testMethod void testCaseTriggerCallout(){

        /** Creando datos prueba **/
        Account cliente = Test_Utilities.crearClientePrueba();

        Desarrollo_Comercial__c desarrolloComercial = Test_Utilities.crearDesarrolloComercial(UserInfo.getUserId(), UserInfo.getUserId());

        Inmueble__c inmueble = Test_Utilities.crearInmueblePrueba('109', desarrolloComercial.Id,'1', 'AP2');
        Carta_Oferta__c entrega = Test_Utilities.crearEntregaPrueba(inmueble.Id, cliente.Id);
        Case unCaso = Test_Utilities.crearCaso(entrega.Id, cliente.Id);
        Detalle_del_Caso__c detalle = Test_Utilities.crearDeficiencia(unCaso.Id);

        /** Creando configuracion personalizada **/
        Test_Utilities.testCrearConfiguracionPersonalizadaEnkontrol();

        /** Creando mocks **/
        Map<String, String> mockResponses = new Map<String, String>();
        mockResponses.put('https://e-pago.begrand.mx/EnKontrol/EnkontrolService.svc', Test_Utilities.RESPUESTA_ENKONTROL);
        Test_WS_DropboxMock multipleMock = new Test_WS_DropboxMock(mockResponses);

        Test.setMock(HttpCalloutMock.class, multipleMock);

        unCaso.Folio_Enkontrol__c = 1001;
        unCaso.Status = 'Resuelto';
		unCaso.FallasPendientes__c = 0; //-> LINEA AGREGADA POR JCS CLOUDMOBILE 211118

        update unCaso;

        Test.startTest();

        QueueableService_CaseEnkontrol queueableCase = new QueueableService_CaseEnkontrol(new List<Case>{unCaso});
        //queueableCase.hacerCalloutEnkontrol();

        Test.stopTest();
    }
}