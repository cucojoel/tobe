global class BATCH_ProspectoNoContactado implements Database.Batchable<SObject>{

	public String soqlquery = '';
	
	global Database.QueryLocator start(Database.BatchableContext BC) { 
		
		//Se obtienen los prospectos de BDC que tengan 10 o mas dias de creados y no hayan sido contactados
		//o que no tengan ya el estatus de No contactar
		soqlquery = 'SELECT Id FROM Lead WHERE Asignado_BDC__c = true AND Dias_Desde_Creacion__c >= 10 AND (Status = \'Asignado\' OR Status = \'En Proceso\')';
		System.debug('*****soqlquery:' + soqlquery);
		
		return database.getquerylocator(soqlquery);
	}
 
 	//Se les cambia el estatus a los prospectos por No contactar
	global void execute(Database.BatchableContext BC, List<Lead> leadsNoContactar) { 
		
		System.enqueueJob(new QUEUEABLE_ProspectoNoContactado(leadsNoContactar));
	}
	
	global void finish(Database.BatchableContext BC) { 
		System.debug('BATCH_ProspectoNoContactado - batch completado.');
	}
}