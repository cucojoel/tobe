@isTest(SeeAllData=true)
private class TestOrgHierarchy {
    
    /*@isTest static void runTest() { //-> METODO COMENTADO POR JCS CLOUDMOBILE
        OrgHierarchyCtrl org = new OrgHierarchyCtrl();
        System.Assert(org.currentFocusUserId != null,'no current user');
        System.Assert(org.me != null,'no user node for current user');
        UserNode u = new UserNode(new User(), new User());
    }*/
    
    @isTest static void runTest() { //-> METODO CREADO POR JCS CLOUDMOBILE
    	
    	UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
		
		User manager = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id,
		     LastName = 'manager',
		     Email = 'manager@manager.com',
		     Username = 'manager@manager.com' + System.currentTimeMillis(),
		     CompanyName = 'manager',
		     Title = 'title',
		     Alias = 'aliasm',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     UserRoleId = r.Id,
		     isActive = true
		);
		insert manager;
    	
    	User usuario1 = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id,
		     LastName = 'test',
		     Email = 'test@test.com',
		     Username = 'test@test.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'aliasu1',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     UserRoleId = r.Id,
		     isActive = true,
		     ManagerId = manager.Id
		);
		insert usuario1;
		
		User usuario2 = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id,
		     LastName = 'test',
		     Email = 'test@test.com',
		     Username = 'test@test.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'aliasu2',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     UserRoleId = r.Id,
		     isActive = true,
		     ManagerId = manager.Id
		);
		insert usuario2;
		
		User esclavo = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id,
		     LastName = 'test',
		     Email = 'test@test.com',
		     Username = 'test@test.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'aliase',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     UserRoleId = r.Id,
		     isActive = true,
		     ManagerId = usuario1.Id
		);
		insert esclavo;
		
		ApexPages.currentPage().getParameters().put('sfdc.userId',usuario1.Id); 		
		OrgHierarchyCtrl org = new OrgHierarchyCtrl();
		
		UserNode u = new UserNode(new User(), new User());
    }
}