@isTest
/**
* ----------------------------------------------------------------------------------------------------------------------
* @Name        Bg_Utils_Test
* @Autor       Joel Soto
* @Date        21/11/2019
*
* @Group       BeGrand
* @Description Clase test para la clase Bg_Utils_Cls
* @Changes
*/
public class Bg_Utils_Test {
  /**
  * --------------------------------------------------------------------------------------------------------------------
  * @Description Cobertura del 100% de la clase con la llamada de sus funciones.
  *
  * @Autor       Joel Soto
  * @Date        21/11/2019
  **/
  static testMethod void testOne() {
    Compania__c getCompania = Bg_Utils_Cls.genCompania('Be Grand', '001');
    insert getCompania;
    User asesorUno = Bg_Utils_Cls.genUser('asesorUno', 'Asesor de Ventas');
    insert asesorUno;
    Desarrollo_Comercial__c desComUno = Bg_Utils_Cls.genDesComercial('Coapa', getCompania.Id, asesorUno.Id );
    insert desComUno;
    Guardia__c guardiaUno = Bg_Utils_Cls.genGuardia(desComUno.Id, desComUno.Id, asesorUno.Id, 1);
    Desarrollo__c desarrollo = Bg_Utils_Cls.genDesarrollo('Coapa', '001', desComUno.Id);
    insert desarrollo;
    Lead leadUno = Bg_Utils_Cls.genLead('name', desComUno.Id, asesorUno.Id);
    RecordType rType = Bg_Utils_Cls.getRtype('PersonAccount', 'Account');
    Account acc = Bg_Utils_Cls.genAccount('persona fisica', '001', rType.Id);
    insert acc;
    Opportunity opp = Bg_Utils_Cls.genOpportunity('name','Nueva',desarrollo.Id, acc.Id);
    Attachment attach = Bg_Utils_Cls.genAttachment('name',acc.Id);
    insert attach;
    String objName = Bg_Utils_Cls.getObjectName(acc.Id);
    Bg_Utils_Cls.formatDdMmAaaa('04/12/2019');
    Bg_Utils_Cls.validateString('myString');
    Etapa__c etapaSf = Bg_Utils_Cls.genEtapa('Torre 2', '001', desarrollo.Id);
    insert etapaSf;
    Inmueble__c depto= Bg_Utils_Cls.genDepto(etapaSf.Id, 'Disponible', '1471');
    EquivalenciaEstados__c eqEstados = Bg_Utils_Cls.genEstados('Aguascalientes', 'Agss');
    Bg_Utils_Cls.stringToDecimal('20');
    Bg_Utils_Cls.genEntrega(depto.Id);
    Bg_Utils_Cls.getStrCampos('Case');
    Map<String, String> mockResponses = new Map<String, String>();
    mockResponses.put('https://e-test.begrand.mx/EnKontrol/EnkontrolService.svc', Bg_Utils_Cls.RESPUESTA_ENKONTROL_PROSPECTOS_COMPLETO_SUCCESS);
    System.AssertNotEquals(null, getCompania);
  }
}