global class Batch_Schedulable_CalculaHacerPDF implements Schedulable {
	global void execute(SchedulableContext SC) {
		// Calcula el tiempo que se va tardar el Batch en generar los PDF's
		// Antes de usar este codigo, hacer el registro "HoraInicioBatchGeneraPDF" y poner la hora de inicio en el campo "Inicio_hora__c"
		// Esta Batch se correra antes de la hora de la creacion de los PDF
		PreparaEstadosCuenta__c tiempoGenerarPDF = [select Id, Name, Inicio_hora__c, Fin_hora__c, BatchSegundos__c, BatchMinutos__c from PreparaEstadosCuenta__c where Name = 'HoraInicioBatchGeneraPDF'];
		decimal minutos = ([select count() from Inmueble__c where OportunidadTexto__c <> null] * 4) / 20;			// Se obtiene la cantidad de minutos aproximados que se va tardar
		minutos = Math.ceil(minutos / 60);																			// Los minutos obtenidos se van a pasar a horas, redondeando hacia arriba
		tiempoGenerarPDF.Fin_hora__c = tiempoGenerarPDF.Inicio_hora__c + minutos;									// Se actualizan los valores
		Database.update(tiempoGenerarPDF, false);
		
		// Se va programar el Batch que ejecutara la generacion de PDF
		String iniciar = String.valueOf(tiempoGenerarPDF.Inicio_hora__c);
		Batch_Schedulable_GeneraPDF bsPDF = new Batch_Schedulable_GeneraPDF();
		String sch = tiempoGenerarPDF.BatchSegundos__c + ' ' + tiempoGenerarPDF.BatchMinutos__c + ' ' + iniciar + ' * * ?';
		String jobID = System.schedule('GeneraPDFSchedulable', sch, bsPDF);
		
		// Se programara Batch para matar este proceso
		DateTime nuevaEjecucion = system.now().addMinutes(Integer.valueOf(system.label.Batchable_Schedulable_Minutos));
		Batch_Schedulable_CalculaHacerPDF_Kill bsKill = new Batch_Schedulable_CalculaHacerPDF_Kill();
		String job = System.schedule('CalculaHacerPDF_Kill', '0 ' + nuevaEjecucion.minute() + ' ' + nuevaEjecucion.hour() + ' * * ?', bsKill);
	}
}