@isTest
private class TEST_ProspectoNoContactado {

	private static Desarrollo_Comercial__c desarrolloComercial;
	private static Desarrollo_Comercial__c desarrolloComercialBDC;
	private static Desarrollo__c desarrollo;
	private static User usuario;
	private static Guardia__c guardia1DesarrolloComercial;
	private static Guardia__c guardia2DesarrolloComercial;
	private static Guardia__c guardia1DesarrolloComercialBDC;
	private static Guardia__c guardia2DesarrolloComercialBDC;
	private static Guardia__c guardia3DesarrolloComercialBDC;
	private static Guardia__c guardia4DesarrolloComercialBDC;
	private static Lead prospecto;

	private static void init(){

		desarrolloComercial = new Desarrollo_Comercial__c();
		desarrolloComercial.Name = 'test';
		desarrolloComercial.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercial.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrolloComercialBDC = new Desarrollo_Comercial__c();
		desarrolloComercialBDC.Name = 'BDC';
		desarrolloComercialBDC.Hora_Inicio__c = Time.newInstance(9, 0, 0, 0);
		desarrolloComercialBDC.Hora_Fin__c = Time.newInstance(19, 0, 0, 0);

		desarrollo = new Desarrollo__c();
		desarrollo.Name = 'test';
		desarrollo.Inicio__c = Time.newInstance(7,0,0,0);
		desarrollo.Fin__c = Time.newInstance(22,0,0,0);

		usuario = new User();
		usuario.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Administrador del sistema'].Id;
		usuario.Alias = 'usreje';
		usuario.Country = 'México';
		usuario.Email = 'ejecutivo@email.com';
		usuario.EmailEncodingKey = 'UTF-8';
		usuario.LastName = 'ejecutivo';
		usuario.LanguageLocaleKey = 'es_MX';
		usuario.LocaleSidKey = 'es_MX';
		usuario.TimeZoneSidKey = 'America/Mexico_City';
		usuario.UserName='ejecutivo@email.com';
		usuario.Country = 'Mexico';

		guardia1desarrolloComercial = new Guardia__c();
		guardia1desarrolloComercial.Fecha_Guardia__c = Date.today().addDays(1);
		guardia1desarrolloComercial.Roll__c = 1;

		guardia2desarrolloComercial = new Guardia__c();
		guardia2desarrolloComercial.Fecha_Guardia__c = Date.today().addDays(1);
		guardia2desarrolloComercial.Roll__c = 2;

		guardia1DesarrolloComercialBDC = new Guardia__c();
		guardia1DesarrolloComercialBDC.Fecha_Guardia__c = Date.today();
		guardia1DesarrolloComercialBDC.Roll__c = 1;

		guardia2DesarrolloComercialBDC = new Guardia__c();
		guardia2DesarrolloComercialBDC.Fecha_Guardia__c = Date.today();
		guardia2DesarrolloComercialBDC.Roll__c = 2;

		guardia3DesarrolloComercialBDC = new Guardia__c();
		guardia3DesarrolloComercialBDC.Fecha_Guardia__c = Date.today().addDays(1);
		guardia3DesarrolloComercialBDC.Roll__c = 1;

		guardia4DesarrolloComercialBDC = new Guardia__c();
		guardia4DesarrolloComercialBDC.Fecha_Guardia__c = Date.today().addDays(1);
		guardia4DesarrolloComercialBDC.Roll__c = 2;

		prospecto = new Lead();
		prospecto.FirstName = 'prospecto';
		prospecto.LastName = 'prospecto';
		prospecto.Email = 'prospecto@test.com';
		prospecto.Desarrollo_Web__c = 'test';
		prospecto.Cantidad_No_Shows__c = 0;
		prospecto.LeadSource = '6';
		prospecto.RFC__c = 'CASJ811112FL7';
		prospecto.Phone = '2222334455';
	}

	static testMethod void testGetters(){

		init();

		insert desarrolloComercial;
		insert desarrolloComercialBDC;

		desarrollo.Desarrollo_Comercial__c = desarrolloComercial.Id;
		insert desarrollo;

		insert usuario;

		guardia1desarrolloComercial.Desarrollo_Comercial__c = desarrolloComercial.Id;
		guardia1desarrolloComercial.Nombre_Asesor__c = usuario.Id;
		insert guardia1desarrolloComercial;

		guardia2desarrolloComercial.Desarrollo_Comercial__c = desarrolloComercial.Id;
		guardia2desarrolloComercial.Nombre_Asesor__c = usuario.Id;
		insert guardia2desarrolloComercial;

		guardia1DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia1DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia1DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia1DesarrolloComercialBDC;

		guardia2DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia2DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia2DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia2DesarrolloComercialBDC;

		guardia3DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia3DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia3DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia3DesarrolloComercialBDC;

		guardia4DesarrolloComercialBDC.Desarrollo_Comercial__c = desarrolloComercialBDC.Id;
		guardia4DesarrolloComercialBDC.Desarrollo_Comercial_Primario__c = desarrolloComercial.Id;
		guardia4DesarrolloComercialBDC.Nombre_Asesor__c = usuario.Id;
		insert guardia4DesarrolloComercialBDC;

		prospecto.Desarrollo__c = desarrollo.Id;
		prospecto.Desarrollo_Comercial__c = desarrolloComercial.Id;
		prospecto.F_CM__c = Date.today().addDays(1);
		prospecto.Agente__c = usuario.Id;
    prospecto.OwnerId = usuario.Id;
		//insert prospecto;

		Datetime DiasAtras = Datetime.now().addDays(-10);
		//Test.setCreatedDate(prospecto.Id, DiasAtras);

		Test.startTest();
		String CRON_EXP = '0 0 0 15 3 ? 2022';
		String jobId = System.schedule('ScheduledApexTest',CRON_EXP, new SCHEDULE_ProspectoNoContactado());
		Test.stopTest();
	}
}